# Contrôle de paramètres en présence d'incertitudes
## Abstract
Many physical phenomena are modelled numerically in order to better understand and/or to predict their behaviour. However, some complex and small scale phenomena can not be fully represented in the models. The introduction of ad-hoc correcting terms is usually the solution to represent these unresolved processes, but those need to be properly estimated.

A good example of this type of problem is the estimation of bottom friction parameters of the ocean floor. This task is further complicated by the presence of uncertainties in certain other characteristics linking the bottom and the surface (eg boundary conditions).

Classical methods of parameter estimation usually imply the minimisation of an objective function, that measures the error between some observations and the results obtained by a numerical model. The optimum is directly dependent on the fixed nominal value given to the uncertain parameter ; and therefore may not be relevant in other conditions.
    Strategies taking into account those uncertainties are to be defined and applied on an academic model of a coastal area, in order to find an optimal value in a robust sense.

**Keywords:**  Robust optimisation ; Bayesian inference ; Design of computer experiments ; multiobjective optimisation

## Documents
### Journal
[Accéder au journal sur GitLab](https://gitlab.inria.fr/vtrapple/These/wikis/Journal)

### Documents
* Rapports
    * [V1](https://gitlab.inria.fr/vtrapple/These/blob/master/Documents/article_draft1.pdf)/[V2](https://gitlab.inria.fr/vtrapple/These/blob/master/Documents/article_draft2.pdf)
    * [Formulation Bayésienne/variationnelle (last update 23/08/18)](https://gitlab.inria.fr/vtrapple/These/blob/master/Documents/Formulation_of_the_problem.pdf)
    * [MSc Thesis (Rapport de stage, 31/07/2017)](https://gitlab.inria.fr/vtrapple/These/blob/master/Documents/MSc_Thesis_Trappler.pdf)
* Présentations
    * [Colloque National d'Assimilation des Données, Rennes (27/09/18)](https://gitlab.inria.fr/vtrapple/These/blob/master/Documents/slides_TRAPPLER_CNA.pdf)
    * [Séminaire des Doctorants, LJK (23/03/18)](https://gitlab.inria.fr/vtrapple/These/blob/master/Documents/slides_seminaire_23_03.pdf)
    * [MSc defense (31/07/2017)](https://gitlab.inria.fr/vtrapple/These/blob/master/Documents/slides_DTU_defence.pdf)
* Posters
    * [Journée des Doctorants, MSTII 2018](https://gitlab.inria.fr/vtrapple/These/blob/master/Documents/Poster_JDD_TRAPPLER.pdf)
    * [Adjoint Workshop, Aveiro, Août 2018](https://gitlab.inria.fr/vtrapple/These/blob/master/Documents/poster_aveiro.pdf)
* Personnels
    * [CV (dernière MàJ le 08/05/2019)](https://gitlab.inria.fr/vtrapple/These/blob/master/Documents/cv_vt_08_05_19.pdf)
 
### Code informatique
Le code présent sur le projet "These" est le code sur lequel je travaille en ce moment, et regroupe par conséquent beaucoup de fichiers plus ou moins pertinents.

* Code Shallow water 1D: vtrapple/SWE_VT>
* Package d'optimisation robuste: vtrapple/RO_VT>

### Chosen illustrations
![relax](/img/relaxation_tuto.png)

### Bibliographie
[Page zotero](https://www.zotero.org/vtrappler/items)


## Label RES (Recherche et Enseignement)
[Description du label RES sur le site de l'UGA](https://doctorat.univ-grenoble-alpes.fr/fr/pendant-la-these/la-formation-durant-la-these/parcours-labels/label-res-recherche-et-enseignement-superieur-577252.htm)
### Porftolio
à ajouter
### Enseignements 

120 heures d’enseignement prévues entre Septembre 2017 et Janvier 2019
<!---
* Sept. – Déc. 2017: TP Méthodes statistiques appliquées à la biologie. Licence 2 Biologie
* Jan. – Mai 2018: TD Calcul et analyse. Licence 1 MIASHS
* Sept. – Déc. 2018: TP Méthodes statistiques appliquées à la biologie. Licence 2 Biologie
* Sept. – Déc. 2018: Cours TD Géométrie et Algèbre. Licence 1 Physique Chimie, Mécanique, Mathématiques (PCMM)
--->

|     Période     |   Type   | Formation | Nombre d'heures |
| --------------- | -------- | --------- | --------------- |
| Sep.-Déc. 2017  |    TP    | Méthodes statistiques appliquées à la biologie. Licence 2 Biologie | 36h |
| Jan.-Mai  2017  |    TD    | Calcul et analyse. Licence 1 MIASHS | 20h |
| Sep.-Déc. 2018  |    TP    | Méthodes statistiques appliquées à la biologie. Licence 2 Biologie | 36h |
| Sep.-Déc. 2018  | Cours-TD | Géométrie et Algèbre. Licence 1 Physique Chimie, Mécanique, Mathématiques (PCMM) | 28h |

<!---
## Estimated density of $`K_{\mathrm{arg max}}`$

![](img/plot_ro2200.png)

--->
