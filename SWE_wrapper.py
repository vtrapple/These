#-*- coding: utf-8 -*-

import numpy as np
import pylab
import scipy.optimize
from code_swe.LaxFriedrichs import LF_flux
from code_swe.direct_MLT_ADJ_LF import *
#from code_swe.animation_SWE import animate_SWE
from code_swe.boundary_conditions import bcL, bcR, bcL_d, bcR_d, bcL_A, bcR_A, BCrand
from code_swe.interpolation_tools import interp_cst
import random as rnd

#%%%%%%%%%%%%%%%%%%%%%%%%%%%#
# Définition des paramètres #
#############################

D = [0, 100]  # Domaine spatial
T = 200 # Fin de la simulation
dt = 0.1  # Pas de temps
g = 9.81   # Constante de gravitation
N = 50  # Nombre de volumes
dx = np.diff(D)[0] / float(N)
xr = np.linspace(D[0] + dx/2, D[1] - dx/2, N)
eta = 7./3.

u0 = lambda x: 0
b = lambda x: 10*x/D[1]
h0 = lambda x: np.where(x > np.mean(D), 20, 20)

def J_function(h1, h2):         # Fonction coût
    return 0.5*np.sum((h1-h2)**2)
    
#def J_function_observation(h,href,ind_to_observe=None):
#    """Compute J, with the index of grid points observed"""
#    space_dim = int(h.shape[0])
#    if ind_to_observe is None:
#        ind_to_observe = range(space_dim)
#        
#    obs_mat = np.zeros((href.shape[0],href.shape[0]))
#    obs_mat[ind_to_observe,ind_to_observe] = 1
#    J = 0.5*np.sum(obs_mat.dot(h-href)**2)
#    return J,obs_mat
    
def observation_matrix_init(h_test, ind_to_observe=None):
    if ind_to_observe is None:
        ind_to_observe = range(int(h_test.shape[0]))
    obs_mat = np.zeros((h_test.shape[0], h_test.shape[0]))
    obs_mat[ind_to_observe, ind_to_observe] = 1
    return obs_mat
    
def J_function_observation_init(h_test, ind_to_observe=None):
    obs_mat = observation_matrix_init(h_test, ind_to_observe)
    J = lambda h1, h2: 0.5*np.sum(obs_mat.dot(h1-h2)**2)
    return J, obs_mat

def J_function_regul(h1, h2, alpha, K1, Kb):
    return 0.5*np.sum((h1-h2)**2) + 0.5*alpha*(K1-Kb)**2

def Froude(h, u):                # Nombre de Froude
    return np.fabs(u) / np.sqrt(g*h)

#%% Boundary conditions

mean_h = 20 + rnd.uniform(-0.05, 0.05)*0
amplitude = rnd.uniform(4.9, 5.1)*0 + 5
period = rnd.uniform(49.9, 50.1)*0 + 50
phase = rnd.uniform(-0.001, 0.001)*0 
print 'Hauteur eau moyenne = ', mean_h
print 'Amplitude = ', amplitude
print 'Periode = ', period
print 'Phase = ', phase
bcLref = lambda h, hu, t: BCrand(h, hu, t, 'L',
                                 mean_h, amplitude, period, phase)


#%%%%%%%%%%%%%#
# Simulations #
###############

reference = lambda K: shallow_water(D, g, T, h0, u0, N, LF_flux, dt, b, K,
                                    bcLref, bcR)
direct = lambda K: shallow_water(D, g, T, h0, u0, N, LF_flux, dt, b, K,
                                 bcL, bcR)
MLT = lambda h, u, K, dK, href: lineaire_tangent_shallow_water(D, g, T, N, dt,
                                                               b, K, dK, h, u,
                                                               href, bcL_d, bcR_d)
ADJ = lambda h, u, K, href: adjoint_shallow_water(D, g, T, N, dt, b, K, h, u,
                                                  h-href, bcL_A, bcR_A)
Kref = 0.2*(1+np.sin(2*np.pi*xr/D[1]))
[xr, href, uref, t] = reference(Kref)
# [xr,href2, uref2,t] = reference(0)
# animate_SWE(xr,[href],b,D, ylim = [0,30])


#idx_to_observe = range(25)
#cost_fun,obs_mat = J_function_observation_init(href, idx_to_observe)

def J_grad(K):
    """ Effectue une simulation directe, puis utilise la méthode adjointe pour calculer le gradient. \n 
    Renvoie la valeur de la fonction coût, ainsi que le gradient en K    
    """
    print type(K)
    return shallow_water_RSS_grad(D, g, T, h0, u0, N, LF_flux, dt, b, K,
                                  bcL, bcR, bcL_A, bcR_A, href, J_function)
    
def J_naive(K):
    """ Calcul RSS (sans gradient) entre href et simulation avec paramètre K """
    return shallow_water_RSS(D, g, T, h0, u0, N, LF_flux, dt, b, K,
                             bcL, bcR, href, J_function)


def J_obs_grad_base(K, cost_fun, obs_mat):
    """Calcul RSS avec gradient, prenant en arg les observations faites """
    return shallow_water_RSS_grad_observation(D, g, T, h0, u0, N, LF_flux, dt,
                                              b, K, bcL, bcR, bcL_A, bcR_A,
                                              href, cost_fun, obs_mat)

def J_grad_observed(K,idx_to_observe):
    """Calcul RSS selon index observés"""
    cost_fun,obs_mat = J_function_observation_init(href, idx_to_observe)
    return shallow_water_RSS_grad_observation(D, g, T, h0, u0, N, LF_flux, dt,
                                              b, K, bcL, bcR, bcL_A, bcR_A,
                                              href, cost_fun, obs_mat)

    
def J_grad_scalar(listofK):
    """K scalaire pour effectuer calculs en batch"""
    Jvec = 0.0*np.ones_like(listofK)
    gradvec = 0.0*np.ones_like(listofK)
    for i,K in enumerate(listofK):
        Jvec[i],gradvec[i] = J_grad(K)
    return [Jvec]+[gradvec]
    
    

#optimization_obs = scipy.optimize.minimize(J_obs_grad, 0.01*np.ones(50), jac = True, bounds = [(0.0,0.5)]*50)
# optimization = scipy.optimize.minimize(J_grad, 0.01*np.ones(50), jac = True, bounds = [(0.0,0.5)]*50)


#%% Interpolation for K, no diff for obs matrix
def interp(coef_array):
    """Interpolation using piecewiste constant values"""
    coef_array=np.array(coef_array)
    D_length = float(np.diff(D)[0])
    cell_vol = D_length/coef_array.size
    pts = np.linspace(cell_vol/2.0, D_length- cell_vol/2.0,
                      num = coef_array.size)
    f_to_ret = lambda x: interp_cst(x, cell_vol, coef_array, pts)
    return f_to_ret
    
def J_piecewise_naive(Kcoeff):
    """Get RSS without gradient, when K evenly distributed on the domain"""
    K_transform = interp(Kcoeff)
    K = np.array(map(K_transform, xr))
    return J_naive(K)
    
def J_piecewise_grad(Kcoeff):
    """Piecewise constant interpolation of K, then computes cost function and gradient"""
    K_transform = interp(Kcoeff)
    K = np.array(map(K_transform, xr))
    cost, grad =  J_grad(K)
    grad_sum_length = 50.0/Kcoeff.size
    grad_coeff = np.zeros(Kcoeff.size)
    for i in range(Kcoeff.size):
        grad_coeff[i] = sum(grad[i*grad_sum_length:(i*grad_sum_length+grad_sum_length)])
    return cost,grad_coeff
        
##%% Examples of run with K interpolated
#Ninterp = 2
#optimization_2parts_grad = scipy.optimize.minimize(J_piecewise_grad, 0.01*np.ones(Ninterp), jac = True, bounds = [(0.0,0.5)]*Ninterp)
#Koptim2 = interp(optimization_2parts_grad.x)
#
#Ninterp = 5
#optimization_5parts_grad = scipy.optimize.minimize(J_piecewise_grad, 0.01*np.ones(Ninterp), jac = True, bounds = [(0.0,0.5)]*Ninterp)
#Koptim5 = interp(optimization_5parts_grad.x)
#
#Ninterp = 10
#optimization_10parts_grad = scipy.optimize.minimize(J_piecewise_grad, 0.01*np.ones(Ninterp), jac = True, bounds = [(0.0,0.5)]*Ninterp)
#Koptim10 = interp(optimization_10parts_grad.x)
#
#Ninterp = 25
#optimization_25parts_grad = scipy.optimize.minimize(J_piecewise_grad, 0.01*np.ones(Ninterp), jac = True, bounds = [(0.0,0.5)]*Ninterp)
#Koptim25 = interp(optimization_25parts_grad.x)


#%% WRAPPING FUNCTION

def J_complete(Kcoeff, idx_to_observe):
    """Interpolates K, and choice of observation points"""
    # Interpolation from K vector of coefficients to a function
    K_transform = interp(Kcoeff)
    K = np.array(map(K_transform, xr))
    
    cost, grad =  J_grad_observed(K, idx_to_observe)
    # Reconstruction du gradient
    grad_sum_length = 50.0/Kcoeff.size
    grad_coeff = np.zeros(Kcoeff.size)
    for i in range(Kcoeff.size):
        grad_coeff[i] = sum(grad[i*grad_sum_length:(i*grad_sum_length+grad_sum_length)])
    return cost,grad_coeff
#%% 
if __name__ == '__main__':
    import cPickle as pickle
    tuple_opt = pickle.load(open("tuple_opt.p", "wb"))
#    tuple_opt = []
#
#    for nu,idx_to_observe in enumerate([25, range(25), range(25,50), None]):
#        Ninterp = 5
#        opt_5 = scipy.optimize.minimize(J_complete, 0.01*np.ones(Ninterp), idx_to_observe, jac = True, bounds = [(0.0,0.5)]*Ninterp)
#        Ninterp = 10
#        opt_10 = scipy.optimize.minimize(J_complete, 0.01*np.ones(Ninterp), idx_to_observe, jac = True, bounds = [(0.0,0.5)]*Ninterp)
#        Ninterp = 2
#        opt_2 = scipy.optimize.minimize(J_complete, 0.01*np.ones(Ninterp), idx_to_observe, jac = True, bounds = [(0.0,0.5)]*Ninterp)
#        Ninterp = 1
#        opt_1 = scipy.optimize.minimize(J_complete, 0.01*np.ones(Ninterp), idx_to_observe, jac = True, bounds = [(0.0,0.5)]*Ninterp)
#        temp_dict = {1: opt_1, 2: opt_2, 5:opt_5, 10:opt_10}
#        tuple_opt.append(temp_dict)
#        pylab.plot(xr,map(interp(opt_1.x),xr), label = "1 value")
#        pylab.plot(xr,map(interp(opt_2.x),xr), label = "2 values")
#        pylab.plot(xr,map(interp(opt_5.x),xr), label = "5 values")
#        pylab.plot(xr,map(interp(opt_10.x),xr), label = "10 values")
#        pylab.plot(xr,Kref, label ="True value")
#        pylab.grid()
#        pylab.legend(loc = 'best')
#        pylab.title("Optimization procedure %d" %nu)
#        pylab.show()
        
    idx_name = ['25','left','right','all']
    Nint = [1,2,5,10]
    nit_mat = np.zeros([4,4])
    nfev_mat = np.zeros([4,4])
    fun_mat = np.zeros([4,4])
    
    for nid,nin in enumerate(Nint): #boucle sur nbre de points interpolants
        for num,id_it in enumerate(idx_name): #boucle sur nbre d'obs
            nit_mat[num,nid] = tuple_opt[num][nin].nit
            nfev_mat[num,nid] = tuple_opt[num][nin].nfev
            fun_mat[num,nid] = tuple_opt[num][nin].fun
    res_1 = np.zeros([4,50])
    res_2 = np.zeros([4,50])
    res_5 = np.zeros([4,50])
    res_10 = np.zeros([4,50])
    for num,id_it in enumerate(idx_name): #boucle sur nbre d'obs
            res_1[num,:] = map(interp(tuple_opt[num][1].x),xr)
            res_2[num,:] = map(interp(tuple_opt[num][2].x),xr)
            res_5[num,:] = map(interp(tuple_opt[num][5].x),xr)
            res_10[num,:] = map(interp(tuple_opt[num][10].x),xr)
            
            
pylab.subplot(221)
pylab.plot(Nint,nit_mat.T, 'o--')
pylab.xlabel("Number of interpolation points")
pylab.ylabel("Number of iterations")
pylab.title("# of iterations")
pylab.grid()
pylab.legend(idx_name,loc = "best")
pylab.subplot(222)
pylab.plot(Nint,nfev_mat.T, 'o--')
pylab.xlabel("Number of interpolation points")
pylab.ylabel("Number of function evaluations")
pylab.title("# of evaluations")
pylab.grid()
pylab.legend(idx_name,loc = "best")
pylab.subplot(223)
pylab.plot(Nint,np.log(fun_mat.T), 'o--')
pylab.xlabel("Number of interpolation points")
pylab.ylabel("Log of final value of the function")
pylab.title("Log of the final value of the function")
pylab.grid()
pylab.legend(idx_name,loc = "best")
pylab.show()

name_pl = idx_name[:]
name_pl.append('Kref')
pylab.subplot(221)
pylab.plot(xr,res_1.T)
pylab.plot(xr,Kref)
pylab.legend(name_pl, loc = "best")
pylab.grid()
pylab.subplot(222)
pylab.plot(xr,res_2.T)
pylab.plot(xr,Kref)
pylab.legend(name_pl, loc = "best")
pylab.grid()
pylab.subplot(223)
pylab.plot(xr,res_5.T)
pylab.plot(xr,Kref)
pylab.legend(name_pl, loc = "best")
pylab.grid()
pylab.subplot(224)
pylab.plot(xr,res_10.T)
pylab.plot(xr,Kref)
pylab.legend(name_pl, loc = "best")
pylab.grid()
pylab.savefig("optimization_result.eps")