#!/usr/bin/env python
# coding: utf-8

#=================================================================================
##.* Definition of parameter and useful functions
#=================================================================================

import SWE_wrapper as swe        # 
import matplotlib.pyplot as plt
import pickle
from scipy import stats, optimize
import scipy
import numpy as np
import weighted_kde
import half_sample_mode
import seaborn as sns
import corner
import pandas as pd
from sklearn import cluster

import pyDOE
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Matern
from mpl_toolkits.mplot3d import Axes3D

from matplotlib import cm
import copy
import bayesian_optimization.bayesian_optimization as BO

class Estimate:
    def __init__(self, value = [], uref = [], method = ''):
        self.k = value
        self.uref = uref
        self.J = np.nan
        self.method = method
        self.validation_SS = []
    def interp(self, xr):
        return map(swe.interp(self.k), xr)

    def summary(self):
        print 'K =', self.k
        print 'AP =', self.uref
        if not np.isnan(self.J):
            print 'J = ', self.J

    def eval_J(self):
        self.J,_ = J_pw_AP(self.k, self.uref[0], self.uref[1])
        
Kref = 0.2*(1+np.sin(2*np.pi*swe.xr/swe.D[1])) #
xr_ext = np.linspace(1,100,200)
Kref_ext = 0.2*(1+np.sin(2*np.pi*xr_ext/swe.D[1]))



mean_h = 20 
phase = 0
amplitude = 5.0
period = 50.0
# href = y_obs
# Ap_Pm -> A = 5.1; P = 49.8
# Ap_Pp -> A = 5.2; P = 50.1
# Am_Pp -> A = 4.9; P = 50.2
# Am_Pm -> A = 4.8; P = 49.9

def shallow_water_KU(K,amplitude = 5.0, period = 50.0, mean_h = 20.0 , phase = 0.0):
    """Computes h and q for a given value of K, and U = [A,P,M,Ph] """
    bcL = lambda h, hu, t: swe.BCrand(h, hu, t, 'L',
                                 mean_h, amplitude, period, phase)
    return swe.shallow_water(swe.D, swe.g, swe.T, swe.h0, swe.u0, swe.N, swe.LF_flux, swe.dt, swe.b, K,
                                 bcL, swe.bcR)




APref = [5.0, 50.0]    
# APref = [5.1, 49.8]
# APref = [5.2, 50.1] 
# APref = [4.9, 50.2]
# APref = [4.8, 49.9]


[xr, href, uref, t] = shallow_water_KU(Kref, # Generation observation --> Training set
                                       amplitude = APref[0], 
                                       period = APref[1],
                                       mean_h = 20.0,
                                       phase = 0.0)
[_, h_pre, _, _ ] = swe.prevision(Kref, # Generation prediction --> Validation set
                                  amplitude = APref[0],
                                  period = APref[1],
                                  mean_h = 20.0,
                                  phase = 0.0)

def J_nograd(K,AP, hreference = href):
    Kc = np.array(map(swe.interp(K), xr))
    _,hKAP,_,_ = shallow_water_KU(Kc,
                                  amplitude = AP[0], 
                                  period = AP[1],
                                  mean_h = 20.0,
                                  phase = 0.0)
    return 0.5*np.sum((hKAP - hreference)**2)
    

def J_validation(K,AP):
    [_, hprevision, _, _] = swe.prevision(Kref, # Generation prediction --> Validation set
                                  amplitude = AP[0],
                                  period = AP[1],
                                  mean_h = 20.0,
                                  phase = 0.0)
    Kc = np.array(map(swe.interp(K), xr))
    [_, h, _, _] = swe.prevision(Kc, # Generation prediction --> Validation set
                                  amplitude = AP[0],
                                  period = AP[1],
                                  mean_h = 20.0,
                                  phase = 0.0)
    return 0.5*np.sum((h-hprevision)**2)

print APref
Nvar = 4


def prediction_cost(Kcoeff, hpr = h_pre):
    """Compute prediction misfit"""
    K_transform = swe.interp(Kcoeff)
    K = np.array(map(K_transform, xr))
    return swe.J_function(h_pre, swe.prevision(K)[1])

def J_pw_AP(Kcoeff, ampli = 5.0, period = 50.0, hr = href):
    """Piecewise constant interpolation of K, then computes cost function and gradient"""
    K_transform = swe.interp(Kcoeff)
    K = np.array(map(K_transform, xr))
    cost, grad =  swe.J_grad_AP_href(K, ampli, period, hr)
    
    grad_sum_length = int(50.0/Kcoeff.size)
    grad_coeff = np.zeros(Kcoeff.size)
    for i in range(Kcoeff.size):
        grad_coeff[i] = sum(grad[i*grad_sum_length:(i*grad_sum_length+grad_sum_length)])
    return cost,grad_coeff


ref = Estimate(Kref, [5.0,50.0], 'ref')
ref.eval_J()
ref.summary()

##.* Sanity check:
test = J_pw_AP(Kref, ampli = APref[0], period = APref[1], hr= href)
test[0] == 0.0
all(test[1] == np.zeros(50))
prediction_cost(Kref) == 0.0

sample_loss = lambda KAP: J_pw_AP(np.asarray([KAP[0]]),ampli = KAP[1], period = KAP[2])[0]


EGO = BO.bayesian_optimisation(200, sample_loss, bounds = np.atleast_2d([[0, 2.0],[4.7,5.3],[49.7,50.3]]), type_optim = 'explo_EGO')
plt.subplot(1,3,1)
plt.plot(EGO[0][:,0],'.');
plt.subplot(1,3,2)
plt.plot(EGO[0][:,1],'.');
plt.subplot(1,3,3)
plt.plot(EGO[0][:,2],'.');
plt.show()

plt.plot(EGO[1],'.');plt.show()
EGO[0][EGO[1].argmin(),:]

datapoints = pyDOE.lhs(2,5)*7.0

def find_farthest_point(datapoints,bounds):
    """ find_farthest_point

    Find the point P st P = argmax ||datapoints - P ||**2

    Arguments:
    ----------
        datapoints : array (npoints, ndim)
        bounds : array (ndim, 2)
    """
    npoints, ndim = np.atleast_2d(datapoints).shape
    
    fun_to_optimize = lambda P: -np.min(np.sum((P - datapoints)**2, 1))
    bnds = [tuple(bounds[i,:]) for i in xrange(ndim)]
    
    opt = scipy.optimize.minimize(fun_to_optimize, x0 = np.mean(bounds,1),
                                  bounds = bnds)
    best_P, best_J = opt.x, opt.fun
    for x0 in np.random.uniform(bounds[:, 0], bounds[:, 1], size=(10, ndim)):
        opt_restart = scipy.optimize.minimize(fun_to_optimize, x0 =x0,
                                  bounds = bnds)
        if opt_restart.fun < opt.fun:
            best_P = opt_restart.x
            best_J = opt_restart.fun
            
    return best_P, best_J

fig, ax = plt.subplots()
plt.plot(datapoints[:,0],datapoints[:,1], '.')
plt.xlim(0,7)
plt.ylim(0,7)
bounds = np.array([[0,7],[0,7]])
P,radius = find_farthest_point(datapoints,bounds)
circle1 = plt.Circle(P, np.sqrt(np.abs(radius)), color='r', alpha = 0.2)
ax.add_artist(circle1)
plt.plot(P[0],P[1],'.r');plt.show()

def sample_next_hyperparameter_explorative(acquisition_func, gaussian_process, evaluated_loss, greater_is_better=False, bounds=(0, 10), n_restarts=25, explo_index= -1):
    """ sample_next_hyperparameter_explorative

    Proposes the next hyperparameter to sample the loss function for.

    Arguments:
    ----------
        acquisition_func: function.
            Acquisition function to optimise.
        gaussian_process: GaussianProcessRegressor object.
            Gaussian process trained on previously evaluated hyperparameters.
        evaluated_loss: array-like, shape = [n_obs,]
            Numpy array that contains the values off the loss function for the previously
            evaluated hyperparameters.
        greater_is_better: Boolean.
            Boolean flag that indicates whether the loss function is to be maximised or minimised.
        bounds: Tuple.
            Bounds for the L-BFGS optimiser.
        n_restarts: integer.
            Number of times to run the minimiser with different starting points.

    """
    best_x = None
    best_acquisition_value = 1
    n_params = bounds.shape[0]

    for starting_point in np.random.uniform(bounds[:, 0], bounds[:, 1], size=(n_restarts, n_params)):

        res = minimize(fun=acquisition_func,
                       x0=starting_point.reshape(1, -1),
                       bounds=bounds,
                       method='L-BFGS-B',
                       args=(gaussian_process, evaluated_loss, greater_is_better, n_params))

        if res.fun < best_acquisition_value:
            best_acquisition_value = res.fun
            best_x = res.x
        u = best_x[explo_index]
        u_train = gaussian_process.X_train_[:,explo_index]
        u_bounds = bounds[explo_index,:]
        u_farthest = find_farthest_point(u_train,u_bounds)
        best_x[explo_index] = u_farthest
    return best_x

