#!/usr/bin/env python
# coding: utf-8
# ----------------------------------------------------------------
from __future__ import print_function, unicode_literals
import warnings
import numpy as np
import collections
import matplotlib.pylab as plt
from matplotlib import cm
import seaborn as sns
import sys
from statsmodels.distributions.empirical_distribution import ECDF
plt.rc('text', usetex=True)
# plt.rc('text.latex', preamble ='\usepackage{amsmath} \usepackage{amssymb}')
# from __future__ import division, absolute_import
# For migration to python3
# import _


@np.vectorize
def pos_part(x):
    if x > 0.0:
        return x
    else:
        return 0.0


@np.vectorize
def neg_part(x):
    if x < 0.0:
        return x
    else:
        return 0.0


class ROproblem:
    """
    Class created in order to have a coherent and clean way of
    computing and visualise problems of robust optimisation, under uniform distribution for U.
    All based on regular grid over K x U

    ...

    Attributes
    ----------
    label : str
        string that names the problem
    function : callable
        cost function
    meshgrid : list of arrays
        meshgrid that has to be evaluated
    totaldim : int
        dimension of K x U
    indicesK:
        Indices corresponding to the K variable
    indicesU:
        Indices corresponding to the U variable
    Methods
    -------

    """

    def __init__(self, label, Kdim, Udim, function=None):
        """
        Initialize a RO object with label, number of Kdim, Udim and optional function
        """
        self.label = label
        self.function = function
        self.meshgrid = None

        self._Kdim = Kdim
        self._NptsK = None
        self._Udim = Udim
        self._NptsU = None

        self.totaldim = Kdim + Udim
        self.indicesK = range(Kdim)
        self.indicesU = filter(lambda i: i in range(self.totaldim) and
                               i not in self.indicesK, range(self.totaldim))

        self.values_on_grid = None
        self.values = None
        
        self.ROestimates = dict()
        self.alpha_p = None
        self.style_options = {'minE': {'color': 'm',
                                       'ls': '-.',
                                       'leg': r'$\hat{\mathbf{k}}_{\mathrm{mean}}$'},
                              'minV': {'color': 'limegreen',
                                       'ls': ':',
                                       'leg': r'$\hat{\mathbf{k}}_{\mathrm{var}}$'},
                              'minsemidev': {'color': 'teal',
                                             'ls': ':',
                                             'leg': r'semidev'},
                              'MPE': {'color': 'blue',
                                      'ls': '-',
                                      'leg': r'$\hat{\mathbf{k}}_{\mathrm{MPE}}$'},
                              'p100': {'color': 'red',
                                       'ls': '-',
                                       'leg': r'$\hat{\mathbf{k}}_{1.00}$'},
                              'Rk': {'color': 'black',
                                     'ls': '-',
                                     'leg': r'$\Gamma_{\alpha}$'}
                              }

    def set_function(self, fun):
        self.function = fun


    def set_indicesK(self, indices):
        """
        Change classical order of (k1, k2, ..., u1, u2, ...)
        """
        if any(ind > (self.totaldim - 1) for ind in indices):
            print('Indices value exceed total dimension')
        self.indicesK = indices
        self.indicesU = filter(lambda i: i in range(self.totaldim) and
                               i not in self.indicesK, range(self.totaldim))

    def set_base_vectors(self, *vec):
        if self.totaldim != len(vec):
            print('Number of vectors does not match dimension of the problem')
        else:
            self.base_vectors = list(vec)
            self._NptsK = np.asarray([len(self.base_vectors[i]) for i in self.indicesK]).prod()
            self._NptsU = np.asarray([len(self.base_vectors[i]) for i in self.indicesU]).prod()



    def set_meshgrid(self, grid=None, overwrite=False):
        if (self.values_on_grid is not None) and not overwrite:
            warnings.warn(('value_on_grid already specified.'
                           'Use overwrite=True to change grid and reset values_on_grid attr'))
        if (self.values_on_grid is not None) and overwrite:
            self.meshgrid = np.meshgrid(*self.base_vectors, indexing='ij')
            self.values_on_grid = None
        if self.values_on_grid is None:
            self.meshgrid = np.meshgrid(*self.base_vectors, indexing='ij')
        # add dimension updates on grid

    def meshgrid_to_comb(self):
        return np.stack((self.meshgrid), axis=-1).reshape(-1, len(self.meshgrid))

    def set_evaluated_grid(self, val):
        """ Save evaluated grid, especially when stored in .npy file"""
        self.values_on_grid = val

    #  ------ end Init functions -----

    def compute_grid(self, grid=None, overwrite=False):
        """ Evaluates the grid that is provided in meshgrid
        """
        if grid is None:
            if self.meshgrid is None:
                print('No grid specified, provide grid argument or set_meshgrid()')
            else:
                vals_seq = self.function(*self.meshgrid_to_comb().T)
        else:  # Grid provided
            if (self.meshgrid is not None) and not overwrite:  # Avoid conflicts of overwrite
                warnings.warn(('Grid attribute already specified before'
                               'Use overwrite=True to change grid and compute'))
                if (self.meshgrid is not None) and overwrite:
                    self.set_meshgrid(grid, overwrite=True)
                    self.compute_grid(self, None, False)
        self.set_evaluated_grid(vals_seq.reshape([len(self.base_vectors[i])
                                                  for i in range(self.totaldim)]))


    # RO oriented methods --------------------------------------------------
    def add_ROestimate(self, estimate, label, style_dict=None):
        self.ROestimates[label] = np.asarray(estimate).squeeze()

        if style_dict is None:
            if label not in self.style_options:
                self.style_options[label] = {'color': 'lightgrey',
                                             'ls': '--',
                                             'leg': label}
        else:
            if label in self.style_options:
                print('default style overwritten by argument provided')
            self.style_options[label] = style_dict


    def estimates2latex(self):
        print('\n%% Generated by estimates2latex method from ROproblem class')
        print(r'\begin{table*}[!h]')
        print('\centering')
        print('\\begin{tabular}{lr}')
        print('Estimator & Value \\\\ \hline')
        for ke in sorted(self.ROestimates.keys(),
                         key=self.ROestimates.get):
            print('{} & {:4.3f} \\\\ '.format(self.style_options[ke]['leg'],
                                              self.ROestimates[ke]))
        print('\end{tabular}')
        print('\caption{}')
        print('\label{tab:' + 'recap_estimates_{}'.format(self.label) + '}')
        print('\end{table*}')


    def compute_moments(self, std=False):
        """ Compute conditional mean and variance wrt U"""
        cond_mean = np.mean(self.values_on_grid, tuple(self.indicesU))
        if not std:
            cond_var = np.var(self.values_on_grid, tuple(self.indicesU))
            return cond_mean, cond_var
        else:
            cond_std = np.std(self.values_on_grid, tuple(self.indicesU))
            return cond_mean, cond_std

    def var_modif(self, array, axis, fun):
        mn = np.mean(array, axis)
        diff = array - np.expand_dims(mn, axis)
        diff = fun(diff)
        s2 = np.mean(diff**2, axis)
        return s2


    def compute_semidev(self, std=False):
        if std:
            return np.sqrt(self.var_modif(self.values_on_grid, axis=tuple(self.indicesU)[0],
                                          fun=pos_part))
        else:
            return self.var_modif(self.values_on_grid, axis=tuple(self.indicesU)[0],
                                  fun=pos_part)



    def compute_moments_optimisers(self, index=False):
        mean, var = self.compute_moments()
        indice_mean = np.unravel_index(mean.argmin(), mean.shape)
        indice_var = np.unravel_index(var.argmin(), var.shape)
        argmin_mean = [self.base_vectors[j][indice_mean[i]] for i, j in enumerate(self.indicesK)]
        argmin_var = [self.base_vectors[j][indice_var[i]] for i, j in enumerate(self.indicesK)]
        self.add_ROestimate(argmin_mean, 'minE')
        self.add_ROestimate(argmin_var, 'minV')
        if not index:
            return argmin_mean, argmin_var
        else:
            return indice_mean, indice_var


    def compute_conditional_optimum(self):
        """ Compute star values
        """
        return self.values_on_grid.min(tuple(self.indicesK), keepdims=True)


    def _index_to_value(self, index_of_value, index_of_vector):
        return np.asarray([self.base_vectors[j][index_of_value[i]]
                           for i, j in enumerate(index_of_vector)]).squeeze()


    def _return_bool_array_optimisers(self, alpha=1.0):
        """ Based on the conditional minimum, returns an array of the shape
            of the meshgrid, True if (J(k,u) <= alpha*J*(U))
        """
        cond_opt = self.compute_conditional_optimum()
        # shape_of_slices = tuple(np.asarray(self.values_on_grid.shape)[tuple(self.indicesK)])
        tile_repeat = tuple([i if n in self.indicesK else 1
                            for n, i in enumerate(self.values_on_grid.shape)])
        rep_array_of_min = np.tile(cond_opt, tile_repeat)
        bool_array = (self.values_on_grid <= alpha * rep_array_of_min)
        return bool_array

    def _count_on_condition(self, alpha=1.0):
        bool_array = self._return_bool_array_optimisers(alpha)
        # print len(zip(*bool_array.nonzero()))
        # print 'U value    K value'
        # for zi in np.asarray(zip(*bool_array.nonzero())):
        #     print zi[self.indicesU], zi[self.indicesK]

        all_val = np.asarray(bool_array.nonzero())
        Kval = all_val[self.indicesK]
        collKval = collections.Counter(zip(*Kval))
        return bool_array.nonzero(), collKval


    def compute_conditional_optimisers(self, indices=False):
        indices_min, count = self._count_on_condition()
        if indices_min[1].shape[0] != self._NptsU:
            warnings.warn('Number of minimum found is different than the number of U samples. '
                          'Weird behaviour can arise')
        # shapmgU = tuple([len(self.base_vectors[i]) for i in self.indicesU])
        # shapmgK = tuple([len(ROtest.base_vectors[i]) for i in ROtest.indicesK])
        indices_min = np.asarray(indices_min)
        indices_min_k = indices_min[self.indicesK]
        indices_min_u = indices_min[self.indicesU]
        if indices:
            return indices_min_k, indices_min_u
        else:
            return (self._index_to_value(indices_min_k, self.indicesK),
                    self._index_to_value(indices_min_u, self.indicesU))
            # return (np.asarray(zip(*[self.base_vectors[j][indices_min_k[i, :]]
            #                             for i, j in enumerate(self.indicesK)])).squeeze(),
            #         np.asarray(zip(*[self.base_vectors[j][indices_min_u[i, :]]
            #                             for i, j in enumerate(self.indicesU)])).squeeze())
        # for i in np.ndindex(shapmgU):
        #     print i, collUval[i], collUval[i]/float(self._NptsK)

    def compute_MPE(self):
        """ Compute MPE by assuming discrete distrib for U, and choosing mode
        """
        k_star, _ = self.compute_conditional_optimisers()
        # TODO: implement mode selection
        # quick an d dirty way: using kde of sns
        if len(self.indicesK) > 1:
            raise NotImplementedError('No MPE estimation using KDE for dim(K)>1')
        else:
            ax = sns.distplot(k_star)
            kde = ax.lines[0]
            MPE = kde.get_xdata()[kde.get_ydata().argmax()]
            ax.clear()
            plt.close()
        # count = collections.Counter(indices_min))
        # if len(np.unique(count.values())) == 1:
        #     warnings.warn('All conditional minimers appears with the same frequency. '
        #                   'MPE is not relevant here')
        # MPE, MPE_count = np.asarray(count.most_common(1)[0][0]), count.most_common(1)[0][1]
        # # MPE = np.asarray(zip(*[ROtest.base_vectors[j][MPE[i]]
        # #                           for i, j in enumerate(ROtest.indicesK)]))
        # print 'MPE is {}, and has a count of {}'.format(MPE, MPE_count)
        self.add_ROestimate(MPE, 'MPE')


    def compute_probability_alpha(self, alpha=1.0):
        indices_min, count = self._count_on_condition(alpha)
        shapmgK = tuple([len(self.base_vectors[i]) for i in self.indicesK])
        R_k = np.empty(shapmgK)
        for i in np.ndindex(shapmgK):  # probably a better way, but works for now
            R_k[i] = count[i] / float(self._NptsU)
        return R_k


    def alpha_vector_p(self, Nalpha=100, alpha_low=1.0, alpha_up=3.0):
        alpha_to_compute = np.linspace(alpha_low, alpha_up, Nalpha)
        p_vector = np.empty(Nalpha)
        associated_k = []
        for i, alpha in enumerate(alpha_to_compute):
            sys.stdout.write("\r" + "-\|/"[i % 4])
            R_k = self.compute_probability_alpha(alpha)
            associated_k.append(np.unravel_index(R_k.argmax(), R_k.shape))
            p_vector[i] = R_k.max()
            # sys.stdout.flush()
        sys.stdout.write("\r")
        sys.stdout.flush()
        return p_vector, associated_k


    def compute_maxratio(self, Nalpha=100, alpha_low=1.0, alpha_up=3.0, plot=False):
        p_vector, associated_k = self.alpha_vector_p(Nalpha, alpha_low, alpha_up)
        alpha_lin = np.linspace(alpha_low, alpha_up, Nalpha)
        indice_maxratio = (p_vector / alpha_lin).argmax()
        p_maxratio = p_vector[indice_maxratio]
        kindice_maxratio = associated_k[indice_maxratio]
        # print(indice_maxratio)
        k_maxratio = np.asarray(zip(
            *np.atleast_2d([self.base_vectors[j][kindice_maxratio[i]]
                            for i, j in enumerate(self.indicesK)]))).squeeze()
        label = 'pratio{:0}'.format(int(p_maxratio * 100))
        leg = r'$\hat{\mathbf{k}}_{\mathrm{maxratio}}' + ',~ p={:4.3f}$'.format(p_maxratio)
        self.add_ROestimate(k_maxratio, label=label, style_dict = {'color': 'gold',
                                                                   'ls': '-',
                                                                   'leg': leg})
        return p_maxratio


    def alpha_by_dichotomy(self, p=1, Nitermax=10, alpha_low=1, alpha_up=3):
        """
        OBSOLETE
        """
        p = list(p)
        a_l = alpha_low
        a_u = alpha_up
        R_k_l = self.compute_probability_alpha(a_l)
        R_k_u = self.compute_probability_alpha(a_u)
        tmp = np.array([[R_k_l.max(), a_l]])
        tmp = np.concatenate((tmp, [[R_k_u.max(), a_u]]), 0)
        ind_max = []
        alpha_p = []
        for ptarget in p:
            if ptarget == 1.0:
                print('p: {} -> by worst case ratio'.format(ptarget))
                ratio = (self.values_on_grid / self.compute_conditional_optimum())
                worst_vals = ratio.max(tuple(self.indicesU))
                alpha_1 = np.min(worst_vals)
                ind_k_1 = np.unravel_index(worst_vals.argmin(), worst_vals.shape)
                k_1 = (np.asarray([self.base_vectors[j][ind_k_1[i]]
                                   for i, j in enumerate(self.indicesK)])).squeeze()
                # ind_max.append(ind_k_1)
                alpha_p.append(alpha_1)
                if self.alpha_p is None:
                    self.alpha_p = np.vstack([[ptarget, alpha_1]])
                else:
                    self.alpha_p = np.vstack([self.alpha_p, [[ptarget, alpha_1]]])
                lab = 'p100'
                self.add_ROestimate(k_1, lab)
            elif (self.alpha_p is None) or (ptarget not in self.alpha_p[:, 0]):
                # finding new alpha_p
                ite = 0
                itmp = 0
                print('p: {}'.format(ptarget))
                while tmp[itmp, 0] < ptarget:  # Finding tightest interval to begin with
                    itmp += 1
                a_u = tmp[itmp, 1]
                a_l = tmp[itmp - 1, 1]
                R_k_u = self.compute_probability_alpha(a_u)
                R_k_l = self.compute_probability_alpha(a_l)
                while ite < Nitermax:
                    sys.stdout.write("\r" + "-\|/"[ite % 4])
                    if R_k_u.max() < ptarget:
                        a_u *= 2
                        R_k_u = self.compute_probability_alpha(a_u)
                        tmp = np.concatenate((tmp, [[R_k_u.max(), a_u]]), 0)
                        ite += 1
                    elif R_k_l.max() >= ptarget:
                        a_l /= 2
                        R_k_l = self.compute_probability_alpha(a_l)
                        tmp = np.concatenate((tmp, [[R_k_l.max(), a_l]]), 0)
                        ite += 1
                    else:
                        a_h = (a_l + a_u) / 2.0
                        R_k_h = self.compute_probability_alpha(a_h)
                        if (R_k_h >= ptarget).any():
                            R_k_u = R_k_h
                            a_u = a_h
                        elif (R_k_h < ptarget).all():
                            R_k_l = R_k_h
                            a_l = a_h
                        tmp = np.concatenate((tmp, [[R_k_h.max(), a_h]]), 0)
                        ite += 1

                ind_max_current = np.unravel_index(R_k_h.argmax(), R_k_h.shape)
                ind_max.append(ind_max_current)
                alpha_p.append(a_u)

                if self.alpha_p is None:
                    self.alpha_p = np.vstack([[ptarget, a_h]])
                else:
                    self.alpha_p = np.vstack([self.alpha_p, [[ptarget, a_h]]])

                k_p = self._index_to_value(ind_max_current, self.indicesK)
                lab = 'p{:0}'.format(int(ptarget * 100))
                leg = r'$\hat{\mathbf{k}}_p,' + '~\hat{p}=' + '{:3.2f}$'.format(ptarget)
                self.add_ROestimate(k_p, lab, {'color': 'lightgrey',
                                               'leg': leg,
                                               'ls': '--'})
                # print(lab + 'ajouté')
                sys.stdout.write("\r")

                # for it, ind in enumerate(ind_max):  #
                #     k_p = self._index_to_value(ind, self.indicesK)
                #     lab = 'p{:0}'.format(int(p[it] * 100))
                #     self.add_ROestimate(k_p, lab)
                #     print(lab + 'ajouté')
            else:
                warnings.warn('alpha associated with this p already computed: {}'.format(ptarget))
        # return ind_max, alpha_p


    def alpha_quantile(self, p=[1., 0.9], exact_minimisers=None):
        ratio = self.values_on_grid / self.compute_conditional_optimum()
        if exact_minimisers is not None:
            ratio = self.values_on_grid / exact_minimisers
        ind_max = []
        alpha_p = []
        for ptarget in p:
            quant = np.quantile(ratio, ptarget, axis=self.indicesU)
            ind_max_current = np.unravel_index(quant.argmin(), quant.shape)
            a = quant.min()
            ind_max.append(ind_max_current)
            alpha_p.append(a)
            if self.alpha_p is None:
                self.alpha_p = np.vstack([[ptarget, a]])
            else:
                self.alpha_p = np.vstack([self.alpha_p, [[ptarget, a]]])
            k_p = self._index_to_value(ind_max_current, self.indicesK)
            lab = 'p{:0}'.format(int(ptarget * 100))
            leg = r'$\hat{\mathbf{k}}_p,' + '~\hat{p}=' + '{:3.2f}$'.format(ptarget)
            if ptarget == 1.0:
                lab = 'p100'
                self.add_ROestimate(k_p, lab)
            else:
                self.add_ROestimate(k_p, lab, {'color': 'lightgrey',
                                               'leg': leg,
                                               'ls': '--'})


    def compute_worstcase(self):
        # TODO: Write method
        pass


    def _get_slice_indexing_tuple(self, indice_fixed, axes):
        if len(indice_fixed) != len(axes):
            raise ValueError('len(indices_fixed) != len(axes)')
        else:
            indexing_tuple = []
            j = 0
            for i in xrange(self.totaldim):
                if i in axes:
                    indexing_tuple.append(indice_fixed[j])
                    j += 1
                else:
                    indexing_tuple.append(slice(None))  # Include all values
            return tuple(indexing_tuple)

    def get_slice_at_indice(self, indice, axes):
        indexing_tuple = self._get_slice_indexing_tuple(indice, axes)
        return self.values_on_grid[indexing_tuple]


    def _compute_ecdf_at_indice(self, indice, axes):
        all_values = self.get_slice_at_indice(indice, axes).flatten()
        ecdf = ECDF(all_values)
        return ecdf

    def compute_ecdf_k(self, indice):
        return self._compute_ecdf_at_indice(list(indice), self.indicesK)


    def compute_ecdf_optimisers(self, alpha=1.0):
        values_optimum = self.compute_conditional_optimum()
        return ECDF(alpha * values_optimum.flatten())




    # Visual methods --------------------------------------------------
    def get_range(self):
        """ Returns range of computed values """
        if self.values_on_grid is None:
            return np.nan, np.nan
        else:
            min_ = self.values_on_grid.min()
            if min_ <= 0:
                warnings.warn('Minimum value of the function <= 0 !')
            return min_, self.values_on_grid.max()


    def summary(self):
        """ Prints a summary of the ROproblem"""
        string_summary = ['ROproblem: {}'.format(self.label),
                          'dim(K) = {}, dim(U) = {}'.format(self._Kdim, self._Udim)]
        string_summary.append('indicesK: {}'.format(self.indicesK))

        for lab, val in self.ROestimates.iteritems():
            string_summary.append('{}: {}'.format(lab, val))

        string_summary.append('range: {}'.format(self.get_range()))
        string_summary.append('NptsK: {}, NptsU: {}'.format(self._NptsK, self._NptsU))
        print("\n".join(string_summary))


    def plot_ecdf(self, figsize=(10, 10), alpha=0.01):
        shapmgK = tuple([len(self.base_vectors[i]) for i in self.indicesK])
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(1, 1, 1)
        for i in np.ndindex(shapmgK):
            ecdf = self.compute_ecdf_k(i)
            ax.plot(ecdf.x, ecdf.y, alpha=alpha, color='k')

        ecdf_o = self.compute_ecdf_optimisers()
        ax.plot(ecdf_o.x, ecdf_o.y, color = 'r')
        ax.set_ylabel(r'$\mathrm{Pr}\left[J(\mathbf{k},\mathbf{U}) \leq x \right]$')
        ax.set_xlabel(r'$x$')
        plt.show()


    def plot_profiles(self, figsize=(10, 8), shaded_alpha_1=True,
                      contour=False, relative_pos_minifig=0.33, x_end_arrow=3, return_fig=False):
        k_star_i, u_i = self.compute_conditional_optimisers(indices=True)
        # k_star_i = k_star_i.squeeze()[np.argsort(u_i.squeeze())]
        # indice_mean, indice_var = self.compute_moments_optimisers(index=True)

        k_axis = np.array(list(self.base_vectors[i] for i in self.indicesK)).squeeze()
        u_axis = self.base_vectors[self.indicesU[0]]
        # Jmean = self.get_slice_at_indice(indice_mean, self.indicesK)
        # Jvar = self.get_slice_at_indice(indice_var, self.indicesK)
        fig = plt.figure(figsize=figsize)
        ax_profile = fig.add_subplot(1, 1, 1)
        # ax_profile.plot(u_axis, Jmean, color=self.style_options['minE'])
        # ax_profile.plot(u_axis, Jvar, color=self.style_options['minV'])
        minimum = self.compute_conditional_optimum().squeeze()

        for lab, value in self.ROestimates.iteritems():
            indice_est = []  # TODO: dirty fix, may change laterr
            if self._Kdim == 1:
                indice_est.append(np.abs(k_axis - value).argmin())
            else:
                for i, val_i in enumerate(value):
                    indice_est.append(np.abs(k_axis[i] - val_i).argmin())
            Jest = self.get_slice_at_indice(indice_est, self.indicesK).squeeze()
            if lab == 'p100':
                J100 = Jest
        # for lab, value in self.ROestimates.iteritems():
        #     try:
        #         self.latex_legend[lab]
        #     except KeyError:
        #         self.latex_options[lab] = 'lightgray'
            ax_profile.plot(u_axis, Jest,
                            color=self.style_options[lab]['color'],
                            label=self.style_options[lab]['leg'],
                            ls=self.style_options[lab]['ls'])

        ax_profile.plot(u_axis, minimum, linewidth=1.5, color='k', label=r'$J^*(\mathbf{u})$')
        if shaded_alpha_1:
            alpha = self.alpha_p[:, 1].max()
            ax_profile.plot(u_axis, alpha * minimum, linewidth=1.5,
                            color='k', label=r'$\alpha_1 J^*(\mathbf{u})$')
            ax_profile.fill_between(u_axis, alpha * minimum, minimum, color='grey', alpha=0.1)
        ax_profile.set_xlabel(r'$\mathbf{u}$')
        ax_profile.set_ylabel(r'$\mathbf{u}\mapsto J(\hat{\mathbf{k}},\mathbf{u})$')

        if contour:
            import matplotlib.patches as mpatches
            ax_contour = plt.axes([relative_pos_minifig, .7, .15, .15])
            k_mg, u_mg = self.meshgrid[self.indicesK[0]], self.meshgrid[self.indicesU[0]]
            k_star_i, u_i = self.compute_conditional_optimisers(indices=True)
            k_star_i = k_star_i.squeeze()[np.argsort(u_i.squeeze())]
            u_i = np.sort(u_i.squeeze())
            ax_contour.contourf(k_mg, u_mg, self.values_on_grid, 10, zorder=0, cmap = cm.Greys)
            alpha = self.alpha_p[:, 1].max()
            bool_array = self._return_bool_array_optimisers(alpha)
            ax_contour.contour(k_mg, u_mg, bool_array, 2)
            ax_contour.plot(k_axis[k_star_i.squeeze()],
                            u_axis[u_i.squeeze()], 'r.', markersize=0.5)
            for lab, value in self.ROestimates.iteritems():
                ax_contour.axvline(value,
                                   color=self.style_options[lab]['color'],
                                   ls=self.style_options[lab]['ls'])

            start_arrow = (self.ROestimates['p100'],
                           0)
            end_arrow = (x_end_arrow,
                         J100[np.abs(u_axis - x_end_arrow).argmin()])
            con_head = mpatches.ConnectionPatch(xyA=start_arrow, xyB=end_arrow,
                                                coordsA="data", coordsB="data",
                                                axesA=ax_contour, axesB=ax_profile,
                                                arrowstyle="<->",
                                                connectionstyle = 'angle')
            con_head.set_color('black')
            ax_contour.add_artist(con_head)
            ax_contour.set_xlabel(r'$\mathbf{k}$')
            ax_contour.set_xticks([])
            ax_contour.set_ylabel(r'$\mathbf{u}$')
            ax_contour.set_yticks([])

        ax_profile.legend()
        if return_fig:
            if contour:
                return fig, ax_profile, ax_contour
            else:
                return fig, ax_profile
        else:
            plt.tight_layout()
            plt.show()


    def plot_evolution_wrt_p(self, figsize=(10, 6), plttype='both',
                             Nalpha=100, alpha_low=1.0, alpha_up=3.0, rev=False):
        prob_of_alpha, k_assoc = self.alpha_vector_p(Nalpha=100, alpha_low=1.0, alpha_up=3.0)
        alpha_lin = np.linspace(alpha_low, alpha_up, Nalpha)
        indice_maxratio = (prob_of_alpha / alpha_lin).argmax()
        p_maxratio = prob_of_alpha[indice_maxratio]
        fig = plt.figure(figsize=figsize)

        if plttype == 'both':
            ax_left = fig.add_subplot(1, 2, 1)
            ax_left.plot(prob_of_alpha, alpha_lin, 'k')
            ax_left.set_title(r'$\alpha_p$ as a function of $p$')
            ax_left.axvline(p_maxratio, linestyle='--', color='k')
            ax_left.set_ylabel(r'$\alpha_p$')
            ax_right = fig.add_subplot(1, 2, 2)
            ax_right.plot(prob_of_alpha, prob_of_alpha / alpha_lin, 'k')
            ax_right.set_title(r'Ratio $p/\alpha_p$ as a function of $p$')
            ax_right.axvline(p_maxratio, linestyle='--', color='k')
        elif plttype == 'ratio':
            ax = fig.add_subplot(1, 1, 1)
            ax.plot(prob_of_alpha, prob_of_alpha / alpha_lin)
            ax.set_title(r'Ratio $p/\alpha_p$ as a function of $p$', 'k')
            ax.axvline(p_maxratio, linestyle='--', color='k')
        else:
            ax = fig.add_subplot(1, 1, 1)
            if rev:
                ax.plot(alpha_lin, prob_of_alpha)
            else:
                ax.plot(prob_of_alpha, alpha_lin)
            ax.set_title(r'$\alpha_p$ as a function of $p$')
            ax.set_ylabel(r'$\alpha_p$')
        # Common prop
        for axe in fig.axes:
            axe.set_xlabel(r'$p$')
            axe.grid(alpha=0.3)
        return fig


    def plot2D(self, front_alpha_1=False, alpha=None, figsize=(10, 10), fill=False, semi=False):
        if self._Kdim != 1 and self._Udim != 1:
            warnings.warn('For the visualisation, dim K and dim U must be equal to 1')
        else:
            if front_alpha_1:
                alpha = self.alpha_p[:, 1].max()
            if alpha is not None:
                bool_array = self._return_bool_array_optimisers(alpha)
            k_mg, u_mg = self.meshgrid[self.indicesK[0]], self.meshgrid[self.indicesU[0]]
            k_axis = self.base_vectors[self.indicesK[0]]
            u_axis = self.base_vectors[self.indicesU[0]]

            k_star_i, u_i = self.compute_conditional_optimisers(indices=True)
            k_star_i = k_star_i.squeeze()[np.argsort(u_i.squeeze())]
            u_i = np.sort(u_i.squeeze())
            c_mean, c_std = self.compute_moments(std=True)
            c_semi = self.compute_semidev(std=True)

            fig = plt.figure(figsize=figsize)

            ax_contour = fig.add_subplot(2, 1, 1)
            ax_contour.contourf(k_mg, u_mg, self.values_on_grid, 10)
            # TODO: Voir pq il faut rev le vector => de la déf du mg ou
            # dans _count_on_condition?
            if alpha is not None:
                if fill:
                    ax_contour.contourf(k_mg, u_mg, bool_array, 2)
                else:
                    ax_contour.contour(k_mg, u_mg, bool_array, 2)
            ax_contour.plot(k_axis[k_star_i.squeeze()],
                            u_axis[u_i.squeeze()], 'r.')
            ax_mean = fig.add_subplot(2, 1, 2)
            ax_mean.set_yticks([])
            ax_std = ax_mean.twinx()
            ax_mean.plot(k_axis, c_mean.squeeze(),
                         color=self.style_options['minE']['color'],
                         ls=self.style_options['minE']['ls'])
            ax_std.plot(k_axis, c_std.squeeze(),
                        color=self.style_options['minV']['color'],
                        ls=self.style_options['minV']['ls'])
            if semi:
                ax_std.plot(k_axis, c_semi.squeeze(),
                            color=self.style_options['minsemidev']['color'],
                            ls=self.style_options['minsemidev']['ls'])
            ax_sns = ax_mean.twinx()
            ax_sns = sns.distplot(k_axis[k_star_i])
            ax_sns.set_yticks([])
            ax_std.set_yticks([])
            for lab, value in self.ROestimates.iteritems():
                ax_mean.axvline(value, color=self.style_options[lab]['color'])
                ax_contour.axvline(value, color=self.style_options[lab]['color'])
            ax_mean.set_xlim(k_axis.min(), k_axis.max())

            if alpha is not None:
                R_k = self.compute_probability_alpha(alpha)
                ax_R = ax_std.twinx()
                ax_R.set_ylabel(r'$\Gamma_{\alpha}(\mathbf{k})$')
                ax_R.plot(k_axis, R_k, color=self.style_options['Rk']['color'])
                ax_R.set_ylim([0, 1.5])

            ax_contour.set_xlabel(r'$\mathbf{k}$')
            ax_mean.set_xlabel(r'$\mathbf{k}$')
            ax_contour.set_ylabel(r'$\mathbf{u}$')
            plt.tight_layout()
            plt.show()


# End of class definition -----------------------------------------------------------


# Multidimensional example ----------------------------------------
def dummy_function(*arguments):
    return (1.0 + arguments[0]) * sum(arguments) / (arguments[1] * arguments[2])


ROtest = ROproblem(label='dummy', Kdim=4, Udim=1,
                   function=dummy_function)
ROtest.set_indicesK([1, 2, 3, 4])
# ROtest.add_ROestimate(estimate = 3.32, label = 'Dummy estimate')
# ROtest.set_evaluated_grid(np.array([1, 4, 3, 5, 10, -3]))
# ROtest.get_range()
k0 = np.array([0, 1, 2], dtype=float)
k1 = np.array([10, 20, 30, 40], dtype=float)
k2 = np.array([100, 200], dtype=float)
u0 = np.array([1000, 2000, 3000, 4000, 5000], dtype=float)
u1 = np.array([10000, 20000, 30000, 40000, 50000, 60000], dtype=float)

ROtest.set_base_vectors(k0, u0, k1, u1, k2)
ROtest.set_meshgrid()
ROtest.compute_grid()
# ROtest.meshgrid
ROtest.compute_moments_optimisers()
ROtest.compute_probability_alpha(alpha=1.5)
ROtest.alpha_by_dichotomy(p = [1, .9, .95])
ROtest.alpha_by_dichotomy(p = [1, .88, .9, .5, .95])
ROtest.alpha_p
ROtest.summary()
ROtest.compute_maxratio()
ROtest.plot_ecdf(alpha=0.5)
ROtest.plot_profiles()
ROtest.alpha_vector_p(Nalpha=100, alpha_low=1.0, alpha_up=3.0)


# 2D example ---------------------------------------------
def branin_hoo(x, y):
    x1 = 3 * x - 5
    x2 = 3 * y
    damp = 1.0
    quad = (x2 - (5.1 / (4 * np.pi**2)) * x1**2 + (5 / np.pi) * x1 - 6)**2
    cosi = (10 - (10 / np.pi * 8)) * np.cos(x1) - 44.81
    return (quad + cosi) / (51.95 * damp) + 2.0


Npts = 1000
branin = ROproblem(label='branin', Kdim=1, Udim=1,
                   function=branin_hoo)
branin.set_indicesK([1])
x0 = np.linspace(0, 5, Npts)
# from scipy.stats import norm
# x1 = np.sort(norm.rvs(loc=2.5, scale=1, size=Npts))
x1 = np.linspace(0, 5, Npts)
branin.set_base_vectors(x1, x0)
branin.set_meshgrid()
branin.compute_grid()

branin.compute_moments_optimisers()
branin.compute_MPE()
branin.summary()
# branin.alpha_by_dichotomy(p=[1, .95], Nitermax=20)
branin.alpha_quantile(p=[1, .95, .90])
# branin.compute_maxratio()
branin.plot2D(semi=True)
branin.estimates2latex()
branin.plot_profiles(contour=True)
branin.plot_evolution_wrt_p(plttype='lese', rev=True)
# branin.plot_ecdf()

fig, ax_pro, _ = branin.plot_profiles(figsize = (8, 6),
                                      contour=True, relative_pos_minifig=0.33, x_end_arrow=2.7,
                                      return_fig=True)
ax_pro.set_title((r'Profiles $\mathbf{u}\mapsto J(\hat{\mathbf{k}},\mathbf{u})$ '
                  r'for different estimates $\hat{\mathbf{k}}$'))
fig.tight_layout()
# fig.savefig('/home/victor/PhD/Article/NPG1/Figures/profile_BHs_all_estimates.pdf')
plt.show()
# branin.plot2D(front_alpha_1=False)

# branin.plot_ecdf()
branin.plot_evolution_wrt_p(figsize=(8, 4))
plt.tight_layout()
# plt.savefig('/home/victor/PhD/Article/NPG1/Figures/alpha_p_BH.pdf')
plt.show()

branin.plot2D()

branin.summary()

# Test with loaded data ------------------------------------------------

from lorenz.lorenz import plot_spread

data_lorenz = np.load('/home/victor/These/source_code/lorenz/xb_y_fun_2000_allobs.npy')
# data_lorenz = np.load('/home/victor/These/lorenz_grid_nooffset.npy') + 1.0
lorenz = ROproblem(label='lorenz', Kdim=1, Udim=1,
                   function=None)
lorenz.set_indicesK([0])
Npts = 2000
xx = np.linspace(-0.05, 0.05, Npts)
lorenz.set_base_vectors(xx, xx)
lorenz.set_meshgrid()
lorenz.set_evaluated_grid(data_lorenz)
lorenz.compute_moments_optimisers()
lorenz.compute_MPE()
lorenz.summary()
lorenz.alpha_by_dichotomy(p=[1], alpha_low=8, alpha_up=12)
lorenz.alpha_quantile(p=[1, 0.95, 0.90])
lorenz.plot2D(front_alpha_1=True, fill=True)
lorenz.compute_maxratio()
fig, ax_pro = lorenz.plot_profiles(contour=False, return_fig=True)
ax_pro.set_title((r'Profiles $\mathbf{u} \mapsto J(\hat{\mathbf{k}},\mathbf{u})$ '
                  r'for different estimates $\hat{\mathbf{k}}$ for the Lorenz model'))
# lorenz.plot_evolution_wrt_p()
fig.tight_layout()
# fig.savefig('/home/victor/PhD/Article/NPG1/Figures/profiles_lorenz_all_estimates.pdf')
plt.show()


Nsim = 500
alpha = 0.01
plt.subplot(3, 2, 1)
plot_spread(lorenz.ROestimates['minE'],
            lambda: np.random.uniform(-0.05, 0.05), Nsim, alpha=alpha)
plt.title('minE')

plt.subplot(3, 2, 2)
plot_spread(lorenz.ROestimates['minV'],
            lambda: np.random.uniform(-0.05, 0.05), Nsim, alpha=alpha)
plt.title('minV')

plt.subplot(3, 2, 3)
plot_spread(lorenz.ROestimates['MPE'],
            lambda: np.random.uniform(-0.05, 0.05), Nsim, alpha=alpha)
plt.title('MPE')

plt.subplot(3, 2, 4)
plot_spread(lorenz.ROestimates['p100'],
            lambda: np.random.uniform(-0.05, 0.05), Nsim, alpha=alpha)
plt.title('p100')

plt.subplot(3, 2, 5)
# plot_spread(lorenz.ROestimates['pratio52'],
#             lambda: np.random.uniform(-0.05, 0.05), Nsim, alpha=alpha)
plt.title('pratio')

# plt.subplot(3, 2, 6)
# plot_spread(lorenz.ROestimates['p99'],
#             lambda: np.random.uniform(-0.05, 0.05), Nsim, alpha=alpha)
# plt.title('p99')

plt.show()


prob_alpha, k_assoc = lorenz.alpha_vector_p(Nalpha=100, alpha_low=1.0, alpha_up=12.0)
lorenz.alpha_by_dichotomy(p=[1.0, 0.99, 0.95, 0.90], Nitermax=10, alpha_up=12.0)
lorenz.compute_maxratio(Nalpha=100, alpha_low=1.0, alpha_up=12.0)
alpha_lin = np.linspace(1, 12, 100)

plt.subplot(1, 2, 1)
plt.plot(prob_alpha, alpha_lin)
plt.subplot(1, 2, 2)
plt.plot(prob_alpha, prob_alpha / alpha_lin)
plt.show()


# Test with six humps camel
def six_humps_camel(x, y):
    return (4 - 2.1 * x**2 + (x**4) / 3) * x**2 \
        + x * y + (-4 + 4 * y**2) * y**2 + 2


camel = ROproblem(label='camel', Kdim=1, Udim=1,
                  function=six_humps_camel)
camel.set_indicesK([1])
Npts = 1000
xx = np.linspace(-2, 2, Npts)
yy = np.linspace(-1, 1, Npts)
camel.set_base_vectors(xx, yy)
camel.set_meshgrid()
camel.compute_grid()
camel.compute_moments_optimisers()
camel.compute_MPE()
camel.alpha_by_dichotomy(p=[1.0, 0.99, .95, .90], Nitermax=8)
camel.compute_maxratio()
camel.plot2D(front_alpha_1=True)
camel.plot_profiles(contour=True)



def Himmelblau(x, y):
    return ((x**2 + y - 11)**2 + (x + y**2 - 7)**2 + 5) / 315.0


himmel = ROproblem(label='himmelblau', Kdim=1, Udim=1,
                   function=Himmelblau)
himmel.set_indicesK([0])
Npts = 1000
xx = np.linspace(-4, 4, Npts)
yy = np.linspace(-4, 4, Npts)
himmel.set_base_vectors(xx, yy)
himmel.set_meshgrid()
himmel.compute_grid()
himmel.compute_moments_optimisers()
himmel.compute_MPE()
himmel.compute_maxratio()
himmel.summary()
himmel.alpha_p
himmel.alpha_by_dichotomy(p=[1.0, 0.99, 0.95, 0.90], alpha_up=15)
himmel.plot2D(front_alpha_1=True)
# himmel.plot_ecdf()
himmel.plot_profiles(contour=True)


# VanDerPol
from vanderpol.vanderpol import vanderpol_all


def vanderpol_J(mu, A, w, x0, dx0):
    par_vector = np.vstack([mu, A, w, x0, dx0]).T
    return vanderpol_all(par_vector)


def vdp_ku(k, u):
    ones = np.ones_like(k)
    return vanderpol_J(8.5 * ones, k, u, ones, 0 * ones)


vdp = ROproblem('vdp', Kdim=1, Udim=1, function = vdp_ku)
vdp.set_indicesK([0])
NptsK = 500
NptsU = 500
# k_ax = np.linspace(8, 9, Npts)
k_ax = np.linspace(1.19, 1.205, NptsK)
u_ax = np.linspace(-0.01, 0.01, NptsU) + np.pi / 5.0
vdp.set_base_vectors(k_ax, u_ax)
vdp.set_meshgrid()
vdp.compute_grid()
vdp.compute_moments_optimisers()
vdp.alpha_by_dichotomy([1])
vdp.summary()
vdp.plot2D()

vdp.plot_profiles()


# Vibration absorber Randall

from vibabsorb.vibabsorb import max_displacement

vib = ROproblem('Randall', Kdim=2, Udim=1,
                function=lambda ksi, T, beta: max_displacement(ksi, T, beta) + 5.0)
vib.set_indicesK([0, 1])
NptsK = 200
NptsU = 500

ksi2 = np.linspace(0, 1, NptsK)
T = np.linspace(0.1, 2, NptsK)
beta = np.linspace(0, 2.5, NptsU)
vib.set_base_vectors(ksi2, T, beta)
vib.set_meshgrid()
# vib.compute_grid()
# np.save('/home/victor/These/source_code/vibabsorb/200x200x500.npy', vib.values_on_grid)
VOG = np.load('/home/victor/These/source_code/vibabsorb/200x200x500.npy')
vib.set_evaluated_grid(VOG)
vib.compute_moments_optimisers()
vib.compute_conditional_optimisers()
# vib.alpha_by_dichotomy([1, .90])
vib.alpha_quantile(p=[1.0, 0.99, 0.95, 0.90, 0.80])
vib.compute_maxratio()
vib.summary()
vib.plot_profiles()

plt.subplot(1, 2, 1)
plt.contour(vib.compute_moments()[0], 50)
plt.subplot(1, 2, 2)
plt.contour(vib.compute_moments()[1], 50)
plt.show()



def var_modif(array, axis, fun):
    mn = np.mean(array, axis)
    diff = array - np.expand_dims(mn, axis)
    diff = fun(diff)
    s2 = np.mean(diff**2, axis)
    return s2


@np.vectorize
def pos_part(x):
    if x > 0.0:
        return x
    else:
        return 0.0


@np.vectorize
def neg_part(x):
    if x < 0.0:
        return x
    else:
        return 0.0


# EOF ------------------------------------------------------------------------------
