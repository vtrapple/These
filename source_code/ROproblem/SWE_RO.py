#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  Application of ROproblem on SWE case

import numpy as np
import pickle
import matplotlib.pyplot as plt
from ROproblem import ROproblem
from SWE_VT import shallowwater as sw
from SWE_VT import animation_SWE as anim
from SWE_VT.numerics.numerical_flux import LF_flux, rusanov_flux
from SWE_VT.numerics.boundary_conditions import (bcR,
                                                 bcL_A, bcR_A,
                                                 BCrand,
                                                 BCperiodic)
from SWE_VT.numerics.interpolation_tools import interp
from SWE_VT.cost import cost_observations as cst




# Model Parameters ----
D = [0, 100]
N = 50  # Nombre de volumes
dx = np.diff(D)[0] / float(N)  # Largeur des volumes
xr = np.linspace(D[0] + dx / 2, D[1] - dx / 2, N)  # Milieux des volumes
T = 100
Kref = 0.2 * (1 + np.sin(2 * np.pi * xr / D[1]))
dt = 0.03
b = lambda x: 10 * x / D[1] + 0.5 * np.cos(x / 3) + 3 / (1 + np.exp(-3 * (x - 50)))


def b(x):
    peakdomain = 2 * (D[1] - D[0]) / 3.0
    return 10 * x / D[1] + 5 * np.exp(-(x - peakdomain)**2 / 9)


h0 = lambda x: 16.0 * np.ones_like(x)


# Boundary conditions ----
def BC(h, hu, t, mean_h, amplitude_vector, fundperiod, phase):
    """ Conditions aux limites du modele direct, avec plus de paramètres"""
    h[0] = mean_h
    period = fundperiod
    for amp in amplitude_vector:
        h[0] += amp * np.sin((t * (2 * np.pi) / period) + phase)
        period /= 2.0
    hu[0] = 0.0
    return [h] + [hu]


bcsin_ref = lambda h, hu, t: BC(h, hu, t, 16.0, [0.5, 1, 0.5, 0.25], fundperiod=100.0,
                                phase=0.0)

bc_sim = lambda h, hu, t, U: BC(h, hu, t, 16.0, [0.4, 1, 0.5, U], fundperiod=100.0,
                                phase=0.0)

ref = sw.ShallowWaterSimulation(T=T, b=b, K=Kref, dt=dt, h0=h0,
                                idx_observation=None,  # np.arange(49, 200, 50, dtype=int),
                                bcL=bcsin_ref)
tst = sw.ShallowWaterSimulation(T=T, b=b, K=Kref, dt=dt, h0=h0,
                                idx_observation=None,  # np.arange(49, 200, 50, dtype=int),
                                bcL=lambda h, q, t: bc_sim(h, q, t, 0.0))
_ = ref.direct_simulation()
# _ = tst.direct_simulation()
anim.animate_SWE(ref.xr, [ref.ssh], b, D, ylim=[0, 30])




model = sw.CostSWE(ref, bc_sim)
fun_eval = lambda k, u: model.J_KU(np.asarray([k, u]).T, parallel=True, adj_gradient=False)

swRO = ROproblem.ROproblem('SWE', 1, 1, fun_eval)
swRO.set_indicesK([0])
k_ax = np.linspace(0.0, 0.4, 50)
u_ax = np.linspace(0.2, .4, 50)
swRO.set_base_vectors(k_ax, u_ax)
swRO.set_meshgrid()
swRO.compute_grid()
swRO.compute_moments_optimisers()
swRO.summary()
swRO.plot2D()
swRO.alpha_quantile(p=[1.0, 0.95, 0.99, 0.90])
swRO.plot_profiles()

# with open('/home/victor/These/source_code/ROproblem/swe2.obj', 'w') as fil:
#     pickle.dump(swRO, fil)

with open('/home/victor/These/source_code/ROproblem/swe2.obj', 'r') as fil:
    swRO = pickle.load(fil)


# validation
def pred_error(Karray, u, utrue, Tpred, BCtrue, BCsim):
    cost_pred = []
    for utrue_ in utrue:
        # bcLtrue = lambda h, q, t: BC(h, q, t, 16.0, [0.5, 1, 0.5, utrue_], fundperiod=100.0,
        #                              phase=0.0)
        bcLtrue = lambda h, q, t: BCtrue(h, q, t, utrue_)
        _, htruepred, _, _ = ref.continue_simulation_par(Karray=None,
                                                         bcL=bcLtrue,
                                                         T2=Tpred)
        # valid for 1D estimates
        for u_sim in u:
            # bcL = lambda h, q, t: bc_sim(h, q, t, u_sim)
            bcL = lambda h, q, t: BCsim(h, q, t, u_sim)
            to_append = [utrue_, u_sim]
            for k in Karray:
                Ka = np.array(map(sw.interp(k, D), ref.xr))
                _, hpred, _, _ = ref.continue_simulation_par(Karray=np.asarray(Ka),
                                                             bcL=bcL,
                                                             T2=Tpred)
                to_append.append((k, np.sum((hpred - htruepred)**2)))
            cost_pred.append(to_append)
        print('utrue_ finished')
    return np.asarray(cost_pred)



sample_size = 20
import pyDOE
u1u2_lhs = pyDOE.lhs(2, samples=sample_size, iterations=20, criterion='m') * 0.2 + 0.2
u1, u2 = u1u2_lhs[:, 0], u1u2_lhs[:, 1]

# u1 = np.random.random(size=sample_size) * 0.2 + 0.2
# u2 = np.random.random(size=sample_size) * 0.2 + 0.2

cost_pred = pred_error((Kref,
                        [swRO.ROestimates['minE']],
                        [swRO.ROestimates['minV']],
                        # [swRO.ROestimates['p100']],
                        [swRO.ROestimates['p90']]),
                       u1, u2, 100)


cost_pred_to_add = pred_error((Kref,
                               [swRO.ROestimates['minE']],
                               [swRO.ROestimates['minV']],
                               # [swRO.ROestimates['p100']],
                               [swRO.ROestimates['p90']]),
                              u1, u_true, 100)
# np.save('/home/victor/These/source_code/ROproblem/cost_pred_swe_2.npy', cost_pred)
# np.save('/home/victor/These/source_code/ROproblem/cost_pred_swe_2_toadd.npy', cost_pred_to_add)
cost_pred_to_add = np.load('/home/victor/These/source_code/ROproblem/cost_pred_swe_2_toadd.npy', allow_pickle=True)
# cost_pred = np.load('/home/victor/These/source_code/ROproblem/cost_pred_swe.npy', allow_pickle=True)
cost_pred = np.load('/home/victor/These/source_code/ROproblem/cost_pred_swe_2.npy',
                    allow_pickle=True)

cost_pred_ref = np.asarray([cost_pred[:, 2][i][1] for i in xrange(5)])
cost_pred_minE = np.asarray([cost_pred[:, 3][i][1] for i in xrange(5)])
cost_pred_minV = np.asarray([cost_pred[:, 4][i][1] for i in xrange(5)])
# cost_pred_p100 = np.asarray([cost_pred[:, 5][i][1] for i in xrange(sample_size**2)])
# cost_pred_p99 = np.asarray([cost_pred[:, 6][i][1] for i in xrange(sample_size**2)])
cost_pred_p90 = np.asarray([cost_pred[:, 5][i][1] for i in xrange(sample_size**2)])

sample_size=10
cost_pred_ref_add = np.asarray([cost_pred_to_add[:, 2][i][1]
                                for i in xrange(sample_size * 20)]).reshape(20, 10)
cost_pred_minE_add = np.asarray([cost_pred_to_add[:, 3][i][1]
                                 for i in xrange(sample_size * 20)]).reshape(20, 10)
cost_pred_minV_add = np.asarray([cost_pred_to_add[:, 4][i][1]
                                 for i in xrange(sample_size * 20)]).reshape(20, 10)
# cost_pred_p100_add = np.asarray([cost_pred_to_add[:, 5][i][1] for i in xrange(sample_size**2)])
# cost_pred_p99_add = np.asarray([cost_pred_to_add[:, 6][i][1] for i in xrange(sample_size**2)])
cost_pred_p90_add = np.asarray([cost_pred_to_add[:, 5][i][1]
                                for i in xrange(sample_size * 20)]).reshape(20, 10)

cost_pred_ref_new = np.hstack([cost_pred_ref.reshape(20, 20), cost_pred_ref_add])
cost_pred_minE_new = np.hstack([cost_pred_minE.reshape(20, 20), cost_pred_minE_add])
cost_pred_minV_new = np.hstack([cost_pred_minV.reshape(20, 20), cost_pred_minV_add])
cost_pred_p90_new = np.hstack([cost_pred_p90.reshape(20, 20), cost_pred_p90_add])


plt.boxplot([cost_pred_ref, cost_pred_minE, cost_pred_minV, cost_pred_p99, cost_pred_p100],
            showmeans=True)
plt.show()

u_true = [cost_pred[i * 20, 0] for i in xrange(20)]
u_sim = [cost_pred[i, 1] for i in xrange(20)]
cost_ref = cost_pred_ref.reshape(20, 20)
inds = np.argsort(u_true)

plt.boxplot(cost_ref[inds, :].T)
plt.boxplot(cost_pred_minE.reshape(20, 20)[inds, :].T)
plt.boxplot(cost_pred_minE.reshape(20, 20)[inds, :].T)
plt.boxplot(cost_pred_minE.reshape(20, 20)[inds, :].T)
plt.show()

plt.plot(cost_pred_ref.reshape(20, 20)[inds, :].mean(1), label='Kref', color='k')
plt.fill_between(range(20),
                 cost_pred_ref.reshape(20, 20)[inds, :].mean(1) +
                 cost_pred_ref.reshape(20, 20)[inds, :].std(1) / np.sqrt(30),
                 cost_pred_ref.reshape(20, 20)[inds, :].mean(1) -
                 cost_pred_ref.reshape(20, 20)[inds, :].std(1) / np.sqrt(30), color='k', alpha=0.1)

plt.plot(cost_pred_minE.reshape(20, 20)[inds, :].mean(1), label='E', color='m')
plt.fill_between(range(20),
                 cost_pred_minE.reshape(20, 20)[inds, :].mean(1) +
                 cost_pred_minE.reshape(20, 20)[inds, :].std(1) / np.sqrt(30),
                 cost_pred_minE.reshape(20, 20)[inds, :].mean(1) -
                 cost_pred_minE.reshape(20, 20)[inds, :].std(1) / np.sqrt(30), color='m', alpha=0.1)



plt.plot(cost_pred_minV.reshape(20, 20)[inds, :].mean(1), label='V', color='limegreen')
plt.fill_between(range(20),
                 cost_pred_minV.reshape(20, 20)[inds, :].mean(1) +
                 cost_pred_minV.reshape(20, 20)[inds, :].std(1) / np.sqrt(30),
                 cost_pred_minV.reshape(20, 20)[inds, :].mean(1) -
                 cost_pred_minV.reshape(20, 20)[inds, :].std(1) / np.sqrt(30),
                 color='limegreen', alpha=0.1)

# plt.plot(cost_pred_p100.reshape(20, 20)[inds, :].mean(1), label='p100', color='red')

plt.plot(cost_pred_p90.reshape(20, 20)[inds, :].mean(1), label='p90', color='red')
plt.fill_between(range(20),
                 cost_pred_p90.reshape(20, 20)[inds, :].mean(1) +
                 cost_pred_p90.reshape(20, 20)[inds, :].std(1) / np.sqrt(30),
                 cost_pred_p90.reshape(20, 20)[inds, :].mean(1) -
                 cost_pred_p90.reshape(20, 20)[inds, :].std(1) / np.sqrt(30),
                 color='red', alpha=0.1)


plt.legend()
plt.show()

from scipy import stats
import pandas as pd

for i in inds:
    print(stats.ttest_ind(cost_pred_p90_new[i, :],
                          cost_pred_minE_new[i, :]))


import seaborn as sns
df = pd.DataFrame(cost_pred, columns = ['utrue', 'usim', 'kref', 'kE', 'kV', 'kp900', 'kp99'])
df['kref'][0][1]

dict_to_save = {'utrue': u_true, 'ref': cost_pred_ref_new, 'minE': cost_pred_minE_new, 'minV': cost_pred_minV_new,
                'p90': cost_pred_p90_new}

np.save('/home/victor/These/source_code/ROproblem/cost_pred_dict_20x30.npy', dict_to_save)


# PERIODIC BC

D = [0, 100]
N = 500  # Nombre de volumes
N = 200
dx = np.diff(D)[0] / float(N)  # Largeur des volumes
xr = np.linspace(D[0] + dx / 2, D[1] - dx / 2, N)  # Milieux des volumes
b = lambda x: np.zeros_like(x)
T = 50
T = 100
dt = 0.01
Kref = 0.2 * (1 + np.sin(2 * np.pi * xr / D[1]))


def bathy(x):
    return 2.0 * (1 - np.cos(2 * np.pi * x / D[1])) / 2.0



def BCperiodic_external_forcing(h, hu, t):
    h[0], h[-1] = h[-1], h[0]
    hu[0], hu[-1], hu[-1], hu[0]
    hu[0] = hu[0] +\
        np.sin(t * np.pi / 1.0) +\
        0.3 * np.sin(3 * t * np.pi) +\
        0.1 * np.sin(t * np.pi / 6.0)
    return [h] + [hu]



def BCperiodic_external_forcing_PA(h, hu, t, period, amplitude):
    h[0], h[-1] = h[-1], h[0]
    hu[0], hu[-1], hu[-1], hu[0]
    for A, per in zip(amplitude, period):
        hu[0] += A * (0 + np.sin(t * (2 * np.pi) / per))
    return [h] + [hu]


period = np.asarray([2.0,
                     2.0 / 3.0,
                     12.0])
amplitude = np.asarray([1.0,
                        0.3,
                        1.0])

h0 = lambda x: 5 * np.ones_like(x) + 10 * np.exp(-(x - 200)**2 / 50) + 1 * np.cos(3 * 2 * np.pi * x / D[1])

BC = lambda h, q, t: BCperiodic_external_forcing_PA(h, q, t, [10.0, 5.0, 1.0], [2.0, 0.0, 0.0])
# BC2 = lambda h, q, t: BCperiodic_external_forcing_PA(h, q, t, [10.0, 5.0, 1.0], [2.0, 1.0, 0.0])

BC_U = lambda h, q, t, U: BCperiodic_external_forcing_PA(h, q, t, [10.0, 6.0, 1.0], [2.0, U, 0.0])
# BC_U = lambda h, q, t, U: BCperiodic_external_forcing_PA(h, q, t, [10.0, 5.0, 1.0], [2.0, U, 0.0])

ref = sw.ShallowWaterSimulation(T=T, dt=dt,
                                D=D, N=N, b=bathy, K=Kref,
                                h0=h0(xr),
                                bcL=BC, periodic=True, external_forcing=None,
                                numflux=rusanov_flux)
ref2 = sw.ShallowWaterSimulation(T=T, dt=dt,
                                 D=D, N=N, b=bathy, K=Kref,
                                 h0=h0(xr),
                                 bcL=lambda h, q, t: BC_U(h, q, t, 0.5),
                                 periodic=True, external_forcing=None,
                                 numflux=rusanov_flux)

_ = ref.direct_simulation()
_ = ref2.direct_simulation()
BC_rst = lambda h, q, t: BC_U(h, q, t, 0.5)
A = ref.continue_simulation_par(Kref, BC_rst, ref.T + 200, tstart=ref.T)
B = ref.continue_simulation_par(Kref, BC, ref.T + 20, tstart=ref.T)

anim.animate_SWE(xr, [htruepred], bathy, D, ylim=[3, 6])

_ = ref2.direct_simulation()
anim.animate_SWE(xr, [ref.ssh, ref2.ssh], bathy, D, ylim=[0, 20])


model = sw.CostSWE(ref, BC_U)
fun_eval = lambda k, u: model.J_KU(np.asarray([k, u]).T, parallel=True, adj_gradient=False)
def fun2(ku):
    ku = np.atleast_2d(ku)
    return fun_eval(ku[:, 0], ku[:, 1])

BCtrue = lambda h, q, t, U: BCperiodic_external_forcing_PA(h, q, t, [10.0, 5.0, 1.0], [2.0, U, 0.0])
start = time.time()
BB = model.comparison_predictions((Kref,
                                   [0.1],
                                   [0.2]),
                                  np.linspace(0, 1, 3),
                                  np.linspace(0, 1, 3),
                                  Tpred=200, BCtrue=BCtrue, parallel=False)
print(time.time() - start)

swRO = ROproblem.ROproblem('SWE', 1, 1, fun_eval)
swRO.set_indicesK([0])
k_ax = np.linspace(0.20, 0.46, 5)
u_ax = np.linspace(0.0, 1.0, 5)
swRO.set_base_vectors(k_ax, u_ax)
swRO.set_meshgrid()
swRO.compute_grid()
gp = create_and_fit_gp(swRO)

k_b, u_b = np.linspace(0.20, 0.46, 50), np.linspace(0, 1, 50)
comb = np.asarray(np.meshgrid(k_b, u_b)).T.reshape(-1, 2)

sys.path.append("/home/victor/RO_VT/RO/")
from bo_wrapper import PEI_algo
import bo_plot as bplt
bplt.plot2d_gp(gpPEI, 50)
plt.show()
bounds = [[0.2, 0.46], [0, 1.0]]
gpPEI = PEI_algo(gp, fun2, idx_U=[1], X_=comb, niterations=10, bounds=np.asarray(bounds),
                 nrestart=5)


with open('/home/victor/These/source_code/ROproblem/swe_periodic_misfit.obj', 'r') as fil:
    swRO = pickle.load(fil)

swRO.compute_moments_optimisers()
swRO.plot2D(True)
swRO.alpha_quantile(p=[1.0, 0.95, 0.99, 0.90], exact_minimisers=None)
swRO.plot_profiles()
swRO.compute_maxratio()


def pred_error(Karray, u, utrue, Tpred):
    cost_pred = []
    for utrue_ in utrue:
        BCtrue = lambda h, q, t: BCperiodic_external_forcing_PA(h, q, t,
                                                                [10.0, 5.0, 1.0],
                                                                [2.0, utrue_, 0.0])

        _, htruepred, _, _ = ref.continue_simulation_par(Karray=Kref,
                                                         bcL=BCtrue,
                                                         T2=ref.T + 100,
                                                         tstart=ref.T)
        # valid for 1D estimates
        for u_sim in u:
            to_append = [utrue_, u_sim]
            for k in Karray:
                Ka = np.array(map(sw.interp(k, D), ref.xr))
                BC_U = lambda h, q, t: BCperiodic_external_forcing_PA(h, q, t,
                                                                      [10.0, 6.0, 1.0],
                                                                      [2.0, u_sim, 0.0])

                _, hpred, _, _ = ref.continue_simulation_par(Karray=np.asarray(Ka),
                                                             bcL=BC_U,
                                                             T2=ref.T + 100,
                                                             tstart=ref.T)
                to_append.append((k, np.sum((hpred - htruepred)**2)))
            cost_pred.append(to_append)
        print('utrue_ finished')
    return np.asarray(cost_pred)

sample_size = 30
import pyDOE
u_true_all = pyDOE.lhs(1, samples=1, iterations=1, criterion='m')
u_sim_all = pyDOE.lhs(1, samples=sample_size, iterations=20, criterion='m')

np.random.random(size=sample_size) * 0.2 + 0.2
# u2 = np.random.random(size=sample_size) * 0.2 + 0.2

cost_pred = pred_error((Kref,
                        [swRO.ROestimates['minE']],
                        [swRO.ROestimates['minV']],
                        [swRO.ROestimates['p100']],
                        [swRO.ROestimates['pratio100']]),
                       u_sim_all, np.linspace(0.2, 0.4, 3), 100)


def separe_cost_pred(cost_pred, ind, Nsim, Ntrue):
    to_return = np.asarray([cost_pred[:, ind][i][1] for i in xrange(Nsim * Ntrue)])
    return to_return.reshape(Ntrue, Nsim)
    
# np.save('/home/victor/These/source_code/ROproblem/swe_periodic_misfit_pred.npy', cost_pred)

cost_pred_ref = separe_cost_pred(cost_pred, 2, 30, 3)
cost_pred_E = separe_cost_pred(cost_pred, 3, 30, 3)
cost_pred_V = separe_cost_pred(cost_pred, 4, 30, 3)
cost_pred_p100 = separe_cost_pred(cost_pred, 5, 30, 3)
cost_pred_prat = separe_cost_pred(cost_pred, 6, 30, 3)
cost_pred_periodic = {'ref': cost_pred_ref,
                      'minE': cost_pred_E,
                      'minV': cost_pred_V,
                      'p100': cost_pred_p100,
                      'prat': cost_pred_prat}
np.save('/home/victor/These/source_code/ROproblem/swe_periodic_misfit_pred_dict.npy', cost_pred_periodic)
for lab, cc in cost_pred_periodic.iteritems():
    plt.plot(np.linspace(0.2, 0.4, 3), cc.mean(1), label=lab)
plt.legend()
plt.show()

# with open('/home/victor/These/source_code/ROproblem/swe_periodic_misfit.obj', 'w') as fil:
#     pickle.dump(swRO, fil)


print('\n\n\n-----------------------------\n END OF MESHGRID COMPUTATION\n-----------------------------\n\n\n')

result_optim = swRO.compute_conditional_optimisers_exact([1.0], Nrestart=1)
# np.save('/home/victor/These/source_code/ROproblem/swe_periodic_misfit_exact_optim.npy', result_optim)

result_optim = np.load('/home/victor/These/source_code/ROproblem/swe_periodic_misfit_exact_optim.npy', allow_pickle=True)
swRO.conditional_optimisers = result_optim
u_opt = np.asarray([re[0] for re in result_optim]).squeeze()
kstar = np.asarray([re[1] for re in result_optim]).squeeze()
Jstar = np.asarray([re[2] for re in result_optim]).squeeze()
# plt.plot(kstar, u_opt); plt.show()
# plt.plot(kstar, Jstar); plt.show()
# plt.plot(u_opt, Jstar); plt.show()
# swRO.alpha_quantile(p=[1.0, 0.95, 0.99, 0.90], exact_minimisers = Jstar)
import matplotlib.tri as tri
KK = np.append(swRO.meshgrid_to_comb()[:, 0], kstar)
UU = np.append(swRO.meshgrid_to_comb()[:, 1], u_opt)
JJ = np.append(swRO.values_on_grid.reshape(-1), Jstar)
triang = tri.Triangulation(KK, UU)
interpolator = tri.LinearTriInterpolator(triang, JJ)
k_plt = np.linspace(0.24, 0.3, 100)
u_plt = np.linspace(0.0, 1.0, 100)
k_plt, u_plt = np.meshgrid(k_plt, u_plt)
Jinterpol = interpolator(k_plt, u_plt)
plt.contourf(k_plt, u_plt, Jinterpol)
plt.plot(kstar, u_opt, 'r.')
plt.show()

sys.path.append('/home/victor/RO_VT/RO/bo_wrapper.py')
import bo_wrapper as bovt
# def fun(self, k):
#     arg = np.empty(self.totaldim)
#     res = np.empty(len(k))
#     for i, k_ in enumerate(k):
#         arg[self.indicesK] = [k_]
#         arg[self.indicesU] = [u]
#         arg = tuple([[a] for a in arg])
#         res[i] = self.function(*arg)
#     return res

gp = GaussianProcessRegressor(kernel=Matern(np.ones(2) / 2.0), n_restarts_optimizer = 50)





utrue = np.linspace(0, 1, 3**5)
forecast = []
for i, utrue_ in enumerate(utrue):
    # BCtrue = lambda h, q, t: BCperiodic_external_forcing_PA(h, q, t,
    #                                                         [10.0, 5.0, 1.0],
    #                                                         [2.0, utrue_, 0.0])

    # _, htruepred, _, _ = ref.continue_simulation_par(Karray=Kref,
    #                                                  bcL=BCtrue,
    #                                                  T2=ref.T + 100,
    #                                                  tstart=ref.T)

    BC_U = lambda h, q, t: BCperiodic_external_forcing_PA(h, q, t,
                                                          [10.0, 6.0, 1.0],
                                                          [2.0, utrue_, 0.0])

    _, hpred, _, _ = ref.continue_simulation_par(Karray=np.asarray(swRO.ROestimates['minE']),
                                                 bcL=BC_U,
                                                 T2=ref.T + 100,
                                                 tstart=ref.T)
    if i % 10 == 0:
        print i
    forecast.append([hpred])
np.save('/home/victor/These/source_code/ROproblem/forecast_ensemble_sim_minE.npy',
        np.asarray(forecast))


utrue = np.linspace(0, 1, 3**5)
forecast = []
for i, utrue_ in enumerate(utrue):
    # BCtrue = lambda h, q, t: BCperiodic_external_forcing_PA(h, q, t,
    #                                                         [10.0, 5.0, 1.0],
    #                                                         [2.0, utrue_, 0.0])

    # _, htruepred, _, _ = ref.continue_simulation_par(Karray=Kref,
    #                                                  bcL=BCtrue,
    #                                                  T2=ref.T + 100,
    #                                                  tstart=ref.T)

    BC_U = lambda h, q, t: BCperiodic_external_forcing_PA(h, q, t,
                                                          [10.0, 6.0, 1.0],
                                                          [2.0, utrue_, 0.0])

    _, hpred, _, _ = ref.continue_simulation_par(Karray=np.asarray(swRO.ROestimates['p100']),
                                                 bcL=BC_U,
                                                 T2=ref.T + 100,
                                                 tstart=ref.T)
    if i % 10 == 0:
        print i
    forecast.append([hpred])
np.save('/home/victor/These/source_code/ROproblem/forecast_ensemble_sim_p100.npy',
        np.asarray(forecast))


def pinball_loss(target, y, forecast):
    difference = y - forecast
    bool_ = difference < 0
    return difference * target - difference * bool_

for tau in [0.0, 0.2, 0.5, 0.8, 1.0]:
    plt.plot(np.linspace(-2, 2, 100), pinball_loss(tau, np.linspace(-2, 2, 100), 0))
plt.show()


def quantile_score(forecast, observation, n_quantiles):
    tau_target = np.linspace(0.0, 1.0, n_quantiles)
    pin_loss = np.empty([len(tau_target), 10002])
    for i, tau in enumerate(tau_target):
        obs_q = np.quantile(observation, tau, 0)
        for_q = np.quantile(forecast, tau, 0)
        obs_q.shape, for_q.shape
        pin_loss[i, :] = pinball_loss(tau, obs_q, for_q)
    return pin_loss



def get_stations(forecast, obs):
    stations = np.empty([len(obs), len(utrue), 75001])
    for i, fore in enumerate(forecast):
        for j, ob in enumerate(obs):
            stations[j, i, :] = fore[0][ob, :]
    return tuple(stations)

observations = np.load('/home/victor/These/source_code/ROproblem/forecast_ensemble.npy')

obs_tuple = get_stations(observations, [10, 50, 100, 150, 190])

forecast = np.load('/home/victor/These/source_code/ROproblem/forecast_ensemble_sim_p100.npy')
fore_tuple = get_stations(forecast, [10, 50, 100, 150, 190])

# qs1 = quantile_score(sta1, obs1, 20)
# qs2 = quantile_score(sta2, obs2, 20)
# qs3 = quantile_score(sta3, obs3, 20)
# qs4 = quantile_score(sta4, obs4, 20)
# qs5 = quantile_score(sta5, obs5, 20)


def qscore_by_station(observations, forecast, obs_stations=range(1, 199)):
    fore_tuple = get_stations(forecast, obs_stations)
    obs_tuple = get_stations(observations, obs_stations)
    qs = np.empty([len(obs_stations), fore_tuple[0].shape[1]])
    for i in xrange(len(obs_stations)):
        qs[i, :] = quantile_score(fore_tuple[i], obs_tuple[i], 20).mean(0)
    return qs


prefix = '/home/victor/These/source_code/ROproblem/forecast_ensemble_'
suffix_forecast = ['sim', 'sim_minE', 'sim_p100']
legend = ['ref', 'minE', 'p100']
qs = dict()
for i in xrange(1, 3):
    filename = prefix + suffix_forecast[i] + '.npy'
    forecast = np.load(filename)
    qs[legend[i]] = qscore_by_station(observations, forecast)
    plt.plot(qs[legend[i]].mean(1), label=legend[i])
    print('{} -> {}'.format(legend[i], qs[legend[i]].mean()))
plt.legend()
plt.show()

    
plt.subplot(5, 1, 1)
plt.plot(qs1.mean(0))
plt.subplot(5, 1, 2)
plt.plot(qs2.mean(0))
plt.subplot(5, 1, 3)
plt.plot(qs3.mean(0))
plt.subplot(5, 1, 4)
plt.plot(qs4.mean(0))
plt.subplot(5, 1, 5)
plt.plot(qs5.mean(0))
plt.show()

plt.plot([qs1.mean(), qs2.mean(), qs3.mean(), qs4.mean(), qs5.mean()])
plt.show()
np.mean([qs1.mean(), qs2.mean(), qs3.mean(), qs4.mean(), qs5.mean()])




obs = [10, 100, 190]
stations = np.empty([len(obs), len(utrue), 10002])


for i, fore in enumerate(forecast):
    for j, ob in enumerate(obs):
        stations[j, i, :] = fore[0][ob, :10002]

plt.subplot(3, 1, 1)
for i in xrange(len(utrue)):
    plt.plot(stations[0, i, :], color='k', alpha = 0.01)
plt.subplot(3, 1, 2)
for i in xrange(len(utrue)):
    plt.plot(stations[1, i, :], color='k', alpha = 0.01)
plt.subplot(3, 1, 3)
for i in xrange(len(utrue)):
    plt.plot(stations[2, i, :], color='k', alpha = 0.01)
plt.show()

























# -- NEW MODEL -------------------------------------------------------------------
