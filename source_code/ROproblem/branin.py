#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from ROproblem import ROproblem
from horsetailmatching import HorsetailMatching, UniformParameter
import scipy.optimize as scopt

#  Fun definition ----------------------------------------------------------------------


def branin_hoo(x, y):
    x1 = 3 * x - 5
    x2 = 3 * y
    damp = 1.0
    quad = (x2 - (5.1 / (4 * np.pi**2)) * x1**2 + (5 / np.pi) * x1 - 6)**2
    cosi = (10 - (10 / np.pi * 8)) * np.cos(x1) - 44.81
    return (quad + cosi) / (51.95 * damp) + 2.0


Npts = 1000

branin = ROproblem.ROproblem(label='branin', Kdim=1, Udim=1,
                             function=branin_hoo)
branin.set_indicesK([1])
x0 = np.linspace(0, 5, Npts)
# from scipy.stats import norm
# x1 = np.sort(norm.rvs(loc=2.5, scale=1, size=Npts))
x1 = np.linspace(0, 5, Npts)
branin.set_base_vectors(x1, x0)
branin.set_meshgrid()
branin.compute_grid()

branin.compute_moments_optimisers()
branin.compute_MPE()
branin.summary()
# branin.alpha_by_dichotomy(p=[1, .95], Nitermax=20)
branin.alpha_quantile(p=[1, .95, .90])
branin.compute_maxratio()
aa = branin.compute_similarity_alpha(np.linspace(0, 2.5, 100))
branin.style_options['p90']['ls'] = ':'
branin.plot2D(semi=False)
branin.estimates2latex()
fig, _, _ = branin.plot_profiles(contour=True, relative_pos_minifig=0.30, x_end_arrow=0.2,
                                 return_fig=True)
fig.tight_layout()
plt.show()
fig.savefig('/home/victor/acadwriting/Article/NPG1/Figures/prog')
# branin.plot_evolution_wrt_p(plttype='lese', rev=True)
# branin.plot_ecdf()


fig, ax_pro, _ = branin.plot_profiles(figsize=(8, 6),
                                      contour=True,
                                      relative_pos_minifig=0.4 if branin.indicesK == [0] else 0.33,
                                      x_end_arrow=0.2 if branin.indicesK == [0] else 2.7,
                                      return_fig=True)
ax_pro.set_title((r'Profiles $\mathbf{u}\mapsto J(\hat{\mathbf{k}},\mathbf{u})$ '
                  r'for different estimates $\hat{\mathbf{k}}$'))
fig.tight_layout()
# fig.savefig('/home/victor/acadwriting/Article/NPG1/Figures/profile_BHs_all_estimates_noku.pdf')
plt.show()
# branin.plot2D(front_alpha_1=False)

# branin.plot_ecdf()
branin.plot_evolution_wrt_p(figsize=(8, 4))
plt.tight_layout()
# plt.savefig('/home/victor/PhD/Article/NPG1/Figures/alpha_p_BH.pdf')
plt.show()

branin.plot2D()









# HM ----------------------------------------------------------------------
u = UniformParameter(lower_bound=0, upper_bound=5)


def target_0(x):
    return 0


mini = branin.values_on_grid.min()


def target_scalmin(x):
    return mini


ecdf_Jstar = branin.compute_ecdf_optimisers()

Jstar = branin.compute_conditional_optimum()


def target_Jstar(x):
    return np.quantile(Jstar, x)
# ecdf_Jstar(x)


theHM = HorsetailMatching(branin_hoo, [u], ftarget=target_Jstar, samples_prob=5000)


Nrestart = 20
best_score = np.inf
for x0 in np.random.uniform(0, 5, Nrestart):
    print best_score
    Horsetailsolution = scopt.minimize(theHM.evalMetric, x0=x0)
    if Horsetailsolution.fun < best_score:
        best_tmp = Horsetailsolution
        best_score = best_tmp.fun

Horsetailsolution = best_tmp

branin.add_ROestimate(Horsetailsolution.x, label='HM',
                      style_dict = {'color': 'c',
                                    'ls': '-.',
                                    'leg': r'$\hat{\mathbf{k}}_{\mathrm{HM}}$'})

branin.summary()
theHM.evalMetric(Horsetailsolution.x)
(x1, y1, t1), (x2, y2, t2), _ = theHM.getHorsetail()
plt.plot(x1, y1, 'b', label='CDFstar')
plt.plot(t1, y1, 'k--', label='Target')
theHM.evalMetric(1.75175175)
(x1, y1, t1), (x2, y2, t2), _ = theHM.getHorsetail()
plt.plot(x1, y1, 'm', label='CDFmean')
plt.xlim([-1, 6])
plt.xlabel('Quantity of Interest')
plt.legend(loc='lower right')
plt.show()

branin.plot_profiles()
