#!/usr/bin/env python
# -*- coding: utf-8 -*-

## Lorenz 63 ---------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from ROproblem import ROproblem
from lorenz.lorenz import simulation, plot_spread, cost_xb


# Test with loaded data ------------------------------------------------

data_lorenz = np.load('/home/victor/These/source_code/lorenz/xb_y_fun_2000_allobs.npy')
# data_lorenz = np.load('/home/victor/These/lorenz_grid_nooffset.npy') + 2.0
lorenz = ROproblem.ROproblem(label='lorenz', Kdim=1, Udim=1,
                             function=None)
lorenz.set_indicesK([0])
Npts = 2000
xx = np.linspace(-0.05, 0.05, Npts)
lorenz.set_base_vectors(xx, xx)
lorenz.set_meshgrid()
lorenz.set_function(cost_xb)
lorenz.set_evaluated_grid(data_lorenz)
lorenz.compute_moments_optimisers()
lorenz.compute_MPE()
lorenz.summary()
# lorenz.alpha_by_dichotomy(p=[1], alpha_low=8, alpha_up=12)
lorenz.alpha_quantile(p=[1, 0.95, 0.90])
lorenz.plot2D(front_alpha_1=True, fill=False)
lorenz.compute_maxratio()

## Visuals ---------------

fig, ax_pro = lorenz.plot_profiles(contour=False, return_fig=True)




ax_pro.set_title((r'Profiles $\mathbf{u} \mapsto J(\hat{\mathbf{k}},\mathbf{u})$ '
                  r'for different estimates $\hat{\mathbf{k}}$ for the Lorenz model'))
fig.tight_layout()
# fig.savefig('/home/victor/acadwriting/Article/NPG1/Figures/profiles_lorenz_all_estimates_nooffset.pdf')
plt.show()


## ----

Nsim = 2000
alpha = 0.01
plt.subplot(3, 2, 1)
lasts = plot_spread(lorenz.ROestimates['minE'],
                    lambda: np.random.uniform(-0.05, 0.05), Nsim, alpha=alpha)
plt.title('minE')

plt.subplot(3, 2, 2)
lasts = plot_spread(lorenz.ROestimates['minV'],
                    lambda: np.random.uniform(-0.05, 0.05), Nsim, alpha=alpha)

plt.title('minV')
plt.subplot(3, 2, 3)
lasts = plot_spread(lorenz.ROestimates['MPE'],
                    lambda: np.random.uniform(-0.05, 0.05), Nsim, alpha=alpha)
plt.title('MPE')

plt.subplot(3, 2, 4)
lasts = plot_spread(lorenz.ROestimates['p100'],
                    lambda: np.random.uniform(-0.05, 0.05), Nsim, alpha=alpha)
plt.title('p100')

plt.subplot(3, 2, 5)
lasts = plot_spread(lorenz.ROestimates['pratio52'],
                    lambda: np.random.uniform(-0.05, 0.05), Nsim, alpha=alpha)
plt.title('pratio')

plt.subplot(3, 2, 6)
lasts = plot_spread(lorenz.ROestimates['p90'],
                    lambda: np.random.uniform(-0.05, 0.05), Nsim, alpha=alpha)
plt.title('p90')
plt.show()


Npred = 100

# simulation(
