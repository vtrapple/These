#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  Application of ROproblem on SWE case

import numpy as np
import sys
sys.path.append('/home/victor/These/source_code/ROproblem/')
import pickle
import matplotlib.pyplot as plt
from ROproblem import ROproblem
from SWE_VT import shallowwater as sw
from SWE_VT import animation_SWE as anim
from SWE_VT.numerics.numerical_flux import LF_flux, rusanov_flux
from SWE_VT.numerics.boundary_conditions import (bcR,
                                                 bcL_A, bcR_A,
                                                 BCrand,
                                                 BCperiodic)
from SWE_VT.numerics.interpolation_tools import interp
from SWE_VT.cost import cost_observations as cst




# -- NEW MODEL -------------------------------------------------------------------

D = [0, 10]
N = 100  # Nombre de volumes
dx = np.diff(D)[0] / float(N)  # Largeur des volumes
xr = np.linspace(D[0] + dx / 2, D[1] - dx / 2, N)  # Milieux des volumes
T = 50
Kref = 0.2 * (1 + np.sin(2 * np.pi * xr / D[1]))
dt = 0.002
b = lambda x: 10 * x / D[1] + 0.5 * np.cos(x / 3) + 3 / (1 + np.exp(-3 * (x - 50)))


def b(x):
    peakdomain = 2 * (D[1] - D[0]) / 3.0
    return 10 * x / D[1] + 5 * np.exp(-(x - peakdomain)**2 / 9)


h0 = lambda x: 20.0 * np.ones_like(x)


def BC(h, hu, t, mean_h, amplitude_vector, fundperiod, phase):
    """ Conditions aux limites du modele direct, avec plus de paramètres"""
    h[0] = mean_h
    period = fundperiod
    for amp in amplitude_vector:
        h[0] += amp * np.sin((t * (2 * np.pi) / period) + phase)
        period /= 2.0
    hu[0] = 0.0
    return [h] + [hu]


# def BC_(h, q, t):
#     h[0] = 20 + 3 * np.sin((2 * np.pi * t) / 5.0) + 0.5 * np.sin((2 * np.pi * t) / 1.5)
#     q[0] = q[1]
#     return [h] + [q]


def BC_(h, q, t):
    h[0] = 20 + 3 * np.sin((2 * np.pi * t) / 2.0) + 1 * np.sin((2 * np.pi * t) / 1.0)
    q[0] = q[1]
    return [h] + [q]


# def BC_U(h, q, t, U):
#     h[0] = 20 + 3 * np.sin((2 * np.pi * t) / 5.0) + 0.8 * U * np.sin((2 * np.pi * t) / 1.5)
#     q[0] = q[1]
#     return [h] + [q]


def BC_U(h, q, t, U):
    h[0] = 20 + 3 * np.sin((2 * np.pi * t) / 2.0) + 1.5 * U * np.sin((2 * np.pi * t) / 1.0)
    q[0] = q[1]
    return [h] + [q]


ref = sw.ShallowWaterSimulation(T=T, b=b,
                                K=Kref, N=N, D=D,
                                dt=dt, h0=h0,
                                bcL=BC_,
                                numflux=rusanov_flux,
                                periodic=False,
                                idx_observation=None)

testU = sw.ShallowWaterSimulation(T=T, b=b,
                                  K=Kref, N=N, D=D,
                                  dt=dt, h0=h0,
                                  bcL=lambda h, q, t: BC_U(h, q, t, 0.5),
                                  numflux=rusanov_flux,
                                  periodic=False,
                                  idx_observation=None)

_ = ref.direct_simulation()
# _ = testU.direct_simulation()
# anim.animate_SWE(xr, [ref.ssh, testU.ssh], b, D, ylim=[0, 50])
model = sw.CostSWE(ref, BC_U)
fun_eval = lambda k, u: model.J_KU(np.asarray([k, u]).T, parallel=True, adj_gradient=False)
del swRO
swRO = ROproblem.ROproblem('SWE', 1, 1, fun_eval)
swRO.set_indicesK([0])
k_ax = np.linspace(0.0, 2.0, 2**5)
u_ax = np.linspace(0.5, 0.7, 2**5)
swRO.set_base_vectors(k_ax, u_ax)
swRO.set_meshgrid()
swRO.compute_grid()
swRO.compute_moments_optimisers()
swRO.plot2D()


# with open('/home/victor/These/source_code/ROproblem/swe_tide_misfit_new.obj', 'w') as fil:
#     pickle.dump(swRO, fil)

with open('/home/victor/These/source_code/ROproblem/swe_tide_misfit_new.obj', 'r') as fil:
    swRO = pickle.load(fil)

print('\n\n\n-----------------------------\n END OF MESHGRID COMPUTATION\n-----------------------------\n\n\n')

result_optim = swRO.compute_conditional_optimisers_exact(u_ax, Nrestart=1)
# np.load('/home/victor/These/source_code/ROproblem/swe_tide_new_optim.npy')
result_optim = np.load('/home/victor/These/source_code/ROproblem/swe_tide_new_optim.npy',
                       allow_pickle=True)
swRO.conditional_optimisers = result_optim
u_opt = np.asarray([re[0] for re in result_optim]).squeeze()
kstar = np.asarray([re[1] for re in result_optim]).squeeze()
Jstar = np.asarray([re[2] for re in result_optim]).squeeze()
swRO.alpha_quantile(p=[1.0, 0.95, 0.99, 0.90], exact_minimisers=None)
swRO.plot2D(True)

import matplotlib.tri as tri
KK = np.append(swRO.meshgrid_to_comb()[:, 0], kstar)
UU = np.append(swRO.meshgrid_to_comb()[:, 1], u_opt)
JJ = np.append(swRO.values_on_grid.reshape(-1), Jstar)
triang = tri.Triangulation(KK, UU)
interpolator = tri.LinearTriInterpolator(triang, JJ)
k_plt = np.linspace(0.0, 2.0, 100)
u_plt = np.linspace(0.5, 0.7,100)
k_plt, u_plt = np.meshgrid(k_plt, u_plt)
Jinterpol = interpolator(k_plt, u_plt)
plt.contourf(k_plt, u_plt, Jinterpol)
plt.plot(kstar, u_opt, 'r.')
plt.show()
## BO APPLICATION
import RO.bo_wrapper as bovt
import RO.bo_plot as boplt
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Matern

gp = swRO.create_and_fit_gp()


def fun_vec_input(ku):
    ku = np.atleast_2d(ku)
    return swRO.function(ku[:, 0], ku[:, 1])


def fun_vec_input_fixed(ku):
    ku = np.atleast_2d(ku)
    return fun_eval(ku[:, 0], (2. / 3.) * np.ones_like(ku))


X_ = np.asarray(np.meshgrid(np.linspace(0.0, 2.0, 30),
                            np.linspace(0.5, 0.7, 30))).T.reshape(-1, 2)
gp2 = bovt.PEI_algo(gp, fun_vec_input, idx_U = [1],
                    niterations=20,
                    plot=False,
                    X_=X_,
                    nrestart=2,
                    bounds=np.array([[0.0, 2.0],
                                     [0.5, 0.7]]))
boplt.plot2d_gp(gp2, 50)
plt.show()

import scipy
scipy.optimize.minimize(fun_vec_input,
                        x0=[0.15220, 2. / 3.],
                        bounds=np.asarray([[0.0, 2.0],
                                           [.5, .7]]))
global_minimizer = np.array([0.17338479, 0.66157518])
single_minimizer = np.array([0.15214459, 2.0 / 3.0])

with open('/home/victor/These/source_code/ROproblem/gp_PEI.obj', 'r') as fil:
    gpPEI = pickle.load(fil)


if 'meta' in locals():
    del meta
meta = ROproblem.ROproblem('metaSWE', 1, 1, None)
meta.set_indicesK([0])
k_ax_meta = np.linspace(0.0, 1.3, 2**9)
u_ax_meta = np.linspace(0.5, 0.7, 2**9)
to_pred = np.asarray(np.meshgrid(k_ax_meta, u_ax_meta)).T.reshape(-1, 2)
meta.set_base_vectors(k_ax_meta, u_ax_meta)
meta.set_meshgrid()
meta.set_evaluated_grid(gpPEI.predict(to_pred).reshape(2**9, 2**9))
meta.compute_moments_optimisers()
meta.alpha_quantile(p=[1.0, 0.90, 0.80, 0.70], exact_minimisers=None)
meta.plot2D(True)
meta.plot_profiles()
meta.plot_evolution_wrt_p(alpha_up=1.6)


import seaborn as sns


def plot_nice(self=meta, front_alpha_1=True, alpha=None, figsize=(10,10), fill=False):
    if front_alpha_1:
        alpha = self.alpha_p[:, 1].max()
    if alpha is not None:
        bool_array = self._return_bool_array_optimisers(alpha)
    k_mg, u_mg = self.meshgrid[self.indicesK[0]], self.meshgrid[self.indicesU[0]]
    k_axis = self.base_vectors[self.indicesK[0]]
    u_axis = self.base_vectors[self.indicesU[0]]
    k_star_i, u_i = self.compute_conditional_optimisers(indices=True)
    k_star_i = k_star_i.squeeze()[np.argsort(u_i.squeeze())]
    u_i = np.sort(u_i.squeeze())
    c_mean, c_std = self.compute_moments(std=True)
    fig = plt.figure(figsize=figsize)
    ax_contour = fig.add_subplot(3, 1, 1)
    ax_contour.contourf(k_mg, u_mg, self.values_on_grid, 10)
    ax_contour.set_xlabel(r'$\mathbf{k}$')
    ax_contour.set_ylabel(r'$\mathbf{u}$')
    ax_contour.set_title(r'Contours of J')
    if alpha is not None:
        if fill:
            ax_contour.contourf(k_mg, u_mg, bool_array, 2)
        else:
            ax_contour.contour(k_mg, u_mg, bool_array, 2)
    ax_contour.plot(k_axis[k_star_i.squeeze()],
                    u_axis[u_i.squeeze()], 'r.',
                    label=r'$\mathbf{k}^*(\mathbf{u})$')
    ax_contour.plot(np.nan, np.nan, color=(0.993248, 0.906157, 0.143936),
                    label=r'$\{J(\mathbf{k},\mathbf{u}) = \hat{\alpha}_1 J^*(\mathbf{u}) \}$')
    ax_contour.legend()
    ax_mean = fig.add_subplot(3, 1, 2)
    ax_mean.set_xlabel(r'$\mathbf{k}$')
    if alpha is not None:
        if fill:
            ax_contour.contourf(k_mg, u_mg, bool_array, 2)
        else:
            ax_contour.contour(k_mg, u_mg, bool_array, 2)
    ax_contour.plot(k_axis[k_star_i.squeeze()],
                    u_axis[u_i.squeeze()], 'r.')
    ax_mean.set_yticks([])
    ax_mean.set_title(r'Conditional moments and conditional minimizers')
    ax_std = ax_mean.twinx()
    ax_mean.plot(k_axis, c_mean.squeeze(),
                 color=self.style_options['minE']['color'],
                 ls=self.style_options['minE']['ls'],
                 label=r'$\mu(\mathbf{k})$')
    ax_std.plot(k_axis, c_std.squeeze(),
                color=self.style_options['minV']['color'],
                ls=self.style_options['minV']['ls'])
    ax_mean.plot(np.nan, np.nan, color=self.style_options['minV']['color'],
                 ls=self.style_options['minV']['ls'],
                 label=r'$\sigma(\mathbf{k})$')
    ax_sns = ax_mean.twinx()
    ax_sns = sns.distplot(k_axis[k_star_i],
                          hist_kws={'color': 'gray', 'alpha': .35},
                          kde_kws={'color': 'blue', 'bw': 'silverman'})
    ax_sns.set_yticks([])
    ax_mean.plot(np.nan, np.nan, color='blue',
                 label=r'KDE estimation of $\mathbf{K}^*$')
    ax_mean.set_ylabel('Mean and standard deviation')
    ax_mean.legend(loc=1)
    ax_mean.annotate(r'$\hat{\mathbf{k}}_{\mathrm{mean}}$',
                     (float(self.ROestimates['minE']),
                      0.017),
                     xytext=(float(self.ROestimates['minE']) + 0.04,
                             0.017),
                     arrowprops=dict(arrowstyle="->"))
    ax_mean.annotate(r'$\hat{\mathbf{k}}_{\mathrm{var}}$',
                     (float(self.ROestimates['minV']),
                      0.017),
                     xytext=(float(self.ROestimates['minV']) + 0.04,
                             0.017),
                     arrowprops=dict(arrowstyle="->"))
    for lab in ['minE', 'minV']:
        ax_mean.axvline(self.ROestimates[lab], color=self.style_options[lab]['color'])
        ax_contour.axvline(self.ROestimates[lab], color=self.style_options[lab]['color'])
    ax_mean.set_xlim(k_axis.min(), k_axis.max())
    ax_R = fig.add_subplot(3, 1, 3)
    if alpha is not None:
        R_k = self.compute_probability_alpha(alpha)
        R_9 = self.compute_probability_alpha(self.alpha_p[1, 1])
        R_8 = self.compute_probability_alpha(self.alpha_p[2, 1])
        R_7 = self.compute_probability_alpha(self.alpha_p[3, 1])
        ax_R.set_ylabel(r'$\hat{\Gamma}_{\alpha}(\mathbf{k})$')
        ax_R.plot(k_axis, R_k, color=self.style_options['Rk']['color'])
        ax_R.annotate(r'$\hat{\mathbf{k}}_1$',
                      (self.ROestimates['p100'], 0.1),
                      (self.ROestimates['p100'] - 0.07, 0.1),
                      arrowprops=dict(arrowstyle="->"))
        ax_R.plot(k_axis, R_9, color=self.style_options['Rk']['color'], alpha=0.8)
        ax_R.annotate(r'$\hat{\mathbf{k}}_{0.9}$',
                      (self.ROestimates['p90'], 0.2),
                      (self.ROestimates['p90'] + 0.03, 0.2),
                      arrowprops=dict(arrowstyle="->"))
        ax_R.plot(k_axis, R_8, color=self.style_options['Rk']['color'], alpha=0.4)
        ax_R.annotate(r'$\hat{\mathbf{k}}_{0.8}$',
                      (self.ROestimates['p80'], 0.1),
                      (self.ROestimates['p80'] + 0.03, 0.1),
                      arrowprops=dict(arrowstyle="->"))
        ax_R.plot(k_axis, R_7, color=self.style_options['Rk']['color'], alpha=0.2)
        ax_R.annotate(r'$\hat{\mathbf{k}}_{0.7}$',
                      (self.ROestimates['p70'], 0.1),
                      (self.ROestimates['p70'] + 0.03, 0.1),
                      arrowprops=dict(arrowstyle="->"))
        ax_R.axhline(.7, color='black', alpha=0.2, ls=':')
        ax_R.axhline(.8, color='black', alpha=0.4, ls=':')
        ax_R.axhline(.9, color='black', alpha=0.8, ls=':')
        ax_R.axhline(1.0, color='black', ls='--')
        ax_R.set_ylim([0.0, 1.1])
        ax_R.set_xlim([0.0, 1.3])
        ax_R.set_xlabel(r'$\mathbf{k}$')
        ax_R.set_title(r'$\hat{\Gamma}_{\alpha_p}$ for $p=0.7, 0.8, 0.9$ and $1$')
        ax_std.set_yticks([])
        for lab in ['p70', 'p80', 'p90', 'p100']:
            ax_R.axvline(self.ROestimates[lab], color=self.style_options[lab]['color'])
            ax_contour.axvline(self.ROestimates[lab], color=self.style_options[lab]['color'])

    plt.tight_layout()

plot_nice(meta, figsize=(8, 10))
plt.savefig('/home/victor/These/source_code/ROproblem/RobustSWE_plots.pdf')
plt.close()












utrue = np.linspace(0.5, 0.7, 2**7)  # 2**8


print('true reference ------------------------------------')
forecast = []
for i, utrue_ in enumerate(utrue):
    # BCtrue = lambda h, q, t: BCperiodic_external_forcing_PA(h, q, t,
    #                                                         [10.0, 5.0, 1.0],
    #                                                         [2.0, utrue_, 0.0])
    BCtrue = lambda h, q, t: BC_U(h, q, t, utrue_)
    _, htruepred, _, _ = ref.continue_simulation_par(Karray=Kref,
                                                     bcL=BCtrue,
                                                     T2=ref.T + 50,
                                                     tstart=ref.T)
    print('{} out of {}'.format(i + 1, len(utrue)))
    forecast.append([htruepred])
# np.save('/home/victor/These/source_code/ROproblem/forecast_tide_ref.npy',
#         np.asarray(forecast))

print('minE ------------------------------------')
forecast = []
for i, utrue_ in enumerate(utrue):
    # BCtrue = lambda h, q, t: BCperiodic_external_forcing_PA(h, q, t,
    #                                                         [10.0, 5.0, 1.0],
    #                                                         [2.0, utrue_, 0.0])
    BCtrue = lambda h, q, t: BC_U(h, q, t, utrue_)
    _, hpred, _, _ = ref.continue_simulation_par(Karray=np.asarray(meta.ROestimates['minE']),
                                                 bcL=BCtrue,
                                                 T2=ref.T + 50,
                                                 tstart=ref.T)
    print('{} out of {}'.format(i + 1, len(utrue)))

    forecast.append([hpred])
# np.save('/home/victor/These/source_code/ROproblem/forecast_tide_minE.npy',
#         np.asarray(forecast))

print('p100 ------------------------------------')
forecast = []
for i, utrue_ in enumerate(utrue):
    # BCtrue = lambda h, q, t: BCperiodic_external_forcing_PA(h, q, t,
    #                                                         [10.0, 5.0, 1.0],
    #                                                         [2.0, utrue_, 0.0])
    BCtrue = lambda h, q, t: BC_U(h, q, t, utrue_)
    _, hpred, _, _ = ref.continue_simulation_par(Karray=np.asarray(meta.ROestimates['p100']),
                                                 bcL=BCtrue,
                                                 T2=ref.T + 50,
                                                 tstart=ref.T)
    print('{} out of {}'.format(i + 1, len(utrue)))
    forecast.append([hpred])
# np.save('/home/victor/These/source_code/ROproblem/forecast_tide_p100.npy',
#         np.asarray(forecast))

print('minV ------------------------------------')
forecast = []
for i, utrue_ in enumerate(utrue):
    # BCtrue = lambda h, q, t: BCperiodic_external_forcing_PA(h, q, t,
    #                                                         [10.0, 5.0, 1.0],
    #                                                         [2.0, utrue_, 0.0])
    BCtrue = lambda h, q, t: BC_U(h, q, t, utrue_)
    _, hpred, _, _ = ref.continue_simulation_par(Karray=np.asarray(meta.ROestimates['minV']),
                                                 bcL=BCtrue,
                                                 T2=ref.T + 50,
                                                 tstart=ref.T)
    print('{} out of {}'.format(i + 1, len(utrue)))
    forecast.append([hpred])
# np.save('/home/victor/These/source_code/ROproblem/forecast_tide_minV.npy',
#         np.asarray(forecast))

print('single ------------------------------------')
forecast = []
for i, utrue_ in enumerate(utrue):
    # BCtrue = lambda h, q, t: BCperiodic_external_forcing_PA(h, q, t,
    #                                                         [10.0, 5.0, 1.0],
    #                                                         [2.0, utrue_, 0.0])
    BCtrue = lambda h, q, t: BC_U(h, q, t, utrue_)
    _, hpred, _, _ = ref.continue_simulation_par(Karray=np.asarray(0.15214459),
                                                 bcL=BCtrue,
                                                 T2=ref.T + 50,
                                                 tstart=ref.T)
    print('{} out of {}'.format(i + 1, len(utrue)))
    forecast.append([hpred])
# np.save('/home/victor/These/source_code/ROproblem/forecast_tide_single.npy',
#         np.asarray(forecast))


print('global ------------------------------------')
forecast = []
for i, utrue_ in enumerate(utrue):
    # BCtrue = lambda h, q, t: BCperiodic_external_forcing_PA(h, q, t,
    #                                                         [10.0, 5.0, 1.0],
    #                                                         [2.0, utrue_, 0.0])
    BCtrue = lambda h, q, t: BC_U(h, q, t, utrue_)
    _, hpred, _, _ = ref.continue_simulation_par(Karray=np.asarray(0.17334144),
                                                 bcL=BCtrue,
                                                 T2=ref.T + 50,
                                                 tstart=ref.T)
    print('{} out of {}'.format(i + 1, len(utrue)))
    forecast.append([hpred])
# np.save('/home/victor/These/source_code/ROproblem/forecast_tide_global.npy',
#         np.asarray(forecast))



observations = np.load('/home/victor/These/source_code/ROproblem/forecast_tide_ref.npy').squeeze()

suffix_list = ['minE', 'p100', 'minV', 'single', 'global']
prefix = '/home/victor/These/source_code/ROproblem/forecast_tide_'
filename = prefix + suffix_list[0] + '.npy'
forecast_minE = np.load(filename).squeeze()
filename = prefix + suffix_list[1] + '.npy'
forecast_p100 = np.load(filename).squeeze()
for i in xrange(128):
    obs = observations[i, 50, 5000:6000]
    fore_E = forecast_minE[i, 50, 5000:6000]
    fore_100 = forecast_p100[i, 50, 5000:6000]
    if i == 0 or i == 127:
        lw = 2
        alpha = 0.5
    else:
        lw = 1
        alpha = 0.01
    plt.plot(obs, color='k', alpha=alpha, lw=lw)
    plt.plot(fore_E, color='b', alpha=alpha, lw=lw)
    plt.plot(fore_100, color='r', alpha=alpha, lw=lw)
plt.show()

suffix_list = ['minE', 'p100', 'minV', 'single', 'global']
prefix = '/home/victor/These/source_code/ROproblem/forecast_tide_'
filename = prefix + suffix_list[0] + '.npy'
forecast = np.load(filename).squeeze()


def quantile_score(forecast, observation, n_quantiles):
    tau_target = np.linspace(0.0, 1.0, n_quantiles)
    pin_loss = np.empty([len(tau_target), forecast.shape[1]])
    for i, tau in enumerate(tau_target):
        obs_q = np.quantile(observation, tau, 0)
        # for_q = np.quantile(forecast, tau, 0)
        pin_loss[i, :] = np.zeros_like(obs_q)
        for fore in forecast:
            pin_loss[i, :] += pinball_loss(tau, obs_q, fore)
        pin_loss[i, :] /= len(forecast)
    return pin_loss


def get_stations(forecast, obs_index_list):
    stations = np.empty([len(obs_index_list), forecast.shape[0], forecast.shape[2]])
    for i, fore in enumerate(forecast):
        for j, ob in enumerate(obs_index_list):
            stations[j, i, :] = fore[ob, :]
    return tuple(stations)


def qscore_by_station(observations, forecast, obs_stations=range(100), nquantiles=20):
    fore_tuple = get_stations(forecast, obs_stations)
    obs_tuple = get_stations(observations, obs_stations)
    qs = np.empty([nquantiles, len(obs_stations), fore_tuple[0].shape[1]])
    for i in xrange(len(obs_stations)):
        qs[:, i, :] = quantile_score(fore_tuple[i], obs_tuple[i], nquantiles)
    return qs



# qs = dict()
plt.subplot(2, 2, 1)
for i in xrange(len(suffix_list)):
    filename = prefix + suffix_list[i] + '.npy'
    forecast = np.load(filename).squeeze()
    qs[suffix_list[i]] = qscore_by_station(observations, forecast, nquantiles=10).mean(0)
    plt.plot(qs[suffix_list[i]].mean(1), label=suffix_list[i])
    print('{} -> {}'.format(suffix_list[i], qs[suffix_list[i]].mean()))
plt.legend()
plt.title(u'Quantile score\n averaged over all time steps')
plt.subplot(2, 2, 2)
plt.title(u'Quantile score')
plt.bar(range(5), np.asarray(qs.values()).mean(2).mean(1))
plt.xticks(range(5), qs.keys())

if 'cost' in locals():
    del cost

cost = np.empty((2**7, 2**7, 100))
# L2 = dict()
# for i in xrange(len(suffix_list)):
#     cost = np.empty((2**7, 2**7, 100))
#     filename = prefix + suffix_list[i] + '.npy'
#     forecast = np.load(filename).squeeze()
#     for k in xrange(2**7):
#         for j in xrange(2**7):
#             cost[k, j, :] = np.sum((observations[k, :, :] - forecast[j, :, :])**2, 1)
#     L2[suffix_list[i]] = cost

plt.subplot(2, 2, 3)
for suf in suffix_list:
    plt.plot(L2[suf].mean(0).mean(0), label=suf)
plt.legend()
plt.title(u'Squared norm of prediction error\n averaged over all time steps')
plt.subplot(2, 2, 4)
plt.title(u'Squared norm')
plt.bar(range(5), map(np.mean, L2.values()))
plt.xticks(range(5), L2.keys())
plt.tight_layout()
plt.show()
