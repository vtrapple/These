#!/usr/bin/env python
# -*- coding: utf-8 -*-


from vanderpol.vanderpol import vanderpol_all, pred_window
import numpy as np
import matplotlib.pyplot as plt
from ROproblem.ROproblem import ROproblem
import pickle


def vanderpol_J(mu, A, w, x0, dx0):
    par_vector = np.vstack([mu, A, w, x0, dx0]).T
    return vanderpol_all(par_vector)


def vdp_ku(k, u):
    ones = np.ones_like(k)
    return vanderpol_J(8.29 * ones, k, u, ones, 0 * ones)



vdp = ROproblem('vdp', Kdim=1, Udim=1, function = vdp_ku)
vdp.set_indicesK([0])
NptsK = 500
NptsU = 500
# k_ax = np.linspace(8, 9, Npts)
k_ax = np.linspace(0.5, 1.5, NptsK)
u_ax = np.linspace(-0.05, 0.05, NptsU) + np.pi / 5.0
u_ax = np.linspace(0.62, 0.65, NptsU)
vdp.set_base_vectors(k_ax, u_ax)
vdp.set_meshgrid()
# vdp.compute_grid()
vdp.compute_moments_optimisers()
vdp.alpha_quantile([1, 0.95, 0.90])
vdp.summary()
vdp.plot2D()
vdp.plot_profiles()
vdp.plot_evolution_wrt_p()
plt.show()



# with open('/home/victor/These/source_code/vanderpol/vdp.obj', 'w') as fil:
#     pickle.dump(vdp, fil)
vdp = pickle.load(open('/home/victor/These/source_code/vanderpol/vdp.obj', 'r'))
vdp.summary()
vdp.compute_maxratio()
vdp.plot_profiles()

vdp.compute_conditional_optimisers_exact(np.linspace(0.62, 0.65, 10), Nrestart=20)


t_start = np.linspace(0, 50, 500)
period = 8
t_next = np.linspace(50, 50 + (period * 50), (period) * 500)
t_tot = np.append(t_start, t_next)
ref = pred_window([1, 0], [8.53, 1.2, np.pi / 5.0], np.linspace(0, 50, 500))

ref_next = pred_window(ref[-1, :], [8.53, 1.2, np.pi / 5.0], t_next)

minE_RMSE = np.empty(())
pratio_RMSE = np.empty(())
p100_RMSE = np.empty(())
p95_RMSE = np.empty(())
minV_RMSE = np.empty(())

for u in np.random.uniform(.62, 0.65, size=[1000]):
    u_next_ref = np.random.uniform(.62, .65, size=[1])
    ref_next = pred_window(ref[-1, :], [8.53, 1.2, u], t_next)

    minE = pred_window([1.0, 0.0], [8.29, vdp.ROestimates['minE'], u],
                       t_tot)
    pratio = pred_window([1.0, 0.0], [8.29, vdp.ROestimates['pratio100'], u],
                         t_tot)
    p100 = pred_window([1.0, 0.0], [8.29, vdp.ROestimates['p100'], u],
                       t_tot)
    p95 = pred_window([1.0, 0.0], [8.29, vdp.ROestimates['p95'], u],
                      t_tot)
    minV = pred_window([1.0, 0.0], [8.29, vdp.ROestimates['minV'], u],
                       t_tot)
    minE_RMSE = np.append(minE_RMSE, np.sum((minE[500:] - ref_next)**2, 0)[0])
    pratio_RMSE = np.append(pratio_RMSE, np.sum((pratio[500:] - ref_next)**2, 0)[0])
    p100_RMSE = np.append(p100_RMSE, np.sum((p100[500:] - ref_next)**2, 0)[0])
    minV_RMSE = np.append(minV_RMSE, np.sum((minV[500:] - ref_next)**2, 0)[0])
    p95_RMSE = np.append(p95_RMSE, np.sum((p95[500:] - ref_next)**2, 0)[0])

plt.figure()
ax = plt.subplot(1, 1, 1)
ax.violinplot(np.vstack([minE_RMSE, pratio_RMSE, p100_RMSE, p95_RMSE, minV_RMSE]).T,
              showmeans=True, showextrema=True)
ax.set_xticks([1, 2, 3, 4, 5])
ax.set_xticklabels(['minE', 'pratio', 'p100', 'p95', 'minV'])
plt.show()

Nsamples = 500
minE_big = np.empty((len(t_next), Nsamples))
p100_big = np.empty((len(t_next), Nsamples))
minV_big = np.empty((len(t_next), Nsamples))
ref_big = np.empty((len(t_next), Nsamples))
p95_big = np.empty((len(t_next), Nsamples))
p90_big = np.empty((len(t_next), Nsamples))
for i, u in enumerate(np.random.uniform(.62, .65, size = Nsamples)):
    # minE_big[:, i] = pred_window([1.0, 0.0], [8.29, vdp.ROestimates['minE'], u],
    #                              t_tot)[500:, 0]
    # p100_big[:, i] = pred_window([1.0, 0.0], [8.29, vdp.ROestimates['p100'], u],
    #                              t_tot)[500:, 0]
    # minV_big[:, i] = pred_window([1.0, 0.0], [8.29, vdp.ROestimates['minV'], u],
    #                              t_tot)[500:, 0]
    # p95_big[:, i] = pred_window([1.0, 0.0], [8.29, vdp.ROestimates['p95'], u],
    #                             t_tot)[500:, 0]
    # p90_big[:, i] = pred_window([1.0, 0.0], [8.29, vdp.ROestimates['p90'], u],
    #                             t_next)[:, 0]
    # ref_big[:, i] = pred_window([1.0, 0.0], [8.53, 1.2, u], t_next)[:, 0]
    minE_big[:, i] = pred_window(ref[-1, :], [8.29, vdp.ROestimates['minE'], u],
                                 t_next)[:, 0]
    p100_big[:, i] = pred_window(ref[-1, :], [8.29, vdp.ROestimates['p100'], u],
                                 t_next)[:, 0]
    minV_big[:, i] = pred_window(ref[-1, :], [8.29, vdp.ROestimates['minV'], u],
                                 t_next)[:, 0]
    p95_big[:, i] = pred_window(ref[-1, :], [8.29, vdp.ROestimates['p95'], u],
                                t_next)[:, 0]
    p90_big[:, i] = pred_window(ref[-1, :], [8.29, vdp.ROestimates['p90'], u],
                                t_next)[:, 0]
    ref_big[:, i] = pred_window(ref[-1, :], [8.53, 1.2, u], t_next)[:, 0]


plt.subplot(3, 1, 1)
mean_pred = np.mean(minE_big, 1)
std_pred = np.std(minE_big, 1)
plt.plot(t_next, mean_pred)
plt.fill_between(t_next,
                 mean_pred + 2 * std_pred,
                 mean_pred - 2 * std_pred, color='k', alpha=0.3)
plt.plot(t_next, ref_big, color='r', alpha=.01)
plt.subplot(3, 1, 2)
mean_pred = np.mean(p100_big, 1)
std_pred = np.std(p100_big, 1)
plt.plot(t_next, mean_pred)
plt.fill_between(t_next,
                 mean_pred + 2 * std_pred,
                 mean_pred - 2 * std_pred, color='k', alpha=0.3)
plt.plot(t_next, ref_big, color='r', alpha=0.01)
plt.subplot(3, 1, 3)
mean_pred = np.mean(minV_big, 1)
std_pred = np.std(minV_big, 1)
plt.plot(t_next, mean_pred)
plt.fill_between(t_next,
                 mean_pred + 2 * std_pred,
                 mean_pred - 2 * std_pred, color='k', alpha=0.3)
plt.plot(t_next, ref_big, color='r', alpha=0.01)
plt.tight_layout()
plt.show()

plt.plot(np.mean((ref_big - minE_big)**2, 1), label = 'minE')
plt.plot(np.mean((ref_big - p100_big)**2, 1), label = 'p100')
plt.plot(np.mean((ref_big - minV_big)**2, 1), label = 'minV')
plt.plot(np.mean((ref_big - p95_big)**2, 1), label = 'p95')
plt.plot(np.mean((ref_big - p90_big)**2, 1), label = 'p90')
plt.tight_layout()
plt.legend()
plt.show()

plt.violinplot(np.vstack([np.mean((ref_big - minE_big)**2, 1),
                          np.mean((ref_big - p100_big)**2, 1),
                          np.mean((ref_big - minV_big)**2, 1),
                          np.mean((ref_big - p95_big)**2, 1),
                          np.mean((ref_big - p90_big)**2, 1)]).T,
               showmeans=True,
               showextrema=True)
plt.show()
import scipy.stats
scipy.stats.ttest_ind(np.mean((ref_big - minE_big)**2, 1),
                      np.mean((ref_big - p95_big)**2, 1), equal_var=False)



plt.subplot(2, 1, 1)
for u in np.random.choice(u_ax, size=[200]):
    minE = pred_window([1.0, 0.0], [8.29, vdp.ROestimates['minE'], u],
                       np.linspace(0, 100, 1000))
    plt.plot(t_next, minE[500:, 0], 'k', alpha=0.02)
plt.plot(t_next, ref_next[:, 0])
plt.subplot(2, 1, 2)
for u in np.random.choice(u_ax, size=[200]):
    rat = pred_window([1.0, 0.0], [8.29, vdp.ROestimates['pratio100'], u],
                      np.linspace(0, 100, 1000))
    plt.plot(t_next, rat[500:, 0], 'k', alpha=0.02)
plt.plot(t_next, ref_next[:, 0])
plt.show()
