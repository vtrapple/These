#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from ROproblem import ROproblem

# Vibration absorber Randall

from vibabsorb.vibabsorb import max_displacement

vib = ROproblem.ROproblem('Randall', Kdim=2, Udim=1,
                          function=lambda ksi, T, beta: max_displacement(ksi, T, beta) + 5.0)
vib.set_indicesK([0, 1])
NptsK = 200
NptsU = 500

ksi2 = np.linspace(0, 1, NptsK)
T = np.linspace(0.1, 2, NptsK)
beta = np.linspace(0, 2.5, NptsU)
vib.set_base_vectors(ksi2, T, beta)
vib.set_meshgrid()
# vib.compute_grid()
# np.save('/home/victor/These/source_code/vibabsorb/200x200x500.npy', vib.values_on_grid)
VOG = np.load('/home/victor/These/source_code/vibabsorb/200x200x500.npy')
vib.set_evaluated_grid(VOG)
vib.compute_moments_optimisers()
vib.compute_conditional_optimisers()
# vib.alpha_by_dichotomy([1, .90])
vib.alpha_quantile(p=[1.0, 0.99, 0.95, 0.90, 0.80])
vib.compute_maxratio()
vib.summary()
vib.plot_profiles(return_fig=True)
plt.tight_layout()
# plt.show()                      
plt.savefig('/home/victor/acadwriting/Article/NPG1/Figures/vibration_profile.pdf')

plt.subplot(1, 2, 1)
plt.contour(vib.compute_moments()[0], 50)
plt.subplot(1, 2, 2)
plt.contour(vib.compute_moments()[1], 50)
plt.show()
