#!/usr/bin/env python
# coding: utf-8

#=================================================================================
##.* Definition of parameter and useful functions
#=================================================================================

import SWE_wrapper as swe        # 
import matplotlib.pyplot as plt
import pickle
from scipy import stats, optimize
import numpy as np
import weighted_kde
import half_sample_mode
import seaborn as sns
import corner
import pandas as pd
from sklearn import cluster
import pyDOE


class Estimate:
    def __init__(self, value = [], uref = [], method = ''):
        self.k = value
        self.uref = uref
        self.J = np.nan
        self.method = method
        self.validation_SS = []
    def interp(self, xr):
        return map(swe.interp(self.k), xr)

    def summary(self):
        print 'K =', self.k
        print 'AP =', self.uref
        if not np.isnan(self.J):
            print 'J = ', self.J

    def eval_J(self):
        self.J,_ = J_pw_AP(self.k, self.uref[0], self.uref[1])
        
Kref = 0.2*(1+np.sin(2*np.pi*swe.xr/swe.D[1])) #
xr_ext = np.linspace(1,100,200)
Kref_ext = 0.2*(1+np.sin(2*np.pi*xr_ext/swe.D[1]))



mean_h = 20 
phase = 0
amplitude = 5.0
period = 50.0
# href = y_obs
# Ap_Pm -> A = 5.1; P = 49.8
# Ap_Pp -> A = 5.2; P = 50.1
# Am_Pp -> A = 4.9; P = 50.2
# Am_Pm -> A = 4.8; P = 49.9

def shallow_water_KU(K,amplitude = 5.0, period = 50.0, mean_h = 20.0 , phase = 0.0):
    """Computes h and q for a given value of K, and U = [A,P,M,Ph] """
    bcL = lambda h, hu, t: swe.BCrand(h, hu, t, 'L',
                                 mean_h, amplitude, period, phase)
    return swe.shallow_water(swe.D, swe.g, swe.T, swe.h0, swe.u0, swe.N, swe.LF_flux, swe.dt, swe.b, K,
                                 bcL, swe.bcR)




# APref = [5.0, 50.0]     
# APref = [5.1, 49.8]
# APref = [5.2, 50.1] 
# APref = [4.9, 50.2]
APref = [4.8, 49.9]


[xr, href, uref, t] = shallow_water_KU(Kref, # Generation observation --> Training set
                                       amplitude = APref[0], 
                                       period = APref[1],
                                       mean_h = 20.0,
                                       phase = 0.0)
[_, h_pre, _, _ ] = swe.prevision(Kref, # Generation prediction --> Validation set
                                  amplitude = APref[0],
                                  period = APref[1],
                                  mean_h = 20.0,
                                  phase = 0.0)

def J_nograd(K,AP, hreference = href):
    Kc = np.array(map(swe.interp(K), xr))
    _,hKAP,_,_ = shallow_water_KU(Kc,
                                  amplitude = AP[0], 
                                  period = AP[1],
                                  mean_h = 20.0,
                                  phase = 0.0)
    return 0.5*np.sum((hKAP - hreference)**2)

J_nograd_AP = lambda K,A,P,hr: J_nograd(K,[A,P],hr)

def J_validation(K,AP):
    [_, hprevision, _, _] = swe.prevision(Kref, # Generation prediction --> Validation set
                                  amplitude = AP[0],
                                  period = AP[1],
                                  mean_h = 20.0,
                                  phase = 0.0)
    Kc = np.array(map(swe.interp(K), xr))
    [_, h, _, _] = swe.prevision(Kc, # Generation prediction --> Validation set
                                  amplitude = AP[0],
                                  period = AP[1],
                                  mean_h = 20.0,
                                  phase = 0.0)
    return 0.5*np.sum((h-hprevision)**2)

print APref
Nvar = 4


def prediction_cost(Kcoeff, hpr = h_pre):
    """Compute prediction misfit"""
    K_transform = swe.interp(Kcoeff)
    K = np.array(map(K_transform, xr))
    return swe.J_function(h_pre, swe.prevision(K)[1])

def J_pw_AP(Kcoeff, ampli = 5.0, period = 50.0, hr = href):
    """Piecewise constant interpolation of K, then computes cost function and gradient"""
    K_transform = swe.interp(Kcoeff)
    K = np.array(map(K_transform, xr))
    cost, grad =  swe.J_grad_AP_href(K, ampli, period, hr)
    
    grad_sum_length = int(50.0/Kcoeff.size)
    grad_coeff = np.zeros(Kcoeff.size)
    for i in range(Kcoeff.size):
        grad_coeff[i] = sum(grad[i*grad_sum_length:(i*grad_sum_length+grad_sum_length)])
    return cost,grad_coeff


ref = Estimate(Kref, [5.0,50.0], 'ref')
ref.eval_J()
ref.summary()

##.* Sanity check:
test = J_pw_AP(Kref, ampli = APref[0], period = APref[1], hr= href)
test[0] == 0.0
all(test[1] == np.zeros(50))
# prediction_cost(Kref) == 0.0

# =================================================================================
##.* COMPUTATIONS
# =================================================================================

def generate_AP_discrete(iter,Ngrid=20):
    A = np.linspace(4.7,5.3, Ngrid)
    P = np.linspace(49.7,50.3, Ngrid)
    return np.array([A[iter%Ngrid], P[iter/Ngrid]])

Nsim = 400
k_max_negloglik = np.empty([Nsim,Nvar])
AP_saved = np.empty([Nsim,2])
negloglik_saved = np.empty(Nsim)

start_opt = 0.2*np.ones(Nvar)
for i in xrange(Nsim):
    AP = generate_AP_discrete(i)
    AP_saved[i,:] = [AP[0],AP[1]]
    args = (AP[0],AP[1], href)
    opt = optimize.minimize(fun = J_pw_AP, x0 = start_opt, args = args,
                            jac = True, bounds = [(0.0, None)]*Nvar)
    k_max_negloglik[i,:] = opt.x
    negloglik_saved[i] = opt.fun

dictionnary_save = {'k_max_negloglik':k_max_negloglik, 'AP_saved': AP_saved, 'negloglik_saved':negloglik_saved}

filename = 'k_max_4dim'
output = open(filename, 'wb') # Save the runs
pickle.dump(dictionnary_save, output)
output.close()

nan_id = np.isnan(negloglik_saved)

AP_nan = AP_saved[nan_id,:]

nll_nan = np.empty(np.sum(nan_id))
k_max_nan = np.empty([np.sum(nan_id),4])

for i,ap in enumerate(AP_nan):
    args = (ap[0],ap[1], href)
    opt = optimize.minimize(fun = lambda k,a,p,hr: J_pw_AP(k,a,p,hr)[0], x0 = start_opt, args = args,
                            bounds = [(0.0, None)]*Nvar)
    k_max_nan[i,:] = opt.x
    nll_nan[i] = opt.fun

output = open('k_max_4dim', 'wb')
dic = {'k_nan' :k_max_nan, 'nll_nan' : nll_nan, 'AP_nan' : AP_nan}
pickle.dump(dic, output)
output.close()

#============================================================================
##.* Classical Estimates (Min of E <-> MMAP)
#===========================================================================

# Naive Gradient descent on estimate of mean evaluated on LHS
LHS = pyDOE.lhs(2, samples = 25, criterion = 'm', iterations = 20)

def mean_optimization(APref, optimization_restarts = 5):
    noptim = 0
    [xr, href, uref, t] = shallow_water_KU(Kref, # Generation observation --> Training set
                                       amplitude = APref[0], 
                                       period = APref[1],
                                       mean_h = 20.0,
                                       phase = 0.0)

    print APref
    test = J_pw_AP(Kref, ampli = APref[0], period = APref[1], hr = href)
    test[0] == 0.0
    all(test[1] == np.zeros(50))

    J_KAP_evallhs = lambda K, lhs : [J_nograd_AP(K,lhs[i,0]*0.6+4.7,lhs[i,1]*0.6+49.7, href) for i in xrange(lhs.shape[0])]
    def J_mean(K):
        eval = J_KAP_evallhs(K, LHS)
        # J_val = np.asarray([eval[i][0] for i in xrange(100)])
        # G_val = np.asarray([eval[i][1] for i in xrange(100)])
        J_val = np.asarray(eval)
        print 'end eval'
        return J_val.mean()#, G_val.mean(0)
    while noptim < optimization_restarts:
        if noptim == 0:
            x0 = 0.1*np.ones(4)
        else:
            x0 = mean_opt.x
        mean_opt = optimize.minimize(J_mean, x0 = x0, jac = False, bounds = 4*[(0.0,None)] ,tol = 1e-8)
        noptim += 1
        
    return mean_opt

def var_optimization(APref, optimization_restarts = 5):
    noptim = 0
    [xr, href, uref, t] = shallow_water_KU(Kref, # Generation observation --> Training set
                                       amplitude = APref[0], 
                                       period = APref[1],
                                       mean_h = 20.0,
                                       phase = 0.0)

    print APref
    test = J_pw_AP(Kref, ampli = APref[0], period = APref[1], hr = href)
    test[0] == 0.0
    all(test[1] == np.zeros(50))

    J_KAP_evallhs = lambda K, lhs : [J_nograd_AP(K,lhs[i,0]*0.6+4.7,lhs[i,1]*0.6+49.7, href) for i in xrange(lhs.shape[0])]
    def J_var(K):
        eval = J_KAP_evallhs(K, LHS)
        # J_val = np.asarray([eval[i][0] for i in xrange(100)])
        # G_val = np.asarray([eval[i][1] for i in xrange(100)])
        J_val = np.asarray(eval)
        print '-- end eval'
        return J_val.var()#, G_val.mean(0)
    while noptim < optimization_restarts:
        if noptim == 0:
            x0 = 0.1*np.ones(4)
            var_opt = optimize.minimize(J_var, x0 = x0, jac = False, bounds = 4*[(0.0,None)] ,tol = 1e-7)
            best_opt =  var_opt
            print 'fin optim'
        else:
            x0 = stats.uniform.rvs(size = 4) # Restart of optim in [0.0,1.0]^4 => good interval
            var_opt = optimize.minimize(J_var, x0 = x0, jac = False, bounds = 4*[(0.0,None)] ,tol = 1e-7)
            print 'fin optim'
            if var_opt.fun < best_opt.fun:
                best_opt = var_opt        
        noptim += 1
    return best_opt

APtot = ([5.0,50.0], [5.1,49.8], [5.2, 50.1], [4.9, 50.2], [4.8,49.9])

# min_E = [mean_optimization(ap,5) for ap in APtot]
# with open('minimum_expected_value', 'wb') as output: # Save the optimization results
#     pickle.dump(min_E, output)

# min_V = [var_optimization(ap,5) for ap in APtot]
# with open('minimum_variance', 'wb') as output: # Save the optimization results
#     pickle.dump(min_V, output)
min_V[-2] = var_optimization([4.9,50.2], 5)
    
with open('minimum_expected_value', 'rb') as input:
    min_E = pickle.load(input)

with open('minimum_variance', 'rb') as input:
    min_V = pickle.load(input)

    
est_minE = [Estimate(min_E[i].x, APtot[i], 'minE') for i in xrange(5)]
[est.eval_J() for est in est_minE]

est_minV = [Estimate(min_V[i].x, APtot[i], 'minV') for i in xrange(5)]
[est.eval_J() for est in est_minV]

J_pw_AP(mean_opt.x)
# min_E = Estimate(mean_opt.x, APref, 'minE'); min_E.eval_J()
# centered => mean_opt.x = [0.39964846, 0.3108978 , 0.09023487, 0.07442487]
# array([0.38796751, 0.30163245, 0.08677233, 0.07159961])

# Ap_Pm => mean_opt.x = np.array([0.        , 0.02011099, 0.14534305, 0.2419404 ])
# array([0.        , 0.02131879, 0.12322838, 0.44181064])
# print min_E
# [      fun: 1581.3025158010705
#  hess_inv: <4x4 LbfgsInvHessProduct with dtype=float64>
#       jac: array([ 0.00402451, -0.00493401,  0.01195986, -0.00686668])
#   message: 'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
#      nfev: 15
#       nit: 1
#    status: 0
#   success: True
#         x: array([0.38472735, 0.32057276, 0.07297159, 0.006027  ]),       fun: 2645.7043546606915
#  hess_inv: <4x4 LbfgsInvHessProduct with dtype=float64>
#       jac: array([2.35675952e+02, 9.09494702e-04, 5.00222086e-04, 3.63797881e-04])
#   message: 'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
#      nfev: 15
#       nit: 1
#    status: 0
#   success: True
#         x: array([0.        , 0.03561469, 0.09096538, 0.51509195]),       fun: 2686.9970131017735
#  hess_inv: <4x4 LbfgsInvHessProduct with dtype=float64>
#       jac: array([ 2.53764938e+01, -2.27373675e-03, -2.18278728e-03,  1.27461135e+01])
#   message: 'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
#      nfev: 15
#       nit: 1
#    status: 0
#   success: True
#         x: array([0.        , 0.45898732, 0.02083556, 0.        ]),       fun: 2308.173833000384
#  hess_inv: <4x4 LbfgsInvHessProduct with dtype=float64>
#       jac: array([ 3.63797881e-04, -4.54747351e-05,  6.36646291e-04,  1.57791419e+01])
#   message: 'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
#      nfev: 15
#       nit: 1
#    status: 0
#   success: True
#         x: array([1.32596824, 0.15695247, 0.12214081, 0.        ]),       fun: 2439.6716480392406
#  hess_inv: <4x4 LbfgsInvHessProduct with dtype=float64>
#       jac: array([ 0.00241016, -0.05620677,  0.03979039,  0.02942215])
#   message: 'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
#      nfev: 15
#       nit: 1
#    status: 0
#   success: True
#         x: array([0.92907382, 0.11507982, 0.03200064, 0.51527653])]
#============================================================================
##.* Exploitation
#============================================================================

### Import files ------------------------------------

filename = 'k_max_4dim_LHS'   # uref = E[U]
filename = 'k_max_4dim'   # uref = E[U]

# filename = 'k_max_4dim_Ap_Pm'
# filename = 'k_max_4dim_Ap_Pp' 
# filename = 'k_max_4dim_Am_Pp'
filename = 'k_max_4dim_Am_Pm'
print APref

with open(filename, 'rb') as input:
    dictionnary_save = pickle.load(input)
    k_max_negloglik = dictionnary_save['k_max_negloglik']
    AP_saved = dictionnary_save['AP_saved']
    negloglik_saved = dictionnary_save['negloglik_saved']

# # Corner plot
# fig_corner = corner.corner(k_max_negloglik, plot_density = True, no_fill_contours = False,
#                            fill_contours = True)
# fig_corner.show()

### Computation of Estimates --------------------------

# Marginal Means
k_mean = np.mean(k_max_negloglik,0)
Mean = Estimate(k_mean, APref, 'Mean')
Mean.eval_J()

# Marginal MPE

ind_0_k1 = (k_max_negloglik[:,0]<0.001)
prop_0_k1 = np.sum(ind_0_k1)/400.0
dens_k1 = stats.gaussian_kde(k_max_negloglik[~ind_0_k1,0])

ind_0_k2 = (k_max_negloglik[:,1]<0.001)
prop_0_k2 = np.sum(ind_0_k2)/400.0
dens_k2 = stats.gaussian_kde(k_max_negloglik[~ind_0_k2,1])

ind_0_k3 = (k_max_negloglik[:,2]<0.001)
prop_0_k3 = np.sum(ind_0_k3)/400.0
dens_k3 = stats.gaussian_kde(k_max_negloglik[~ind_0_k3,2])

ind_0_k4 = (k_max_negloglik[:,3]<0.001)
prop_0_k4 = np.sum(ind_0_k4)/400.0
dens_k4 = stats.gaussian_kde(k_max_negloglik[~ind_0_k4,3])
k1_mpe = optimize.minimize(fun = lambda k: -dens_k1(k),x0 = 0.2, bounds = [(0.0,None)]).x
k2_mpe = optimize.minimize(fun = lambda k: -dens_k2(k),x0 = 0.2, bounds = [(0.0,None)]).x
k3_mpe = optimize.minimize(fun = lambda k: -dens_k3(k),x0 = 0.2, bounds = [(0.0,None)]).x
k4_mpe = optimize.minimize(fun = lambda k: -dens_k4(k),x0 = 0.2, bounds = [(0.0,None)]).x

k_mpe_marg = np.array([k1_mpe, k2_mpe, k3_mpe, k4_mpe])[:,0]
MMPE = Estimate(k_mpe_marg, APref, 'MMPE')
MMPE.eval_J()

# Joint MPE
#--   optimize
kde_joint = stats.gaussian_kde(k_max_negloglik.T)
st_opt = np.ones(Nvar)*.0
st_opt = k_mpe_marg
mpe_joint = optimize.minimize(fun = lambda k: -np.log(kde_joint(k)), x0 = st_opt, bounds = [(0.0,None)]*Nvar)
k_mpe_joint = mpe_joint.x

JMPE = Estimate(k_mpe_joint, APref, 'JMPE')
JMPE.eval_J()
JMPE.summary()


#--  MeanShift


mean_shift = cluster.MeanShift(# bandwidth = 0.5
)
mean_shift.fit_predict(k_max_negloglik)
clust_cent = mean_shift.cluster_centers_
ncluster = clust_cent.shape[0]
print ncluster
cluster_Estimates = [Estimate(clust_cent[i,:],APref, str('CLUS')) for i in xrange(ncluster)]
[cluster_Estimates[i].eval_J() for i in xrange(ncluster)]
cluster_Estimates = sorted(cluster_Estimates, key = lambda cl: cl.J)
for i, clus in enumerate(cluster_Estimates):
    clus.method += str(i)

# ncluster = np.empty(np.arange(0.2,1,0.05).shape)
# best_clust = np.empty(np.arange(0.2,1,0.05).shape)

# for idx,bw in enumerate(np.arange(0.2,1,0.05)):
#     mean_shift = cluster.MeanShift(bandwidth = bw)
#     mean_shift.fit_predict(k_max_negloglik)
#     clust_cent = mean_shift.cluster_centers_
#     ncluster[idx] = clust_cent.shape[0]
#     cluster_Estimates = [Estimate(clust_cent[i,:], APref, str('CLUS')) for i in xrange(int(ncluster[idx]))]
#     [cluster_Estimates[i].eval_J() for i in xrange(int(ncluster[idx]))]
#     cluster_Estimates = sorted(cluster_Estimates, key = lambda cl: cl.J)
#     for i, clus in enumerate(cluster_Estimates):
#         clus.method += str(i)
#     best_clust[idx] = cluster_Estimates[0].J

# plt.subplot(2,1,1)
# plt.plot(np.arange(0.2,1,0.05), ncluster)
# plt.grid()
# plt.subplot(2,1,2)
# plt.plot(np.arange(0.2,1,0.05), best_clust)
# plt.show()

    
    


# K = 0
# k_0 = 0.*np.ones(Nvar)

# J(k,E[U])
k_nounc = optimize.minimize(fun = lambda k: J_pw_AP(k),
                            x0 = [0.2,0.2,0.2,0.2], jac = True,
                            bounds = [(0.0,None)]*Nvar).x # Optimization using J(k,E[U])
NoUn = Estimate(k_nounc, APref, 'NoUn (E[U])')
NoUn.eval_J()

# J(k,u_ref)
k_best = optimize.minimize(fun = lambda k: J_pw_AP(k, APref[0],APref[1]),
                           x0 = [0.2,0.2,0.2,0.2], jac = True,
                           bounds = [(0.0,None)]*Nvar).x # Optimization using J(k,u_ref)

BEST = Estimate(k_best, APref, 'BEST')
BEST.eval_J()

from collections import OrderedDict

[minE] = filter(lambda ee: ee.uref == APref, est_minE)

estimate_sample = OrderedDict(BEST=BEST, Mean = Mean, MMPE = MMPE, JMPE = JMPE, NoUn = NoUn, MeSh = cluster_Estimates, minE = minE)

# k_vec = np.array([k_nounc, k_mean, k_mpe_marg, k_mpe_joint, k_0, k_clus1, k_clus2])
# tick_lab = ['No unc','mean','marg mpe','joint mpe', '0', 'clus1', 'clus2']

# cost_val = [prediction_cost(k) for k in k_vec]

# oin = open('estimates_diffconditions', 'rb')
# estimate_dict = pickle.load(oin)
# oin.close()

# k_nounc = estimate_dict['centered']['NoUn'].k
# k_mean = estimate_dict['centered']['Mean'].k
# k_mpe_marg = estimate_dict['centered']['MMPE'].k
# k_mpe_joint = estimate_dict['centered']['JMPE'].k

def plot_dict_estimate(estimate_sample, filename_txt, save = False, save_txt = ''):
    fig = plt.figure(figsize=(10.0, 8.0), dpi=100)
    plt.subplot(2,1,1)
    plt.plot(xr_ext, Kref_ext, 'r--', label= r'$\mathbf{k}_{\mathrm{ref}}$')
    for _, est in estimate_sample.items():
        if isinstance(est, list):
            for est_unfolded in est:
                plt.plot(xr_ext, map(swe.interp(est_unfolded.k), xr_ext), label = est_unfolded.method)

        else:
            plt.plot(xr_ext, map(swe.interp(est.k), xr_ext), label = est.method)
    plt.grid(alpha = 0.5)

    plt.title(r'Estimates generated using $\mathbf{u}_{\mathrm{ref}}$ =' + filename_txt, fontsize = 15)

    plt.legend()

    ax = fig.add_subplot(2,1,2)
    plt.title('SS of the residual', fontsize = 15)
    i=0
    ticks_labels = ()
    for _, (name, est) in enumerate(estimate_sample.items()):
        if isinstance(est, list):
            for name_clus,est_unfolded in enumerate(est):
                plt.bar(i, est_unfolded.J)
                ticks_labels = tuple(ticks_labels) + ('CLUS' + str(name_clus),)
                i+=1
        else:
            plt.bar(i, est.J)
            ticks_labels =  tuple(ticks_labels) + (name,) 
            i+=1
    ax.set_xticks(np.arange(i))
    ax.set_xticklabels(ticks_labels)
    plt.tight_layout()
    if save:
        plt.savefig('/home/victor/' + save_txt + '.pdf', transparent = True)
    else:
        plt.show()

plt.rc('text.latex', preamble='\usepackage{amsmath},\usepackage{amssymb}')
plot_dict_estimate(estimate_sample, r'$\mathbb{E}[\mathbf{U}] + (-0.2, -0.1)$ (Am\_Pm)', save = True, save_txt = 'estimate_uref_AmPm')




 ## ------------------------------------

import mixem
wgts, distributions, ll = mixem.em(k_max_negloglik, [
    mixem.distribution.MultivariateNormalDistribution(k_mean, np.identity(4)), 
    mixem.distribution.MultivariateNormalDistribution(k_clus2, np.identity(4)),
])

k_max_df = pd.DataFrame(k_max_negloglik, col)
sns.set()
g = sns.PairGrid(k_max_df)
g = g.map_upper(plt.plot, ls = 'none', marker = '.', alpha = 0.2)
g = g.map_lower(sns.kdeplot, cmap = "Blues_d", shade = False)
g = g.map_diag(plt.hist)
plt.show()

axes  = g.fig.axes
axes = np.array(axes).reshape((5,4))
for yi in range(4):
    for xi in range(yi):
        ax = axes[int(yi), int(xi)]
        ax.plot(k_max_mean[xi], k_max_mean[yi],'o', label ='Mean')
        ax.plot(BEST.k[xi], BEST.k[yi], 'mo', label ='best')        
        for i in xrange(ncluster):
            ax.plot(cluster_Estimates[i].k[xi], cluster_Estimates[i].k[yi], 'o', label = 'cluster')
g.add_legend()
plt.show()




DoE = pyDOE.lhs(2 , 1000, 'maximin')
out = open('LHS_2_1000', 'wb')
pickle.dump(DoE,out)
out.close()

inp = open('LHS_2_1000', 'rb')
DoE01 = pickle.load(inp)
DoE = np.array([5,50]) + 0.6*DoE01 - 0.3
inp.close()

plt.plot(DoE[:,0], DoE[:,1], '.')
plt.plot(AP_saved[:,0], AP_saved[:,1], '.', markersize = 10)
# plt.plot(indLHS[:,0]/20, indLHS[:,1]/20, '.')
plt.show()

inp = open('LHS_2_20', 'rb')
DoE01 = pickle.load(inp)
DoE = np.array([5,50]) + 0.6*DoE01 - 0.3
inp.close()



indLHS = np.zeros(DoE01.shape)
for i in xrange(20):
    indLHS[i,:] = [int(DoE01[i,0]*20),int(DoE01[i,1]*20)]



def SS_validation(K):
    SS = np.empty(DoE.shape[0])
    for i in xrange(DoE.shape[0]):
        SS[i] = J_validation(K, DoE[i,:])
        
    heatmap_SS = np.zeros([20,20])
    for i,index in enumerate(indLHS):
        x,y =  int(index[0]), int(index[1])
        heatmap_SS[x,y] = SS[i]
    return SS, heatmap_SS

SS_ref, hm_ref = SS_validation(Kref)
SS_mean, hm_mean = SS_validation(k_mean)
SS_mmpe, hm_mmpe = SS_validation(k_mpe_marg)
SS_jmpe, hm_jmpe = SS_validation(k_mpe_joint)
SS_best, hm_best = SS_validation(k_best)



{'SS_ref':SS_ref, 'SS_mean':SS_mean, 'SS_mmpe':SS_mmpe, 'SS_jmpe':SS_jmpe, }

print SS_mmpe
print SS_jmpe
print SS_best

ax = sns.heatmap(hm_best)
plt.show()



centered = dict(BEST=BEST, Mean = Mean, MMPE = MMPE, JMPE = JMPE, NoUn = NoUn, MeSh = cluster_Estimates)
Ap_Pm = dict(BEST=BEST, Mean = Mean, MMPE = MMPE, JMPE = JMPE, NoUn = NoUn, MeSh = cluster_Estimates)
Ap_Pp = dict(BEST=BEST, Mean = Mean, MMPE = MMPE, JMPE = JMPE, NoUn = NoUn, MeSh = cluster_Estimates)
Am_Pp = dict(BEST=BEST, Mean = Mean, MMPE = MMPE, JMPE = JMPE, NoUn = NoUn, MeSh = cluster_Estimates)
Am_Pm = dict(BEST=BEST, Mean = Mean, MMPE = MMPE, JMPE = JMPE, NoUn = NoUn, MeSh = cluster_Estimates)

estimate_dict = dict(centered = centered, Ap_Pm = Ap_Pm, Ap_Pp = Ap_Pp, Am_Pp = Am_Pp, Am_Pm = Am_Pm)

# out = open('estimates_diffconditions', 'wb')
# pickle.dump(estimate_dict, out)
# out.close()

inp = open('estimates_diffconditions', 'rb')
estimate_dict = pickle.load(inp)
inp.close()

for cond,val  in estimate_dict.items():
    for est_type, est in val.items():
        SS,_ = SS_validation(est.k)
        plt.bar(xrange(20),SS)
        plt.title(str(est_type)+' '+str(cond))
        fname = str(est_type)+'_'+str(cond)
        plt.tight_layout()
        plt.savefig('/home/victor/Bureau/validation/'+fname)
        plt.close()


for conditions, est_d in estimate_dict.items(): # Iterate over conditions 
    for _, est in est_d.items(): # Iterate over methods
        if isinstance(est, list): # Cluster of MeanShift
            for est_unfolded in est:
                SS,_ = SS_validation(est_unfolded.k)
                plt.bar(xrange(20),SS)
                plt.title(str(est_unfolded.method)+' '+str(conditions))
                fname = str(est_unfolded.method)+'_'+str(conditions)
                plt.tight_layout()
                plt.savefig('/home/victor/Bureau/validation2/'+fname)
                plt.close()
        else:
            SS,_ = SS_validation(est.k)
            plt.bar(xrange(20),SS)
            plt.title(str(est.method)+' '+str(conditions))
            fname = str(est.method)+'_'+str(conditions)
            plt.tight_layout()
            plt.savefig('/home/victor/Bureau/validation2/'+fname)
            plt.close()    

# for _,val in estimate_dict.items():
#     val['MMAP'].method = 'Mean'
#     val['Mean'] = val.pop('MMAP')

    

# for i in xrange(400):
#     AP = generate_AP_discrete(i)
#     if mean_shift.labels_[i] ==0:
#         fmt = '.b'
#     else:
#         fmt = '.r'
#     plt.plot(AP[0],AP[1], fmt)

# plt.show()

#==================================================
#.*  BAYESIAN LIKELIHOOD 
#==================================================

DoE01 = pyDOE.lhs(2 , 20, 'corr')
ki_vector = np.linspace(0.0,0.8,10)
DoE = np.array([5,50]) + 0.6*DoE01 - 0.3

plt.plot(DoE01[:,0], DoE01[:,1], '.')
plt.show()


(ki_vector[:,None,None] + ki_vector[None,:,None] + ki_vector[None,None,:]).shape

(ki_vector[:,None],ki_vector[None,:])

g = meshgrid2(ki_vector, ki_vector, ki_vector, ki_vector)
positions = np.vstack(map(np.ravel, g))


lik_KAP_4D = np.empty([positions.shape[1] * 8])

for idx, DoE_ite in enumerate(DoE[12:20,:]):
    for j,k in enumerate(positions.T):
        print idx,j
        lik_KAP_4D[idx*4 + j] = J_nograd(k, DoE_ite)

dictt = {'lik': lik_KAP_4D, 'bndsLHS' : '[12:20,:]'}

output = open('lik_KAP_4D_12_20', 'wb')
pickle.dump(dictt,output)
output.close()

index_comp = ['0_2', '2_4', '4_6', '6_8', '8_12', '12_20']
index_comp = ['lik_KAP_4D_' + name for name in index_comp]

lik_TOT = np.empty([20,10000])
st = 0
for i,idx in enumerate(index_comp):
    inp = open(idx,'rb')
    dicc = pickle.load(inp)
    inp.close()
    nbLHS = dicc['lik'].shape[0]/10000
    lik_KAP_tmp = dicc['lik'].reshape(nbLHS, 10000)
    lik_TOT[st:(st+nbLHS),:] = lik_KAP_tmp
    st += nbLHS

prob_TOT = np.exp(-np.abs(lik_TOT) /swe.Nobs)

prob_TOT_array = prob_TOT.reshape([20,10,10,10,10])

marg_prob = np.mean(prob_TOT_array,0)

marg_prob[0,0,0,0]
marg_prob[5,0,0,0]

plt.subplot(2,2,1)
marg_DOE_prob = np.mean(prob_TOT_array,(2,3,4))
for i in xrange(marg_DOE_prob.shape[0]):
    plt.plot(ki_vector, marg_DOE_prob[i,:])
    
plt.subplot(2,2,2)
marg_DOE_prob = np.mean(prob_TOT_array,(1,3,4))
for i in xrange(marg_DOE_prob.shape[0]):
    plt.plot(ki_vector, marg_DOE_prob[i,:])
    
plt.subplot(2,2,3)
marg_DOE_prob = np.mean(prob_TOT_array,(1,2,4))
for i in xrange(marg_DOE_prob.shape[0]):
    plt.plot(ki_vector, marg_DOE_prob[i,:])
    
plt.subplot(2,2,4)
marg_DOE_prob = np.mean(prob_TOT_array,(1,2,3))
for i in xrange(marg_DOE_prob.shape[0]):
    plt.plot(ki_vector, marg_DOE_prob[i,:])
    
plt.show()


def meshgrid2(*arrs):
    arrs = tuple(reversed(arrs))
    lens = map(len, arrs)
    dim = len(arrs)
    sz = 1
    for s in lens:
        sz *= s
        ans = []
        for i, arr in enumerate(arrs):
            slc = [1]*dim
            slc[i] = lens[i]
            arr2 = np.asarray(arr).reshape(slc)
            for j, sz in enumerate(lens):
                if j != i:
                    arr2 = arr2.repeat(sz, axis=j)
            ans.append(arr2)
        return tuple(ans)
