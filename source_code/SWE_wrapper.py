#-*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize
from code_swe.LaxFriedrichs import LF_flux
from code_swe.direct_MLT_ADJ_LF import *
from code_swe.animation_SWE import animate_SWE
from code_swe.boundary_conditions import bcL, bcR, bcL_d, bcR_d, bcL_A, bcR_A, BCrand
from code_swe.interpolation_tools import interp_cst
import random as rnd

#%%%%%%%%%%%%%%%%%%%%%%%%%%%#
# Définition des paramètres #
#############################
# np.random.seed(1234)
D = [0, 100]  # Domaine spatial
T = 200 # Fin de la simulation
dt = 0.1  # Pas de temps
g = 9.81   # Constante de gravitation
N = 50  # Nombre de volumes
dx = np.diff(D)[0] / float(N)
xr = np.linspace(D[0] + dx/2, D[1] - dx/2, N)
eta = 7./3.

u0 = lambda x: 0
b = lambda x: 10*x/D[1]
h0 = lambda x: np.where(x > np.mean(D), 20, 20)

def J_function(h1, h2):         # Fonction coût
    return 0.5*np.sum((h1-h2)**2)
    
#def J_function_observation(h,href,ind_to_observe=None):
#    """Compute J, with the index of grid points observed"""
#    space_dim = int(h.shape[0])
#    if ind_to_observe is None:
#        ind_to_observe = range(space_dim)
#        
#    obs_mat = np.zeros((href.shape[0],href.shape[0]))
#    obs_mat[ind_to_observe,ind_to_observe] = 1
#    J = 0.5*np.sum(obs_mat.dot(h-href)**2)
#    return J,obs_mat
    
def observation_matrix_init(h_test, ind_to_observe=None):
    if ind_to_observe is None:
        ind_to_observe = range(int(h_test.shape[0]))
    obs_mat = np.zeros((h_test.shape[0], h_test.shape[0]))
    obs_mat[ind_to_observe, ind_to_observe] = 1
    return obs_mat
    
def J_function_observation_init(h_test, ind_to_observe=None):
    obs_mat = observation_matrix_init(h_test, ind_to_observe)
    J = lambda h1, h2: 0.5*np.sum(obs_mat.dot(h1-h2)**2)
    return J, obs_mat

def J_function_regul(h1, h2, alpha, K1, Kb):
    return 0.5*np.sum((h1-h2)**2) + 0.5*alpha*(K1-Kb)**2

def Froude(h, u):                # Nombre de Froude
    return np.fabs(u) / np.sqrt(g*h)

#%% Boundary conditions

mean_h = 20 
amplitude = rnd.uniform(4.9, 5.1)
period = rnd.uniform(49.9, 50.1)
phase = 0
mean_h = 20 
phase = 0
amplitude =  5.0
period =  50.0

# Ap_Pm -> A = 5.1; P = 49.8
# Ap_Pp -> A = 5.2; P = 50.1
# Am_Pp -> A = 4.9; P = 50.2
# Am_Pm -> A = 4.8; P = 49.9

print 'Hauteur eau moyenne = ', mean_h
print 'Amplitude = ', amplitude
print 'Periode = ', period
print 'Phase = ', phase
bcLref = lambda h, hu, t: BCrand(h, hu, t, 'L',
                                 mean_h, amplitude, period, phase)


#%%%%%%%%%%%%%#
# Simulations #
###############

reference = lambda K: shallow_water(D, g, T, h0, u0, N, LF_flux, dt, b, K,
                                    bcLref, bcR)
direct = lambda K: shallow_water(D, g, T, h0, u0, N, LF_flux, dt, b, K,
                                 bcL, bcR)
MLT = lambda h, u, K, dK, href: lineaire_tangent_shallow_water(D, g, T, N, dt,
                                                               b, K, dK, h, u,
                                                               href, bcL_d, bcR_d)
ADJ = lambda h, u, K, href: adjoint_shallow_water(D, g, T, N, dt, b, K, h, u,
                                                  h-href, bcL_A, bcR_A)




Kref = 0.2*(1+np.sin(2*np.pi*xr/D[1])) #
# Kref = 1.0 + 0.2*(np.sin(2*np.pi*xr/D[1]))

# Kref = 0.25
[xr, href, uref, t] = reference(Kref)

def prevision(K = Kref, amplitude = 5.0, period = 50.0, mean_h = 20.0, phase = 0.0):
    bcL = lambda h, hu, t: BCrand(h, hu, t+T, 'L',
                                  mean_h, amplitude, period, phase)
    
    return shallow_water(D, g, 2*T, href[:,-1]+b(xr), uref[:,-1], N, LF_flux, dt, b, K,
                                    bcL, bcR)

def cost_pre(K, h_pr, amplitude = 5.0, period = 50.0, mean_h = 20.0, phase = 0.0):
    bcL = lambda h, hu, t: BCrand(h, hu, T+t, 'L',
                                  mean_h, amplitude, period, phase)
    
    [xr, h, u, t] = prevision(K, amplitude, period, mean_h, phase)
    return J_function(h, h_pr)


[_, h_pr, _, _] = prevision(Kref, amplitude, period, mean_h, phase)

cost_prediction = lambda K: cost_pre(K, h_pr, amplitude, period, mean_h, phase)

[xr,href2, uref2,t] = reference(Kref*10)

if __name__ == '__main__':
    animate_SWE(xr,[href, href2],b,D, ylim = [0,30])

Nobs = np.prod(href.shape)
#idx_to_observe = range(25)
#cost_fun,obs_mat = J_function_observation_init(href, idx_to_observe)

def J_grad(K):
    """ Effectue une simulation directe, puis utilise la méthode adjointe pour calculer le gradient. \n 
    Renvoie la valeur de la fonction coût, ainsi que le gradient en K    
    """
    return shallow_water_RSS_grad(D, g, T, h0, u0, N, LF_flux, dt, b, K,
                                  bcL, bcR, bcL_A, bcR_A, href, J_function)

def J_grad_AP(K, ampli, period):
    bcL = lambda h, hu, t: BCrand(h, hu, t, 'L',
                                 mean_h, ampli, period, phase)
    return shallow_water_RSS_grad(D, g, T, h0, u0, N, LF_flux, dt, b, K,
                                  bcL, bcR, bcL_A, bcR_A, href, J_function)

def J_grad_AP_href(K, ampli, period, hr):
    bcL = lambda h, hu, t: BCrand(h, hu, t, 'L',
                                 mean_h, ampli, period, phase)
    return shallow_water_RSS_grad(D, g, T, h0, u0, N, LF_flux, dt, b, K,
                                  bcL, bcR, bcL_A, bcR_A, hr, J_function)


def J_naive(K):
    """ Calcul RSS (sans gradient) entre href et simulation avec paramètre K """
    return shallow_water_RSS(D, g, T, h0, u0, N, LF_flux, dt, b, K,
                             bcL, bcR, href, J_function)


def J_obs_grad_base(K, cost_fun, obs_mat):
    """Calcul RSS avec gradient, prenant en arg les observations faites """
    return shallow_water_RSS_grad_observation(D, g, T, h0, u0, N, LF_flux, dt,
                                              b, K, bcL, bcR, bcL_A, bcR_A,
                                              href, cost_fun, obs_mat)

def J_grad_observed(K,idx_to_observe):
    """Calcul RSS selon index observés"""
    cost_fun,obs_mat = J_function_observation_init(href, idx_to_observe)
    return shallow_water_RSS_grad_observation(D, g, T, h0, u0, N, LF_flux, dt,
                                              b, K, bcL, bcR, bcL_A, bcR_A,
                                              href, cost_fun, obs_mat)

    
def J_grad_scalar(listofK):
    """K scalaire pour effectuer calculs en batch"""
    Jvec = 0.0*np.ones_like(listofK)
    gradvec = 0.0*np.ones_like(listofK)
    
    for i,K in enumerate(listofK):
        Jvec[i],gradvec[i] = J_grad(K)
    return [Jvec]+[gradvec]
    
    
@np.vectorize
def likelihood(K,ampli = amplitude, period = period):
    # bcL = lambda h, hu, t: BCrand(h, hu, t, 'L',
    #                              mean_h, ampli, period, phase)
    negloglik = shallow_water_RSS(D, g, T, h0, u0, N, LF_flux, dt, b, K,
                                  lambda h, hu, t: BCrand(h, hu, t, 'L',
                                                          mean_h, ampli, period, phase),                            
                                  bcR, href, J_function)/Nobs
    return np.exp(-negloglik)


def interp(coef_array):
    """Interpolation using piecewiste constant values"""
    coef_array=np.array(coef_array)
    D_length = float(np.diff(D)[0])
    cell_vol = D_length/coef_array.size
    pts = np.linspace(cell_vol/2.0, D_length- cell_vol/2.0,
                      num = coef_array.size)
    f_to_ret = lambda x: interp_cst(x, cell_vol, coef_array, pts)
    return f_to_ret


def J_piecewise_grad(Kcoeff):
    """Piecewise constant interpolation of K, then computes cost function and gradient"""
    K_transform = interp(Kcoeff)
    K = np.array(map(K_transform, xr))
    cost, grad =  J_grad(K)
    grad_sum_length = int(50.0/Kcoeff.size)
    grad_coeff = np.zeros(Kcoeff.size)
    for i in range(Kcoeff.size):
        grad_coeff[i] = sum(grad[i*grad_sum_length:(i*grad_sum_length+grad_sum_length)])
    return cost,grad_coeff

