#!/usr/bin/env python
# -*- coding: utf-8 -*-


from netCDF4 import Dataset
import os
import numpy as np
import matplotlib.pyplot as plt


MATLAB_PATH = '/home/victor/MATLAB/R2017b/bin/matlab -nojvm -nodisplay -nosplash -r'
OCTAVE_PATH = 'octave --no-gui -HW'

CROCO_TOOLS_dir = '/home/victor/croco_tools/'
# nc_obs = Dataset("/home/victor/croco/Run/CROCO_FILES/croco_obs_tide.nc", "r", format="NETCDF4")
nc_obs = Dataset("/home/victor/croco/Run/CROCO_FILES/croco_rst_observation.nc",
                 "r", format="NETCDF4")
zeta_1 = np.array(nc_obs.variables['zeta'])

# his0 = Dataset("/home/victor/croco/Run/CROCO_FILES/croco_his_est.nc", "r", format="NETCDF4")
# z0 = np.array(his0.variables['zeta'])
# his1 = Dataset("/home/victor/croco/Run/CROCO_FILES/croco_frc_1.nc", "r", format="NETCDF4")
# z1 = np.array(his1.variables['tide_Pamp'])
# np.sum((z0-zeta_1)**2)

# Zob_in = range(21) * 1e-5
# Z = 20
# Zob = '{:2d}'.format(Z) + '.d-6' ;Zob


def compile_croco():
    os.chdir('/home/victor/croco/Run')
    os.system('../OCEAN/jobcomp')


def change_number_of_tides(Ntides=3, croco_compile=True, frcname = 'croco_frc_M2S2K1.nc'):
    with open('/home/victor/croco/Run/param.h', 'r') as paramfile:
        parameters = paramfile.read().split('\n')
    index_of_tides = [i for i in xrange(len(parameters))
                      if parameters[i] == '# elif defined(FRICTION_TIDES)']
    parameters[index_of_tides[0] + 1] = '      parameter (Ntides={}) '.format(int(Ntides))
    with open('/home/victor/croco/Run/param.h', 'w') as paramfile:
        paramfile.write('\n'.join(parameters))


    with open('/home/victor/croco/AD/CONFIGS/ATLN/crocotools_param.m', 'r') as croparam:
        cromatlab = croparam.read().split('\n')
    index_of_tides = [i for i in xrange(len(cromatlab))
                      if cromatlab[i].startswith('Ntides=')]
    index_name_frc = [i for i in xrange(len(cromatlab))
                      if cromatlab[i].startswith('frcname  =')]
    cromatlab[index_of_tides[0]] = 'Ntides={};'.format(int(Ntides))
    cromatlab[index_name_frc[0]] = "frcname  = [CROCO_files_dir,'{}'];".format(str(frcname))
    with open('/home/victor/croco/AD/CONFIGS/ATLN/crocotools_param.m', 'w') as croparam:
        croparam.write('\n'.join(cromatlab))
    if croco_compile:
        compile_croco()


def generate_in_Zob(Z, in_file_name):
    """ Obsolete """
    Zob = '{:2d}'.format(Z) + 'd-6'
    Zob = '{:.2e}'.format(Z).replace('e', 'd')
    f_to_read = open("/home/victor/croco/Run/in_files/croco-est-atln-dummy.in", 'r')
    f1 = f_to_read.readlines()
    bottom_drag = f1[59]
    new_bottom_drag = bottom_drag[:39] + Zob + bottom_drag[45:]
    f1[59] = new_bottom_drag
    f_to_read.close()
    f_to_write = open(in_file_name, 'w')
    for line in f1:
        f_to_write.write(line)
    f_to_write.close()


def generate_in(in_file_name,
                RDRG=1e-4,
                RDRG2=0.0e-2,
                Zob=1e-5,
                Cdb_min=1e-4,
                Cdb_max=1e-1,
                frcname = 'croco_frc_M2S2K1.nc'):
    RDRG_str = '{:.2e}'.format(RDRG).replace('e', 'd')
    RDRG2_str = '{:.2e}'.format(RDRG2).replace('e', 'd')
    Zob_str = '{:.2e}'.format(Zob).replace('e', 'd')
    Cdb_min_str = '{:.2e}'.format(Cdb_min).replace('e', 'd')
    Cdb_max_str = '{:.2e}'.format(Cdb_max).replace('e', 'd')
    str_to_replace = ' ' * 16 + (' ' * 4).join([RDRG_str,
                                                RDRG2_str,
                                                Zob_str,
                                                Cdb_min_str,
                                                Cdb_max_str]) + '\n'
    # with open("/home/victor/croco/Run/in_files/croco-est-atln-dummy.in", 'r') as f_to_read:
    with open("/home/victor/croco/Run/in_files/croco-est-atln-dummy-rst.in", 'r') as f_to_read:

        f1 = f_to_read.readlines()
    index_btmdrag = [i for i in xrange(len(f1))
                     if f1[i].startswith('bottom_drag')]
    index_frc = [i for i in xrange(len(f1))
                 if f1[i].startswith('forcing:')]
    f1[index_btmdrag[0] + 1] = str_to_replace
    f1[index_frc[0] + 1] = ' ' * 26 + 'CROCO_FILES/' + frcname + '\n'
    with open(in_file_name, 'w') as f_to_write:
        for line in f1:
            f_to_write.write(line)

    # amplitudes and elasticity factors for:
    #     M2 S2 N2 K2 K1 O1 P1 Q1 Mf Mm


def generate_make_tides_u(u_val):
    with open('/home/victor/croco_tools/Tides/make_tides_dummy.m', 'r') as make_tides:
        rl = make_tides.readlines()
        rl[87] = 'u={:1.15f}\r\n'.format(u_val)
        write_tide = open('/home/victor/croco_tools/Tides/make_tides_u.m', 'w')
        for line in rl:
            write_tide.write(line)
        write_tide.close()


def make_frc_u(u, PATH):
    print '---- Generation of the .m file'
    generate_make_tides_u(u)
    print '---- Generation of croco_frc_tide.nc'
    os.system((PATH + ' \'' +
               'run ' + CROCO_TOOLS_dir + 'Preprocessing_tools/make_forcing_tide.m;' +
               'disp(\"-> make_tides_u.m \");' +
               'run ' + CROCO_TOOLS_dir + 'Tides/make_tides_u.m;' +
               # 'disp(\"-> make_bulk.m \");' + # A priori inutile
               # 'evalc(char(\"run ' + CROCO_TOOLS_dir + 'Preprocessing_tools/make_bulk.m\"));' +
               'disp(\"-> make_clim.m \");' +
               'evalc(char(\"run ' + CROCO_TOOLS_dir + 'Preprocessing_tools/make_clim.m\"));' +
               # 'disp(\"-> make_bry.m \");' + # a priori inutile
               # 'evalc(char(\"run ' + CROCO_TOOLS_dir + 'Preprocessing_tools/make_bry.m\"));' +
               'quit();' +
               '\' '))

# make_frc_u(0.5)

# J = np.empty(21)
# for i in xrange(1, 21, 1):
#     in_file_name = "/home/victor/croco/Run/in_files/croco-est-atln-" + str(i) + ".in"
#     print in_file_name
#     os.system('ln -sf ' + in_file_name + ' croco-est-atln.in')
#     os.system('./croco croco-est-atln.in | tee log_py | grep MAIN')
#     nc2 = Dataset("/home/victor/croco/Run/CROCO_FILES/croco_his_est.nc", "r", format="NETCDF4")
#     zeta_2 = np.array(nc2.variables['zeta'])
#     J[i] = np.sum((zeta_1 - zeta_2)**2)

# for i in xrange(21):
#     Zob = '{:2d}'.format(i) + '.d-5'
#     Zob_in[i] = i
#     in_file_name = "/home/victor/croco/Run/in_files/croco-est-atln-" + str(i) + ".in"
#     # shutil.copy2("/home/victor/croco/Run/in_files/croco-est-atln.in", in_file_name)
#     f_to_read = open("/home/victor/croco/Run/in_files/croco-est-atln-dummy.in", 'r')
#     f1 = f_to_read.readlines()
#     bottom_drag = f1[59]
#     new_bottom_drag = bottom_drag[:39] + Zob + bottom_drag[45:]
#     f1[59] = new_bottom_drag
#     f_to_read.close()
#     f_to_write = open(in_file_name, 'w')
#     for line in f1:
#         f_to_write.write(line)
#     f_to_write.close()



def black_box_croco(Z, u, frcname='croco_frc_M2S2K1.nc', PATH=MATLAB_PATH):
    os.chdir('/home/victor/croco/Run')
    in_file_name = '/home/victor/croco/Run/in_files/croco-est-atln-py.in'
    print '---- Generation of the .in file: ' + in_file_name
    generate_in(in_file_name, Zob=Z, frcname=frcname)
    make_frc_u(u, PATH)
    os.system('ln -sf ' + in_file_name + ' croco-from-python.in')
    os.system('./croco croco-from-python.in | tee log_py | grep MAIN')
    nc2 = Dataset("/home/victor/croco/Run/CROCO_FILES/croco_rst_simulation.nc", "r",
                  format="NETCDF4")
    zeta_2 = np.array(nc2.variables['zeta'])
    return np.mean((zeta_1 - zeta_2)**2)







def black_box_croco_on_combinations(combinations, frcname='croco_frc_M2S2K1.nc'):
    J = np.empty(len(combinations))
    for i, Zu in enumerate(combinations):
        print Zu
        J[i] = black_box_croco(Zu[0], Zu[1], frcname)
        print J[i]
    os.system('spd-say "computations over"')
    return J


if __name__ == '__main__':
    change_number_of_tides(Ntides=10)
    black_box_croco(1e-5, 1, PATH=MATLAB_PATH)  # Generation of the observation
    change_number_of_tides(Ntides=10, croco_compile=False)
    J = black_box_croco_on_combinations(np.vstack([np.ones(2) * 1e-5,
                                                   [0, 1.0]]).T, frcname='croco_frc_M2S2K1.nc')

    black_box_croco(1e-5, 0.5)
    black_box_croco(1e-5, 0.3)
    black_box_croco(1e-5, 0.)

    black_box_croco(0.5e-5, 0.5)  # 83413.45611474692

    Zob_v = np.linspace(5e-6, 2e-4, 3); Zob_v
    u_v = np.linspace(0, 1, 3);u_v
    J = black_box_croco_on_combinations(np.array([np.meshgrid(Zob_v, u_v)]).T.reshape(-1, 2))
    # np.save('/home/victor/These/source_code/croco_py/J_3x3.npy', J)
    J = np.load('/home/victor/These/source_code/croco_py/J_5x6.npy')
    Npts = 50
    Z_v = np.linspace(1, 50, Npts)
    u_v = np.linspace(0, 1, 17)

    u_v_h = np.diff(u_v) / 2 + u_v[:-1]
    Z_v = np.linspace(26, 50, 25)
    combinations = np.array([np.meshgrid(Z_v, u_v_h)]).T.reshape(-1, 2)
    J_M2S2K1_n = black_box_croco_on_combinations(combinations[-20:])
    np.save('/home/victor/croco/Computation_M2S2K1/J_M2S2K1_26_50_9h_0.npy', J_M2S2K1_n)
    plt.plot(J_M2S2K1_n)
    plt.show()

    J_M2S2K1_00_25_9h = np.load('/home/victor/croco/Computation_M2S2K1/J_M2S2K1_25_9h.npy')
    J_M2S2K1_26_50_9h = np.load('/home/victor/croco/Computation_M2S2K1/J_M2S2K1_26_50_9h.npy')
    J_M2S2K1_50_9h = np.append(J_M2S2K1_00_25_9h, J_M2S2K1_26_50_9h)

    J_M2S2K1_50_9 = np.load('/home/victor/croco/Computation_M2S2K1/J_M2S2K1_50_9.npy')
    J_M2S2K1_50_9h = np.load('/home/victor/croco/Computation_M2S2K1/J_M2S2K1_50_9h.npy')
    J_M2S2K1_50_9.shape
    J_M2S2K1_50_9h.shape

    J_M2S2K1_50_17 = np.empty([50, 17])
    for i in xrange(50):
        J_M2S2K1_50_17[i, [0, 2, 4, 6, 8, 10, 12, 14, 16]] = J_M2S2K1_50_9[i, :]
        J_M2S2K1_50_17[i, [1, 3, 5, 7, 9, 11, 13, 15]] = J_M2S2K1_50_9h[i, :]

    J_M2S2K1 = np.load('/home/victor/croco/Computation_M2S2K1/J_M2S2K1_50_17.npy')

    u_v = np.linspace(0, 1, 17)
    Z_v = np.linspace(1, 50, 50)
    Z_mg, u_mg = np.meshgrid(Z_v, u_v)
    plt.contour(Z_mg, u_mg, J_M2S2K1_50_17.T, 200)
    plt.show()

    # filenames_temp = ['/home/victor/croco/Computation_M2S2K1/J_M2S2K1_26_50_9h_80.npy',
    #                   '/home/victor/croco/Computation_M2S2K1/J_M2S2K1_26_50_9h_60.npy',
    #                   '/home/victor/croco/Computation_M2S2K1/J_M2S2K1_26_50_9h_30.npy',
    #                   '/home/victor/croco/Computation_M2S2K1/J_M2S2K1_26_50_9h_20.npy',
    #                   '/home/victor/croco/Computation_M2S2K1/J_M2S2K1_26_50_9h_80_20.npy']
    # J = []
    # for files in filenames_temp:
    #     print files
    #     JJ = np.load(files)
    #     J = np.append(J, JJ)

    # np.save('/home/victor/croco/Computation_M2S2K1/J_M2S2K1_25_9h.npy', J_M2S2K1.reshape(25, 8))
    # J_M2S2K1 = np.load('/home/victor/croco/Computation_M2S2K1/J_M2S2K1_25_9h.npy')
    # plt.plot(J_M2S2K1.reshape(25, 8))
    # plt.show()

    # J_M2S2K1 = np.load('/home/victor/croco/Computation_M2S2K1/J_M2S2K1_50_9.npy')
    # plt.contour(J_M2S2K1)
    Z_star = Z_v[[J_M2S2K1[:, uu].argmin() for uu in xrange(len(u_v))]]


    plt.subplot(2, 1, 1)
    plt.contour(Z_v, u_v, J_M2S2K1.T, 50)
    plt.plot(Z_star, u_v, 'r.')

    ax = plt.subplot(2, 1, 2)
    ax.plot(Z_v, np.mean(J_M2S2K1, 1), color = 'magenta')
    axstd = ax.twinx()
    axstd.plot(Z_v, np.std(J_M2S2K1, 1), color = 'limegreen')
    plt.tight_layout()
    plt.show()





    J_tot = np.load('/home/victor/croco/Computation_M2S2J_Zu.npy')
    # np.save('/home/victor/croco/J_Zu.npy', J_tot)
    plt.contourf(J.reshape(10, 5));plt.show()

    Jbis = black_box_croco_on_combinations(combinations[-8:])
    Jnew = J
    Jnew[-8:] = Jbis
    # np.save('/home/victor/croco/J_Zu.npy', Jnew)


    def dummy_fun(combinations):
        dJ = np.empty(len(combinations))
        for i, Zu in enumerate(combinations):
            print Zu
            dJ[i] = np.sum(Zu)
            print dJ[i]
        return dJ


    dJ = dummy_fun(combinations)
    dJ.reshape(10, 5)
    plt.imshow(Jnew.reshape(10, 5))
    plt.show()
# 85632.81165550595 -> 50, 1
# 88009.96369991673 -> 90, 1
# 88679.08235691156 -> 101, 1

# EOF ------------------------------------------------------------------------------
