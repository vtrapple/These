#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import scipy.optimize as scopt
import matplotlib.pyplot as plt

from horsetailmatching import HorsetailMatching, UniformParameter

u = UniformParameter(lower_bound=0, upper_bound=5)

import ROproblem

def branin_hoo(x, y):
    x1 = 3 * x - 5
    x2 = 3 * y
    damp = 1.0
    quad = (x2 - (5.1 / (4 * np.pi**2)) * x1**2 + (5 / np.pi) * x1 - 6)**2
    cosi = (10 - (10 / np.pi * 8)) * np.cos(x1) - 44.81
    return (quad + cosi) / (51.95 * damp) + 2.0


theHM = HorsetailMatching(branin_hoo, [u], ftarget=lambda x: 0)


print(theHM.evalMetric(x=1))
solution = scopt.minimize(theHM.evalMetric, x0=[2.5], method='Nelder-Mead')


(x1, y1, t1), (x2, y2, t2), _ = theHM.getHorsetail()
plt.plot(x1, y1, 'b', label='CDF')
plt.plot(t1, y1, 'k--', label='Target')
plt.xlim([-1, 6])
plt.xlabel('Quantity of Interest')
plt.legend(loc='lower right')
plt.show()
