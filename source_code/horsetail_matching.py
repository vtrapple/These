#!/usr/bin/env python
# coding: utf-8

import numpy as np
import scipy
from matplotlib import cm
import matplotlib.pyplot as plt
from statsmodels.distributions.empirical_distribution import ECDF
import scipy.optimize


# -------------------------------------------------------------------------------
def branin_hoo(X, damp = 1.0, switch = False):
    y, x = X[:, 1], X[:, 0]
    if switch:
        x, y = y, x
    x2 = 3 * y
    x1 = 3 * x - 5

    quad = (x2 - (5.1 / (4 * np.pi**2)) * x1**2 + (5 / np.pi) * x1 - 6)**2
    cosi = (10 - (10 / np.pi * 8)) * np.cos(x1) - 44.81
    return (quad + cosi) / (51.95 * damp) + 2.0


Npts = 1000
NptsK, NptsU = Npts, Npts
xx = np.linspace(0, 5, NptsK)
yy = np.linspace(0, 5, NptsU)
X_, Y_ = np.meshgrid(xx, yy, indexing = 'ij')
comb = np.array([X_, Y_]).T.reshape(-1, 2)
branin = lambda X: branin_hoo(X, switch=True)
y_fun = branin(comb).reshape(Npts, Npts)


# -------------------------------------------------------------------------------
def compute_all_cdfs(y_fun, index_min, plot_curves=True):
    mmax = np.ceil(y_fun.max())
    mmin = np.floor(y_fun.min())
    val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(len(y_fun[0, :]))])
    range_fun = np.linspace(mmin, mmax, 500)
    NptsK, NptsU = y_fun.shape
    i_mean = np.mean(y_fun, 1).argmin()
    i_var = np.var(y_fun, 1).argmin()
    print i_mean, i_var
    for i in xrange(NptsK):
        color = 'gray'
        alpha = 0.01
        lw = 1
        if i == i_mean:
            color = 'magenta'
            alpha = 0.5
            lw = 2
        elif i == i_var:
            color = 'limegreen'
            alpha = 0.5
            lw = 2
        elif i == index_min:
            color = 'cyan'
            alpha = 1
            lw = 2
        ecdf = ECDF(y_fun[i, :])
        plt.plot(range_fun, ecdf(range_fun), alpha=alpha, color=color, lw=lw)
    ecdf_star = ECDF(val_star_2d_true)
    plt.plot(range_fun, ecdf_star(range_fun), alpha=1, color='k', lw=2)


# -------------------------------------------------------------------------------
def distance_to_target(ecdf, delta, p_norm=2):
    """
    Approximated integral using rectangular
    """
    Npts = len(ecdf.x) - 1
    difference = (ecdf.x[1:] - delta)**p_norm
    if p_norm == 1:
        difference = np.abs(difference)
    sum_of_diff = np.sum(difference) / Npts
    distance = sum_of_diff**(1.0 / p_norm)
    return distance


# -------------------------------------------------------------------------------
def find_best_design_delta(y_fun, delta, p_norm=2):
    bestdistance = np.inf
    index_best_target = -1
    NptsK, NptsU = y_fun.shape
    for i in xrange(NptsK):
        ecdf = ECDF(y_fun[i, :])
        dhm = distance_to_target(ecdf, delta, p_norm)
        if dhm < bestdistance:
            bestdistance = dhm
            index_best_target = i
    return bestdistance, index_best_target


# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------

Ndim = 2
Npts = 2000
xx = np.linspace(-0.05, 0.05, Npts)
yy = np.linspace(-0.05, 0.05, Npts)
X_, Y_ = np.meshgrid(xx, yy, indexing = 'ij')
NptsK, NptsU = X_.shape
comb = np.array([X_, Y_]).T.reshape(-1, 2)
y_fun = np.load('/home/victor/These/Bayesian_SWE/lorenz/xb_y_fun_2000_allobs.npy')





mmax, mmin = np.ceil(y_fun.max()), np.floor(y_fun.min())
_, index_min = find_best_design_delta(y_fun, 0)
plt.subplot(2, 1, 1)
plt.contourf(X_, Y_, y_fun, 10)
plt.axvline(xx[index_min], color = 'cyan')
plt.axvline(xx[np.mean(y_fun, 1).argmin()], color = 'magenta')
plt.axvline(xx[np.var(y_fun, 1).argmin()], color = 'limegreen')

plt.subplot(2, 1, 2)
compute_all_cdfs(y_fun, index_min)
plt.axvline(mmin, color = 'orange')
plt.show()


val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(len(y_fun[0, :]))])
(dhm, index_min) = find_best_design_delta(y_fun, delta=ECDF(val_star_2d_true).x[1:], p_norm=2)
print index_min
compute_all_cdfs(y_fun, index_min=index_min, plot_curves=True)
plt.show()


def test_minimum(delta, p_norm):
    d, ind = find_best_design_delta(y_fun, delta, p_norm)
    return d


scipy.optimize.minimize(test_minimum, x0=35, args=(2))
np.mean(y_fun, 1)[np.var(y_fun, 1).argmin()]


scipy.optimize.minimize(test_minimum, x0=43, args=(1))
np.mean(y_fun, 1).min()







dlin = np.linspace(37, 65, 200)
distlin = np.empty_like(y_fun[:, 0])
for i in xrange(y_fun.shape[0]):
    distlin[i] = distance_to_target(ECDF(y_fun[i, :]), 0)
plt.plot(distlin); plt.show()
