#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -- SIMULATION WITH T = 50 (FAST) -----------------------------------------------

import haute_resolution_fast.wrapper_HR as swe
from haute_resolution_fast.wrapper_HR import J_KAP_array
from bayesian_optimization_VT.bo_wrapper import PEI_algo
# from code_swe.animation_SWE import animate_SWE
import pyDOE
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize
# -- Paramètres de référence ------------------------------------------------------
print 'Hauteur eau moyenne = ', swe.mean_h
print 'Amplitude = ', swe.amplitude
print 'Periode = ', swe.period
print 'Phase = ', swe.phase
bcLref = lambda h, hu, t: swe.BCrand(h, hu, t, 'L',
                                     swe.mean_h, swe.amplitude,
                                     swe.period, swe.phase)

# [xr, href, uref, t] = swe.swe_KAP(swe.Kref, 5.0, 15.0)
# [xr, hrefApPm, urefApPm, t] = swe.swe_KAP(swe.Kref, 5.1, 14.8)
# [xr, hrefApPp, urefApPp, t] = swe.swe_KAP(swe.Kref, 5.2, 15.1)
# [xr, hrefAmPp, urefAmPp, t] = swe.swe_KAP(swe.Kref, 4.9, 15.2)
# [xr, hrefAmPm, urefAmPm, t] = swe.swe_KAP(swe.Kref, 4.8, 14.9)
[xr, hrefPpp, _, t] = swe.swe_KAP(swe.Kref, 5.0, 15.2)
[xr, hrefPm, _, t] = swe.swe_KAP(swe.Kref, 5.0, 14.9)


# np.save(('/home/victor/These/Bayesian_SWE/haute_resolution_fast/'
#          '/href.npy'), href)
# np.save(('/home/victor/These/Bayesian_SWE/haute_resolution_fast/'
#          '/hrefApPm.npy'), hrefApPm)
# np.save(('/home/victor/These/Bayesian_SWE/haute_resolution_fast/'
#          '/hrefApPp.npy'), hrefApPp)
# np.save(('/home/victor/These/Bayesian_SWE/haute_resolution_fast/'
#          '/hrefAmPp.npy'), hrefAmPp)
# np.save(('/home/victor/These/Bayesian_SWE/haute_resolution_fast/'
#          '/hrefAmPm.npy'), hrefAmPm)

href = np.load(('/home/victor/These/Bayesian_SWE/haute_resolution_fast/'
                'href.npy'))
hrefApPm = np.load(('/home/victor/These/Bayesian_SWE/haute_resolution_fast/href'
                    'ApPm.npy'))
hrefApPp = np.load(('/home/victor/These/Bayesian_SWE/haute_resolution_fast/href'
                    'ApPp.npy'))
hrefAmPp = np.load(('/home/victor/These/Bayesian_SWE/haute_resolution_fast/href'
                    'AmPp.npy'))
hrefAmPm = np.load(('/home/victor/These/Bayesian_SWE/haute_resolution_fast/href'
                    'AmPm.npy'))

# swe.J_KAP_pw_obs(swe.Kref, A = swe.amplitude, P = swe.period,
#                       idx_to_observe = np.arange(swe.href.shape[0]), hreference = hrefApPm)

# doit être égal à 0, [0.,...,0.]


idx_to_observe = np.arange(49, 200, 50, dtype = int)
# = array([ 49,  99, 149, 199])

# rng = np.random.RandomState()
# bounds = np.array([[0, 1.0],       # for K
#                    [4.7, 5.3]])     # for A


# base01vector_big = np.linspace(0, 1, 50)
# Kbase = swe.inv_transformation_variable_to_unit(base01vector_big, np.atleast_2d(bounds[0, :]))
# Abase = swe.inv_transformation_variable_to_unit(base01vector_big, np.atleast_2d(bounds[1, :]))
# Kmesh, Amesh = np.meshgrid(Kbase, Abase)
# combinationsKA = np.array([Kmesh, Amesh]).T.reshape(-1, 2)

# Pconstant = 15.0 * np.ones([combinationsKA.shape[0], 1])
# combinations_to_compute = np.append(combinationsKA, Pconstant, axis = 1)

# full_output_Pconstant = J_KAP_array(combinations_to_compute, idx_to_observe, swe.href,
#                                     parallel=True)

# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/full_output_Pconstant.npy'), full_output_Pconstant)

# ---------------------------------------------------------------------

bounds = np.array([[0, 1.],       # for K
                   [14.7, 15.3]])     # for P


Kbase = swe.inv_transformation_variable_to_unit(np.linspace(0, 1, 150), np.atleast_2d(bounds[0, :]))
Pbase = swe.inv_transformation_variable_to_unit(np.linspace(0, 1, 150), np.atleast_2d(bounds[1, :]))
# Pbase = scipy.stats.uniform.rvs(size = 150, loc = 14.7, scale = 0.6)  #

Kmesh, Pmesh = np.meshgrid(Kbase, Pbase)
combinationsKP = np.array([Kmesh, Pmesh]).T.reshape(-1, 2)

Aconstant = 5.01 * np.ones(combinationsKP.shape[0])
combinations_to_compute = np.vstack([combinationsKP[:, 0], Aconstant, combinationsKP[:, 1]]).T

full_output_Aconstant = swe.J_KAP_array(combinations_to_compute, swe.idx_to_observe, hrefAmPm,
                                        parallel=True)

np.save(('/home/victor/These/Bayesian_SWE/haute_resolution_fast/'
         '/full_output_Aconstant_fast_AmPm.npy'), full_output_Aconstant)


# full_output_Pconstant = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
#                                  '/full_output_Pconstant.npy')).reshape(50, 50).T
full_output_Aconstant = np.load(('/home/victor/These/Bayesian_SWE/haute_resolution_fast'
                                 '/full_output_Aconstant_fast_Am.npy')).reshape(150, 150).T

# plt.subplot(1, 2, 1)
# plt.contour(Kbase, Abase, full_output_Pconstant, 200)
# plt.xlabel(r'$\mathbf{k}$')
# plt.ylabel(r'$\mathbf{A}$')
# plt.subplot(1, 2, 2)



plt.contourf(Kbase, Pbase, full_output_Aconstant.reshape(150, 150).T, 200)
plt.xlabel(r'$\mathbf{k}$')
plt.ylabel(r'$\mathbf{P}$')
plt.show()

#  OPTIMISATION WITH WRONGS INFO



dimK = 1
print 'href'
opt = scipy.optimize.minimize(fun = swe.J_KAP, x0 = 0.1 * np.ones(dimK),
                              args = (swe.amplitude,
                                      swe.period,
                                      idx_to_observe, href),
                              jac = True,  # U = uref, N_obs = 8
                              bounds = dimK * [(0.0, None)],
                              options = {'maxiter': 100})
print 'AmPm'
opt_AmPm = scipy.optimize.minimize(fun = swe.J_KAP, x0 = 0.1 * np.ones(dimK),
                                   args = (swe.amplitude,
                                           swe.period,
                                           idx_to_observe, hrefAmPm),
                                   jac = True,  # U = uref, N_obs = 8
                                   bounds = dimK * [(0.0, None)],
                                   options = {'maxiter': 100})
print 'AmPp'
opt_AmPp = scipy.optimize.minimize(fun = swe.J_KAP, x0 = 0.1 * np.ones(dimK),
                                   args = (swe.amplitude,
                                           swe.period,
                                           idx_to_observe, hrefAmPp),
                                   jac = True,  # U = uref, N_obs = 8
                                   bounds = dimK * [(0.0, None)],
                                   options = {'maxiter': 100})
print 'ApPm'
opt_ApPm = scipy.optimize.minimize(fun = swe.J_KAP, x0 = 0.1 * np.ones(dimK),
                                   args = (swe.amplitude,
                                           swe.period,
                                           idx_to_observe, hrefApPm),
                                   jac = True,  # U = uref, N_obs = 8
                                   bounds = dimK * [(0.0, None)],
                                   options = {'maxiter': 100})
print 'ApPp'
opt_ApPp = scipy.optimize.minimize(fun = swe.J_KAP,
                                   x0 = 0.1 * np.ones(dimK),
                                   args = (swe.amplitude,
                                           swe.period,
                                           idx_to_observe, hrefApPp),
                                   jac = True,  # U = uref, N_obs = 8
                                   bounds = dimK * [(0.0, None)],
                                   options = {'maxiter': 100})
print 'Pp'
opt_Pp = scipy.optimize.minimize(fun = swe.J_KAP,
                                 x0 = 0.1 * np.ones(dimK),
                                 args = (swe.amplitude,
                                         swe.period,
                                         idx_to_observe, hrefPpp),
                                 jac = True,  # U = uref, N_obs = 8
                                 bounds = dimK * [(0.0, None)],
                                 options = {'maxiter': 100})
print 'Pp'
opt_Pm = scipy.optimize.minimize(fun = swe.J_KAP,
                                 x0 = 0.1 * np.ones(dimK),
                                 args = (swe.amplitude,
                                         swe.period,
                                         idx_to_observe, hrefPm),
                                 jac = True,  # U = uref, N_obs = 8
                                 bounds = dimK * [(0.0, None)],
                                 options = {'maxiter': 100})


opt
opt_AmPm
opt_AmPp
opt_ApPm
opt_ApPp
opt_Pp
opt_Pm

opt.x
opt_AmPm.x
opt_AmPp.x
opt_ApPm.x
opt_ApPp.x
opt_Pp.x
opt_Pm.x

from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Matern

initial_design_2D = swe.inv_transformation_variable_to_unit(
    pyDOE.lhs(n = 2, samples = 30,
              criterion = 'maximin',
              iterations=100),
    bounds)


def fix_A_to_5(KP):
    KP = np.atleast_2d(KP)
    return np.vstack([KP[:, 0], 5.0 * np.ones(len(KP)), KP[:, 1]]).T



def true_function(KP, hreference=swe.href):
    KAP = fix_A_to_5(KP)
    return swe.J_KAP_array(KAP, swe.idx_to_observe, hreference,
                           parallel=False)

initial_runs_2D = true_function(initial_design_2D)


gpSWE = GaussianProcessRegressor(kernel = Matern([1, 1]), n_restarts_optimizer = 1000)
gpSWE.fit(initial_design_2D, initial_runs_2D)


gpSWE_PEI = PEI_algo(gpSWE_PEI, true_function, idx_U=[1],
                     X_=None, niterations=100, plot=False, nrestart=20,
                     bounds=bounds)
plt.plot(gpSWE_PEI.X_train_[:30, 0], gpSWE_PEI.X_train_[:30, 1], '.')
plt.plot(gpSWE_PEI.X_train_[30:, 0], gpSWE_PEI.X_train_[30:, 1], 'r.')

plt.show()
# EOF ----------------------------------------------------------------
