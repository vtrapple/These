#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

N = 100
dt = 0.05
sigma = 10.0
rho = 28.0
beta = 8.0 / 3.0


# ------------------------------------------------------------------------------
def update(x, y, z, sigma, rho, beta, dt):
    dxdt = (sigma * dt / 2) * (2 * (y - x) +
                               dt * (rho * x - y - x * z))
    dydt = (dt / 2) * (rho * x - y - x * z +
                       rho * (x + sigma * dt * (y - x)) -
                       y - dt * (rho * x - y - x * z) -
                       (x + sigma * dt * (y - x)) * (z + dt * (x * y - beta * z)))
    dzdt = (dt / 2) * (x * y - beta * z +
                       (x + dt * sigma * (y - x)) * (y + dt * (rho * x - y - x * z)) -
                       beta * z - dt * (x * y - beta * z))
    xnew = x + dxdt
    ynew = y + dydt
    znew = z + dzdt
    return xnew, ynew, znew


# ------------------------------------------------------------------------------
def simulation(init, srb, dt, Niter):
    i = 0
    x = np.empty(Niter)
    y = np.empty(Niter)
    z = np.empty(Niter)
    x[0], y[0], z[0] = init
    while i < Niter - 1:
        xnew, ynew, znew = update(x[i], y[i], z[i], srb[0], srb[1], srb[2], dt)
        x[i + 1] = xnew
        y[i + 1] = ynew
        z[i + 1] = znew
        i += 1
    return x, y, z


x, y, z = simulation([1, 0, -0], [sigma, rho + 0.5, beta], dt, N)

x, y, z = simulation([1, 0, -0], [sigma, rho, beta], dt, N)


# ------------------------------------------------------------------------------
def cost_function_lorenz(x, xref_sim, yzsrb, dt, Niter, time_observed=None):
    # cost = np.empty(len(x))
    # for i, xiter in enumerate(x):
    if time_observed is None:
        obs_arr = 1
    else:
        obs_arr = np.zeros(Niter)
        obs_arr[time_observed] = 1
    x_, y_, z_ = simulation([x, yzsrb[0], yzsrb[1]], yzsrb[2:], dt, Niter)
    cost = (0.5 / len(x_)) * np.sum(obs_arr * (x_ - xref_sim)**2)
    return cost


# ------------------------------------------------------------------------------
def lorenz_xb(xb):
    return cost_function_lorenz(1 + xb[0], x, [0, 0, sigma, rho, beta + xb[1]], dt, N)


# ------------------------------------------------------------------------------
def lorenz_xs(xs):
    return cost_function_lorenz(1 + xs[0], x, [0, 0, sigma + xs[1], rho, beta], dt, N)


# ------------------------------------------------------------------------------
def lorenz_xr(xr):
    return cost_function_lorenz(1 + xr[0], x, [0, 0, sigma, rho + xr[1], beta], dt, N)


# ------------------------------------------------------------------------------
def lorenz_onaxis(lorenz_fun, xaxis, uaxis):
    return np.asarray(map(lorenz_fun,
                          zip(*(y.flat for y in np.meshgrid(xaxis, uaxis))))).reshape(len(xaxis),
                                                                                      len(uaxis))


# ------------------------------------------------------------------------------
def plot_trajectory(k, u, color='k', alpha=1):
    x, y, z = simulation([1 + k, 0, 0], [sigma + u, rho, beta], dt, N)
    plt.plot(x, color=color, alpha=alpha)
    return x[-1], y[-1], z[-1]


# ------------------------------------------------------------------------------
def plot_spread(k, fun_to_generate_u, Nsim=50, color='k', alpha=0.2):
    lasts = []
    for _ in xrange(Nsim):
        u = fun_to_generate_u()
        x, y, z = plot_trajectory(0, u, color='k', alpha=alpha)
        lasts.append([x, y, z])
    lasts = np.asarray(lasts)
    plot_trajectory(0, 0, color='r', alpha=1)
    print(np.mean(lasts, 0))
    print(np.var(lasts, 0))
    return lasts


# ------------------------------------------------------------------------------
class Lorenz:
    """
    Class for simulation of the lorenz system
    """

    def __init__(self, initial_state=[1, 0, 0], parameters = [sigma, rho, beta], dt=0.05):
        "docstring"
        self.dt = dt
        self.initial_state = initial_state
        self.parameters = parameters
        self.current = self.initial_state
        self._N_total_it = 0


    def update(self):
        x, y, z = self.current
        xn, yn, zn = update(x, y, z,
                            self.parameters[0], self.parameters[1], self.parameters[2], self.dt)
        self.current = xn, yn, zn
        self._N_total_it += 1

    def simulate(self, Niter, save='all'):
        if save == 'all':
            out = np.empty([Niter, 3])
            i = 0
            while i < Niter:
                self.update()
                out[i, :] = self.current
                i += 1
        else:
            pass
        return out

    def reset(self):
        self.current = self.initial_state
        self._N_total_it = 0

# x, y, z = simulation([1, 0, -0], [sigma, rho + 0.5, beta], dt, N)


reference = Lorenz(initial_state = [1, 0, 0],
                   parameters = [sigma, rho + 0.1, beta], dt=dt)



def cost_lorenz_class(inst1, inst2=reference, Niter=500):
    i = 0
    cost = 0
    while i < Niter:
        cost += (inst1.current[0] - inst2.current[0])**2
        inst1.update()
        inst2.update()
        i += 1
    inst1.reset()
    inst2.reset()
    return cost / Niter



def cost_xb(*arg):
    x0 = arg[0]
    b = arg[1]
    lor = Lorenz([x0, 0, 0], parameters = [sigma, rho, b], dt = 0.05)
    return cost_lorenz_class(lor, reference)

# ------------------------------------------------------------------------------
if __name__ == '__main__':
    x2, y2, z2 = simulation([1., 1, -1], [sigma, rho, beta], dt, N)
    x3, y3, z3 = simulation([1., 0, 0], [sigma, rho, beta], dt, N)
    x4, y4, z4 = simulation([1., 0, 0], [sigma, rho, beta], dt, N)

    plt.subplot(3, 1, 1)
    plt.plot(x)
    plt.plot(x2)
    plt.plot(x3)
    plt.plot(x4)
    plt.subplot(3, 1, 2)
    plt.plot(y)
    plt.plot(y2)
    plt.plot(y3)
    plt.plot(y4)
    plt.subplot(3, 1, 3)
    plt.plot(z)
    plt.plot(z2)
    plt.plot(z3)
    plt.plot(z4)
    plt.show()


    Npts = 200
    var_range = 0.05
    x_test = np.linspace(-1.0, 1.0, Npts) * var_range
    u_test = np.linspace(-1.0, 1.0, Npts) * var_range

    x_mg, u_mg = np.meshgrid(x_test, u_test, indexing = 'ij')

    grid_result = lorenz_onaxis(lorenz_xb, x_test, u_test)
    print grid_result.min(), grid_result.max()
    plt.contourf(x_test, u_test, grid_result.T, 50)
    plt.xlabel(r'$\beta$')
    plt.ylabel(r'$x_0$')
    plt.show()

    cost1 = np.empty(len(x_test))
    cost2 = np.copy(cost1)
    for i, xx in enumerate(x_test):
        cost1[i] = cost_function_lorenz(xx, x, [0, 0, sigma, rho, beta], dt, N)
        cost2[i] = cost_function_lorenz(xx, x, [0, 0, sigma, rho, beta + 0.01], dt, N)
    plt.plot(x_test, np.vstack([cost1, cost2]).T)
    plt.plot(x_test, cost2)
    plt.show()

    # ROproblem: lorenz
    # dim(K) = 1, dim(U) = 1
    # indicesK: [0]
    # p99: -0.0107303651826
    # MPE: 0.00551679539413
    # p90: -0.0107303651826
    # pratio51: -0.0493996998499
    # minE: -0.00692846423212
    # p95: -0.0107303651826
    # p100: -0.0107303651826
    # minV: 0.00947973986993
    # range: (2.000001669540938, 59.290154442844525)
    # NptsK: 2000, NptsU: 2000 

    Nsim = 100
    plt.subplot(3, 1, 1)
    plot_spread(-0.00692846423212, lambda: np.random.uniform(-0.05, 0.05), Nsim, alpha=0.05)
    plt.subplot(3, 1, 2)
    plot_spread(0.00947973986993, lambda: np.random.uniform(-0.05, 0.05), Nsim, alpha=0.05)
    plt.subplot(3, 1, 3)
    plot_spread(0.05, lambda: np.random.uniform(-0.05, 0.05), Nsim, alpha=0.05)
    plt.show()

# EOF ----------------------------------------
