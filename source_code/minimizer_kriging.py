#!/usr/bin/env python
# coding: utf-8

#=================================================================================
##.* Definition of parameter and useful functions
#=================================================================================

import SWE_wrapper as swe        # 
import matplotlib.pyplot as plt
import pickle
from scipy import stats, optimize
import numpy as np
import weighted_kde
import half_sample_mode
import seaborn as sns
import corner
import pandas as pd
from sklearn import cluster
import pyDOE
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Matern
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

class Estimate:
    def __init__(self, value = [], uref = [], method = ''):
        self.k = value
        self.uref = uref
        self.J = np.nan
        self.method = method
        self.validation_SS = []
    def interp(self, xr):
        return map(swe.interp(self.k), xr)

    def summary(self):
        print 'K =', self.k
        print 'AP =', self.uref
        if not np.isnan(self.J):
            print 'J = ', self.J

    def eval_J(self):
        self.J,_ = J_pw_AP(self.k, self.uref[0], self.uref[1])
        
Kref = 0.2*(1+np.sin(2*np.pi*swe.xr/swe.D[1])) #
xr_ext = np.linspace(1,100,200)
Kref_ext = 0.2*(1+np.sin(2*np.pi*xr_ext/swe.D[1]))



mean_h = 20 
phase = 0
amplitude = 5.0
period = 50.0
# href = y_obs
# Ap_Pm -> A = 5.1; P = 49.8
# Ap_Pp -> A = 5.2; P = 50.1
# Am_Pp -> A = 4.9; P = 50.2
# Am_Pm -> A = 4.8; P = 49.9

def shallow_water_KU(K,amplitude = 5.0, period = 50.0, mean_h = 20.0 , phase = 0.0):
    """Computes h and q for a given value of K, and U = [A,P,M,Ph] """
    bcL = lambda h, hu, t: swe.BCrand(h, hu, t, 'L',
                                 mean_h, amplitude, period, phase)
    return swe.shallow_water(swe.D, swe.g, swe.T, swe.h0, swe.u0, swe.N, swe.LF_flux, swe.dt, swe.b, K,
                                 bcL, swe.bcR)




APref = [5.0, 50.0]    
# APref = [5.1, 49.8]
# APref = [5.2, 50.1] 
# APref = [4.9, 50.2]
# APref = [4.8, 49.9]


[xr, href, uref, t] = shallow_water_KU(Kref, # Generation observation --> Training set
                                       amplitude = APref[0], 
                                       period = APref[1],
                                       mean_h = 20.0,
                                       phase = 0.0)
[_, h_pre, _, _ ] = swe.prevision(Kref, # Generation prediction --> Validation set
                                  amplitude = APref[0],
                                  period = APref[1],
                                  mean_h = 20.0,
                                  phase = 0.0)

def J_nograd(K,AP, hreference = href):
    Kc = np.array(map(swe.interp(K), xr))
    _,hKAP,_,_ = shallow_water_KU(Kc,
                                  amplitude = AP[0], 
                                  period = AP[1],
                                  mean_h = 20.0,
                                  phase = 0.0)
    return 0.5*np.sum((hKAP - hreference)**2)
    

def J_validation(K,AP):
    [_, hprevision, _, _] = swe.prevision(Kref, # Generation prediction --> Validation set
                                  amplitude = AP[0],
                                  period = AP[1],
                                  mean_h = 20.0,
                                  phase = 0.0)
    Kc = np.array(map(swe.interp(K), xr))
    [_, h, _, _] = swe.prevision(Kc, # Generation prediction --> Validation set
                                  amplitude = AP[0],
                                  period = AP[1],
                                  mean_h = 20.0,
                                  phase = 0.0)
    return 0.5*np.sum((h-hprevision)**2)

print APref
Nvar = 4


def prediction_cost(Kcoeff, hpr = h_pre):
    """Compute prediction misfit"""
    K_transform = swe.interp(Kcoeff)
    K = np.array(map(K_transform, xr))
    return swe.J_function(h_pre, swe.prevision(K)[1])

def J_pw_AP(Kcoeff, ampli = 5.0, period = 50.0, hr = href):
    """Piecewise constant interpolation of K, then computes cost function and gradient"""
    K_transform = swe.interp(Kcoeff)
    K = np.array(map(K_transform, xr))
    cost, grad =  swe.J_grad_AP_href(K, ampli, period, hr)
    
    grad_sum_length = int(50.0/Kcoeff.size)
    grad_coeff = np.zeros(Kcoeff.size)
    for i in range(Kcoeff.size):
        grad_coeff[i] = sum(grad[i*grad_sum_length:(i*grad_sum_length+grad_sum_length)])
    return cost,grad_coeff


ref = Estimate(Kref, [5.0,50.0], 'ref')
ref.eval_J()
ref.summary()

##.* Sanity check:
test = J_pw_AP(Kref, ampli = APref[0], period = APref[1], hr= href)
test[0] == 0.0
all(test[1] == np.zeros(50))
prediction_cost(Kref) == 0.0

# =================================================================================
##.* COMPUTATIONS
# =================================================================================

def generate_AP_discrete(iter,Ngrid=20):
    A = np.linspace(4.7,5.3, Ngrid)
    P = np.linspace(49.7,50.3, Ngrid)
    return np.array([A[iter%Ngrid], P[iter/Ngrid]])

Ngrid  = 20
AP_grid = np.meshgrid(np.linspace(4.7,5.3, Ngrid),np.linspace(49.7,50.3, Ngrid), indexing = 'ij' )
AP_grid_flat = np.array(AP_grid).T.reshape(-1,2)


initial_design = pyDOE.lhs(3, samples = Nsim, criterion = 'maximin', iterations = 100)
initial_design[:,1:3] = initial_design[:,1:3]*0.6 + [5,50] - 0.3

plt.plot(initial_design[:,0],initial_design[:,1],'.');plt.show()

def evaluate_DoE(doe_scaled):
    K,A,P = doe_scaled[:,0],doe_scaled[:,1],doe_scaled[:,2]
    K = K*2
    A =  A*0.6 + 5 - 0.3
    P = P*0.6 + 50 - 0.3
    return [J_pw_AP(np.asarray([K[i]]), ampli = A[i], period = P[i], hr = href) for i in xrange(doe_scaled.shape[0])]

initial_evals = evaluate_DoE(initial_design)


k_max_negloglik = np.empty([Nsim,Nvar])
AP_saved = np.empty([Nsim,2])
negloglik_saved = np.empty(Nsim)
start_opt = 0.2*np.ones(Nvar)



for i in xrange(Nsim):
    AP = initial_design[i,:]
    AP_saved[i,:] = [AP[0],AP[1]]
    args = (AP[0],AP[1], href)
    opt = optimize.minimize(fun = J_pw_AP, x0 = start_opt, args = args,
                            jac = True, bounds = [(0.0, None)]*Nvar,
                            options = {'maxiter':max_iter_optim})
    k_max_negloglik[i,:] = opt.x
    negloglik_saved[i] = opt.fun

dictionnary_save = {'k_max_negloglik':k_max_negloglik, 'AP_saved': AP_saved, 'negloglik_saved':negloglik_saved}

filename = 'k_max_2dim_kriging'
output = open(filename, 'wb') # Save the runs
with open(filename, 'wb') as output:
    pickle.dump(dictionnary_save, output)

plt.plot(k_max_negloglik[:,0],k_max_negloglik[:,1],'.');plt.show()

fig = plt.figure()
ax = fig.add_subplot(121,projection = '3d')
surf = ax.plot_trisurf(initial_design[:,0],initial_design[:,1],k_max_negloglik[:,0])
ax = fig.add_subplot(122,projection = '3d')
surf = ax.plot_trisurf(initial_design[:,0],initial_design[:,1],k_max_negloglik[:,1])
plt.show()

ind = 1

kriging_minim = GaussianProcessRegressor(kernel = Matern(0.5*np.ones(2)))
kriging_minim.fit(initial_design, k_max_negloglik[:,ind])
krig_pred = kriging_minim.predict(AP_grid_flat)
fig = plt.figure()
ax = fig.add_subplot(121,projection = '3d')
surf = ax.plot_trisurf(initial_design[:,0],initial_design[:,1],k_max_negloglik[:,ind])
ax = fig.add_subplot(122,projection = '3d')
surf = ax.contourf(AP_grid[0],AP_grid[1], krig_pred.reshape(Ngrid,Ngrid).T)
plt.show()


fig = plt.figure()
ax = fig.add_subplot(121,projection = '3d')
surf = ax.plot_trisurf(initial_design[:,0],initial_design[:,1],negloglik_saved)
plt.show()
