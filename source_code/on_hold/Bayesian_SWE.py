#!/usr/bin/env python
# coding: utf-8

##.* Definition of parameter and useful functions

from SWE_wrapper import *
import matplotlib.pyplot as plt
import pickle
from scipy import stats, optimize
import weighted_kde
import half_sample_mode

mean_h = 20 
phase = 0
amplitude = 5.0
period = 50.0
# href = y_obs
# Ap_Pm -> A = 5.1; P = 49.8
# Ap_Pp -> A = 5.2; P = 50.1
# Am_Pp -> A = 4.9; P = 50.2
# Am_Pm -> A = 4.8; P = 49.9


def negloglik_gradient(Kvec,ampli,period):
    bcL = lambda h, hu, t: BCrand(h, hu, t, 'L',
                                 mean_h, ampli, period, phase)

    negloglik = 0.0*np.ones_like(Kvec)

    grad_negloglik = 0.0*np.ones_like(Kvec)
    
    for i,K in enumerate(Kvec):
        negloglik[i], grad_negloglik[i] = shallow_water_RSS_grad(D, g, T, h0, u0, N, LF_flux, dt, b, K,
                                  bcL, bcR, bcL_A, bcR_A, href, J_function)
    
    return np.array([negloglik, grad_negloglik])


def negloglik_gradient_normal_prior(Kvec,ampli,period):
    bcL = lambda h, hu, t: BCrand(h, hu, t, 'L',
                                 mean_h, ampli, period, phase)

    negloglik = 0.0*np.ones_like(Kvec)

    grad_negloglik = 0.0*np.ones_like(Kvec)
    
    for i,K in enumerate(Kvec):
        negloglik[i], grad_negloglik[i] = shallow_water_RSS_grad(D, g, T, h0, u0, N, LF_flux, dt, b, K,
                                  bcL, bcR, bcL_A, bcR_A, href, J_function)
        negloglik[i] = negloglik[i] + np.log(prior_K(K))
        grad_negloglik[i] += deriv_regul(K)
    
    return np.array([negloglik, grad_negloglik])


    



##.* MPE

####################################
###### MOST PROBABLE ESTIMATE ######
####################################

def generate_AP(N=1):
    A = np.random.uniform(4.7,5.3,N)
    P = np.random.uniform(49.7,50.3,N)
    return np.array([A,P])

def generate_AP_discrete(iter,Ngrid=30):
    A = np.linspace(4.7,5.3, Ngrid)
    P = np.linspace(49.7,50.3, Ngrid)

    # A = np.linspace(4.95,5.05, Ngrid)
    # P = np.linspace(49.95,50.05, Ngrid)
    return np.array([A[iter%Ngrid], P[iter/Ngrid]])
    

# filename = 'K_025_06range_shift_Am_Pm'
# filename = 'K_025_06range_shift_Ap_Pm'
# filename = 'K_025_06range_rightopen_discrete'
filename = 'K_025_06range_rightopen'
filename = 'K_025_06range_rightopen_discrete_Ap_Pm'
# filename = 'K_max_gradient_weights_06range_normalK'

input = open('evidence_025_06range', 'rb')
evidence = pickle.load(input)
input.close()

## Optimization with gradient
Nsim = 900
k_max_negloglik = np.empty(Nsim)
AP_saved = np.empty([Nsim,2])
negloglik_saved = np.empty(Nsim)
for i in xrange(Nsim):
    # AP = generate_AP(1)
    AP = generate_AP_discrete(i)
    AP_saved[i,:] = [AP[0],AP[1]]
    opt = optimize.minimize(fun = lambda K: negloglik_gradient(K,AP[0],AP[1]),
                            x0 = 2.5 ,jac = True, bounds = [(0.00001,1.5)])
    k_max_negloglik[i] = opt.x
    negloglik_saved[i] = opt.fun

dictionnary_save = {'k_max_negloglik':k_max_negloglik, 'AP_saved': AP_saved, 'negloglik_saved':negloglik_saved}

#Save the initial runs
output = open(filename, 'wb')
pickle.dump(dictionnary_save, output)
output.close()

#########

# Yields the previous results from the written file
input = open(filename, 'rb')
dic = pickle.load(input)
input.close()
k_max_negloglik = dic['k_max_negloglik']
AP_saved = dic['AP_saved']
negloglik_saved = dic['negloglik_saved']

# Continues the computation of the maximum
Ncont = 1
AP_cont = np.empty([Ncont,2])
k_maxcont = np.empty([Ncont])
negloglik_cont = np.empty([Ncont])
for i in xrange(Ncont):   
    # AP = generate_AP(1)
    AP = generate_AP_discrete(899)
    AP_cont[i,:] = [AP[0],AP[1]]
    opt = optimize.minimize(fun = lambda K: negloglik_gradient(K,AP[0],AP[1]),
                            x0 = 2.5 ,jac = True, bounds = [(0.0001,1.5)])
    k_maxcont[i] = opt.x
    negloglik_cont[i] = opt.fun


AP_saved = np.vstack([AP_saved, AP_cont])
k_max_negloglik = np.append(k_max_negloglik, k_maxcont)
negloglik_saved = np.append(negloglik_saved, negloglik_cont)
dictionnary_save = {'k_max_negloglik':k_max_negloglik,
                    'AP_saved':AP_saved,
                    'negloglik_saved':negloglik_saved}
output = open(filename, 'wb')
pickle.dump(dictionnary_save, output)
output.close()


max_posterior = np.exp(-negloglik_saved/Nobs)/evidence
####
# Kernel estimation (non)-censored
p_k_max = np.exp(-negloglik_saved/np.prod(href.shape))
RMSE = negloglik_saved/np.prod(href.shape)
weights = p_k_max
ind_min = (np.abs(k_max_negloglik-np.min(k_max_negloglik))<0.001)
k_max_censored = k_max_negloglik[~ind_min]
ind_max = (np.abs(k_max_censored-np.max(k_max_censored))<0.001)
k_max_censored = k_max_censored[~ind_max]
prop_dirac_min = np.sum(ind_min) / float(k_max_negloglik.shape[0])
prop_dirac_max = np.sum(ind_max) / float(k_max_negloglik.shape[0])
np.sum(ind_min)
num_discrete = np.sum(ind_max) + np.sum(ind_min)
prop_dirac = num_discrete/float(k_max_negloglik.shape[0])
density_estimated = stats.gaussian_kde(k_max_negloglik)
# density_reweighted = weighted_kde.gaussian_kde(k_max_negloglik,weights = evidence)
density_reweighted = weighted_kde.gaussian_kde(k_max_negloglik, weights = p_k_max)
density_censored = stats.gaussian_kde(k_max_censored)
k_plot = np.arange(0,1.5,0.001)
k_est = k_plot[np.argmax(density_censored(k_plot))]
# k_est = half_sample_mode.HS_mode(k_max_censored)

## Variance and average
xlim = [0,k_max_negloglik.max()]
nbin = 30
hist_k_max = plt.hist(k_max_negloglik,nbin)
plt.show()
binsorted = np.digitize(k_max_negloglik,hist_k_max[1])
var_max_vec = np.empty(nbin)
avg_max_vec = np.empty(nbin)
violin_dataset = ()
for i in xrange(nbin):
    violin_dataset += (np.exp(-negloglik_saved[(binsorted==(i+1))]/np.prod(href.shape)),)
    var_max_vec[i] = np.var(np.exp(-negloglik_saved[(binsorted==(i+1))]/np.prod(href.shape)))
    avg_max_vec[i] = np.mean(np.exp(-negloglik_saved[(binsorted==(i+1))]/np.prod(href.shape)))

# Plots of results
plt.figure(figsize=(8.27, 11.69), dpi=100)
plt.subplot(411)
plt.grid(alpha = 0.5, linewidth = 0.5)
plt.plot(k_max_negloglik, np.exp(-negloglik_saved/np.prod(href.shape)), '.', markersize=1.3)
# plt.plot(hist_k_max[1][:-1],avg_max_vec)
# plt.plot(k_max_negloglik, max_posterior, '.', markersize = 1.3)
plt.axhline(y=1, color = 'g')
plt.xlabel(r'$k_{\max}$')
plt.ylabel(r'$\max\, p_{K|Y,U}$')
plt.title(r'Maximizer and maximum of $\max\, p_{K|Y,U}$')
plt.axvline(x= k_est, color= 'k', label = r'$k_{\mathrm{est}}$')
plt.axvline(x = Kref, color= 'r', label = r'$k_{\mathrm{true}}$')
# plt.xlim([0,0.5])
plt.xlim(xlim)
ax = plt.violinplot(violin_dataset, positions = hist_k_max[1][1:], widths = 0.02, showmeans = True, showmedians = True, showextrema = False)
ax['cmeans'].set_color('r')
ax['cmedians'].set_color('g')
for vp in ax['bodies']:
    vp.set_alpha(0.3)
    # vp.set_color('o')
plt.grid(alpha = 0.5, linewidth = 0.5)

plt.subplot(412)
plt.hist(k_max_negloglik, 50, density = True, alpha = 0.4)
# plt.ylim([0,5])
plt.plot(k_plot, density_estimated(k_plot), label ='KDE')
plt.plot(k_plot, density_reweighted(k_plot), ':',label ='KDE reweighted')
plt.plot(k_plot, (1-prop_dirac)*density_censored(k_plot), 'm', label = 'censored KDE')
plt.stem(np.array([0,0.5]),3*np.array([prop_dirac_min, prop_dirac_max]), linefmt= 'm', markerfmt = 'm^')
plt.axvline(x=k_est, color='k', label = r'$k_{\mathrm{est}}$')
plt.axvline(x = Kref,color= 'r',label = r'$k_{\mathrm{true}}$')
plt.title(r'Histogram and estimated density of $k_{\mathrm{max}}$')
plt.legend()
plt.xlabel(r'$k_{\max}$')
plt.ylabel(r'Histogram')
plt.ylim([0, density_reweighted(k_plot).max()+0.5])
plt.grid(alpha = 0.5, linewidth = 0.5)
# plt.xlim([-0.005,0.505])
plt.xlim(xlim)


plt.subplot(413)
plt.plot(hist_k_max[1][:-1],avg_max_vec)
plt.ylabel(r'$\mathcal{E}(k_{\max})$')
plt.xlabel(r'$k_{\max}$')
plt.axvline(x= k_est, color= 'k', label = r'$k_{\mathrm{est}}$')
plt.axvline(x = Kref, color= 'r', label = r'$k_{\mathrm{true}}$')
plt.grid(alpha = 0.5, linewidth = 0.5)
plt.title(r'$\mathcal{E}(k_{\max}) = \mathbb{E}[p_{K|Y,U}(k_{\max}|y^{obs},U)]$')
# plt.xlim([0,0.5])
plt.xlim(xlim)
plt.xlabel(r'$k_{\max}$')


plt.subplot(414)
plt.grid(alpha = 0.5, linewidth = 0.5)
plt.plot(hist_k_max[1][:-1],var_max_vec)
plt.title(r'$\mathcal{V}(k_{\max}) = \mathbb{V}ar[p_{K|Y,U}(k_{\max}|y^{obs},U)]$')
plt.axvline(x= k_est, color= 'k', label = r'$k_{\mathrm{est}}$')
plt.axvline(x = Kref, color= 'r', label = r'$k_{\mathrm{true}}$')
plt.ylabel(r'$\mathcal{V}(k_{\max})$')
plt.xlabel(r'$k_{\max}$')
# plt.xlim([0,0.5])
plt.xlim(xlim)

plt.tight_layout()
plt.show()
# plt.savefig('krange_06_rightopen_Am_Pm.eps')

plt.plot(k_max_negloglik, np.exp(-negloglik_saved/Nobs), '.', markersize = 1.3)
plt.plot(k_max_negloglik, max_posterior, '.', markersize=1.3)
plt.plot(k_max_negloglik, evidence , '.', markersize=1.3)
plt.show()

### Computation of the evidence
nstart = 1500
nend = 2000
Z_ev_temp = np.empty(nend - nstart)

K_evidence_computation = np.linspace(0,0.5,5)
delta_ev = K_evidence_computation[1] - K_evidence_computation[0]
for i,U in enumerate(AP_saved[nstart:nend,:]):
    comp = likelihood(K_evidence_computation,U[0],U[1])
    Z_ev_temp[i] = delta_ev * (np.sum(comp[1:-1]) + 0.5*comp[0] + 0.5*comp[-1]) # trapez method

filename_evidence = 'evidence_025_06range'

input = open(filename_evidence, 'rb')
Z_evidence = pickle.load(input)
input.close()
Z_evidence = np.append(Z_evidence, Z_ev_temp)
output = open(filename_evidence, 'wb')
pickle.dump(Z_evidence, output)
output.close()

plt.subplot(1,2,1)
plt.plot(k_max_negloglik[:2000], np.exp(-negloglik_saved[:2000]/np.prod(href.shape)), '.', markersize=1.3)
plt.subplot(1,2,2)
plt.plot(k_max_negloglik[:2000], Z_evidence * np.exp(-negloglik_saved[:2000]/np.prod(href.shape)), '.', markersize=1.3)
plt.show()
##### ANIMATION ##########################################

for i in xrange(0,3000):
    k_argmax_anim = k_max_negloglik[i]
    # lik_anim = np.exp(-negloglik_saved[i]/Nobs)
    lik_anim = negloglik_saved[i]
    AP_anim = AP_saved[i,:]
    if (k_argmax_anim < 1.5e-04):
        fmt = '.r'
    # elif (k_argmax_anim>0.49):
    #     fmt = '.m'
    else:
        fmt = '.b'
    plt.subplot(221)
    plt.plot(k_argmax_anim,lik_anim, fmt, markersize=1.3)
    # plt.title(r'$\max\, p(k |\mathbf{y}^{\mathrm{obs}},\mathbf{u}^{(i)})$')
    plt.title(r'$\max\, \{-\log p(\mathbf{y}^{\mathrm{obs}}|k,\mathbf{u}^{(i)})\}$')
    plt.ylabel('max')
    # plt.xlabel('$k_{\max}$')
    plt.xlim([0,0.8])
    # plt.ylim([0.94,1])
    plt.subplot(222)
    plt.plot(AP_anim[0],AP_anim[1], fmt, markersize = 1.3)
    # plt.xlabel('A', fontsize = 5)
    # plt.ylabel('P', fontsize = 5)
    plt.title(r'Samples $\mathbf{u}^{(i)}$')
    plt.xlim([4.7,5.3])
    plt.ylim([49.7,50.3])
    plt.subplot(2,2,(3,4))
    plt.hist(k_max_negloglik[:i], color = 'blue', bins = 30)
    # k_max_censored = k_max_negloglik[~(k_max_negloglik[:i]==np.min(k_max_negloglik[:i]))]
    # k_max_censored = k_max_censored[~(k_max_censored==np.max(k_max_censored))]
    # density_estimated = stats.gaussian_kde(k_max_negloglik[:i])
    # density_censored = stats.gaussian_kde(k_max_censored)
    # k_plot = np.arange(0,0.5,0.001)
    # k_est = k_plot[np.argmax(density_estimated(k_plot))]
    # plt.plot(k_plot, density_estimated(k_plot))
    # plt.plot(k_plot, density_censored(k_plot))
    plt.xlim([0,0.8])
    plt.ylabel(r'Histogram of $k_{\argmax}$')
    # plt.pause(0.005)
    pic_name = './animation_plots/plot_ro{:04d}.png'.format(i)
    if (i>1699 and np.mod(i,100)==0):
        plt.savefig(pic_name, dpi = 600)

    


plt.show()                      #





























#######################################################################################
#######################################################################################"

## No gradient
Nsim = 100
k_max_ng = np.empty(Nsim)
AP_ng = np.empty([Nsim,2])
negloglik_ng = np.empty(Nsim)
for i in xrange(Nsim):
    AP = generate_AP(1)
    AP_ng[i,:] = [AP[0],AP[1]]
    opt = optimize.minimize(fun = lambda K: -np.log(likelihood(K,AP[0],AP[1]))*Nobs,
                            x0 = 2.5 , bounds = [(0,0.5)])
    k_max_ng[i] = opt.x
    negloglik_ng[i] = opt.fun

dictionnary_save = {'k_max_ng':k_max_ng, 'AP_ng': AP_ng, 'negloglik_ng':negloglik_ng}

## Continuer optimization
Nsimcont = 500
for i in xrange(Nsimcont):
    AP = generate_AP(1)
    k_max_g = np.append(k_max_g, optimize.minimize(fun = lambda K: -likelihood_gradient(K,AP[0],AP[1]), x0 = 0.25 ,jac = True, bounds = [(0,0.5)]).x)

output = open('K_max_gradient_weights', 'wb')
pickle.dump(k_max_g, output)
output.close()

# input = open('K_max_gibbs_gradient', 'rb')
# k_max_g = pickle.load(input)
# input.close()

plt.subplot(411)
plt.scatter(AP_saved[:,0],AP_saved[:,1])
plt.subplot(412)
plt.scatter(k_max_negloglik, np.exp(-negloglik_saved/Nobs))
plt.subplot(413)
plt.hist(k_max_negloglik)
density_estimated = stats.gaussian_kde(k_max_negloglik)
plt.plot(np.arange(0,0.5,0.005), density_estimated(np.arange(0,0.5,0.005)))
plt.subplot(414)
plt.hist(k_max_negloglik, weights = np.exp(-negloglik_saved/Nobs))
plt.show()

plt.hist(k_max_g[600:], density = True)
ker_max = stats.gaussian_kde(k_max_g)
plt.plot(np.arange(0,0.5,0.005), ker_max(np.arange(0,0.5,0.005)))
estimate = optimize.minimize_scalar(lambda K: -ker_max(K)).x
plt.plot([estimate, estimate], [0, np.max(ker_max(np.arange(0,0.2,0.005)))])
plt.show()


input = open('K_max_gibbs', 'rb')
k_max = pickle.load(input)
input.close()

# k_max[0:69] : au matin 7 février
ker_max = stats.gaussian_kde(k_max)
plt.subplot(121)
plt.hist(k_max, 50, density = True)
plt.plot(np.arange(0,0.5,0.005), ker_max(np.arange(0,0.5,0.005)))
estimate = optimize.minimize_scalar(lambda K: -ker_max(K)).x
plt.plot([estimate, estimate], [0, np.max(ker_max(np.arange(0,0.2,0.005)))])
plt.xlabel(r'$\mathbf{k}$')
plt.title(r'Histogram and estimated density of the $\mathbf{k}_{\mathrm{argmax}}$')
plt.plot(k_max, np.zeros(k_max.shape), 'k+', ms=20)
#
plt.subplot(122)
plt.plot(k_max)
plt.ylabel(r'$\mathbf{k}_{\mathrm{argmax}}$')
plt.title('Results of optimization procedure')
plt.xlabel('# of optimization procedure')
plt.suptitle('(Estimated) Distribution of the MAP')
plt.show()



