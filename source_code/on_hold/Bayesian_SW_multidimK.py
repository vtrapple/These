#!/usr/bin/env python
# coding: utf-8

##.* Definition of parameter and useful functions

from SWE_wrapper import *
import matplotlib.pyplot as plt
import pickle
from scipy import stats, optimize
import weighted_kde
import half_sample_mode
import seaborn as sns
import pandas as pd

mean_h = 20 
phase = 0
amplitude = 5.0
period = 50.0
# href = y_obs
# Ap_Pm -> A = 5.1; P = 49.8
# Ap_Pp -> A = 5.2; P = 50.1
# Am_Pp -> A = 4.9; P = 50.2
# Am_Pm -> A = 4.8; P = 49.9

Kref = 0.2*(1+np.sin(2*np.pi*xr/D[1])) #
# Kref = 1+0.2*(np.sin(2*np.pi*xr/D[1])) #

# Kref = 0.25
[xr, href, uref, t] = reference(Kref)


def interp(coef_array):
    """Interpolation using piecewise constant values"""
    coef_array=np.array(coef_array)
    D_length = float(np.diff(D)[0])
    cell_vol = D_length/coef_array.size
    pts = np.linspace(cell_vol/2.0, D_length- cell_vol/2.0,
                      num = coef_array.size)
    f_to_ret = lambda x: interp_cst(x, cell_vol, coef_array, pts)
    return f_to_ret


def J_pw(Kcoeff):
    """Piecewise constant interpolation of K, then computes cost function and gradient"""
    K_transform = interp(Kcoeff)
    K = np.array(map(K_transform, xr))
    cost, grad =  J_grad(K)
    grad_sum_length = int(50.0/Kcoeff.size)
    grad_coeff = np.zeros(Kcoeff.size)
    for i in range(Kcoeff.size):
        grad_coeff[i] = sum(grad[i*grad_sum_length:(i*grad_sum_length+grad_sum_length)])
    return cost,grad_coeff

def J_pw_AP(Kcoeff, ampli = 5.0, period = 50.0):
    """Piecewise constant interpolation of K, then computes cost function and gradient"""
    K_transform = interp(Kcoeff)
    K = np.array(map(K_transform, xr))
    cost, grad =  J_grad_AP(K,ampli,period)
    grad_sum_length = int(50.0/Kcoeff.size)
    grad_coeff = np.zeros(Kcoeff.size)
    for i in range(Kcoeff.size):
        grad_coeff[i] = sum(grad[i*grad_sum_length:(i*grad_sum_length+grad_sum_length)])
    return cost,grad_coeff

def cost_prediction_interp(Kcoeff):
    K_transform = interp(Kcoeff)
    K = np.array(map(K_transform, xr))
    return cost_prediction(K)

k1d = np.linspace(0,1.5,100)

def cost_computation_bounds(Kcoeff):
    cost = np.empty([3,3])
    for i in xrange(9):
        AP = generate_AP_discrete(i, Ngrid = 3)
        cost[i%3, i/3] = J_pw_AP(Kcoeff, AP[0], AP[1])[0]
    return cost



Nvar = 4
start_opt = 0.2*np.ones(Nvar)
opt_multidim = optimize.minimize(fun = J_pw, x0 = start_opt, jac = True, bounds = [(0.0, None)]*Nvar)
st_opt = interp(start_opt)
plt.plot(xr_ext, Kref_ext)
plt.plot(xr_ext, map(st_opt, xr_ext))
plt.plot(xr_ext, map(interp(opt_multidim.x), xr_ext))
# plt.plot(map(interp(opt_multidim_unc.x), xr))

plt.grid(alpha = 0.5)
plt.show()

def generate_AP(N=1):
    A = np.random.uniform(4.7,5.3,N)
    P = np.random.uniform(49.7,50.3,N)
    return np.array([A,P])

def generate_AP_discrete(iter,Ngrid=20):
    A = np.linspace(4.7,5.3, Ngrid)
    P = np.linspace(49.7,50.3, Ngrid)
    return np.array([A[iter%Ngrid], P[iter/Ngrid]])

#.* COMPUTATION REGULAR GRID -------------------

Nsim = 400
k_max_negloglik = np.empty([Nsim,Nvar])
AP_saved = np.empty([Nsim,2])
negloglik_saved = np.empty(Nsim)
for i in xrange(Nsim):
    # AP = generate_AP(1)
    AP = generate_AP_discrete(i)
    AP_saved[i,:] = [AP[0],AP[1]]
    args = (AP[0],AP[1])
    opt = optimize.minimize(fun = J_pw_AP, x0 = start_opt, args = args,
                            jac = True, bounds = [(0.0, None)]*Nvar)
    k_max_negloglik[i,:] = opt.x
    negloglik_saved[i] = opt.fun
    
dictionnary_save = {'k_max_negloglik':k_max_negloglik, 'AP_saved': AP_saved, 'negloglik_saved':negloglik_saved}

filename = 'k_max_4dim'
#Save the initial runs
output = open(filename, 'wb')
pickle.dump(dictionnary_save, output)
output.close()

#*. COMPUTATION LHS -----------------------------------

Nvar = 4
inp = open('LHS_2_1000', 'rb')
DoE01 = pickle.load(inp)
DoE = np.array([5,50]) + 0.6*DoE01 - 0.3
inp.close()
k_max_negloglik = np.empty([1000,Nvar])
AP_saved = np.empty([1000,2])
negloglik_saved = np.empty(1000)
start_opt = 0.2*np.ones(Nvar)

for i in xrange(1000):
    args = (DoE[i,0], DoE[i,1])
    opt = optimize.minimize(fun = J_pw_AP, x0 = start_opt, args = args,
                            jac = True, bounds = [(0.0, None)]*Nvar)
    k_max_negloglik[i,:] = opt.x
    negloglik_saved[i] = opt.fun

dictionnary_save = {'k_max_negloglik':k_max_negloglik, 'AP_saved': AP_saved, 'negloglik_saved':negloglik_saved}
filename = 'k_max_4dim_LHS'
#Save
output = open(filename, 'wb')
pickle.dump(dictionnary_save, output)
output.close()


##############################################
#.* Exploitation of the results --------------
##############################################

##.* Dimension = 3 ---------------------------------------------------

filename = 'k_max_3dim'
k1d = np.linspace(0,1.5,100)

input = open(filename, 'rb')
dictionnary_save = pickle.load(input)
input.close()
k_max_negloglik = dictionnary_save['k_max_negloglik']
AP_saved = dictionnary_save['AP_saved']
negloglik_saved = dictionnary_save['negloglik_saved']

k_max_mean = np.mean(k_max_negloglik,0)

opt_multidim = optimize.minimize(fun = lambda K: J_pw(K)[0], x0 = [0.37237059, 0.20643795, 0.], jac = False, bounds = [(0.0, None)]*Nvar)

ind_0_k1 = (k_max_negloglik[:,0]<0.001)
prop_0_k1 = np.sum(ind_0_k1)/400.0
dens_k1 = stats.gaussian_kde(k_max_negloglik[~ind_0_k1,0])

ind_0_k2 = (k_max_negloglik[:,1]<0.001)
prop_0_k2 = np.sum(ind_0_k2)/400.0
dens_k2 = stats.gaussian_kde(k_max_negloglik[~ind_0_k2,0])

ind_0_k3 = (k_max_negloglik[:,2]<0.001)
prop_0_k3 = np.sum(ind_0_k3)/400.0
dens_k3 = stats.gaussian_kde(k_max_negloglik[~ind_0_k3,0])

k1_mpe = optimize.minimize(fun = lambda k: -dens_k1(k),x0 = 0.2, bounds = [(0.0,None)]).x
k2_mpe = optimize.minimize(fun = lambda k: -dens_k2(k),x0 = 0.2, bounds = [(0.0,None)]).x
k3_mpe = optimize.minimize(fun = lambda k: -dens_k3(k),x0 = 0.2, bounds = [(0.0,None)]).x

k_mpe = np.array([k1_mpe, k2_mpe, k3_mpe])[:,0]

kde_3d = stats.gaussian_kde(k_max_negloglik.T)
k_joint_mpe = optimize.minimize(fun  = lambda k: -kde_3d(k), x0 = [0.1,0.1,0.1],bounds = [(0,None)]*Nvar).x



plt.subplot(2,1,1)
for i in xrange(400):
    plt.plot(map(interp(k_max_negloglik[i,:]),xr), 'k', linewidth = 0.05)
plt.plot(Kref, 'r--', label= r'$\mathbf{k}_{\mathrm{ref}}$')
plt.plot(map(interp(opt_multidim.x), xr), label = 'w/o uncertainties')
plt.plot(map(interp(k_max_mean), xr), label = 'marginal means')
plt.plot(map(interp(k_mpe), xr), label = 'mpe')
plt.legend()

plt.subplot(2,3,4)
plt.hist(k_max_negloglik[:,0], 30, density = True)
plt.plot(k1d, dens_k1(k1d))
plt.title(r'$k_1$')
plt.xlim([k_max_negloglik[:,0].min(), k_max_negloglik[:,0].max()])
plt.subplot(2,3,5)
plt.hist(k_max_negloglik[:,1], 20, density = True)

plt.plot(k1d, dens_k2(k1d))
plt.title(r'$k_2$')
plt.xlim([k_max_negloglik[:,1].min(), k_max_negloglik[:,1].max()])
plt.subplot(2,3,6)
plt.hist(k_max_negloglik[:,2], 30, density = True)

plt.plot(k1d, dens_k3(k1d))
plt.title(r'$k_3$')
plt.xlim([k_max_negloglik[:,2].min(), k_max_negloglik[:,2].max()])
plt.tight_layout()
plt.show()

print prop_0_k1
print prop_0_k2
print prop_0_k3

    
# cost_true = cost_computation_bounds(opt_multidim.x)
# cost_mean = cost_computation_bounds(k_max_mean)
# cost_0 = cost_computation_bounds(np.array([0,0,0]))
# cost_start = cost_computation_bounds(start_opt)
# cost_mpe = cost_computation_bounds(k_mpe)
Nbar = 3
wd = (1.0 / float(Nbar))*0.75
Xind = np.arange(9)
plt.bar(Xind,cost_true.ravel(),width = wd, alpha = 1, label = 'w/o unc')
plt.bar(Xind+wd,cost_mean.ravel(), width = wd, alpha = 1, label = 'mean of families')
plt.bar(Xind+2*wd,cost_0.ravel(), width = wd, alpha = 1, label = 'id= 0')
# plt.bar(Xind+3*wd,cost_start.ravel(), width = wd, alpha = 1, label = 'start')
# plt.bar(Xind+2*wd,cost_mpe.ravel(), width = wd, alpha = 1, label = 'MPE')
plt.legend()
plt.show()


C_mpe = cost_prediction_interp(k_mpe)
C_mean = cost_prediction_interp(k_max_mean)
C_0 = cost_prediction_interp(np.array([0.0,0.0,0.0]))
C_nounc = cost_prediction_interp(opt_multidim.x)
C_joint_mpe = cost_prediction_interp(k_joint_mpe)

C_vector = np.array([C_nounc,C_0, C_mean, C_mpe, C_joint_mpe])
tick_lab = np.array(['No uncertainties', 'all 0', 'marginal mean', 'marginal mpe', 'joint mpe'])
plt.bar(np.arange(5), height = C_vector, tick_lab)
plt.show()

#
#.* Dimension = 2 ------------------------------------------------------------
#

k1d = np.linspace(0,1.5,100)

Nvar = 2
start_opt = 0.2*np.ones(Nvar)
opt_dim2 = optimize.minimize(fun = J_pw, x0 = start_opt, jac = True, bounds = [(0.0, None)]*Nvar)

filename = 'k_max_2dim'

input = open(filename, 'rb')
dictionnary_save = pickle.load(input)
input.close()
k_max_negloglik = dictionnary_save['k_max_negloglik']
AP_saved = dictionnary_save['AP_saved']
negloglik_saved = dictionnary_save['negloglik_saved']

k_max_mean = np.mean(k_max_negloglik,0)

ind_0_k1 = (k_max_negloglik[:,0]<0.001)
prop_0_k1 = np.sum(ind_0_k1)/400.0
dens_k1 = stats.gaussian_kde(k_max_negloglik[~ind_0_k1,0])

ind_0_k2 = (k_max_negloglik[:,1]<0.001)
prop_0_k2 = np.sum(ind_0_k2)/400.0
dens_k2 = stats.gaussian_kde(k_max_negloglik[~ind_0_k2,0])

k_max_kdejoint = stats.gaussian_kde(k_max_negloglik.T)

k_mpe = [0.51, 0.0065]

plt.subplot(2,1,1)
for i in xrange(400):
    plt.plot(map(interp(k_max_negloglik[i,:]),xr), 'k', linewidth = 0.05)
plt.plot(Kref, 'r--', label= r'$\mathbf{k}_{\mathrm{ref}}$')
plt.plot(map(interp(opt_dim2.x), xr), label = 'w/o uncertainties')
plt.plot(map(interp(k_max_mean), xr), label = 'marginal means')
plt.plot(map(interp(k_mpe), xr), label = 'MPE')

plt.legend()
plt.subplot(2,2,3)
plt.hist(k_max_negloglik[:,0], 30, density = True)
plt.plot(k1d, dens_k1(k1d))
plt.title(r'$k_1$')
plt.xlim([k_max_negloglik[:,0].min(), k_max_negloglik[:,0].max()])
plt.subplot(2,2,4)
plt.hist(k_max_negloglik[:,1], 20, density = True)
plt.plot(k1d, dens_k2(k1d))
plt.title(r'$k_2$')
plt.xlim([k_max_negloglik[:,1].min(), k_max_negloglik[:,1].max()])
plt.show()

# sns.kdeplot(k_max_negloglik[:,0],k_max_negloglik[:,1], shade = True)
jointplot = sns.jointplot(k_max_negloglik[:,0],k_max_negloglik[:,1], kind = 'kde')
plt.show()
sns.jointplot(k_max_negloglik[:,0], k_max_negloglik[:,1], kind = 'scatter')
plt.show()


sns.kdeplot(k_max_negloglik[:,0], k_max_negloglik[:,1],100)
# graphical MPE: [0.51, 0.0065]
plt.show()
kde_2d = stats.gaussian_kde(k_max_negloglik.T)
optimize.minimize(fun  = lambda k: -kde_2d(k), x0 = [0.5,0.],bounds = [(0,None)]*2)

# Comparaison of cost

cost_true = cost_computation_bounds(opt_dim2.x)
cost_mean = cost_computation_bounds(k_max_mean)
cost_0 = cost_computation_bounds(np.array([0,0]))
cost_mpe = cost_computation_bounds(np.asarray(k_mpe))

Nbar = 5
wd = (1.0 / float(Nbar))*0.75
Xind = np.arange(9)
plt.bar(Xind,cost_true.ravel(),width = wd, alpha = 1, label = 'w/o unc')
plt.bar(Xind+wd,cost_mean.ravel(), width = wd, alpha = 1, label = 'mean of families')
plt.bar(Xind+2*wd,cost_0.ravel(), width = wd, alpha = 1, label = 'id= 0')
plt.bar(Xind+3*wd,cost_mpe.ravel(), width = wd, alpha = 1, label = 'MPE')
plt.legend()
plt.title('Comparison of cost function on 9 points for different estimates')
plt.show()

jtp = sns.jointplot(k_max_negloglik[:,0], k_max_negloglik[:,1])
jtp.plot_joint(plt.plot, ls = '')
plt.show()


##* dim =4 --------------------------------------------------------------------------------------

filename = 'k_max_4dim_LHS'
k1d = np.linspace(0,1.5,100)

input = open(filename, 'rb')
dictionnary_save = pickle.load(input)
input.close()
k_max_negloglik = dictionnary_save['k_max_negloglik']
AP_saved = dictionnary_save['AP_saved']
negloglik_saved = dictionnary_save['negloglik_saved']




k_max_mean = np.mean(k_max_negloglik,0)
k_max_std = np.std(k_max_negloglik,0)



ind_0_k1 = (k_max_negloglik[:,0]<0.001)
prop_0_k1 = np.sum(ind_0_k1)/400.0
dens_k1 = stats.gaussian_kde(k_max_negloglik[~ind_0_k1,0])

ind_0_k2 = (k_max_negloglik[:,1]<0.001)
prop_0_k2 = np.sum(ind_0_k2)/400.0
dens_k2 = stats.gaussian_kde(k_max_negloglik[~ind_0_k2,1])

ind_0_k3 = (k_max_negloglik[:,2]<0.001)
prop_0_k3 = np.sum(ind_0_k3)/400.0
dens_k3 = stats.gaussian_kde(k_max_negloglik[~ind_0_k3,2])

ind_0_k4 = (k_max_negloglik[:,3]<0.001)
prop_0_k4 = np.sum(ind_0_k4)/400.0
dens_k4 = stats.gaussian_kde(k_max_negloglik[~ind_0_k4,3])

kde_joint = stats.gaussian_kde(k_max_negloglik.T)

k1_mpe = optimize.minimize(fun = lambda k: -dens_k1(k),x0 = 0.2, bounds = [(0.0,None)]).x
k2_mpe = optimize.minimize(fun = lambda k: -dens_k2(k),x0 = 0.2, bounds = [(0.0,None)]).x
k3_mpe = optimize.minimize(fun = lambda k: -dens_k3(k),x0 = 0.2, bounds = [(0.0,None)]).x
k4_mpe = optimize.minimize(fun = lambda k: -dens_k4(k),x0 = 0.2, bounds = [(0.0,None)]).x

k_mpe = np.array([k1_mpe, k2_mpe, k3_mpe, k4_mpe])[:,0]

xr_ext = np.linspace(1,100,200)
Kref_ext = 0.2*(1+np.sin(2*np.pi*xr_ext/D[1]))
# Kref_ext = 1 + 0.2*np.sin(2*np.pi*xr_ext/D[1])

mpe_joint = optimize.minimize(fun = lambda k: np.exp(-kde_joint(k)), x0 = [0.2,0.2,0.2,0.2], bounds = [(0.0,None)]*4)
k_mpe_joint = mpe_joint.x


plt.subplot(2,1,1)
for i in xrange(0,1000,3):
    plt.plot(xr_ext, map(interp(k_max_negloglik[i,:]),xr_ext), 'k', linewidth = 0.05)
plt.plot(xr_ext, Kref_ext, 'r--', label= r'$\mathbf{k}_{\mathrm{ref}}$')
plt.plot(xr_ext, map(interp(opt_multidim.x), xr_ext), label = 'w/o uncertainties')
plt.plot(xr_ext, map(interp(k_max_mean), xr_ext), label = 'marginal means')
plt.plot(xr_ext, map(interp(k_mpe), xr_ext), label = 'marginal mpe')
plt.plot(xr_ext, map(interp(k_mpe_joint), xr_ext), label = 'joint mpe')
plt.legend()

plt.subplot(2,4,5)
plt.hist(k_max_negloglik[:,0], 30, density = True)
plt.plot(k1d, dens_k1(k1d))
plt.title(r'$k_0$')
plt.xlim([k_max_negloglik[:,0].min(), k_max_negloglik[:,0].max()])

plt.subplot(2,4,6)
plt.hist(k_max_negloglik[:,1], 20, density = True)
plt.plot(k1d, dens_k2(k1d))
plt.title(r'$k_1$')
plt.xlim([k_max_negloglik[:,1].min(), k_max_negloglik[:,1].max()])

plt.subplot(2,4,7)
plt.hist(k_max_negloglik[:,2], 30, density = True)
plt.plot(k1d, dens_k3(k1d))
plt.title(r'$k_2$')
plt.xlim([k_max_negloglik[:,2].min(), k_max_negloglik[:,2].max()])

plt.subplot(2,4,8)
plt.hist(k_max_negloglik[:,3], 30, density = True)
plt.plot(k1d, dens_k4(k1d))
plt.title(r'$k_3$')
plt.xlim([k_max_negloglik[:,3].min(), k_max_negloglik[:,3].max()])
plt.tight_layout()
plt.show()

print prop_0_k1
print prop_0_k2
print prop_0_k3
print prop_0_k4
    
cost_true = (cost_computation_bounds(opt_multidim.x))
cost_mean = (cost_computation_bounds(k_max_mean))
cost_0 = (cost_computation_bounds(np.array([0,0,0])))
cost_mpe_joint = (cost_computation_bounds(k_mpe_joint))
cost_mpe = (cost_computation_bounds(k_mpe))

plt.subplot(2,1,1)
Nbar = 5
wd = (1.0 / float(Nbar))*0.75
Xind = np.arange(9)
plt.bar(Xind, cost_true.ravel(), width = wd, alpha = 1, label = 'w/o unc')
plt.bar(Xind + wd, cost_mean.ravel(), width = wd, alpha = 1, label = 'mean of families')
plt.bar(Xind + 2*wd, cost_0.ravel(), width = wd, alpha = 1, label = '0')
plt.bar(Xind + 3*wd, cost_mpe_joint.ravel(), width = wd, alpha = 1, label = 'joint MPE')
plt.bar(Xind + 4*wd, cost_mpe.ravel(), width = wd, alpha = 1, label = 'marginal MPE')
for i in Xind:
    plt.axvline(x=(i + 0.95), ls =':')
# plt.axvline(x= 2)
plt.legend()
plt.show()



k_max_df = pd.DataFrame(k_max_negloglik , columns = ['k_0','k_1','k_2', 'k_3'])
k_max_df['label'] = 'k_max'
best = pd.DataFrame([opt_multidim.x]*5, columns = ['k_0','k_1','k_2', 'k_3'])
best['label'] = 'best'
means = pd.DataFrame([k_max_mean]*5, columns = ['k_0','k_1','k_2', 'k_3'])
means['label'] = 'MMAP'
jmpe = pd.DataFrame([k_mpe_joint]*5, columns = ['k_0','k_1','k_2', 'k_3'])
jmpe['label'] = 'JMPE'
mmpe =  pd.DataFrame([k_mpe]*5, columns = ['k_0','k_1','k_2', 'k_3'])
mmpe['label'] = 'MMPE'
full_k = pd.concat([k_max_df, best, means, jmpe, mmpe])


sns.set()
g = sns.PairGrid(full_k, hue = 'label',
                 hue_order = ['k_max', 'best', 'MMAP', 'JMPE', 'MMPE'],
                 hue_kws = {'s':[10,60,60,60,60]})

g = g.map_upper(plt.scatter)
# g = g.map_lower(sns.kdeplot, cmap="Blues_d", vars = 'k_max')
g = g.map_diag(plt.hist)
g.map_lower(sns.kdeplot)
g.add_legend()

plt.show()



plt.subplot(2,1,2)
for i in xrange(9):
    AP = generate_AP_discrete(i, Ngrid = 3)
    plt.plot([5],[50])
    plt.text(x = AP[0], y = AP[1],s = str(i))
    plt.xlim([4.6,5.4])
    plt.ylim([49.6,50.4])
    plt.grid(alpha = 0.2)
plt.show()

import corner
corner.corner(k_max_negloglik)
plt.show()
