#!/usr/bin/env python
# coding: utf-8


# ---------------------------------------------------------------------------------
#               HAMILTONIAN MONTE CARLO -- with pyhmc
# ---------------------------------------------------------------------------------

import haute_resolution.wrapper_HR as swe

import matplotlib.pyplot as plt
import pickle
import numpy as np

from pyhmc import hmc

# -- Paramètres de référence ------------------------------------------------------
# print 'Hauteur eau moyenne = ', swe.mean_h
# print 'Amplitude = ', swe.amplitude
# print 'Periode = ', swe.period
# print 'Phase = ', swe.phase

# [xr, href, uref, t] = swe.swe_KAP(swe.Kref, 5.0, 15.0)
# [xr, hrefApPm, urefApPm, t] = swe.swe_KAP(swe.Kref, 5.1, 14.8)
# [xr, hrefApPp, urefApPp, t] = swe.swe_KAP(swe.Kref, 5.2, 15.1)
# [xr, hrefAmPp, urefAmPp, t] = swe.swe_KAP(swe.Kref, 4.9, 15.2)
# [xr, hrefAmPm, urefAmPm, t] = swe.swe_KAP(swe.Kref, 4.8, 14.9)
# [xr, hrefAmPp50, urefAmPp50, t] = swe.swe_KAP(np.ones(200) * 3, 5.0, 15.0)
# animate_SWE(xr, [href, hrefAmPp50], swe.b, swe.D, ylim = [0, 30])

# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/href.npy'), href)
# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/hrefApPm.npy'), hrefApPm)
# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/hrefApPp.npy'), hrefApPp)
# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/hrefAmPp.npy'), hrefAmPp)
# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/hrefAmPm.npy'), hrefAmPm)
href = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
                '/href.npy'))
hrefApPm = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
                    '/hrefApPm.npy'))
hrefApPp = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
                    '/hrefApPp.npy'))
hrefAmPp = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
                    '/hrefAmPp.npy'))
hrefAmPm = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
                    '/hrefAmPm.npy'))


# -- HAMILTONIAN MONTE CARLO ----------------------------------------------------


def logp(Kcoeff):
    KAPin = (Kcoeff, 5.0, 15.0)
    minlogp, gradlogp = swe.J_KAP_array(KAPin, idx_to_observe=swe.idx_to_observe,
                                        hreference=href, adj_gradient=True)
    return - minlogp, -gradlogp

samples = hmc(logp, x0= [0.25], n_samples=int(10), epsilon = 0.01, n_steps = 10, display = True)

samples2 = hmc(logp, x0= [samples[-1]], n_samples=int(50), epsilon = 0.07, n_steps = 5, display = True)
samples = np.append(samples,samples2)
plt.plot(np.exp(samples))
plt.show()


output = open('HMC_samples', 'wb')
dic = {'samples' :samples}
pickle.dump(dic, output)
output.close()
