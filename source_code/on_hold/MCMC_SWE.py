# coding: utf-8
#!/usr/bin/env python

import SWE_wrapper as swe
import matplotlib.pyplot as plt
import pickle
import numpy as np
from scipy import stats, optimize

mean_h = 20 
phase = 0
amplitude =  5
period =  50
# href = y_obs
Kref = 0.25
[xr, href, uref, t] = swe.reference(Kref)

Nobs = href.size

@np.vectorize
def prior_K(k):
    return np.where( (k>0. and k<0.5), 0.5, 0)
@np.vectorize
def prior_A(a):
    return np.where( (a>4.9 and a<5.1),0.2,0)
@np.vectorize
def prior_P(p):
    return np.where((p>49.9 and p<50.1), 0.2,0)

def likelihood(K,ampli = amplitude, period = period):
    # bcL = lambda h, hu, t: BCrand(h, hu, t, 'L',
    #                              mean_h, ampli, period, phase)
    negloglik = swe.shallow_water_RSS(swe.D, swe.g, swe.T, swe.h0, swe.u0, swe.N, swe.LF_flux, swe.dt, swe.b, K,
                                  lambda h, hu, t: swe.BCrand(h, hu, t, 'L',
                                                          mean_h, ampli, period, phase),                            
                                  swe.bcR, href, swe.J_function)/Nobs
    return np.exp(-negloglik)

@np.vectorize
def likelihood_gradient(Kvec,ampli,period):
    bcL = lambda h, hu, t: BCrand(h, hu, t, 'L',
                                 mean_h, ampli, period, phase)

    negloglik = 0.0*np.ones_like(Kvec)

    grad_negloglik = 0.0*np.ones_like(Kvec)
    
    for i,K in enumerate(Kvec):
        negloglik[i], grad_negloglik[i] = shallow_water_RSS_grad(D, g, T, h0, u0, N, LF_flux, dt, b, K,
                                  bcL, bcR, bcL_A, bcR_A, href, J_function)
    
    return np.array([np.exp(-negloglik/Nobs), -np.exp(-negloglik/Nobs)*grad_negloglik/Nobs])

def normalize_vector_1D(Ve, delta):
    return Ve / (delta*np.sum(Ve))

def normalize_vector_2D(grid, delta_x, delta_y):
    return grid / (delta_x*delta_y*np.sum(grid))


def proposal_distribution(KAP_current, sigma = 0.05):
    K_found = False
    A_found = False
    P_found = False
    K_current,A_current,P_current = KAP_current
    while not K_found:
        K_new = np.random.normal(K_current, sigma)
        K_found =  (prior_K(K_new) !=0)
    while not A_found:
        A_new = np.random.normal(A_current, sigma)
        A_found = (prior_A(A_new) != 0)
    while not P_found:
        P_new = np.random.normal(P_current, sigma)
        P_found = (prior_P(P_new) != 0)
    return [K_new, A_new, P_new]

def MCMC_step(KAP_current, likelihood_current):
    KAP_candidate = proposal_distribution(KAP_current, sigma=0.02)
    K_can,A_can,P_can = KAP_candidate
    likelihood_candidate = likelihood(K_can, A_can, P_can)*prior_K(K_can)*prior_A(A_can)*prior_P(P_can)
    # print prior_K(K_can)
    # print prior_A(A_can)
    # print prior_P(P_can)
    alpha = likelihood_candidate/likelihood_current
    U = np.random.uniform()
    if U<alpha:
        # print "accept"          
        return [KAP_candidate, likelihood_candidate]
    else:
        # print "reject"
        return [KAP_current, likelihood_current]

def MCMC_procedure(N_iteration, KAP_previous = None):
    KAP_MCMC = np.empty([N_iteration,3])
    if KAP_previous is None:
        KAP_MCMC[0,:] = [np.random.uniform(0.,0.2),
                         np.random.uniform(4.9,5.1),
                         np.random.uniform(49.9,50.1)]
    else:
        KAP_MCMC[0,:] = KAP_previous[-1,:]
                         
    i = 1  
    likelihood_current = likelihood(KAP_MCMC[0,0], KAP_MCMC[0,1], KAP_MCMC[0,2])
    
    while i<N_iteration:
        print i
        KAP_MCMC[i,:],likelihood_current= MCMC_step(KAP_MCMC[i-1,:], likelihood_current)
        i += 1
    if KAP_previous is not None:
        KAP_MCMC = np.vstack([KAP_previous, KAP_MCMC])
    return KAP_MCMC


KAP_MCMC_12500 = MCMC_procedure(2500, KAP_MCMC_10000)


KAP_MCMC_6000 = MCMC_procedure(2000, KAP_MCMC_4000)


KAP_MCMC = KAP_MCMC_12500

# output = open("KAP_MCMC", 'wb')
# pickle.dump(KAP_MCMC, output)
# output.close()

input = open("KAP_MCMC", 'rb')
KAP_MCMC = pickle.load(input)
input.close()

plt.subplot(311)
plt.plot(KAP_MCMC[:,0])
plt.subplot(312)
plt.plot(KAP_MCMC[:,1])
plt.subplot(313)
plt.plot(KAP_MCMC[:,2])
plt.show()

nbreaks = 50
plt.subplot(311)
plt.hist(KAP_MCMC[100:,0], nbreaks, density=True)
density_est_K = stats.gaussian_kde(KAP_MCMC[:,0])
plt.plot(np.arange(0,0.5,0.005), density_est_K(np.arange(0,0.5,0.005)))
plt.plot([Kref,Kref],[0,3])
plt.subplot(312)
plt.hist(KAP_MCMC[100:,1], nbreaks, density = True)
density_est_A = stats.gaussian_kde(KAP_MCMC[:,1])
plt.plot(np.arange(4.9,5.1,0.005), density_est_A(np.arange(4.9,5.1,0.005)))
plt.plot([amplitude, amplitude], [0,5])
plt.subplot(313)
plt.hist(KAP_MCMC[100:,2], nbreaks, density = True)
density_est_P = stats.gaussian_kde(KAP_MCMC[:,2])
plt.plot(np.arange(49.9,50.1,0.005), density_est_P(np.arange(49.9,50.1,0.005)))
plt.plot([period, period], [0,5])
plt.show()

# >>> >>> >>> Hauteur eau moyenne =  20
# Amplitude =  4.98674268725
# Periode =  49.9572971629
# Phase =  0
KAP_kde_3d = stats.gaussian_kde(KAP_MCMC.T)
