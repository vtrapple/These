import numpy as np
from scipy.spatial.distance import cdist
from scipy.stats import beta, uniform, norm
import matplotlib.pyplot as plt

beta.pdf(0.5,2,1)
Ker_beta = lambda a,b,x: beta.pdf(x,a,b)
X= uniform.rvs(size = 100)

b = lambda n: 1.0/n
def KDE_beta(X, b=b, gkde = False):
    n = float(X.shape[0])
    b_val = b(n)
    if not gkde:
        KB = lambda x,Xobs: Ker_beta(1.0 + x/b_val, 1.0+ (1.0-x)/b_val, Xobs)
    else:
        KB = lambda x,Xobs: norm.pdf(x,Xobs,b_val)
    KDE = lambda x: (1/n)*sum([KB(x,Xi) for Xi in X])
    return KDE

plt.hist(X, density = True)
for i in xrange(5,20):
    b = lambda n: 1.06*X.var()*(n**(-1/float(i)))
    
    KDE_X = KDE_beta(X, b, gkde = False)
    GKDE_X = KDE_beta(X, b, gkde = True)
    plt.plot(np.linspace(0,1,200),KDE_X(np.linspace(0,1,200)), 'r')
    # plt.plot(np.linspace(0,1,200),GKDE_X(np.linspace(0,1,200)), 'm')
    plt.ylim([0,2.5])
    plt.pause(0.5)
plt.show()
