import numpy as np
from scipy.spatial.distance import cdist
from scipy.stats import beta, uniform, norm, gamma, gaussian_kde
import scipy as sp
import matplotlib.pyplot as plt




def beta_kde(X, b = None, kernel_type = 1):
    if b is None:
        b= np.var(X)*X.shape[0]**(-0.2)
    print b
    n = float(X.shape[0])
    
    if kernel_type == 1:
        def beta_kernel(Xobs, z):
            return beta.pdf(Xobs, z/b + 1, (1-z)/b + 1)

    elif kernel_type == 2:
        rho_z = lambda z: 2*b**2 + 2.5 - np.sqrt(4*b**4 + 6*b**2 + 2.25 - z**2 - z/b)
        def beta_kernel(Xobs, z):
            if (z >= 0 and z < 2*b):
                return beta.pdf(Xobs, rho_z(z), (1-z)/b)
            elif (z >= 2*b and z <= 1-2*b):
                return beta.pdf(Xobs, z/b, (1-z)/b)
            elif (z >= 2*b and z <= 1-2*b):
                return beta.pdf(Xobs, z/b, rho_z(1-z))
        
    KDE = lambda x: (1/n)*sum([beta_kernel(x,Xi) for Xi in X])
    return KDE

X = uniform.rvs(size = 20)
plt.hist(X, density = True)
beta_1 = beta_kde(X, b = 0.1)
# beta_2 = beta_kde(X, b = 0.1, kernel_type = 2)
x_plot = np.linspace(0,1,100)
plt.plot(x_plot, beta_1(x_plot), label = 'type 1')
# plt.plot(x_plot, beta_2(x_plot), label = 'type 2')
print sp.integrate.quad(beta_1, a = 0, b = 1)
# print sp.integrate.quad(beta_2, a = 0, b = 1)
plt.legend()
plt.show()

for i in xrange(5,20):
    b = lambda n: 1.06*X.var()*(n**(-1/float(i)))
    
    KDE_X = beta_kde(X, b =  1.06*X.var()*(X.shape[0]**(-1/float(i))))
    plt.plot(np.linspace(0,1,200),KDE_X(np.linspace(0,1,200)), 'r')
    # plt.plot(np.linspace(0,1,200),GKDE_X(np.linspace(0,1,200)), 'm')
    plt.ylim([0,2.5])
    plt.pause(0.5)
plt.show()

@np.vectorize
def rho_b(x,b):
    if x >= 2*b:
        return x/b
    else:
        return 0.25*(x/b)**2 + 1

def gamma_kernel_1(X,b,x):
    # return X**(x/b) * np.exp(-X/b) / (b**(x/b + 1)*sp.special.gamma(x/b +1))  #
    return gamma.pdf(X, x/b +1, scale = b)

def gamma_kernel_2(X,b,x):
    return gamma.pdf(X, rho_b(x,b), scale =  b)

sp.integrate.quad(gamma_kernel_1, a = 0, b = np.infty, args =(1,1))
sp.integrate.quad(gamma_kernel_2, a = 0, b = np.infty, args =(1,1))


def gamma_kde(X,b = None, kernel_type = 1):
    n = float(X.shape[0])

    if b is None:
        bkde = np.var(X)*n**(-0.2)
    elif callable(b):
        bkde = b(n)
    else:
        bkde = b

    if kernel_type == 1:
        Kernel = lambda x,Xobs: gamma_kernel_1(Xobs,bkde,x)
    elif kernel_type == 2:
        Kernel = lambda x,Xobs: gamma_kernel_2(Xobs,bkde,x)
        
    KDE = lambda x: (1/n)*sum([Kernel(Xi, x) for Xi in X])
    return KDE

X = uniform.rvs(size = 100)
X = -np.log(X)+  np.exp(X)


gamma_1 = gamma_kde(X, kernel_type =2)
sp.integrate.quad(gamma_1, a = 0, b = np.infty)
x_vec = np.linspace(0,10,200)
plt.hist(X, density = True)
plt.plot(x_vec, gamma_1(x_vec), label = 'gamma 1')
plt.show()

plt.plot(X,np.zeros(X.shape[0]), '+')
gkde = gaussian_kde(X)

for expo in [1.,2.,3.,4.,5.,6.]:
    b = lambda n: n**(-expo**(-1))
    gamma_1 = gamma_kde(X,b)
    # gamma_2 = gamma_KDE(b, X, kernel_type = 2)
    # plt.plot(X,np.zeros(X.shape[0]), '+')
    x_vec = np.linspace(0,X.max(),500)
    plt.plot(x_vec, gamma_1(x_vec), label = 'gamma 1')
    # plt.plot(x_vec, gamma_2(x_vec), label = 'gamma 2')
    # print sp.integrate.quad(gamma_1, a = 0, b = np.infty)

    plt.pause(0.2)
plt.show()


class gamma_kd(object):
    def __init__ (self, dataset, bw_method = None):
        self.dataset = np.atleast_2d(dataset)
        if not self.dataset.size > 1:
            raise ValueError("`dataset` input should have multiple elements.")

        self.d, self.n = self.dataset.shape
        self.set_bandwidth(bw_method=bw_method)
