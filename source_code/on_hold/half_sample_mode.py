#!/usr/bin/env python
# coding: utf-8

##.*
import numpy as np

def HS_mode(X):
    """
    Estimation of the mode based on Half-sample algorithm.
    """
    Xs = np.sort(X)
    n = X.shape[0]
    if n == 1:
        return X
    elif n ==2:
        return (X[0]+X[1]) / 2.0
    elif n == 3:
        decision = (Xs[1]-Xs[0]) - (Xs[2] - Xs[1])
        if decision < 0:
            return (Xs[0] + Xs[1])/ 2.0
        elif decision > 0:
            return (Xs[2] + Xs[1])/ 2.0
        else:
            return Xs[1]
    else:
        wmin = Xs[-1] - Xs[0]
        N = int(np.ceil(n/2.0))
        for i in xrange(n-N+1):
            w = Xs[i+N-1] - Xs[i]
            if w<wmin:
                wmin = w
                j=i
        return HS_mode(Xs[j: j+N-1])



if __name__ == '__main__':
    import matplotlib.pyplot as plt
    X =  np.exp(np.random.randn(500)+10)
    estimate_mode = HS_mode(X)        
    plt.hist(X, 50)
    plt.axvline(estimate_mode)
    plt.show()
