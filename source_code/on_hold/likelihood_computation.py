#!/usr/bin/env python
# coding: utf-8

##.* Definition of parameter and useful functions

from SWE_wrapper import *
import matplotlib.pyplot as plt
import pickle
from scipy import stats, optimize
import weighted_kde 

mean_h = 20 
phase = 0
amplitude = 5.0
period = 50.0
# href = y_obs
# Ap_Pm -> A = 5.1; P = 49.8
# Ap_Pp -> A = 5.2; P = 50.1
# Am_Pp -> A = 4.9; P = 50.2
# Am_Pm -> A = 4.8; P = 49.9

@np.vectorize
def posterior(K,ampli,period):
    lik = likelihood(K,ampli,period)
    return lik*prior_A(ampli)*prior_K(K)*prior_P(period)

def likelihood_gradient(Kvec,ampli,period):
    bcL = lambda h, hu, t: BCrand(h, hu, t, 'L',
                                 mean_h, ampli, period, phase)

    negloglik = 0.0*np.ones_like(Kvec)
    grad_negloglik = 0.0*np.ones_like(Kvec)
    
    for i,K in enumerate(Kvec):
        negloglik[i], grad_negloglik[i] = shallow_water_RSS_grad(D, g, T, h0, u0, N, LF_flux, dt, b, K,
                                  bcL, bcR, bcL_A, bcR_A, href, J_function)
    
    return np.array([np.exp(-negloglik/Nobs), -np.exp(-negloglik/Nobs)*grad_negloglik/Nobs])

def normalize_vector_1D(Ve, delta):
    return Ve / (delta*np.sum(Ve))

def normalize_vector_2D(grid, delta_x, delta_y):
    return grid / (delta_x*delta_y*np.sum(grid))


##.* Definition of the meshgrid of evaluation

delta_K = 0.05
# K_vector = np.arange(0.0, 0.8, delta_K)
# K_vector = np.linspace(0.0, 0.8, 25, endpoint = True)
K_vector = np.linspace(0.0, 1.2, 40, endpoint = True)

delta_A = 0.03
# amplitude_vector = np.arange(4.7, 5.3, delta_A, endpoint = True)
amplitude_vector = np.linspace(4.7,5.3,30, endpoint = True)

delta_P = 0.03
period_vector = np.arange(49.7, 50.3, delta_P)
period_vector = np.linspace(49.7,50.3,30, endpoint = True)

# K_t, A_t, P_t = np.meshgrid([1.0],[4,5],[49,50,51]) # Meshgrid for test

K_mesh, A_mesh, P_mesh = np.meshgrid(K_vector, amplitude_vector, period_vector)

# lik_test = likelihood(K_t,A_t,P_t)

lik_KAP = likelihood(K_mesh, A_mesh, P_mesh)



output = open('lik_KAP_range_06_36000_Am_Pm', 'wb')
pickle.dump(lik_KAP, output)
output.close()

input = open('lik_KAP_range_06_36000', 'rb')
lik_KAP = pickle.load(input)
input.close()

##.* Computation of the evidence

pK_Y = np.sum(lik_KAP, axis = (1))*(amplitude_vector[1]-amplitude_vector[0])*(period_vector[1] - period_vector[0])
evidence = np.trapz(lik_KAP, dx = K_vector[1]-K_vector[0], axis = (1)) # shape (A,P)

def generate_AP_discrete(iter,Ngrid=30):
    A = np.linspace(4.7,5.3, Ngrid)
    P = np.linspace(49.7,50.3, Ngrid)

    # A = np.linspace(4.95,5.05, Ngrid)
    # P = np.linspace(49.95,50.05, Ngrid)
    return np.array([A[iter%Ngrid], P[iter/Ngrid]])


evidence_vector = np.ravel(evidence, 'F') # evidence_vector[i] <=> generate_AP_discrete(i)
# output = open('evidence_025_06range_Ap_Pm', 'wb')
# pickle.dump(evidence_vector, output)
# output.close()


###.* Plots 
# nlevels = 50
# for i in np.arange(0,9):
#     plt.subplot(3,3,i+1)
#     plt.contourf(K_mesh[i,:,:], P_mesh[i,:,:],
#                  normalize_vector_2D(lik_KAP[i,:,:],delta_A,delta_P),
#                  nlevels) 
#     plt.xlabel('K')
#     plt.ylabel('P')
#     plt.title(np.round(amplitude_vector[i],2))
#     plt.plot([K_vector[0],K_vector[-1]], [period, period])
#     plt.plot([Kref,Kref], [period_vector[0], period_vector[-1]])
# plt.suptitle('A constant')
# plt.show()

# for i in np.arange(0,9):
#     plt.subplot(3,3,i+1)
#     plt.contourf(K_mesh[:,:,i],A_mesh[:,:,i],
#                  normalize_vector_2D(lik_KAP[:,:,i],delta_K,delta_A),
#                  nlevels) 
#     plt.xlabel('K')
#     plt.ylabel('A')
#     plt.title(np.round(period_vector[i],2))
#     plt.plot([K_vector[0],K_vector[-1]], [amplitude, amplitude])
#     plt.plot([Kref,Kref], [amplitude_vector[0], amplitude_vector[-1]])
# plt.suptitle('P constant')
# plt.show()

# for i in np.arange(0,9):
#     plt.subplot(3,3,i+1)
#     plt.contourf(A_mesh[:,i,:],P_mesh[:,i,:],
#                  normalize_vector_2D(lik_KAP[:,i,:], delta_A, delta_P),
#                  nlevels) 
#     plt.xlabel('A')
#     plt.ylabel('P')
#     plt.title(np.round(K_vector[i],2))
#     plt.plot([amplitude_vector[0],amplitude_vector[-1]], [period, period])
#     plt.plot([amplitude, amplitude], [period_vector[0], period_vector[-1]])
# plt.suptitle('K constant')
# plt.show()


# integrate wrt to U = (A,P):
pK_Y = np.sum(lik_KAP, axis = (0,2))*(amplitude_vector[1]-amplitude_vector[0])*(period_vector[1] - period_vector[0])
plt.plot(K_vector, pK_Y, label = r'$p_{K|Y}(\mathbf{k} | \mathbf{y}^{\mathrm{obs}}$)')
plt.xlabel(r'$\mathbf{k}$')
plt.ylabel(r'$p(\mathbf{k} | \mathbf{y}^{\mathrm{obs}})$')
plt.title(r'Marginal density of $\mathbf{K}$')
plt.legend()
plt.grid(linewidth = 0.2)
plt.tight_layout()
plt.show()

for i in xrange(40):
    plt.contourf(A_mesh[:,0,:],P_mesh[:,0,:],lik_KAP[:,i,:], 50)
    plt.clim([0.95,1.0])
    plt.pause(0.2)
    plt.title(K_vector[i])
plt.show()

K_plot = np.linspace(0,1.2,500)

plt.subplot(2,2,1) # MMAP
p_K_Y = np.empty(K_vector.shape)
for i in xrange(0,K_vector.shape[0]):
    p_K_Y[i] = np.sum(lik_KAP[:,i,:])

p_K_Y = normalize_vector_1D(p_K_Y, delta_K)
K_MAP = K_vector[np.argmax(p_K_Y)]
plt.plot(K_vector, p_K_Y)
plt.xlabel('K')
plt.ylabel(r"$p_{K|Y}(k|y^{\mathrm{obs}})$")
plt.title('$p_{K|Y}$ and MAP estimate')
plt.axvline(x = Kref, color= 'r', label = r'$k_{\mathrm{true}}$')
plt.axvline(x = K_MAP,color = 'm')
plt.grid()

plt.subplot(2,2,2) # Variance
var_K = np.empty(K_vector.shape)
for i in xrange(0,K_vector.shape[0]):
    var_K[i] = np.var(lik_KAP[:,i,:])
plt.plot(K_vector, np.sqrt(var_K))
plt.xlabel('K')
plt.ylabel("Var")
K_varmin = K_vector[var_K.argmin(0)]
plt.axvline(x = K_varmin,color = 'm')
plt.title('Variance')
plt.axvline(x = Kref, color= 'r', label = r'$k_{\mathrm{true}}$')
plt.grid()

# plt.subplot(2,2,3)              # Most Probable Estimate
K_argmax = np.empty(0)
val_argmax = np.empty(0)
for i in xrange(30):
    for j in xrange(30):
        K_argmax = np.append(K_argmax,K_vector[np.argmax(lik_KAP[i,:,j])])
        val_argmax = np.append(val_argmax,lik_KAP[i,:,j].max())

        
K_censored = K_argmax[(~ (K_argmax < 0.01))]
prop_dirac_0 = np.sum((K_argmax < 0.01))/float(K_argmax.shape[0])
# plt.hist(K_argmax, 10, density = True)
ker = stats.gaussian_kde(K_argmax)
ker_censored = stats.gaussian_kde(K_censored)
# plt.plot(K_plot, ker(K_plot))
# plt.plot(K_plot, ker_censored(K_plot))
# plt.axvline(x = Kref, color= 'r', label = r'$k_{\mathrm{true}}$')
k_max = scipy.optimize.minimize_scalar(lambda x: -ker_censored(x)).x
# plt.axvline(x = k_max,color = 'm')
# plt.title('Histogram of Most Probable')
# plt.xlabel(r'$k_{\max}$')
# plt.grid()




plt.subplot(2,2,4) # lambda
lam = 10
rho_lam = p_K_Y - lam*np.sqrt(var_K)
plt.plot(K_vector, rho_lam)
K_rho = K_vector[np.argmax(rho_lam)]
plt.plot([K_rho, K_rho], [np.min(rho_lam), np.max(rho_lam)])
plt.title('Lambda, k_lambda = '+ str(K_rho))
plt.axvline(x = Kref, color= 'r', label = r'$k_{\mathrm{true}}$')
plt.axvline(x = K_rho,color = 'm')
plt.xlabel('K')
plt.grid()

plt.tight_layout()
plt.show()


plt.figure(figsize=(8.27, 8.0), dpi=100)
normalized_density = np.ones_like(lik_KAP)
max_values = np.empty(0)
plt.subplot(2,1,1)
for i in xrange(30):
    for j in xrange(30):
        norm_value = np.sum(lik_KAP[i,:,j])*(K_vector[1]-K_vector[0])
        normalized_density[i,:,j] = lik_KAP[i,:,j]/norm_value
        plt.plot(K_vector,normalized_density[i,:,j], linewidth = 0.005, color = 'k')
        max_values = np.append(max_values, K_vector[lik_KAP[i,:,j].argmax()])

var_vector = np.ones_like(K_vector)
worst_case = np.ones_like(K_vector)

for i in xrange(40):
    var_vector[i] = np.var(normalized_density[:,i,:])
    worst_case[i] = normalized_density[:,i,:].min()

pK_Y = np.sum(lik_KAP, axis = (0,2))*(amplitude_vector[1]-amplitude_vector[0])*(period_vector[1] - period_vector[0])
pK_Y = pK_Y/(np.sum(pK_Y*(K_vector[1]-K_vector[0])))

plt.plot(K_vector, pK_Y, 'b', linewidth = 2, label = r'$p(\mathbf{k}|\mathbf{y}^{\mathrm{obs}})$')
plt.plot(K_vector, worst_case, color = 'orange', label = r'$\min \, p(\mathbf{k}|\mathbf{y}^{\mathrm{obs}},\mathbf{u})$')
plt.plot(K_plot, 0.76+0.03*ker_censored(K_plot), color = 'm', label = 'KDE')
plt.stem([0],[.76+0.03*prop_dirac_0], bottom = 0.76, linefmt = 'm', markerfmt = 'm^')
k_max = scipy.optimize.minimize_scalar(lambda x: -ker_censored(x)).x
plt.axvline(k_max, ls = '--', color = 'm', label = 'MPE')
plt.axvline(K_vector[pK_Y.argmax()], ls = '--', color = 'b', label = 'MMAP')
plt.axvline(0.25, ls = '--', color = 'r', label = 'True value')
plt.axvline(K_vector[worst_case.argmax()], color = 'orange', ls = '--', label = 'Worst-case')
plt.grid(alpha = 0.5)

plt.title(r'Family of densities parametrized by $\mathbf{u}$: $\{p(\mathbf{k}|\mathbf{y}^{\mathrm{obs}},\mathbf{u}) | \mathbf{u}\}$')
plt.ylabel(r'$p(\mathbf{k}|\mathbf{y}^{\mathrm{obs}})$')
# plt.plot(max_values+0.005*np.random.randn(900),0.7+np.zeros(max_values.shape), '+', linewidth = 0.2)
plt.legend()

plt.subplot(2,1,2)
plt.plot(K_vector, np.sqrt(var_vector), label = r'$\sqrt{\mathbb{V}\left[p(\mathbf{k}|\mathbf{y}^{\mathrm{obs}},\mathbf{U})\right]} $', color = 'g')
plt.grid(alpha = 0.5)
plt.title(r'SD: $\sigma(\mathbf{k}) = \sqrt{\mathbb{V}_U[p(\mathbf{k}|\mathbf{y}^{\mathrm{obs}},\mathbf{U})]}$')
plt.xlabel(r'$\mathbf{k}$')
plt.ylabel(r'Standard Deviation')
plt.axvline(K_vector[var_vector.argmin()], ls = '--', color = 'g', label = 'Min of variance')
plt.axvline(0.25, ls = '--', color = 'r', label = 'True value')
plt.legend()
plt.tight_layout()

plt.savefig('MMAP_minvariance.png')
plt.show()


plt.plot(5,50, '.')
plt.plot(5.1,49.8, '.')
plt.plot(5.2,50.1, '.')
plt.plot(4.9,50.2, '.')
plt.plot(4.8,49.9, '.')
plt.xlim([4.7,5.3])
plt.ylim([49.7,50.3])
plt.grid(alpha = 0.5)
plt.text(5,50, 'Centered conditions')
plt.text(5.1,49.8, 'Ap_Pm')
plt.text(5.2,50.1, 'Ap_Pp')
plt.text(4.9,50.2, 'Am_Pp')
plt.text(4.8,49.9, 'Am_Pm')
plt.xlabel(r'$A$ (Amplitude)')
plt.ylabel(r'$P$ (Period)')
plt.title(r'Position of the values of $\mathbf{u}_{\mathrm{ref}}$')
plt.show()


plt.subplot(2,1,1)
plt.subplot(2,2,1)
plt.plot(K_argmax, evidence_vector, '.')
plt.title('evidence')
plt.subplot(2,2,2)
plt.plot(K_argmax, val_argmax , '.')
plt.title('val argmax')
plt.subplot(2,1,2)
plt.plot(K_argmax, val_argmax*evidence_vector, '.')
plt.title('val argmax*evidence')
plt.show()

plt.hist(K_argmax, 27, alpha = 0.5, density = True, label =r'Histogram of $K_{\mathrm{argmax}}$')
plt.hist(K_argmax, 27, weights = val_argmax, alpha = 0.5, density = True,label =r'Weighted histogram of $K_{\mathrm{argmax}}$')
plt.legend()
plt.title(r'Histogram of $K_{\mathrm{argmax}}$')
plt.show()
sns.set_style('white')
plt.title(r'Histogram of $K_{\mathrm{argmax}}$')
plt.hist([K_argmax, K_argmax, K_argmax, K_argmax], 27, weights = [np.ones_like(K_argmax), val_argmax*evidence_vector, val_argmax, evidence_vector], density = [True, True, True, True], label = ['Classical Histogram', r'$w(\mathbf{u})= Z(\mathbf{u})\cdot p(\mathbf{k}_{\mathrm{argmax}}|\mathbf{y}^{\mathrm{obs}}, \mathbf{u})$', '$w(\mathbf{u})= p(\mathbf{k}_{\mathrm{argmax}}|\mathbf{y}^{\mathrm{obs}}, \mathbf{u})$', '$w(\mathbf{u})= Z(\mathbf{u})$'])
plt.xlabel(r'$k_{\mathrm{argmax}}$')
plt.legend()
plt.tight_layout()
plt.show()
