#!/usr/bin/env python
# coding: utf-8

#=================================================================================
##.* Definition of parameter and useful functions
#=================================================================================

import SWE_wrapper as swe        # 
import matplotlib.pyplot as plt
import pickle
from scipy import stats, optimize
import numpy as np
import weighted_kde
import half_sample_mode
import seaborn as sns
import corner
import pandas as pd
from sklearn import cluster
import pyDOE


class Estimate:
    def __init__(self, value = [], uref = [], method = ''):
        self.k = value
        self.uref = uref
        self.J = np.nan
        self.method = method
        self.validation_SS = []
    def interp(self, xr):
        return map(swe.interp(self.k), xr)
    def summary(self):
        print 'K =', self.k
        print 'AP =', self.uref
        if not np.isnan(self.J):
            print 'J = ', self.J
    def eval_J(self):
        self.J,_ = J_pw_AP(self.k, self.uref[0], self.uref[1])
        
Kref = 0.2*(1+np.sin(2*np.pi*swe.xr/swe.D[1])) #
xr_ext = np.linspace(1,100,200)
Kref_ext = 0.2*(1+np.sin(2*np.pi*xr_ext/swe.D[1]))



mean_h = 20 
phase = 0
amplitude = 5.0
period = 50.0
# href = y_obs
# Ap_Pm -> A = 5.1; P = 49.8
# Ap_Pp -> A = 5.2; P = 50.1
# Am_Pp -> A = 4.9; P = 50.2
# Am_Pm -> A = 4.8; P = 49.9

def shallow_water_KU(K,amplitude = 5.0, period = 50.0, mean_h = 20.0 , phase = 0.0):
    """Computes h and q for a given value of K, and U = [A,P,M,Ph] """
    bcL = lambda h, hu, t: swe.BCrand(h, hu, t, 'L',
                                 mean_h, amplitude, period, phase)
    return swe.shallow_water(swe.D, swe.g, swe.T, swe.h0, swe.u0, swe.N, swe.LF_flux, swe.dt, swe.b, K,
                                 bcL, swe.bcR)




APref = [5.0, 50.0]    
# APref = [5.1, 49.8]
# APref = [5.2, 50.1] 
# APref = [4.9, 50.2]
# APref = [4.8, 49.9]


[xr, href, uref, t] = shallow_water_KU(Kref, # Generation observation --> Training set
                                       amplitude = APref[0], 
                                       period = APref[1],
                                       mean_h = 20.0,
                                       phase = 0.0)
[_, h_pre, _, _ ] = swe.prevision(Kref, # Generation prediction --> Validation set
                                  amplitude = APref[0],
                                  period = APref[1],
                                  mean_h = 20.0,
                                  phase = 0.0)

def J_nograd(K,AP, hreference = href):
    Kc = np.array(map(swe.interp(K), xr))
    _,hKAP,_,_ = shallow_water_KU(Kc,
                                  amplitude = AP[0], 
                                  period = AP[1],
                                  mean_h = 20.0,
                                  phase = 0.0)
    return 0.5*np.sum((hKAP - hreference)**2)
    

def J_validation(K,AP):
    [_, hprevision, _, _] = swe.prevision(Kref, # Generation prediction --> Validation set
                                  amplitude = AP[0],
                                  period = AP[1],
                                  mean_h = 20.0,
                                  phase = 0.0)
    Kc = np.array(map(swe.interp(K), xr))
    [_, h, _, _] = swe.prevision(Kc, # Generation prediction --> Validation set
                                  amplitude = AP[0],
                                  period = AP[1],
                                  mean_h = 20.0,
                                  phase = 0.0)
    return 0.5*np.sum((h-hprevision)**2)

print APref
Nvar = 4


def prediction_cost(Kcoeff, hpr = h_pre):
    """Compute prediction misfit"""
    K_transform = swe.interp(Kcoeff)
    K = np.array(map(K_transform, xr))
    return swe.J_function(h_pre, swe.prevision(K)[1])

def J_pw_AP(Kcoeff, ampli = 5.0, period = 50.0, hr = href):
    """Piecewise constant interpolation of K, then computes cost function and gradient"""
    K_transform = swe.interp(Kcoeff)
    K = np.array(map(K_transform, xr))
    cost, grad =  swe.J_grad_AP_href(K, ampli, period, hr)
    
    grad_sum_length = int(50.0/Kcoeff.size)
    grad_coeff = np.zeros(Kcoeff.size)
    for i in range(Kcoeff.size):
        grad_coeff[i] = sum(grad[i*grad_sum_length:(i*grad_sum_length+grad_sum_length)])
    return cost,grad_coeff


ref = Estimate(Kref, [5.0,50.0], 'ref')
ref.eval_J()
ref.summary()

##.* Sanity check:
test = J_pw_AP(Kref, ampli = APref[0], period = APref[1], hr= href)
test[0] == 0.0
all(test[1] == np.zeros(50))
prediction_cost(Kref) == 0.0


import pymc

K0 = pymc.Uniform('K0',0,2.0)
K1 = pymc.Uniform('K1',0,2.0)
K2 = pymc.Uniform('K2',0,2.0)
K3 = pymc.Uniform('K3',0,2.0)
A = pymc.Uniform('A', 4.7,5.3)
P = pymc.Uniform('P', 49.7,50.3)

@pymc.deterministic
def model(K0 = K0, K1=K1, K2=K2, K3 = K3,
          A =A, P = P):
    K = np.array([K0,K1,K2,K3])
    K_transform = swe.interp(K)
    Kin = np.array(map(K_transform, xr))
    AP = [A,P]
    [_, href,_, _] = shallow_water_KU(Kin, # Generation observation --> Training set
                                       amplitude = AP[0], 
                                       period = AP[1],
                                       mean_h = 20.0,
                                       phase = 0.0)
    return href

data = pymc.Normal('data', mu = model, tau = 1, value = href, observed = True)

sampler = pymc.MCMC([K0,K1,K2,K3,A,P,model], db='pickle', dbname='sampler_6D.pickle')
sampler.use_step_method(pymc.AdaptiveMetropolis, [K0,K1,K2,K3,A,P],
                        scales={K0:0.1, K1:0.1, K2:0.1, K3:0.1,
                                A:0.1, P:0.1}, verbose = 2)

db = pymc.database.pickle.load('sampler_6D.pickle')
sampler = pymc.MCMC([K0,K1,K2,K3,A,P,model], db = db)
sampler.sample(iter=1980)
len(sampler.trace('K0', chain = None)[:])
sampler.db.close()
pymc.Matplot.plot(sampler)
plt.show()

# sampler.write_csv("sampler_pymc_MCMC_6D.csv", variables=["K0", "K1", "K2", "K3", "A", "P"])


dicttosave = {'K0t':sampler.trace('K0')[:],
              'K1t':sampler.trace('K1')[:],
              'K2t':sampler.trace('K2')[:],
              'K3t':sampler.trace('K3')[:],
              'At': sampler.trace('A')[:],
              'Pt': sampler.trace('P')[:]}

out_save = open('pymc_MCMC_6D', 'wb')
pickle.dump(dicttosave, out_save)
out_save.close()

#======================================
#= Comparison with best 4D parameter ==
#======================================

Nvar = 4
stopt = 0.5*np.ones(4)
args = (APref[0],APref[1], href)
opt = optimize.minimize(fun = J_pw_AP, x0 = [0.5], args = args,
                            jac = True, bounds = [(0.0, None)]*1)


def full_cond_log_density(K, AP_tuple):
    gamma = len(AP_tuple)
    sum_tot = 0
    for AP in AP_tuple:
        sum_tot -= J_nograd(K,AP)
    return sum_tot/swe.Nobs

AP_tuple = [[5,50], [4.5,50]]
    
np.exp(full_cond_log_density([0.05],AP_tuple))

def slice_sampler_step(p,z):
    p_z = p(z)
    w = 0.2
    u = stats.uniform.rvs(size = 1)*p_z
    # u = 0.8
    z_minus = np.max([0.,z - w]) 
    # z_plus = z + w
    z_plus = np.min([6.0, z + w])
    p_z_minus = p(z_minus)
    p_z_plus = p(z_plus)
    # print u, p_z, p_z_minus, p_z_plus
    while ((p_z_minus >= u and z_minus !=0.0) or (p_z_plus >= u and z_plus != 6.0)):
        w += 0.5
        z_minus = np.max([0.,z - w])
        z_plus = np.min([6.0, z + w])
        p_z_minus = p(z_minus)
        p_z_plus = p(z_plus)
    z_prime = stats.uniform.rvs(z_minus, z_plus - z_minus, size = 1)
    p_z_pr = p(z_prime[0])
    # print 'step 2'
    while (p_z_pr <= u):
        w_prime = np.abs(z - z_prime)
        z_prime = np.max([stats.uniform.rvs(z-w_prime, 2*w_prime, size = 1), 0.0])
        p_z_pr = p(z_prime[0])        
    return z_prime, p_z_pr

def slice_sampler(p,zstart, Nsample, log = False):
    if log:
        p_step = lambda z: np.exp(p(z))
    else:
        p_step = p
    z_vector = np.empty(Nsample)
    z_vector[0] = zstart
    p_vector = np.empty(Nsample)
    p_vector[0] = p_step(zstart)
    for i in xrange(Nsample-1):
        z_vector[i+1], p_vector[i+1] = slice_sampler_step(p_step,z_vector[i])
    return z_vector, p_vector
    

def SAME_slice_step(p, zstart, gamma):
    AP_gamma = []
    for _ in xrange(gamma):
        A_r = stats.uniform.rvs(4.7, 0.6, size = 1)
        P_r = stats.uniform.rvs(49.7,0.6, size = 1)
        AP_gamma += [[A_r, P_r]]
    p_Tosample = lambda K: p([K], AP_gamma)
    zz, pp = slice_sampler(p_Tosample, zstart, 2, log = True)
    return zz[-1], pp[-1]
    

SAME_slice_step(full_cond_log_density, 0.2, 1)

def SAME_sampler(p_log, start, gamma_fun, Nsample):
    z_vector = np.empty(Nsample)
    z_vector[0] = start
    p_vector = np.empty(Nsample)
    # p_vector[0] = np.exp(p_log(start, [5.0,50.0]))
    for i_ in xrange(Nsample-1):
        print i_
        gamma = gamma_fun(i_)
        z_vector[i_+1], p_vector[i_+1] = SAME_slice_step(p_log, z_vector[i_], gamma)
        print z_vector[i_+1], p_vector[i_+1]
    return z_vector, p_vector

def gamma_generation_function(N, nmax=100):
    return lambda i: int(1 +  i/N)




SAME_test = SAME_sampler(p_log = full_cond_log_density,
             start = 0.2,
             gamma_fun = gamma_generation_function(10),
                         Nsample = 10 )

SAME_cont = SAME_sampler(p_log = full_cond_log_density,
             start = SAME_test[-1],
             gamma_fun = lambda i: 19+i+1,
                         Nsample = 20)

SAME_test = np.append(SAME_test, SAME_cont)

SAME = SAME_sampler(p_log = full_cond_log_density,
             start = 0.2,
             gamma_fun = lambda i: 1+2*i,
                         Nsample = 10)


SAME3 = SAME_sampler(p_log = full_cond_log_density,
             start = SAME_tot[0][-1],
             gamma_fun = lambda i: 105+2*i,
                         Nsample = 20)

SAME_tot = np.append(SAME_tot[0], SAME3[0]), np.append(SAME_tot[1], SAME3[1]) 

plt.subplot(2,1,1)
plt.plot(SAME_tot[0])
plt.subplot(2,1,2)
plt.plot(np.log(SAME_tot[1][np.log(SAME_tot[1])>-200]))
plt.show()

def SAME_decompose(Nsamples, i_start, SAME_previous = None):
    if SAME_previous is None:
        SAME_previous = [],[]
        start = 0.5
    else:
        start = SAME_previous[0][-1]

    gamma_fun = lambda i: 1 + np.floor(i_start/5).astype(int) + np.floor(i/5).astype(int)

    SAME_new = SAME_sampler(p_log = full_cond_log_density,
                        start = start,
                        gamma_fun = gamma_fun,
                        Nsample = Nsamples)
    return np.append(SAME_previous[0], SAME_new[0][1:]), np.append(SAME_previous[1], SAME_new[1][1:]) 


SAME = SAME_decompose(2,i_start = 0)
for i in xrange(340,500,10):
    print 'i = ', i
    SAME = SAME_decompose(10, i+1, SAME)
    np.savetxt('SAME.txt', SAME, fmt='%f')

SAME = np.loadtxt('SAME.txt')
plt.subplot(2,1,1)
plt.plot(SAME[0])
plt.subplot(2,1,2)
plt.plot(np.log(SAME[1]))
plt.show()

zz= slice_sampler(lambda x: int(np.abs(x-0.5)<0.5), 0.5, 1000)
plt.hist(zz)
plt.show()

SAME_test.shape

 
