#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.append('/home/victor/These/source_code/')
# imp hau.wrapper_HR as swe
from haute_resolution.wrapper_HR import J_KAP_array
# from code_swe.animation_SWE import animate_SWE
import numpy as np
import pyDOE
import matplotlib.pyplot as plt
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Matern
from bayesian_optimization_VT.bo_wrapper import (EGO_analytical,
                                                 exploEGO,
                                                 PI_alpha_allspace,
                                                 find_extrema_gp,
                                                 PI_alpha_check_tol_dichotomy,
                                                 EGO_LCB,
                                                 EI_VAR,
                                                 EI_MC_initial_design,
                                                 gp_worst_case_fixedgrid,
                                                 PEI_algo,
                                                 inv_transformation_variable_to_unit,
                                                 transformation_variable_to_unit)
import bayesian_optimization_VT.bo_plot as boplt
import scipy
import numpy as np

# -- Paramètres de référence ------------------------------------------------------
print 'Hauteur eau moyenne = ', swe.mean_h
print 'Amplitude = ', swe.amplitude
print 'Periode = ', swe.period
print 'Phase = ', swe.phase
bcLref = lambda h, hu, t: swe.BCrand(h, hu, t, 'L',
                                     swe.mean_h, swe.amplitude,
                                     swe.period, swe.phase)

# [xr, href, uref, t] = swe.swe_KAP(swe.Kref, 5.0, 15.0)
# [xr, hrefApPm, urefApPm, t] = swe.swe_KAP(swe.Kref, 5.1, 14.8)
# [xr, hrefApPp, urefApPp, t] = swe.swe_KAP(swe.Kref, 5.2, 15.1)
# [xr, hrefAmPp, urefAmPp, t] = swe.swe_KAP(swe.Kref, 4.9, 15.2)
# [xr, hrefAmPm, urefAmPm, t] = swe.swe_KAP(swe.Kref, 4.8, 14.9)
# [xr, hrefAmPp50, urefAmPp50, t] = swe.swe_KAP(np.ones(200) * 3, 5.0, 15.0)
# animate_SWE(xr, [href, hrefAmPp50], swe.b, swe.D, ylim = [0, 30])

# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/href.npy'), href)
# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/hrefApPm.npy'), hrefApPm)
# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/hrefApPp.npy'), hrefApPp)
# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/hrefAmPp.npy'), hrefAmPp)
# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/hrefAmPm.npy'), hrefAmPm)
href = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
                'href.npy'))
hrefApPm = np.load(('/home/victor/These/Bayesian_SWE/data_observations/href'
                    'ApPm.npy'))
hrefApPp = np.load(('/home/victor/These/Bayesian_SWE/data_observations/href'
                    'ApPp.npy'))
hrefAmPp = np.load(('/home/victor/These/Bayesian_SWE/data_observations/href'
                    'AmPp.npy'))
hrefAmPm = np.load(('/home/victor/These/Bayesian_SWE/data_observations/href'
                    'AmPm.npy'))

# swe.J_KAP_pw_obs(swe.Kref, A = swe.amplitude, P = swe.period,
#                       idx_to_observe = np.arange(swe.href.shape[0]), hreference = hrefApPm)

# doit être égal à 0, [0.,...,0.]


idx_to_observe = np.arange(49, 200, 50, dtype = int)
# = array([ 49,  99, 149, 199])


# ==============================================================================


# KrefAP = np.hstack([swe.Kref, swe.amplitude, swe.period])
# J_KAP_array(KrefAP, idx_to_observe = idx_to_observe, hreference = href)

# - Optimization w/o uncertainties -------------------------------------------

#  Optimization in dim K = 200
# optimization = scipy.optimize.minimize(fun = J_KAP, x0 = 0.1 * np.ones_like(swe.xr),
#                                        args = (swe.amplitude, swe.period), jac = True,  #  U = uref, N_obs = 4
#                                        bounds = swe.N * [(0.0, None)])



dimK = 4
# def callbackF(Xi):
#     print Xi



# optimization_4pts_1obs_nfeval = scipy.optimize.minimize(fun = J_KAP, x0 = 0.1 * np.ones(dimK),
#                                                         args = (swe.amplitude, swe.period,
#                                                                 idx_to_observe, href),
#                                                         jac = True,  # U = uref, N_obs = 8
#                                                         bounds = dimK * [(0.0, None)],
#                                                         options = {'maxiter': 100})
# print 'AmPm'
# optimization_4pts_1obs_nfeval_AmPm = scipy.optimize.minimize(fun = J_KAP,
#                                                              x0 = optimization_4pts_1obs_nfeval.x,
#                                                              args = (swe.amplitude, swe.period,
#                                                                      idx_to_observe, hrefAmPm),
#                                                              jac = True,  # U = uref, N_obs = 8
#                                                              bounds = dimK * [(0.0, 2.0)],
#                                                              options = {'maxiter': 100,
#                                                                         'disp': True},
#                                                              # method = 'TNC',
#                                                              callback = callbackF)
# print 'AmPp'
# optimization_4pts_1obs_nfeval_AmPp = scipy.optimize.minimize(fun = J_KAP,
#                                                              x0 = optimization_4pts_1obs_nfeval.x,
#                                                              args = (swe.amplitude, swe.period,
#                                                                      idx_to_observe, hrefAmPp),
#                                                              jac = True,  # U = uref, N_obs = 8
#                                                              bounds = dimK * [(0.0, 2.0)],
#                                                              options = {'maxiter': 100,
#                                                                         'disp': True},
#                                                              # method = 'TNC',
#                                                              callback = callbackF)
# print 'ApPm'
# optimization_4pts_1obs_nfeval_ApPm = scipy.optimize.minimize(fun = J_KAP,
#                                                              x0 = optimization_4pts_1obs_nfeval.x,
#                                                              args = (swe.amplitude, swe.period,
#                                                                      idx_to_observe, hrefApPm),
#                                                              jac = True,  # = uref, N_obs = 8
#                                                              bounds = dimK * [(0.0, 2.0)],
#                                                              options = {'maxiter': 100,
#                                                                         'disp': True},
#                                                              # method = 'TNC',
#                                                              callback = callbackF)
# print 'ApPp'
# optimization_4pts_1obs_nfeval_ApPp = scipy.optimize.minimize(fun = J_KAP,
#                                                              x0 = optimization_4pts_1obs_nfeval.x,
#                                                              args = (swe.amplitude, swe.period,
#                                                                      idx_to_observe, hrefApPp),
#                                                              jac = True,  # U = uref, N_obs = 8
#                                                              bounds = dimK * [(0.0, 2.0)],
#                                                              options = {'maxiter': 100,
#                                                                         'disp': True},
#                                                              # method = 'TNC',
#                                                              callback = callbackF)
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble = r'\usepackage{amsmath} \usepackage{amssymb}')

optimization_4pts = np.array([0.25777003, 0.35040328, 0.05947522, 0.04117051])
optimization_4pts_AmPm = np.array([0., 2., 0.53158671, 0.])
optimization_4pts_AmPp = np.array([0.36835951, 0., 1.07113719, 2.])
optimization_4pts_ApPm = np.array([2., 2., 0.94897403, 0.])
optimization_4pts_ApPp = np.array([0., 0., 0., 1.4537471])
lw = 1
plt.figure(figsize=(8, 6))
plt.plot(swe.xr[1:-1], swe.Kref[1:-1], label = r'$\mathbf{k}_{\mathrm{ref}}$',
         linewidth=lw + 2, ls='--')
plt.plot(swe.xr[1:-1], map(swe.interp(optimization_4pts), swe.xr[1:-1]),
         label = 'optimization result, no uncertainties', linewidth=lw + 1, color = 'red')
plt.plot(swe.xr[1:-1], map(swe.interp(optimization_4pts_AmPm), swe.xr[1:-1]),
         label = 'optimization result, AmPm', linewidth=lw)
plt.plot(swe.xr[1:-1], map(swe.interp(optimization_4pts_AmPp), swe.xr[1:-1]),
         label = 'optimization result, AmPp', linewidth=lw)
plt.plot(swe.xr[1:-1], map(swe.interp(optimization_4pts_ApPm), swe.xr[1:-1]),
         label = 'optimization result, ApPm', linewidth=lw)
plt.plot(swe.xr[1:-1], map(swe.interp(optimization_4pts_ApPp), swe.xr[1:-1]),
         label = 'optimization result, ApPp', linewidth=lw)
# plt.vlines(idx_to_observe / 2, 0, .4, label = 'obs points', linestyles = 'dotted')
plt.grid(alpha=0.3)
plt.legend(loc=1)
plt.xlabel(r'$x$')
plt.ylabel(r'$\mathbf{k}$')
plt.title(r'Optimization using adjoint on $\mathbf{k}\mapsto J(\mathbf{k},\mathbb{E}\left[\mathbf{U}\right])$,' +
          r' $\dim \mathcal{K}$=4')
plt.tight_layout()
plt.show()

# -- Evaluation of LHS =========================================================================

# J_KAP_scaled = lambda K,As,Ps : J_KAP(K, As*0.6 + 4.7, Ps*0.6+14.7, idx_to_observe)
# J_KAP_evallhs = lambda K, lhs : [J_KAP_scaled(K,lhs[i,0],lhs[i,1]) for i in xrange(lhs.shape[0])]

# big_LHS_evalKref = J_KAP_evallhs(swe.Kref, pyDOE.lhs(2, samples = 100, criterion = 'm', iterations = 20))
# J_lhs = [big_LHS_evalKref[i][0] for i in xrange(100)]
# plt.subplot(1,2,1)
# plt.plot(J_lhs, '.')
# plt.subplot(1,2,2)
# plt.hist(J_lhs);plt.show()
# np.asarray(J_lhs).mean();np.asarray(J_lhs).var()

# big_LHS_eval_zero = J_KAP_evallhs([0.0], pyDOE.lhs(2, samples = 100, criterion = 'm', iterations = 20))
# J_lhs = [big_LHS_evalzero[i][0] for i in xrange(100)]
# plt.subplot(1,2,1)
# plt.plot(J_lhs, '.')
# plt.subplot(1,2,2)
# plt.hist(J_lhs);plt.show()
# np.asarray(J_lhs).mean();np.asarray(J_lhs).var()

# big_LHS_eval_44 = J_KAP_evallhs(optimization_4pts_4obs_nfeval.x, pyDOE.lhs(2, samples = 100, criterion = 'm', iterations = 20))
# J_lhs = [big_LHS_eval_44[i][0] for i in xrange(100)]
# plt.subplot(1,2,1)
# plt.plot(J_lhs, '.')
# plt.subplot(1,2,2)
# plt.hist(J_lhs);plt.show()
# np.asarray(J_lhs).mean();np.asarray(J_lhs).var()

# def J_mean(K):
#     eval = J_KAP_evallhs(K,pyDOE.lhs(2, samples = 20, criterion = 'm', iterations = 20))
#     J_val = np.asarray([eval[i][0] for i in xrange(20)])
#     G_val = np.asarray([eval[i][1] for i in xrange(20)])
#     return J_val.mean(), G_val.mean(0)
    
    

# mean_opt = scipy.optimize.minimize(J_mean, x0 = [0.1], jac = True, bounds = [(0.0, None)], options = {'maxiter': 20})
# result mean_opt.x = 1.09999454, .fun = 17397.310362


# ------------------------------------------------------------------------------
#                      Bayesian (Custom) Optimization
# ------------------------------------------------------------------------------


# Dim K = 1 --------------------------------------------------------------------
rng = np.random.RandomState()
bounds = np.array([[0, 1.0],       # for K
                   [4.7, 5.3],     # for A
                   [14.7, 15.3]])  # for P


#  COMPUTATION OF MEAN AND VARIANCE BASED ON LHS -------------------------------

ninit = 40
hr = href
hr_array = [href, hrefAmPm, hrefAmPp, hrefApPm, hrefApPp]
suffixes = ['_href', '_AmPm', '_AmPp', '_ApPm', '_ApPp']

for index, hr_iter in enumerate(hr_array):
    suff = suffixes[index]
    print suff
    k_init, mean_hr, var_hr = EI_MC_initial_design(lambda kap: J_KAP_array(kap,
                                                                           parallel=True,
                                                                           hreference=hr_iter),
                                                   [0], [1, 2], ninitial = 10,
                                                   npoints_LHS = 150, bounds=bounds, LHS_K=False)
    np.save('/home/victor/These/Bayesian_SWE/data_observations/k_initial_design' +
            suff + '_dense.npy', k_init)
    np.save('/home/victor/These/Bayesian_SWE/data_observations/mean' + suff + '_dense.npy', mean_hr)
    np.save('/home/victor/These/Bayesian_SWE/data_observations/var' + suff + '_dense.npy', var_hr)

# k_initial_design_sparse, mean_href_sparse, var_href_sparse = EI_MC_initial_design(lambda kap: J_KAP_array(kap,
#                                                                                                           parallel=True,
#                                                                                                           hreference=hr),
#                                                                                   [0], [1, 2], ninitial = 10,
#                                                                                   npoints_LHS = 10, bounds=bounds)

# plt.plot(k_initial_design_sparse, mean_href_sparse, 'o')
# plt.plot(k_initial_design_sparse, mean_href_sparse + np.sqrt(var_href_sparse) / 10, 'o')
# plt.plot(k_initial_design_sparse, mean_href_sparse - np.sqrt(var_href_sparse) / 10, 'o')
# plt.show()

# np.save('/home/victor/These/Bayesian_SWE/data_observations/k_initial_design_href_sparse.npy',
#         k_initial_design_sparse)
# np.save('/home/victor/These/Bayesian_SWE/data_observations/mean_href_sparse.npy', mean_href_sparse)
# np.save('/home/victor/These/Bayesian_SWE/data_observations/var_href_sparse.npy', var_href_sparse)


# suffixes=['_href']
colors = ['k', 'b', 'g', 'm', 'r']
quant = 1.0
plt.subplot(1, 2, 1)
for i, suffix in enumerate(suffixes):
    kinit = np.load('/home/victor/These/Bayesian_SWE/data_observations/k_initial_design' +
                    suffix + '_dense.npy')
    meanU = np.load('/home/victor/These/Bayesian_SWE/data_observations/mean' +
                    suffix + '_dense.npy')
    varU = np.load('/home/victor/These/Bayesian_SWE/data_observations/var' +
                   suffix + '_dense.npy')
    col = colors[i]
    print suffix
    plt.plot(kinit, meanU, col + 'o-', label=suffix[1:])
    plt.fill_between(kinit.ravel(), meanU + np.sqrt(varU/150), meanU - np.sqrt(varU/150), alpha=0.2, color = col)
    # plt.plot(kinit, meanU + 1.98 * np.sqrt(varU) / 50, col + '_')
    # plt.plot(kinit, meanU - 1.98 * np.sqrt(varU) / 50, col + '_')
    # plt.errorbar(kinit, meanU, quant * np.sqrt(varU / 50),fmt='o', label=suffix[1:])
plt.title(r'Estimates of $\mathbb{E}[J(\mathbf{k},\mathbf{U})]$')
plt.xlabel(r'$\mathbf{k}$')
plt.axvline(0.09, ls='--')
plt.legend()
##
plt.subplot(1, 2, 2)
for i, suffix in enumerate(suffixes):
    kinit = np.load('/home/victor/These/Bayesian_SWE/data_observations/k_initial_design' +
                    suffix + '_dense.npy')
    varU = np.load('/home/victor/These/Bayesian_SWE/data_observations/var' +
                   suffix + '_dense.npy')
    col = colors[i]
    plt.plot(kinit, np.sqrt(varU), col + 'o-', label =suffix[1:])
    plt.legend()
plt.title(r'Estimates of the standard deviation')
plt.xlabel(r'$\mathbf{k}$')
plt.tight_layout()
plt.show()





sd = np.sqrt(var_href_sparse)
mean_href_sparse_norm = mean_href_sparse - mean_href_sparse.mean()
gpKAP = GaussianProcessRegressor(kernel = Matern([0.0001], length_scale_bounds = (1e-5, 0.1)),
                                 alpha=10,
                                 n_restarts_optimizer=100,
                                 normalize_y=False)

gpKAP.fit(k_initial_design_sparse, mean_href_sparse); gpKAP.kernel_
boplt.plot_gp(gpKAP, np.linspace(0, 2, 1000), true_function = None, nsamples=10)

# initial_design = inv_transformation_variable_to_unit(pyDOE.lhs(n = 3, samples = ninit,
#                                                                criterion = 'maximin',
#                                                                iterations=100),
#                                                      bounds)

# np.save('/home/victor/These/Bayesian_SWE/data_observations/LHS_initial_design.npy', initial_design)
# np.save('/home/victor/These/Bayesian_SWE/data_observations/LHS_initial_response.npy',
#         initial_response)


# Initialisation of GP in joint space ------------------------------------

base01vector = np.linspace(0, 1, 50)
base01vector_big = np.linspace(0, 1, 200)
xx, yy, zz = np.meshgrid(base01vector, base01vector, base01vector, indexing = 'ij')
combinations_3d = np.array([xx, yy, zz]).T.reshape(-1, 3, order = 'F')

yy, zz = np.meshgrid(base01vector, base01vector, indexing = 'ij')
# combinations_U = np.array([yy, zz]).T.reshape(-1, 2, order = 'F')
# combinations_U = inv_transformation_variable_to_unit(combinations_U, bounds[1:, :])
combinations_U = inv_transformation_variable_to_unit(pyDOE.lhs(n = 2, samples = 2500,
                                                               criterion = 'maximin',
                                                               iterations=10), bounds[-2:, :])
combinations_K = inv_transformation_variable_to_unit(base01vector_big, np.atleast_2d(bounds[0, :]))

initial_design = np.load('/home/victor/These/Bayesian_SWE/data_observations/'
                         'LHS_initial_design.npy')

ref_array = ['', '_AmPm', '_AmPp', '_ApPm', '_ApPp']
suffix


for index, ref_type in enumerate(ref_array):

    initial_response = np.load('/home/victor/These/Bayesian_SWE/data_observations/'
                               'LHS_initial_response' + ref_type + '.npy')
    if index == 0:
        ref_type = 'ref_'
    # Multicore
    # initial_response = J_KAP_array(initial_design, idx_to_observe, parallel = True, ncores = 4)
    # eiX_train_ = np.load('/home/victor/These/train_EIVAR.npy')
    # eiX_response_ = np.load('/home/victor/These/response_EIVAR.npy')

    print ref_type
    dict_ref = {'ref': href, 'AmPm': hrefAmPm, 'AmPp': hrefAmPp, 'ApPm': hrefApPm, 'ApPp': hrefApPp}
    if index == 0:
        hr_iter = href
    else:
        hr_iter = dict_ref[ref_array[index][1:]]
    print ref_array[index][1:]
    # for name, hr in dict_ref.iteritems():
    #     resp = J_KAP_array(initial_design, idx_to_observe, hreference=hr, parallel = True, ncores = 4)
    #     fname = '/home/victor/These/Bayesian_SWE/data_observations/LHS_initial_response_' \
    #             + name + '.npy'
    #     np.save(fname, resp)


    # optimize_uref = scipy.optimize.minimize(lambda k: J_KAP_array([k, 5.0, 15.0],
    #                                                               hreference=hr),
    #                                         x0 = [0.092], bounds = [(0.0, None)])



    gpKAP = GaussianProcessRegressor(kernel = Matern([1, 1, 1]), n_restarts_optimizer = 100)
    gpKAP.fit(initial_design, initial_response)
    gpKAP.kernel_
    mini, maxi = find_extrema_gp(gpKAP, bounds, 100)
    print mini.fun, -maxi.fun

    gpKAP = EGO_analytical(gpKAP, lambda kap: J_KAP_array(kap, hreference=hr_iter),
                           X_=np.array([[1, 2, 3], [2, 2, 4]]),
                           niterations=25, plot = False, nrestart = 30, bounds=bounds)


    gpKAP = exploEGO(gpKAP, lambda kap: J_KAP_array(kap, hreference=hr_iter),
                     idx_U = [1, 2], X_ = np.array([[1, 2, 3], [2, 2, 4]]),
                     niterations=25, nrestart=100, bounds = bounds)

    gpKAP = EGO_LCB(gpKAP, lambda kap: J_KAP_array(kap, hreference=hr_iter),
                    kappa=0, X_ = np.array([[1, 2, 3], [2, 2, 4]]),
                    niterations=10, bounds=bounds)

    while mini.fun < 0:
        gpKAP = EGO_LCB(gpKAP, lambda kap: J_KAP_array(kap, hreference=hr_iter),
                        kappa=0, X_ = np.array([[1, 2, 3], [2, 2, 4]]),
                        niterations=10, bounds=bounds)
        mini, maxi = find_extrema_gp(gpKAP, bounds, 100)
        print mini.fun, -maxi.fun


    # 


    # gpEI_VAR = EI_VAR(gpEI_VAR[0], lambda kap: J_KAP_array(kap, hreference=hr),
    #                   idx_U=[1, 2],
    #                   X_ = np.array([[1, 2, 3], [2, 2, 4]]),
    #                   niterations= 10, nrestart=50, bounds = bounds)
    gp = gpKAP

    # mini, maxi = find_extrema_gp(gp, bounds, 100)
    # print mini.fun, -maxi.fun
    alpha, kcheck = PI_alpha_check_tol_dichotomy(gp, idx_to_explore = [1, 2],
                                                 X_to_minimize = combinations_K,
                                                 X_to_explore = combinations_U,
                                                 bounds=np.atleast_2d(bounds[0, :]),
                                                 ptol = 1.0,
                                                 nrestart = 20, alpha_up = 5.0, PI=False)
    alpha95, kcheck95 = PI_alpha_check_tol_dichotomy(gp, idx_to_explore = [1, 2],
                                                     X_to_minimize = combinations_K,
                                                     X_to_explore = combinations_U,
                                                     bounds=np.atleast_2d(bounds[0, :]),
                                                     ptol = .95,
                                                     nrestart = 20, alpha_up = 5.0, PI=False)
    alpha90, kcheck90 = PI_alpha_check_tol_dichotomy(gp, idx_to_explore = [1, 2],
                                                     X_to_minimize = combinations_K,
                                                     X_to_explore = combinations_U,
                                                     bounds=np.atleast_2d(bounds[0, :]),
                                                     ptol = .90,
                                                     nrestart = 20, alpha_up = 5.0, PI=False)


    step = (alpha - 1) / 4.0
    alpha_1 = PI_alpha_allspace(gp = gp, alpha = 1.0, idx_to_explore = [1, 2],
                                X_to_minimize = combinations_K,
                                X_to_explore = combinations_U,
                                bounds = np.atleast_2d(bounds[0, :]))
    alpha_2 = PI_alpha_allspace(gp = gp, alpha = 1 + step, idx_to_explore = [1, 2],
                                X_to_minimize = combinations_K,
                                X_to_explore = combinations_U,
                                bounds = np.atleast_2d(bounds[0, :]))
    alpha_3 = PI_alpha_allspace(gp = gp, alpha = 1 + 2 * step, idx_to_explore = [1, 2],
                                X_to_minimize = combinations_K,
                                X_to_explore = combinations_U,
                                bounds = np.atleast_2d(bounds[0, :]))
    alpha_4 = PI_alpha_allspace(gp = gp, alpha = 1 + 3 * step, idx_to_explore = [1, 2],
                                X_to_minimize = combinations_K,
                                X_to_explore = combinations_U,
                                bounds = np.atleast_2d(bounds[0, :]))
    alpha_5 = PI_alpha_allspace(gp = gp, alpha = alpha, idx_to_explore = [1, 2],
                                X_to_minimize = combinations_K,
                                X_to_explore = combinations_U,
                                bounds = np.atleast_2d(bounds[0, :]))
    alpha_95p = PI_alpha_allspace(gp = gp, alpha = alpha95, idx_to_explore = [1, 2],
                                  X_to_minimize = combinations_K,
                                  X_to_explore = combinations_U,
                                  bounds = np.atleast_2d(bounds[0, :]))
    alpha_90p = PI_alpha_allspace(gp = gp, alpha = alpha90, idx_to_explore = [1, 2],
                                  X_to_minimize = combinations_K,
                                  X_to_explore = combinations_U,
                                  bounds = np.atleast_2d(bounds[0, :]))
    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble = r'\usepackage{amsmath} \usepackage{amssymb}')
    plt.plot(combinations_K, alpha_1, label = r'$\alpha=1$')
    plt.plot(combinations_K, alpha_2, label = r'$\alpha=' + str(round(1 + step, 4)) + '$')
    plt.plot(combinations_K, alpha_3, label = r'$\alpha=' + str(round(1 + 2 * step, 3)) + '$')
    plt.plot(combinations_K, alpha_4, label = r'$\alpha=' + str(round(1 + 3 * step, 3)) + '$')
    plt.plot(combinations_K, alpha_5, label = r'$\alpha=\check{\alpha}$')
    plt.plot(combinations_K, alpha_95p, label = r'$\alpha=\check{\alpha}_{0.05}$')
    plt.plot(combinations_K, alpha_90p, label = r'$\alpha=\check{\alpha}_{0.10}$')

    plt.axhline(1.0, ls = '--', color = 'k')
    plt.axhline(0.95, ls = '--', color = 'k', alpha = 0.90)
    plt.axhline(0.90, ls = '--', color = 'k', alpha = 0.50)
    plt.axvline(combinations_K[alpha_5.argmax()], ls = '--', color='k')
    plt.axvline(combinations_K[alpha_95p.argmax()], ls = '--', color='k', alpha=.90)
    plt.axvline(combinations_K[alpha_90p.argmax()], ls = '--', color='k', alpha=.50)

    kwc, _ = gp_worst_case_fixedgrid(gp, [0], combinations_K, bounds, full_output=False)
    plt.plot(combinations_K, worst_perf);plt.show()
    plt.axvline(kwc, ls = '--', color='b', alpha=.50)

    plt.legend(loc = 1)
    plt.grid(alpha = 0.15)
    plt.title(r'$\mathbb{P}_U\left[J(\mathbf{k},\mathbf{U})' +
              r'\leq \alpha J^*(\mathbf{U})\right]$' +
              r' for different values of $\alpha$')
    plt.xlabel(r'$\mathbf{k}$')
    plt.tight_layout();plt.show()
    plt.savefig('/home/victor/alpha_check' + ref_type + '_previous_eval.pdf')
    plt.close()


# Dim K = 2 ------------------------------------------------------------------------------
DIMK = 2
rng = np.random.RandomState()
boundK = [0, 2.0]
boundKdim = np.array(boundK * DIMK).reshape(DIMK, 2)
bounds = np.vstack([boundKdim,     # for K
                   [4.7, 5.3],     # for A
                   [14.7, 15.3]])  # for P
ninit = 50


# Initial design evaluation ------------------------------
# initial_design_2d = inv_transformation_variable_to_unit(pyDOE.lhs(n = 4, samples = ninit,
#                                                                   criterion = 'maximin',
#                                                                   iterations=100),
#                                                         bounds)
# initial_response_2d = J_KAP_array(initial_design_2d,
#                                   idx_to_observe, parallel = True, ncores = 4)

# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          'K_2d/LHS_initial_design.npy'), initial_design_2d)
# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          'K_2d/LHS_initial_response.npy'), initial_response_2d)

initial_design_2d = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
                             'K_2d/LHS_initial_design.npy'))
initial_response_2d = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
                               'K_2d/LHS_initial_response.npy'))


# GP initialisation and fit -----------------------------
gpKAP2d = GaussianProcessRegressor(kernel = Matern([1, 1, 1, 1]), n_restarts_optimizer = 1000)
gpKAP2d.fit(initial_design_2d, initial_response_2d)
gpKAP2d.kernel_

# Bayesian Optimization ---------------------------------
gpKAP2d_EGO = EGO_analytical(gpKAP2d, J_KAP_array, X_ = np.array([[1, 2, 2, 3], [2, 1, 2, 4]]),
                             niterations = 40, plot = False, nrestart = 50, bounds = bounds)
mini, maxi = find_extrema_gp(gpKAP2d_EGO, bounds, 100)
print mini.fun, -maxi.fun

import scipy
scipy.optimize.minimize(J_KAP_array, x0 = [0.363, 0.0519, 5.0, 15.000],
                        bounds = bounds, options={'disp':True})

gpKAP2d_exploEGO = exploEGO(gpKAP2d, J_KAP_array, idx_U = [2, 3],
                            X_ = np.array([[1, 2, 2, 3],
                                           [2, 1, 2, 4]]),
                            niterations = 50, plot = False,
                            nrestart = 30, bounds = bounds)
mini, maxi = find_extrema_gp(gpKAP2d_LCB, bounds, 20)
print mini.fun, -maxi.fun
gpKAP2d_LCB = EGO_LCB(gpKAP2d_EGO, J_KAP_array, kappa = 0.,
                      X_ = np.array([[1, 2, 2, 3],
                                     [2, 1, 2, 4]]),
                      niterations=20, plot=False,
                      nrestart=100, bounds=bounds)

base01vector = np.linspace(0, 1, 20)
base01vector_big = np.linspace(0, 1, 200)
k1, k2, A, P = np.meshgrid(base01vector, base01vector, base01vector, base01vector, indexing = 'ij')
combinations_4d = np.array([k1, k2, A, P]).T.reshape(-1, 4, order = 'F')

yy, zz = np.meshgrid(base01vector, base01vector, indexing = 'ij')
combinations = np.array([yy, zz]).T.reshape(-1, 2, order = 'F')
combinations_U = inv_transformation_variable_to_unit(combinations, bounds[-2:, :])
combinations_U = inv_transformation_variable_to_unit(pyDOE.lhs(n = 2, samples = 1000,
                                                               criterion = 'maximin',
                                                               iterations=50), bounds[-2:, :])
combinations_K = inv_transformation_variable_to_unit(combinations, bounds[:-2, :])



gp_to_consider = gpKAP2d_LCB
mini, maxi = find_extrema_gp(gpKAP2d, bounds, 20)
print mini.fun, -maxi.fun
alpha_exploEGO, kcheck_exploEGO = PI_alpha_check_tol_dichotomy(gp_to_consider,
                                                               idx_to_explore = [2, 3],
                                                               X_to_minimize = combinations_K,
                                                               X_to_explore = combinations_U,
                                                               bounds= bounds[:-2, :],
                                                               ptol = 1,
                                                               nrestart = 20, PI=False)
alpha_exploEGO, kcheck_exploEGO = PI_alpha_check_tol_dichotomy(gp_to_consider,
                                                               idx_to_explore = [2, 3],
                                                               X_to_minimize = combinations_K,
                                                               X_to_explore = combinations_U,
                                                               bounds= bounds[:-2, :],
                                                               ptol = .95,
                                                               nrestart = 20, PI=False)


kalpha_exploEGO = PI_alpha_allspace(gp = gp_to_consider,
                                    alpha = alpha_exploEGO,
                                    idx_to_explore = [2, 3],
                                    X_to_minimize = combinations_K,
                                    X_to_explore = combinations_U,
                                    bounds = bounds[:-2, :])

plt.contourf(yy * 2, zz * 2, kalpha_exploEGO.reshape(20, 20), 100)
plt.plot(kcheck_exploEGO[0], kcheck_exploEGO[1], 'ro')
plt.xlabel(r'$k_1$')
plt.ylabel(r'$k_2$')
plt.colorbar()
plt.show()


gp_to_consider = gpKAP2d_EGO
alpha_EGO, kcheck_EGO = PI_alpha_check_tol_dichotomy(gp_to_consider, idx_to_explore = [2, 3],
                                                     X_to_minimize = combinations_K,
                                                     X_to_explore = combinations_U,
                                                     bounds= bounds[:-2, :],
                                                     ptol = 1.0,
                                                     nrestart = 20, PI=False)

kalpha_EGO = PI_alpha_allspace(gp = gp_to_consider, alpha = alpha_EGO, idx_to_explore = [2, 3],
                               X_to_minimize = combinations_K,
                               X_to_explore = combinations_U,
                               bounds = bounds[:-2, :])

plt.contourf(yy * 2, zz * 2, kalpha_EGO.reshape(20, 20), 100)
plt.plot(kcheck_EGO[0], kcheck_EGO[1], 'ro')
plt.xlabel(r'$k_1$')
plt.ylabel(r'$k_2$')
plt.colorbar()
plt.show()

# ------------------------------------------------------------------------------
gp_to_consider = gpKAP2d
alpha, kcheck = PI_alpha_check_tol_dichotomy(gp_to_consider, idx_to_explore = [2, 3],
                                             X_to_minimize = combinations_K,
                                             X_to_explore = combinations_U,
                                             bounds= bounds[:-2, :],
                                             ptol = 1.0,
                                             nrestart = 20, PI=False)

kalpha = PI_alpha_allspace(gp = gp_to_consider, alpha = alpha, idx_to_explore = [2, 3],
                           X_to_minimize = combinations_K,
                           X_to_explore = combinations_U,
                           bounds = bounds[:-2, :])

plt.contourf(yy * 0.6, zz * 0.6, kalpha.reshape(20, 20), 100)
plt.plot(kcheck[0], kcheck[1], 'ro')
plt.xlabel(r'$k_1$')
plt.ylabel(r'$k_2$')
plt.colorbar()
plt.show()

# Comparison between methods  -------------------------
# -----------------
plt.subplot(2, 2, 1)
plt.contourf(yy * 0.6, zz * 0.6, kalpha.reshape(20, 20), 100)
plt.plot(kcheck[0], kcheck[1], 'ro')
plt.xlabel(r'$k_1$')
plt.ylabel(r'$k_2$');plt.title('Initial Design')
plt.colorbar()
# -----------------
plt.subplot(2, 2, 2)
plt.contourf(yy * 0.6, zz * 0.6, kalpha_EGO.reshape(20, 20), 100)
plt.plot(kcheck_EGO[0], kcheck_EGO[1], 'ro')
plt.xlabel(r'$k_1$')
plt.ylabel(r'$k_2$'); plt.title('EGO')
plt.colorbar()
# -----------------
plt.subplot(2, 2, 3)
plt.contourf(yy * 0.6, zz * 0.6, kalpha_exploEGO.reshape(20, 20), 100)
plt.plot(kcheck_exploEGO[0], kcheck_exploEGO[1], 'ro')
plt.xlabel(r'$k_1$')
plt.ylabel(r'$k_2$'); plt.title('exploEGO')
plt.colorbar()
# -----------------
kalpha1 = PI_alpha_allspace(gp = gpKAP2d, alpha = 1.0, idx_to_explore = [2, 3],
                            X_to_minimize = combinations_K,
                            X_to_explore = combinations_U,
                            bounds = bounds[:-2, :])
kMPE = combinations_K[kalpha1.argmax()]
plt.subplot(2, 2, 4)
plt.contourf(yy * 2.0, zz * 2.0, kalpha1.reshape(20, 20), 100)
plt.plot(kMPE[0], kMPE[1], 'ro')
plt.xlabel(r'$k_1$')
plt.ylabel(r'$k_2$'); plt.title('initial, alpha = 1')
plt.colorbar()
plt.show()



# On initial_design --------------------------------

kalpha1 = PI_alpha_allspace(gp = gpKAP2d, alpha = 1.0, idx_to_explore = [2, 3],
                            X_to_minimize = combinations_K,
                            X_to_explore = combinations_U,
                            bounds = bounds[:-2, :])
kMPE = combinations_K[kalpha1.argmax()]
plt.subplot(2, 2, 1)
plt.contourf(yy * 0.6, zz * 0.6, kalpha1.reshape(20, 20), 100)
plt.plot(kMPE[0], kMPE[1], 'ro')
plt.xlabel(r'$k_1$')
plt.ylabel(r'$k_2$'); plt.title('initial, alpha = 1')
# plt.clim(0, 1)
plt.colorbar()
# --------------------------------
kalpha2 = PI_alpha_allspace(gp = gpKAP2d, alpha = 1.1, idx_to_explore = [2, 3],
                            X_to_minimize = combinations_K,
                            X_to_explore = combinations_U,
                            bounds = bounds[:-2, :])
kMPE = combinations_K[kalpha2.argmax()]
plt.subplot(2, 2, 2)
plt.contourf(yy * 0.6, zz * 0.6, kalpha2.reshape(20, 20), 100)
plt.plot(kMPE[0], kMPE[1], 'ro')
plt.xlabel(r'$k_1$')
plt.ylabel(r'$k_2$'); plt.title('initial, alpha = 1.1')
# plt.clim(0, 1)
plt.colorbar()
# --------------------------------
kalpha3 = PI_alpha_allspace(gp = gpKAP2d, alpha = 1.2, idx_to_explore = [2, 3],
                            X_to_minimize = combinations_K,
                            X_to_explore = combinations_U,
                            bounds = bounds[:-2, :])
kMPE = combinations_K[kalpha3.argmax()]
plt.subplot(2, 2, 3)
plt.contourf(yy * 0.6, zz * 0.6, kalpha3.reshape(20, 20), 100)
plt.plot(kMPE[0], kMPE[1], 'ro')
plt.xlabel(r'$k_1$')
plt.ylabel(r'$k_2$'); plt.title('initial, alpha = 1.2')
# plt.clim(0, 1)
plt.colorbar()
# --------------------------------
kalpha4 = PI_alpha_allspace(gp = gpKAP2d, alpha = 1.3, idx_to_explore = [2, 3],
                            X_to_minimize = combinations_K,
                            X_to_explore = combinations_U,
                            bounds = bounds[:-2, :])
kMPE = combinations_K[kalpha4.argmax()]
plt.subplot(2, 2, 4)
plt.contourf(yy * 0.6, zz * 0.6, kalpha4.reshape(20, 20), 100)
plt.plot(kMPE[0], kMPE[1], 'ro')
plt.xlabel(r'$k_1$')
plt.ylabel(r'$k_2$'); plt.title('initial, alpha = 1.3')
plt.colorbar()
# plt.clim(0, 1)
plt.show()


# ------------------------------------------------------------------------------
#                    N-DIMENSIONAL FOR K
# ------------------------------------------------------------------------------

DIMK = 4
rng = np.random.RandomState()
boundK = [0, 2.0]
boundKdim = np.array(boundK * DIMK).reshape(DIMK, 2)
bounds = np.vstack([boundKdim,     # for K
                   [4.7, 5.3],     # for A
                   [14.7, 15.3]])  # for P
ninit = 40


# Initial design evaluation ------------------------------
initial_design_4d = inv_transformation_variable_to_unit(pyDOE.lhs(n = DIMK + 2,
                                                                  samples = ninit,
                                                                  criterion = 'maximin',
                                                                  iterations=50),
                                                        bounds)
initial_response_4d = J_KAP_array(KAP = initial_design_4d,
                                  idx_to_observe = idx_to_observe,
                                  hreference = href,
                                  parallel = True, ncores = 4)
np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
         'K_4d/LHS_initial_design.npy'), initial_design_4d)

initial_response_4d_AmPm = J_KAP_array(KAP = initial_design_4d,
                                       idx_to_observe = idx_to_observe,
                                       hreference = hrefAmPm,
                                       parallel = True, ncores = 4)
np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
         'K_4d/LHS_initial_response_AmPm.npy'), initial_response_4d_AmPm)

initial_response_4d_AmPp = J_KAP_array(KAP = initial_design_4d,
                                       idx_to_observe = idx_to_observe,
                                       hreference = hrefAmPp,
                                       parallel = True, ncores = 4)
np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
         'K_4d/LHS_initial_response_AmPp.npy'), initial_response_4d_AmPp)

initial_response_4d_ApPm = J_KAP_array(KAP = initial_design_4d,
                                       idx_to_observe = idx_to_observe,
                                       hreference = hrefApPm,
                                       parallel = True, ncores = 4)
np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
         'K_4d/LHS_initial_response_ApPm.npy'), initial_response_4d_ApPm)

initial_response_4d_ApPp = J_KAP_array(KAP = initial_design_4d,
                                       idx_to_observe = idx_to_observe,
                                       hreference = hrefApPp,
                                       parallel = True, ncores = 4)
np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
         'K_4d/LHS_initial_response_ApPp.npy'), initial_response_4d_ApPp)







initial_design_4d = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
                             'K_4d/LHS_initial_design.npy'))
initial_response_4d = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
                               'K_4d/LHS_initial_response_AmPm.npy'))


# GP initialisation and fit -----------------------------
kernel = Matern([1] * (DIMK + 2))
gpKAP = GaussianProcessRegressor(kernel = kernel, n_restarts_optimizer = 500)
gpKAP.fit(initial_design_4d, initial_response_4d)
gpKAP.kernel_
X_dummy = np.arange(2 * (DIMK + 2)).reshape(2, DIMK + 2)

# Bayesian Optimization ---------------------------------
gpKAP_EGO = EGO_analytical(gpKAP, J_KAP_array, X_ = X_dummy,
                           niterations = 30, plot = False,
                           nrestart = 30,
                           bounds = bounds)


gpKAP_exploEGO = exploEGO(gpKAP, J_KAP_array, idx_U = [4, 5], X_ = X_dummy,
                          niterations = 50, plot = False,
                          nrestart = 50,
                          bounds = bounds)

find_extrema_gp(gpKAP, bounds)[0].fun
find_extrema_gp(gpKAP_EGO, bounds)[0].fun
find_extrema_gp(gpKAP_exploEGO, bounds)[0].fun


idx_to_explore = [DIMK, DIMK + 1]
base01vector = np.linspace(0, 1, 8)

amesh, pmesh = np.meshgrid(base01vector, base01vector, indexing = 'ij')
combinations = np.array([amesh, pmesh]).T.reshape(-1, 2, order = 'F')
combinations_U = inv_transformation_variable_to_unit(combinations, bounds[-2:, :])


mmgrid = np.meshgrid(*(DIMK * [base01vector]), indexing = 'ij')
combinations_K = inv_transformation_variable_to_unit(np.array([mmgrid]).T.reshape(-1,
                                                                                  DIMK,
                                                                                  order = 'F'),
                                                     bounds[:-2, :])
gp_to_consider = gpKAP
alpha, kcheck = PI_alpha_check_tol_dichotomy(gp_to_consider,
                                             idx_to_explore = idx_to_explore,
                                             X_to_minimize = combinations_K,
                                             X_to_explore = combinations_U,
                                             bounds= bounds[:-2, :],
                                             ptol = 1.0,
                                             nrestart = 20, alpha_low =3, alpha_up= 20, PI=False)
alphak = PI_alpha_allspace(gp = gp_to_consider, alpha = 1, idx_to_explore = idx_to_explore,
                           X_to_minimize = combinations_K,
                           X_to_explore = combinations_U,
                           bounds = bounds[:-2, :])










#  End Of File -----------------------------------------------------------------
