#!/usr/bin/env python
# coding: utf-8
# ----------------------------------------------------------------
import numpy as np
import scipy
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import seaborn as sns
import npeet.entropy_estimators as ee
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Matern
from sklearn.neighbors.kde import KernelDensity
import mixem
from lorenz.lorenz import (lorenz_xb,
                           lorenz_xs,
                           lorenz_xr,
                           cost_function_lorenz,
                           lorenz_onaxis)
import lorenz.lorenz as lrz
from statsmodels.distributions.empirical_distribution import ECDF

# from pyDOE import *

# ----------------- Definition of functions -----------------------------


# ----------------------------------------------------------------
def find_prob_threshold(Jku, k, T):
    if np.isscalar(T):
        T = T * np.ones_like(Jku[0, :])
    ss = 0
    for i in xrange(Jku.shape[1]):  # Loop on u
        ss += np.int(Jku[k, i] <= T[i])
    return float(ss) / float(Jku.shape[1])


# ----------------------------------------------------------------
def MPE(Jku, threshold_multiplier = 1.0, normalize = False, minimum = True):
    if minimum:
        res_vec = np.empty(Jku.shape[0])
        for kind in xrange(Jku.shape[0]):
            res_vec[kind] = find_prob_threshold(Jku, kind, threshold_multiplier * Jku.min(0))
        if normalize:
            res_vec /= np.sum(res_vec)
    else:
        res_vec = np.empty(Jku.shape[0])
        for kind in xrange(Jku.shape[0]):
            res_vec[kind] = 1 - find_prob_threshold(Jku, kind, Jku.max(0) / threshold_multiplier)
        if normalize:
            res_vec /= np.sum(res_vec)
    return res_vec


# ----------------------------------------------------------------
def alpha_check(Jku, lower = 1.0, upper = 10.0, niter = 10, p = 1.0, tol = 1e-5):
    alpha_low = lower
    alpha_up = upper
    i = 1
    while i < niter and np.abs(alpha_low - alpha_up) > tol:
        if np.sum(MPE(Jku, alpha_up) >= p):
            alpha = (alpha_low + alpha_up) / 2.0
            if np.sum(MPE(Jku, alpha) >= p):
                alpha_up = alpha
            else:
                alpha_low = alpha
        else:
            alpha_up *= 2
        print i, alpha_low, alpha_up
        i += 1
    return alpha_up


# ----------------------------------------------------------------
def Jminus_min(alpha, y_fun, val_star_2d_true):
    value = y_fun - alpha * np.asarray(val_star_2d_true)[np.newaxis, :]
    bool_grid = (value <= 0.0)
    return value, bool_grid


# ----------------------------------------------------------------
# def frontier_alpha(alpha, y_fun, val_star_2d_true):
#     dist = np.abs(y_fun - alpha * np.asarray(val_star_2d_true)[np.newaxis, :])
#     pass


# ----------------------------------------------------------------
def plot_contourrobust(alpha, funtest, xx, yy,
                       figsize = (6, 6), save = False,
                       figname = '/home/victor/Dropbox/PhD/Article/NPG1/Figures/contourrobust',
                       ylim = None, hline = np.nan):
    """
    Plot contour, and below, the mean, variance and kde
    """
    color_mean = 'm'
    color_std = 'limegreen'
    NptsK, NptsU = len(xx), len(yy)
    X_, Y_ = np.meshgrid(xx, yy, indexing = 'ij')
    comb = np.array([X_, Y_]).T.reshape(-1, 2)
    if hasattr(funtest, '__call__'):
        y_fun = funtest(comb).reshape(NptsK, NptsU)
    else:
        NptsK, NptsU = funtest.shape
        y_fun = funtest

    x_star_2d_true = [xx[y_fun[:, uu].argmin()] for uu in xrange(NptsU)]
    val_star_2d_true = [y_fun[:, uu].min() for uu in xrange(NptsU)]

    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble = r'\usepackage{amsmath} \usepackage{amssymb}')
    plt.figure(figsize=figsize)
    value, bool_grid = Jminus_min(alpha, y_fun, val_star_2d_true)

    kcheck = xx[np.sum(bool_grid, 1) / float(NptsU) == 1]

    plt.subplot(2, 1, 1)
    plt.contourf(X_, Y_, y_fun, 10)
    if alpha != 1:
        plt.contour(X_, Y_, bool_grid, 3)

    plt.title(r'$J(\mathbf{k},\mathbf{u})$')
    plt.plot(x_star_2d_true, yy, '.r', markersize = 0.5,
             label = r'$J^*(\mathbf{u})$')
    plt.xlim([comb[:, 0].min(), comb[:, 0].max()])
    plt.xlabel(r'$\mathbf{k}$')
    plt.ylabel(r'$\mathbf{u}$')

    # # Add vertical line at worst case
    # plt.axvline(xx[np.asarray(val_pire_true).argmin()], color ='green', ls = '--',
    # label = 'worstcase')

    # # Add vertical line at minimum of variance
    # plt.axvline(xx[np.var(y_fun, 1).argmin()], color = 'blue', ls = '--',
    #             label = r'min of $\mathbb{V}ar$')

    # plt.axvline(xx[np.mean(y_fun, 1).argmin()], color=color_mean, ls='--',
    #             label = r'min of $\mathbb{E}$')  # Minimum of mean

    if len(kcheck) == 1:
        plt.axvline(kcheck, color='red', ls='--')
        plt.axvline(xx[MPE(y_fun, alpha, minimum=minimum).argmax()],
                    color='red', ls='--', label='$\mathbf{k}_1$')
    plt.legend()

    #  -----------------------------------------------------
    ax = plt.subplot(2, 1, 2)
    if alpha != 1:
        ax.plot(xx, np.sum(bool_grid, 1) / float(NptsU), linewidth=2,
                color='black', label=r'$\Gamma_{\alpha}(\mathbf{k})$')
        ax.axhline(hline, color='black', alpha=0.3, ls='--')
    ax.set_xlim([comb[:, 0].min(), comb[:, 0].max()])
    ax_sns = ax.twinx()
    ax.set_ylim([0, 1.1])
    ax_sns = sns.distplot(x_star_2d_true, color='r',
                          kde_kws={"color": "r", "lw": 1, "label": "KDE"},
                          hist_kws ={"color": 'r', 'alpha': .35, "label": "Histogram"})
    plt.xlabel(r'$\mathbf{k}$')
    if alpha != 1.0:
        plt.title((r'$\Gamma_{\alpha}(\mathbf{k}) = \mathbb{P}_U[J(\mathbf{k},\mathbf{U}) \leq '
                   r' \alpha J^*(\mathbf{U})],'
                   r' \quad \alpha=$' + str(np.round(alpha, 4))))

    else:
        plt.title((r'Hist. and estimated density of minimisers'))
        # # Add vertical line at kcheck if it exists
    if len(kcheck) == 1:
        ax.axvline(kcheck, color='red', ls='--')
    else:
        kcheck = xx[(np.sum(bool_grid, 1)).argmax()]
    # # Add horizontal line at y = 1.0
    # ax.axhline(1.0, ls = '--', color = 'black', alpha=0.5)

    # # Add vertical line at minimum of variance
    # plt.axvline(xx[np.var(y_fun, 1).argmin()], color = 'blue', ls = '--',
    #             label =r'min of $\mathbb{V}ar$')
    ax.set_xlabel(r'$\mathbf{k}$')
    # ax.axvline(xx[np.mean(y_fun, 1).argmin()], color=color_mean, ls='--',
    #            label =r'min of $\mathbb{E}$')
    ax.set_ylabel(r'$\Gamma_{\alpha}$')
    plt.legend(loc=1)
    if ylim is not None:
        ax.set_ylim(ylim)
    ax_mean = ax.twinx()
    ax_mean.plot(xx, np.mean(y_fun, 1), color=color_mean, ls='-.')
    ax_mean.set_ylabel(r'$\mu(\mathbf{k})$', color=color_mean)
    # ax_mean.tick_params('y', color=color_mean)
    ax_mean.set_yticks([])
    ax_std = ax.twinx()
    ax_std.plot(xx, np.std(y_fun, 1), color=color_std, ls=':')
    ax_std.set_yticks([])
    # Legend hack
    ax_sns.plot(np.nan, np.nan, color=color_mean, ls='-.', label=r'$\mu(\mathbf{k})$')
    ax_sns.plot(np.nan, np.nan, color=color_std, ls=':', label=r'$\sigma(\mathbf{k})$')
    ax_sns.plot(np.nan, np.nan, color='black', lw=2, label=r'$\Gamma_{\alpha}(\mathbf{k})$')
    ax_sns.plot(np.nan, np.nan, color='red', ls='--', label=r'$\mathbf{k}_1$')

    ax_sns.legend(loc=2)
    # plt.plot(x_star_2d_true,
    #          -0.005 - 0.05 * np.random.random(np.asarray(x_star_2d_true).shape[0]),
    #          'k.', alpha = 0.1)
    plt.tight_layout()
    if save:
        plt.savefig(figname + '.pdf')
        plt.close()
    return kcheck


# ---------------- TEST IN 2D ------------------------------------
# ----------------------------------------------------------------
def bumps(X):
    # x1, x2 = X[:,0],X[:,1]
    x, y = X[:, 0], X[:, 1]
    return 0.8 * np.exp(-(((x)**2 + (y)**2) / 3)) \
        + 1.2 * np.exp(-(((y - 2.5)**2) + (x - 2.0)**2) / 1) \
        + np.exp(-(x - 0.5)**2 / 3 - (y - 4)**2 / 2) \
        + 0.8 * np.exp(-(x - 5)**2 / 4 - (y)**2 / 4) \
        + 1 * np.exp(-(x - 5)**2 / 4 - (y - 5)**2 / 4)\
        + (1 / (1 + x + y)) / 25


# ----------------------------------------------------------------
def branin_hoo(X, damp = 1.0, switch = False):
    y, x = X[:, 1], X[:, 0]
    if switch:
        x, y = y, x
    x2 = 3 * y
    x1 = 3 * x - 5

    quad = (x2 - (5.1 / (4 * np.pi**2)) * x1**2 + (5 / np.pi) * x1 - 6)**2
    cosi = (10 - (10 / np.pi * 8)) * np.cos(x1) - 44.81
    return (quad + cosi) / (51.95 * damp) + 2.0


# ----------------------------------------------------------------
def two_valleys(X, sigma=1, rotation_angle=0, translate = np.array([0, 0])):
    x, y = X[:, 1], X[:, 0]
    x = x - translate[0]
    y = y - translate[1]
    k = np.cos(rotation_angle) * x - np.sin(rotation_angle) * y
    u = np.sin(rotation_angle) * x + np.cos(rotation_angle) * y
    k = k + translate[0]
    u = u + translate[1]
    alpha = 5.0 / 4.0
    return -(u) * np.exp(-(k - alpha)**2 / sigma**2) \
        - (2 * alpha - u) * np.exp(-(k - 3 * alpha)**2 / sigma**2) \
        + np.exp(-k**2 / sigma**2) + 8 / (sigma**2)


# ----------------------------------------------------------------
def one_valley(X, sigma=1, rotation_angle=0, translate=np.array([0, 0])):
    x, y = X[:, 1], X[:, 0]
    x = x - translate[0]
    y = y - translate[1]
    k = np.cos(rotation_angle) * x - np.sin(rotation_angle) * y
    u = np.sin(rotation_angle) * x + np.cos(rotation_angle) * y
    k = k + translate[0]
    u = u + translate[1]
    return - np.exp(-((k - 2.5)**2 / sigma**2)) + 2 / sigma**2

#
#
#
# kde = KernelDensity(kernel='gaussian',
#                     bandwidth = .15).fit(np.array(x_star_2d_true).reshape(-1,1))
# plt.plot(xx, np.exp(kde.score_samples(xx.reshape(-1,1))));plt.show()
#  MPE

plt.rc('text', usetex=True)
plt.rc('text.latex', preamble = r'\usepackage{amsmath} \usepackage{amssymb}')

# ------------------------------------------------------------------------------
# When working on Branin
Ndim = 2
Npts = 1000
NptsK, NptsU = Npts, Npts
xx = np.linspace(0, 5, Npts)
yy = xx.copy()
X_, Y_ = np.meshgrid(xx, yy, indexing = 'ij')
comb = np.array([X_, Y_]).T.reshape(-1, 2)
nameBH = r'$J_{\mathrm{BH}}$'
nameBHs = r'$J_{\mathrm{BHswap}}$'

# ------------------------------------------------------------------------------
# When working on datasets from SWE
Npts = 150
xx_K = np.linspace(0, 1.0, Npts)
yy_P = np.linspace(14.7, 15.3, Npts)
X_K, Y_P = np.meshgrid(xx_K, yy_P, indexing = 'ij')
comb_KP = np.array([X_K, Y_P]).T.reshape(-1, 2)
xx = xx_K
yy = yy_P
X_, Y_, comb = X_K, Y_P, comb_KP

# ------------------------------------------------------------------------------
# When working with croco
y_fun = np.load('/home/victor/croco/Computation_M2S2K1/J_M2S2K1_50_17.npy')
NptsK, NptsU = 50, 17
xx = np.linspace(1, 50, NptsK)
yy = np.linspace(0, 1, NptsU)
X_, Y_ = np.meshgrid(xx, yy, indexing = 'ij')
comb = np.array([X_, Y_]).T.reshape(-1, 2)
plt.contour(X_, Y_, y_fun, 50)
plt.show()


# ------------------------------------------------------------------------------
# When working on lorenz
Ndim = 2
Npts = 2000
xx = np.linspace(-0.05, 0.05, Npts)
yy = np.linspace(-0.05, 0.05, Npts)
X_, Y_ = np.meshgrid(xx, yy, indexing = 'ij')
NptsK, NptsU = X_.shape

y_fun = lorenz_onaxis(lorenz_xb, xx, yy)
np.save('/home/victor/These/lorenz_grid_nooffset.npy', y_fun)

comb = np.array([X_, Y_]).T.reshape(-1, 2)

funtest = lorenz_xb




bounds = np.asarray([[-1, 1], [-1, 1]]) * 0.05
rng = np.random.RandomState()
x0 = np.random.uniform(bounds[:, 0], bounds[:, 1], size=(1, Ndim))
opt = scipy.optimize.minimize(funtest, x0 =x0,
                              bounds = bounds)
# best_xb, best_J = opt.x, opt.fun
best_xb = [0.02414384, -0.00551042]
best_J = funtest(best_xb)
for x0 in np.random.uniform(bounds[:, 0], bounds[:, 1], size=(2000, Ndim)):
    opt_restart = scipy.optimize.minimize(funtest, x0=x0,
                                          bounds=bounds)
    if opt_restart.fun < best_J:
            best_xb = opt_restart.x
            best_J = opt_restart.fun
print best_xb, best_J

# --------- Distribution of minimisers: single figure ------------------
NptsK, NptsU = y_fun.shape
# funtest = lambda X: two_valleys(X, rotation_angle = -np.pi / 2, translate = [2.5, 2.5])
# funtest = lambda X: one_valley(X, sigma=0.5, rotation_angle= np.pi / 4, translate = [2.5, 2.5])
funtest = lambda X: branin_hoo(X, switch=True)
funtest = lorenz_xb
y_fun = lorenz_onaxis(funtest, xx, yy)
y_fun = np.load('/home/victor/These/Bayesian_SWE/lorenz/xb_y_fun_2000_allobs.npy')
# np.save('/home/victor/These/Bayesian_SWE/lorenz/xb_y_fun_2000_allobs_rhohalf.npy', y_fun)
y_fun = funtest(comb).reshape(Npts, Npts)
resolution = 50
colormap_contourf = cm.viridis
# y_fun = np.load(('/home/victor/These/Bayesian_SWE/haute_resolution_fast'
#                  '/full_output_Aconstant_fast_Pm.npy')).reshape(NptsK, NptsU)
x_star_2d_true = np.asarray([xx[y_fun[:, uu].argmin()] for uu in xrange(NptsU)])
val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(NptsU)])

plt.figure(figsize = (6, 8))
ax_cont = plt.subplot(2, 1, 1)
ax_cont.set_title(r'$J$ and conditional minimisers')
ax_cont.contour(X_, Y_, y_fun, resolution, cmap=colormap_contourf)
ax_cont.plot(x_star_2d_true, yy, 'r.', markersize=1,
             label=r'$(\mathbf{u},\mathbf{k}^*(\mathbf{u}))$')
ax_cont.set_xlabel(r'$\mathbf{k}$')
ax_cont.set_ylabel(r'$\mathbf{u}$')
ax_cont.legend()

##
ax_dens = plt.subplot(2, 1, 2)
ax_dens.set_title(r'Mean of the response and estimated density of the minimisers')
ax_dens = sns.distplot(x_star_2d_true, color='r',
                       kde_kws={"color": "r", "lw": 1, "label": "KDE"},
                       hist_kws={"color": 'r', 'alpha': .35, "label": "Histogram"})
# ax_dens.plot(xx, EM_xstar(xx), label = 'Mixture of 3 Gaussians fitted by EM algorithm', lw = 2)
kde = ax_dens.lines[0]
ax_mean = ax_dens.twinx()
ax_mean.plot(xx, np.mean(y_fun, 1),
             label=(r'$\mu(\mathbf{k})$'),
             lw=1, ls='-.', color='m')
ax_dens.axvline(xx[np.mean(y_fun, 1).argmin()],
                color='m', ls='--', label=r'$\mathbf{k}_{\mathrm{mean}}$')
ax_cont.axvline(xx[np.mean(y_fun, 1).argmin()],
                color='m', ls='--', label=r'$\mathbf{k}_{\mathrm{mean}}$')
ax_mean.tick_params('y', color='m')
ax_dens.plot(np.nan, np.nan, label=(r'$\mu(\mathbf{k})$'),
             lw=1, ls='-.', color='m')
ax_std = ax_dens.twinx()
ax_std.plot(xx, np.std(y_fun, 1), color='limegreen', ls=':', label=r'$\sigma(\mathbf{k})$')
ax_dens.plot(np.nan, np.nan, label=(r'$\sigma(\mathbf{k})$'),
             lw=1, ls=':', color='limegreen')
ax_dens.axvline(xx[np.std(y_fun, 1).argmin()],
                color='limegreen', ls='--', label=r'$\mathbf{k}_{\mathrm{var}}$')
ax_cont.axvline(xx[np.std(y_fun, 1).argmin()],
                color='limegreen', ls='--', label=r'$\mathbf{k}_{\mathrm{var}}$')
ax_dens.axvline(kde.get_xdata()[kde.get_ydata().argmax()], color='red', ls='--',
                label = r'$\mathbf{k}_{\mathrm{MPE}}$ by KDE')
ax_cont.axvline(kde.get_xdata()[kde.get_ydata().argmax()], color='red', ls='--')
ax_dens.set_xlabel(r'$\mathbf{k}$')
ax_mean.tick_params('y', colors='m')
ax_mean.set_ylabel(r'$\mu$', color='m')
ax_dens.set_ylabel(r'$p_{K^*}$ estimated by KDE')
ax_std.set_yticks([])
# plt.ylim(0, 4)
ax_dens.set_xlim(comb[:, 0].min(), comb[:, 0].max())
# ax_cont.axvline(0.02007)
# ax_dens.axvline(0.02007)
ax_dens.legend(loc=8, ncol=4, bbox_to_anchor=(0.5, -0.4), frameon=False)
plt.tight_layout()

# plt.subplots_adjust(bottom = 0.17)
# plt.savefig('/home/victor/croco/moments_croco')
plt.show()
plt.close()


# ----------------------------------------------------------------
def entropy_from_array(samples):
    list_compr = [[sample] for sample in samples]
    return ee.entropy(list_compr)

# entropy_from_array(scipy.stats.uniform.rvs(size = 100000))


weights, distributions, log_likelihood = mixem.em(x_star_2d_true, [
    mixem.distribution.NormalDistribution(mu=0, sigma=1),
    mixem.distribution.NormalDistribution(mu=2.5, sigma=1),
    mixem.distribution.NormalDistribution(mu=5, sigma=1),
], initial_weights=[0.5, 0.5, .5])


# ----------------------------------------------------------------
def fit_EM_density(xvalues, k=3):
    mu_vec = np.percentile(xvalues, np.linspace(0, 100, k))
    init_density = [mixem.distribution.NormalDistribution(mu, 1) for mu in mu_vec]
    weights, distributions, log_likelihood = mixem.em(xvalues,
                                                      init_density,
                                                      initial_weights=np.ones(k) / k)
    return EM_density(weights, distributions)


# ----------------------------------------------------------------
def EM_density(weights, distributions):
    def fun_to_return(x):
        summ = 0
        for i, dist in enumerate(distributions):
            summ += weights[i] * scipy.stats.norm.pdf(x, dist.mu, dist.sigma)
        return summ
    return fun_to_return
#


# ----- Sequential 'flood' procedure -----------------------------------------------------

# ----------------------------------------------------------------
def seq_flood(fun, xx, yy, save=False, profiles=False, upper=3.0,
              figname='/home/victor/Dropbox/PhD/Article/NPG1/Figures/test',
              alpha_to_compute=None):

    NptsK, NptsU = len(xx), len(yy)
    X_, Y_ = np.meshgrid(xx, yy, indexing = 'ij')
    comb = np.array([X_, Y_]).T.reshape(-1, 2)
    if hasattr(fun, '__call__'):
        y_fun = fun(comb).reshape(NptsK, NptsU)
    else:
        NptsK, NptsU = fun.shape
        y_fun = fun

    # x_star_2d_true = np.asarray([xx[y_fun[:, uu].argmin()] for uu in xrange(NptsU)])
    val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(NptsU)])
    if (alpha_to_compute is None) or (len(alpha_to_compute) != 4):
        print 'alpha_1, alpha_095, alpha_090 to be computed'
        alpha_c_100 = alpha_check(y_fun, lower = 1.0, upper = upper, niter = 20,
                                  tol = 1e-9, p=1.0)
        alpha_c_095 = alpha_check(y_fun, lower = 1.0, upper = alpha_c_100, niter = 20,
                                  tol = 1e-9, p=0.95)
        alpha_c_090 = alpha_check(y_fun, lower = 1.0, upper = alpha_c_095, niter = 20,
                                  tol = 1e-9, p=0.9)
        alpha_to_compute = [1.0, alpha_c_090, alpha_c_095, alpha_c_100]
    else:
        alpha_c_090, alpha_c_095, alpha_c_100 = alpha_to_compute[1:]
    global minimum
    minimum = True
    kcheck_vec = np.empty(len(alpha_to_compute))
    for index, alpha in enumerate(alpha_to_compute):
        kcheck_vec[index] = plot_contourrobust(alpha, funtest=y_fun, xx=xx, yy=yy,
                                               save=save,
                                               ylim=None,
                                               figname=figname + str(index))
        plt.close()  # TODO removex
        # plt.show()
    kcheck = kcheck_vec[-1]
    if profiles:
        plt.figure(figsize=(10, 6))
        plt.subplot(1, 2, 1)
        plt.plot(yy, alpha_c_100 * val_star_2d_true,
                 label=r'$\alpha_1 \cdot J^*(\mathbf{u})$',
                 color='b', lw=1)
        plt.plot(yy, val_star_2d_true,
                 label=r'$J^*(\mathbf{u})$', color='k', lw=1)
        plt.fill_between(yy, alpha_c_100 * val_star_2d_true, val_star_2d_true,
                         color='black', alpha=0.05,
                         label=r'$J(\mathbf{k},\mathbf{u})<J^*(\mathbf{u})$')

        plt.plot(yy, y_fun[np.std(y_fun, 1).argmin(), :], '--', color='limegreen',
                 label = r'$\mathbf{u} \mapsto J(\mathbf{k}_{\mathrm{var}}, \mathbf{u})$')
        plt.plot(yy, y_fun[np.abs(xx - kcheck).argmin(), :], 'r-', lw=1,
                 label = r'$\mathbf{u} \mapsto J({\mathbf{k}}_{1}, \mathbf{u})$')
        plt.plot(yy, y_fun[np.mean(y_fun, 1).argmin(), :], '-.m', lw=1,
                 label = r'$\mathbf{u} \mapsto J(\mathbf{k}_{\mathrm{mean}}, \mathbf{u})$')
        plt.plot(yy, y_fun[((yy - 0.005277638819409708)**2).argmin(), :], 'c', lw=1,
                 label = r'$\mathbf{k}$ of maximal ratio')
        # plt.plot(yy, y_fun[((yy - 0.008729364682341173)**2).argmin(), :], 'orange', lw=1,
        #          label = r'$\mathbf{k}$ of maximal ratio (local maxima)')
        plt.legend()
        plt.ylabel(r'$J$')
        plt.xlabel(r'$\mathbf{u}$')
        plt.title((r'$\mathbf{u}\mapsto J(\mathbf{k},\mathbf{u})$ '
                   r'for different estimated $\mathbf{k}$'))
        plt.subplot(1, 2, 2)
        compute_all_cdfs(y_fun, 1.31100463867)
        plt.tight_layout()
        if save:
            plt.savefig(figname + '_profile.pdf')
        else:
            plt.show()
        plt.close()
    kmean = xx[np.mean(y_fun, 1).argmin()]
    kvar = xx[np.std(y_fun, 1).argmin()]
    print r'------------------------------ '
    print 'Minimum of mean: ' + str(round(kmean, 4))
    print 'Minimum of std:  ' + str(round(kvar, 4))
    print 'MPE:             ' + str(round(kcheck_vec[0]))
    print 'kcheck090:       ' + str(round(kcheck_vec[1], 4)), str(round(alpha_to_compute[1], 4))
    print 'kcheck095:       ' + str(round(kcheck_vec[2], 4)), str(round(alpha_to_compute[2], 4))
    print 'kcheck100:       ' + str(round(kcheck_vec[3], 4)), str(round(alpha_to_compute[3], 4))
    return kcheck_vec, alpha_to_compute, kmean, kvar


# ----------------------------------------------------------------
#  Floods on Branin
Npts = 1000
xx = np.linspace(0, 5, Npts)
yy = np.linspace(0, 5, Npts)
NptsK, NptsU = Npts, Npts
(kcheck_vec, alpha_vec,
 kmean, kvar) = seq_flood(lambda X: branin_hoo(X, switch = True),
                          xx, yy, save=True, profiles=False,
                          figname= '/home/victor/acadwriting/Slides/Figures/branin')

# ----------------------------------------------------------------
#  Floods on Lorenz
Npts = 2000
# xx = np.linspace(-, 5, Npts)
# yy = np.linspace(0, 5, Npts)
NptsK, NptsU = Npts, Npts
(kcheck_vec, alpha_vec,
 kmean, kvar) = seq_flood(y_fun,
                          xx, yy, save=True, profiles=True,
                          figname= '/home/victor/These/Bayesian_SWE/lorenz/lorenz_cdf',
                          upper = 2.6,
                          alpha_to_compute=[1.0, 1.20278778954, 1.22605091414, 1.31100220227])
# allobs
                          # alpha_to_compute=[1.0, 1.1961024, 1.21314481, 1.3947998]

                          # alpha_to_compute=[1.0, 1.8864768, 1.9507417816, 2.57720336914])


# ----------------------------------------------------------------
# Flood on croco
(kcheck_vec, alpha_vec,
 kmean, kvar) = seq_flood(y_fun,
                          xx, yy, save=True, profiles=True,
                          figname= '/home/victor/croco/crocoM2S2K1_',
                          upper = 1.005)




# ----------------------------------------------------------------
# Application on SWE
file_prefix = (('/home/victor/These/Bayesian_SWE/haute_resolution_fast'
                '/full_output_Aconstant_fast_'))
NptsK, NptsU = 150, 150
xx_K = np.linspace(0, 1.0, NptsK)
yy_P = np.linspace(14.7, 15.3, NptsU)

suffix_list = ['2', 'Pm', 'ApPm', 'AmPm', 'Pp', 'ApPp', 'AmPp']
suffix = '2'
J_output = np.load(file_prefix + suffix + '.npy').reshape(NptsK, NptsU)

(kcheck_vec, alpha_vec,
 kmean, kvar) = seq_flood(J_output, xx_K, yy_P,
                          save=False, profiles=True,
                          figname = '/home/victor/PhD/Article/NPG1/Figures/SWE_' + suffix)


kvar_SWE = np.empty(len(suffix_list))
kmean_SWE = np.empty(len(suffix_list))
kcheck_SWE = np.empty([4, len(suffix_list)])
alpha_check_SWE = np.empty([4, len(suffix_list)])

for i, suff in enumerate(suffix_list):
    J_output = np.load(file_prefix + suff + '.npy').reshape(NptsK, NptsU)
    if suff == '2':
        suff = 'big'
    (kcheck_SWE[:, i], alpha_check_SWE[:, i],
     kmean_SWE[i], kvar_SWE[i]) = seq_flood(J_output, xx_K, yy_P,
                                            save=False,
                                            profiles=True,
                                            figname = ('/home/victor/PhD/'
                                                       'Article/NPG1/Figures/SWE_') + suff)



plt.plot(kcheck_SWE[-1:, :].T, label = r'$\check{\mathbf{k}}_1$')
plt.plot(kmean_SWE, label =r'mean sense')
plt.plot([0.1043, 0.2028, 0.2709, 0.2427, 0, 0, 0.0015], label =r'opt without uncertainties')
plt.axhline(0.1043, ls = '--', color = 'black', alpha = 0.4)
plt.show()

Npts = 1000

alpha_BH = alpha_check(branin_hoo(comb, switch=True).reshape(Npts, Npts),
                       lower = 1.9, upper = 2.0, niter = 30, tol = 1e-9, p=1.0)
alpha_BHs = alpha_check(branin_hoo(comb, switch=False).reshape(Npts, Npts),
                        lower = 2.2, upper = 2.3, niter = 30, tol = 1e-9, p=1.0)


alpha_lorenz = alpha_check(y_fun, lower=1.3, upper=1.32, niter=20, tol=1e-9, p=1.0)




# ----------------------------------------------------------------------------------------
# - Computation of alpha_p and kcheck_p with respect to p changing -----------------------
# ----------------------------------------------------------------------------------------
funtest = lambda X: branin_hoo(X, switch=False)
y_fun = funtest(comb).reshape(Npts, Npts)
# y_fun = np.load(('/home/victor/These/Bayesian_SWE/haute_resolution_fast'
#                  '/full_output_Aconstant_fast_2.npy')).reshape(NptsK, NptsU)
x_star_2d_true = np.asarray([xx[y_fun[:, uu].argmin()] for uu in xrange(NptsU)])
val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(NptsU)])
Np = 200
alpha_c = np.empty(Np)
for i, p in enumerate(np.linspace(0., 1, Np)):
    if i == 0:
        lower = 1
    else:
        lower = alpha_c[i - 1]
    alpha_c[i] = alpha_check(y_fun, lower=lower, upper=2.58, niter=20,
                             tol=1e-5, p=p)
# np.save('/home/victor/These/Bayesian_SWE/lorenz/alpha_p_xb.npy', alpha_c)
alpha_c = np.load('/home/victor/These/Bayesian_SWE/lorenz/alpha_p_xb.npy')

alpha_c = np.load('/home/victor/These/alpha_p_BH.npy')
kcheck = np.empty(Np)
for i, al in enumerate(alpha_c):
    value, bool_grid = Jminus_min(al, y_fun, val_star_2d_true)
    kcheck[i] = xx[(np.sum(bool_grid, 1)).argmax()]

p_vec = np.linspace(0, 1, 200)
plt.subplot(2, 1, 1)
plt.plot(p_vec, p_vec / alpha_c)
plt.xlabel(r'$p$')
plt.ylabel(r'$p/\alpha_p$')
plt.title(r'$\frac{p}{\alpha_p}$')
plt.subplot(2, 1, 2)
plt.plot(kcheck, p_vec / alpha_c)
# plt.title('k')
plt.show()


# --------------------------------------------------------------------------------------
def incr_alpha_p(alpha_max, Nalp, y_fun, val_star_2d_true, x_star_2d_true, xx):
    alpha_vec = np.linspace(1.0, alpha_max, Nalp)
    corres_p = np.empty(Nalp)
    kcheck = np.empty(Nalp)
    for i, al in enumerate(alpha_vec):
        value, bool_grid = Jminus_min(al, y_fun, val_star_2d_true)
        corres_p[i] = np.sum(bool_grid, 1).max() / float(len(np.sum(bool_grid, 1)))
        kcheck[i] = xx[(np.sum(bool_grid, 1)).argmax()]
    ratio = corres_p / alpha_vec
    return alpha_vec, corres_p, ratio, kcheck


alpha_vec, p_alpha, ratio, kcheck = incr_alpha_p(1.311, 10000,
                                                 y_fun,
                                                 val_star_2d_true, x_star_2d_true,
                                                 xx)

plt.show()
plt.plot(alpha_vec, ratio)
plt.show()
plt.subplot(2, 2, 1)
plt.plot(p_alpha, alpha_vec)
plt.subplot(2, 2, 3)
plt.plot(p_alpha, kcheck)
plt.subplot(2, 2, 2)
plt.plot(p_alpha, ratio)
plt.subplot(2, 2, 4)
plt.plot(kcheck, ratio, '.')
plt.show()


# --------------------------------------------------------------------------------------
def evolution_p(alpha_c, y_fun, val_star_2d_true, x_star_2d_true, p_vec, nplots=2):
    color = 'black'
    n = alpha_c.shape[0]
    kcheck = np.empty(n)
    plt.figure(figsize = (7, 4))
    for i, al in enumerate(alpha_c):
        value, bool_grid = Jminus_min(al, y_fun, val_star_2d_true)
        kcheck[i] = xx[(np.sum(bool_grid, 1)).argmax()]
    if p_vec is None:
        p_vec = np.linspace(0.0, 1.0, n)

    if nplots == 2:
        ax1 = plt.subplot(1, 2, 1)
    else:
        ax1 = plt.subplot(2, 2, 1)
    ax1.plot(p_vec, alpha_c, color=color)
    ax1.set_xlim([0, 1])
    ax1.grid(alpha = 0.30)
    ax1.set_xlabel(r'$p$')
    ax1.set_ylabel(r'$\alpha_p$')
    # plt.axvline(0.60)
    # plt.axvline(0.23)
    ax1.set_title(r'$\alpha_p$ as a function of $p$')
    if nplots == 2:
        ax3 = plt.subplot(1, 2, 2)
    else:
        ax3 = plt.subplot(2, 2, 2)
    ax3.plot(p_vec, p_vec / alpha_c, color=color)
    ax3.set_xlabel(r'$p$')
    ax3.set_ylabel(r'$p / \alpha_p$')
    ax3.axvline(p_vec[(p_vec / alpha_c).argmax()], alpha=0.4, color='k', ls = ':')
    ax3.grid(alpha=0.30)
    # ax2.axvline(0.60)
    # ax2.axvline(0.23)
    ax3.set_xlim([0, 1.0])
    # ax2.ylim([0, 5.0])
    ax3.set_title(r'Ratio $p/\alpha_p$ as a function of $p$')

    if nplots != 2:
        ax2 = plt.subplot(2, 2, 3)
        ax2.plot(p_vec, kcheck, color=color)
        ax2.set_xlabel(r'$p$')
        ax2.set_ylabel(r'$\mathbf{k}_p$')
        ax2.axhline(kcheck[-1], alpha = 0.4, color=color, ls = ':')
        ax2.grid(alpha = 0.30)
        # ax2.axvline(0.60)
        # ax2.axvline(0.23)
        ax2.set_xlim([0, 1.0])
        # ax2.ylim([0, 5.0])
        ax2.set_title(r'Evolution of $\mathbf{k}_p$ when $p$ varies')
        ax4 = plt.subplot(2, 2, 4)
        ax4.plot(kcheck, p_vec / alpha_c, '.', markersize=0.5, color=color)
        ax4.set_ylabel(r'$p / \alpha_p$')
        ax4.set_xlabel(r'$\mathbf{k}_p$')
        # ax4.axhline(kcheck[-1], alpha = 0.4, color = 'k', ls = ':')
        ax4.grid(alpha=0.30)
        # ax2.axvline(0.60)
        # ax2.axvline(0.23)
        # ax2.ylim([0, 5.0])
        ax4.set_title(r'$\mathbf{k}_p$ vs $p / \alpha_p$')

    plt.tight_layout()
    return kcheck, kcheck[(p_vec / alpha_c).argmax()]


evolution_p(alpha_vec, y_fun, val_star_2d_true, x_star_2d_true, p_vec=p_alpha)
# plt.savefig('/home/victor/These/Bayesian_SWE/ratio_p_BHs_2.pdf')
# plt.savefig('/home/victor/These/Bayesian_SWE/lorenz/evol_p_4plots.pdf')
plt.show()


def find_local_maxima(array):
    c = (np.gradient(np.sign(np.gradient(array))) < 0).nonzero()[0]  # local max
    return c



suffix_list = ['AmPm', 'AmPp', 'ApPm', 'ApPp', 'Pm', 'Pp', 'ref']
suffix_list = ['BH', 'BHs']
for i, suffix in enumerate(suffix_list):
    # alpha_c = np.load('/home/victor/These/Bayesian_SWE/data_observations/alpha_p_' +
    #                   suffix + '.npy')
    # y_fun = np.load(('/home/victor/These/Bayesian_SWE/haute_resolution_fast'
    #                  '/full_output_Aconstant_fast_' +
    #                  suffix + '.npy')).reshape(NptsK, NptsU)
    Ndim = 2
    Npts = 1000
    NptsU, NptsK = Npts, Npts
    xx = np.linspace(0, 5, Npts)
    yy = xx.copy()
    X_, Y_ = np.meshgrid(xx, yy, indexing = 'ij')
    comb = np.array([X_, Y_]).T.reshape(-1, 2)
    alpha_c = np.load('/home/victor/These/alpha_p_' +
                      suffix + '.npy')
    Np = len(alpha_c)

    # print i
    if i == 0:
        y_fun = branin_hoo(comb, switch=True).reshape(Npts, Npts)
    elif i == 1:
        y_fun = branin_hoo(comb, switch=False).reshape(Npts, Npts)
    x_star_2d_true = np.asarray([xx[y_fun[:, uu].argmin()] for uu in xrange(NptsU)])
    val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(NptsU)])
    kcheck = np.empty(Np)
    p_vec = np.linspace(0, 1.0, Np)
    for i, al in enumerate(alpha_c):
        value, bool_grid = Jminus_min(al, y_fun, val_star_2d_true)
        kcheck[i] = xx[(np.sum(bool_grid, 1)).argmax()]

    plt.subplot(2, 2, 1)
    plt.plot(np.linspace(0.0, 1.0, Np), alpha_c)
    plt.xlim([0, 1])
    plt.grid(alpha = 0.30)
    plt.xlabel(r'$p$')
    plt.ylabel(r'$\alpha_p$')
    # plt.axvline(0.60)
    # plt.axvline(0.23)
    plt.title(r'Evolution of $\alpha_p$ when $p$ varies')
    plt.subplot(2, 2, 3)
    plt.plot(np.linspace(0.0, 1.0, Np), kcheck)
    plt.xlabel(r'$p$')
    plt.ylabel(r'$\mathbf{k}_p$')
    plt.axhline(kcheck[-1], alpha = 0.4, color = 'k', ls = ':')
    plt.grid(alpha = 0.30)
    # plt.axvline(0.60)
    # plt.axvline(0.23)
    # plt.xlim([0, 1.0])
    # plt.ylim([0, 5.0])
    plt.title(r'Evolution of $\mathbf{k}_p$ when $p$ varies')

    plt.subplot(2, 2, 2)
    plt.plot(p_vec, p_vec / alpha_c)
    plt.xlabel(r'$p$')
    plt.title(r'$\frac{p}{\alpha_p}$')
    plt.grid(alpha = 0.30)
    plt.subplot(2, 2, 4)
    plt.plot(kcheck, p_vec / alpha_c)
    plt.xlabel(r'$\mathbf{k}_p$')
    plt.axvline(kcheck[(p_vec / alpha_c).argmax()], color = 'k', ls = ":")
    plt.grid(alpha = 0.30)
    plt.tight_layout()
    plt.savefig('/home/victor/These/Bayesian_SWE/ratio_p_' + suffix)
    plt.show()
    plt.close()
#

# ----------------------------------------------------------------------------------------
# ----      Plots of profiles ------------------------------------------------------------
# ----------------------------------------------------------------------------------------

color_std = 'limegreen'
color_mean = 'm'

funtest = lambda X: branin_hoo(X, switch=False)
y_fun = funtest(comb).reshape(Npts, Npts)
x_star_2d_true = np.asarray([xx[y_fun[:, uu].argmin()] for uu in xrange(Npts)])
val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(Npts)])

upper = 1.3110023
alpha_c_100 = alpha_check(y_fun, lower = 1.31100, upper = upper, niter = 20,
                          tol = 1e-9, p=1.0)
alpha_c_095 = alpha_check(y_fun, lower = 1.0, upper = alpha_c_100, niter = 20,
                          tol = 1e-9, p=0.95)
alpha_c_090 = alpha_check(y_fun, lower = 1.0, upper = alpha_c_095, niter = 20,
                          tol = 1e-9, p=0.9)
alpha_to_compute = [1.0, alpha_c_090, alpha_c_095, alpha_c_100]
p_vec = [0, 0.9, 0.95, 1.0]
# At check1

minimum = True
kcheck_vec = np.empty(len(alpha_to_compute))
for index, alpha in enumerate(alpha_to_compute):
    kcheck_vec[index] = plot_contourrobust(alpha, funtest=y_fun, xx=xx, yy=yy,
                                           save=False, ylim=None,
                                           figname='/home/victor/Dropbox/PhD/' +
                                           'Article/NPG1/Figures/branin_switch' + str(index))
    # plt.show()
    plt.close()
kcheck = kcheck_vec[-1]

abscisse_end_arrow = 4.5  # switch = True
abscisse_end_arrow = 2.8  # switch = False
ratio_abs_minifig = 0.45  # switch=True
ratio_abs_minifig = 0.33  # switch=True


fig, ax = plt.subplots()
ax.plot(xx, y_fun[np.std(y_fun, 1).argmin(), :], ls=':', color = color_std,
        label = r'$\mathbf{u} \mapsto J(\mathbf{k}_{\mathrm{var}}, \mathbf{u})$',
        zorder=1)
ax.plot(xx, y_fun[np.mean(y_fun, 1).argmin(), :],
        color=color_mean, ls='-.', lw=1,
        label = r'$\mathbf{u} \mapsto J(\mathbf{k}_{\mathrm{mean}}, \mathbf{u})$')
ax.plot(xx, y_fun[np.abs(xx - kcheck).argmin(), :], 'r--', lw=2,
        label = r'$\mathbf{u} \mapsto J(\mathbf{k}_{1}, \mathbf{u})$')
# ax.plot(xx, y_fun[np.abs(xx - 1.55655655655655).argmin(), :], color='c', lw=1,
#         label=r'$\mathbf{k}$ of maximal ratio')
ax.plot(xx, val_star_2d_true,
        label = r'$J^*(\mathbf{u})$', color='k', lw=2)
ax.plot(xx, alpha_c_100 * val_star_2d_true,
        label = r'$\alpha_1 \cdot J^*(\mathbf{u})$', color='b', lw=2)
ax.fill_between(xx, alpha_c_100 * val_star_2d_true, val_star_2d_true, color='black', alpha=0.05,
                label=r'$[J^*(\mathbf{u}), \alpha_1 J^*(\mathbf{u})]$')
ax.legend()
# plt.text(x=4.5, y=2.5, s=r'$\alpha J(\mathbf{k}^*(\mathbf{u}),\mathbf{u})$')
#
ax.set_ylabel(r'$J$')
ax.set_xlabel(r'$\mathbf{u}$')
ax.set_title((r'Profiles $\mathbf{u}\mapsto J(\mathbf{k},\mathbf{u})$ '
              'for different values of $\mathbf{k}$'))

ax_cont = plt.axes([ratio_abs_minifig, .7, .15, .15])
_, bool_grid = Jminus_min(alpha_c_100, y_fun, val_star_2d_true)
ax_cont.contourf(X_, Y_, y_fun, 10, zorder=0, cmap = cm.Greys)
ax_cont.contour(X_, Y_, bool_grid, zorder=0)
ax_cont.plot(x_star_2d_true, yy, '.r', markersize=1, zorder=0)
ax_cont.axvline(xx[(np.sum(bool_grid, 1)).argmax()], color='r', ls='--', zorder=0)
ax_cont.axvline(xx[np.std(y_fun, 1).argmin()], color=color_std, ls=':', zorder=0)
ax_cont.axvline(xx[np.mean(y_fun, 1).argmin()], color=color_mean, ls='-.', zorder=0)

# ax_cont.set_xlabel(r'$\mathbf{k}$')
# ax_cont.set_ylabel(r'$\mathbf{u}$')
ax_cont.set_xticks([])
ax_cont.set_yticks([])


start_arrow = (xx[(np.sum(bool_grid, 1)).argmax()], 0)
end_arrow = (abscisse_end_arrow, y_fun[np.abs(xx - kcheck).argmin(),
                                       np.abs(xx - abscisse_end_arrow).argmin()])

con_head = mpatches.ConnectionPatch(xyA=start_arrow, xyB=end_arrow,
                                    coordsA="data", coordsB="data",
                                    axesA=ax_cont, axesB=ax,
                                    arrowstyle="<->",
                                    connectionstyle = 'angle')
con_head.set_color('black')

ax_cont.add_artist(con_head)

plt.tight_layout()
plt.draw()
plt.savefig('/home/victor/Dropbox/PhD/Article/NPG1/Figures/profile_branin_switch.pdf')
plt.show()
#

# -----------------------------------------------------------------------
# -  2 figures side by side : Optimisation of the moments  --------------
# -----------------------------------------------------------------------
color_std = 'limegreen'
color_mean = 'm'

fig = plt.figure(figsize = (6, 6))
resolution = 10
colormap_contourf = cm.viridis
funtest = lambda X: branin_hoo(X, switch = True)
y_fun = funtest(comb).reshape(Npts, Npts)
ax_cont = plt.subplot(2, 2, 1)
ax_cont.set_title(nameBH + r'$(\mathbf{k},\mathbf{u})$')
ax_cont.contourf(X_, Y_, y_fun, resolution, cmap=colormap_contourf)
ax_cont.set_xlabel(r'$\mathbf{k}$')
ax_cont.set_ylabel(r'$\mathbf{u}$')

ax_mean = plt.subplot(2, 2, 3)
ax_mean.set_title(r'Mean and standard deviation')
# ax.plot(np.nan, np.nan, 'm--', label=r'$\mu(\mathbf{k})=$'
#         r'$\mathbb{E}_U\left[J(\mathbf{k},\mathbf{U})\right]$', lw=1)
# ax.plot(xx, EM_xstar(xx), label = 'Mixture of 3 Gaussians fitted by EM algorithm', lw = 2)
ax_mean.plot(xx, np.mean(y_fun, 1), label=(r'$\mu(\mathbf{k})$'),
             lw=1, ls='-.', color=color_mean)
ax_mean.axvline(xx[np.mean(y_fun, 1).argmin()], color=color_mean, ls='--')
ax_cont.axvline(xx[np.mean(y_fun, 1).argmin()], color=color_mean, ls='--')

# ax_mean.set_ylabel(r'$\mu$', color=color_mean)
ax_mean.set_yticks([])
# ax_mean.tick_params('y', colors=color_mean)
ax_mean.plot(np.nan, np.nan, label=r'$\sigma(\mathbf{k})$', lw=1, ls=':', color=color_std)
ax_std = ax_mean.twinx()
ax_std.plot(xx, np.std(y_fun, 1),
            lw=1, ls=':', color=color_std)
ax_std.set_ylim(0, 2)
# ax_std.tick_params('y', colors=color_std)
# ax_std.set_ylabel(r'$\sigma$', color=color_std)
ax_mean.set_xlabel(r'$\mathbf{k}$')
ax_std.set_yticks([])

ax_std.axvline(xx[np.std(y_fun, 1).argmin()], color=color_std, ls='--')
ax_cont.axvline(xx[np.std(y_fun, 1).argmin()], color=color_std, ls='--')
ax_mean.plot(np.nan, np.nan, color_mean, ls='--', label=r'$\hat{\mathbf{k}}_{\mathrm{mean}}$')
ax_mean.plot(np.nan, np.nan, color_std, ls='--', label=r'$\hat{\mathbf{k}}_{\mathrm{var}}$')
ax_mean.set_xlim(comb[:, 0].min(), comb[:, 0].max())
handles, labels = ax_mean.get_legend_handles_labels()


#  Second column -------------
funtest = lambda X: branin_hoo(X, switch = False)

y_fun = funtest(comb).reshape(Npts, Npts)
x_star_2d_true = np.asarray([xx[y_fun[:, uu].argmin()] for uu in xrange(NptsU)])
val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(NptsU)])

ax_cont = plt.subplot(2, 2, 2)
ax_cont.set_title(nameBHs + r'$(\mathbf{k},\mathbf{u})$')
ax_cont.contourf(X_, Y_, y_fun, resolution, cmap=colormap_contourf)

ax_cont.set_xlabel(r'$\mathbf{k}$')
ax_cont.set_ylabel(r'$\mathbf{u}$')
ax_mean = plt.subplot(2, 2, 4)
ax_mean.set_title(r'Mean and standard deviation')

# ax.plot(np.nan, np.nan, 'm--', label =r'$\mu(\mathbf{k}) =$'
#         r'$\mathbb{E}_U\left[J(\mathbf{k},\mathbf{U})\right]$', lw=1)
ax_mean.plot(xx, np.mean(y_fun, 1), label=(r'$\mu(\mathbf{k})$'),
             lw=1, ls='-.', color=color_mean)
ax_mean.set_yticks([])
# ax_mean.set_ylabel(r'$\mu$', color=color_mean)
# ax_mean.tick_params('y', colors=color_mean)
ax_mean.axvline(xx[np.mean(y_fun, 1).argmin()], color=color_mean, ls='--')
ax_cont.axvline(xx[np.mean(y_fun, 1).argmin()], color=color_mean, ls='--')

ax_mean.set_xlim(comb[:, 0].min(), comb[:, 0].max())
ax_mean.set_xlabel(r'$\mathbf{k}$')
ax_std = ax_mean.twinx()
ax_std.plot(xx, np.std(y_fun, 1), 'g:', lw=1)
ax_std.set_ylim(0, 2)
ax_std.set_yticks([])
# ax_std.tick_params('y', colors=color_std)
# ax_std.set_ylabel(r'$\sigma$', color=color_std)
ax_std.set_xlabel(r'$\mathbf{k}$')
ax_std.set_xlim(comb[:, 0].min(), comb[:, 0].max())
ax_std.axvline(xx[np.std(y_fun, 1).argmin()], color=color_std, ls='--')
ax_cont.axvline(xx[np.std(y_fun, 1).argmin()], color=color_std, ls='--')


fig.legend(handles, labels, loc=8, ncol=4, bbox_to_anchor=(0.5, -0.025), frameon=False)

plt.subplots_adjust(bottom = 0.11)
plt.tight_layout()
# plt.show()
plt.savefig('/home/victor/Dropbox/PhD/Article/NPG1/Figures/branin_side_moments_noscale.pdf')
plt.close()


# -----------------------------------------------------------------------
##-  2 figures side by side : Distribution of minimisers  ---------------
# -----------------------------------------------------------------------

color_std = 'limegreen'
color_mean = 'm'
color_hist = 'gray'

fig = plt.figure(figsize = (6, 6))
resolution = 10
colormap_contourf = cm.viridis
funtest = lambda X: branin_hoo(X, switch = True)
y_fun = funtest(comb).reshape(Npts, Npts)
x_star_2d_true = np.asarray([xx[y_fun[:, uu].argmin()] for uu in xrange(NptsU)])
val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(NptsU)])

ax_cont = plt.subplot(2, 2, 1)
ax_cont.set_title(nameBH + r' and conditional minimisers')
ax_cont.contourf(X_, Y_, y_fun, resolution, cmap=colormap_contourf)
ax_cont.plot(x_star_2d_true, yy, 'r.', markersize=1,
             label='Conditional minimisers')
ax_cont.set_xlabel(r'$\mathbf{k}$')
ax_cont.set_ylabel(r'$\mathbf{u}$')
# ax_cont.legend()

ax = plt.subplot(2, 2, 3)
ax.set_title(r'Estimated density of the minimisers'
             # "\n"
             # r'Differential entropy = ' +
             # str(round(entropy_from_array(x_star_2d_true), 3))
             )
ax = sns.distplot(x_star_2d_true, color='r',
                  kde_kws={"color": "r", "lw": 1},  # "label": "KDE"},
                  hist_kws ={"color": color_hist, 'alpha': .35},  # "label": "Histogram"})
                  )
kde = ax.lines[0]
kMPE_KDE = kde.get_xdata()[kde.get_ydata().argmax()]

his = ax.patches
ba_height = np.asarray([bar.get_height() for bar in his])
ba_x = np.asarray([bar.get_x() for bar in his])
ba_x = ba_x + np.diff(ba_x)[0] / 2.0
kMPE_his = ba_x[ba_height.argmax()]

EM_xstar = fit_EM_density(x_star_2d_true, k=3)
kMPE_EM = xx[EM_xstar(xx).argmax()]

ax.plot(xx, EM_xstar(xx), label = 'Mix. of 3 Gaussians fitted by EM',
        lw=1, color='blue')
# ax.plot(np.nan, np.nan, 'm-.', label=r'$\mu(\mathbf{k})$', lw=1)
# ax_mean = ax.twinx()
# ax_mean.plot(xx, np.mean(y_fun, 1),
#              label=(r'$\mu(\mathbf{k})$'),
#              lw=1, ls='-.', color=color_mean)
# ax_mean.set_ylabel(r'', color=color_mean)
# ax_mean.tick_params(axis='y', colors=color_mean)
# ax_mean.set_yticks([])
# ax_std = ax.twinx()
# ax_std.plot(xx, np.std(y_fun, 1), color=color_std, ls=':')
# ax_std.set_yticks([])
ax.set_ylim(0, 2)
ax.set_xlabel(r'$\mathbf{k}$')
ax.set_xlim(comb[:, 0].min(), comb[:, 0].max())
ax.plot(np.nan, np.nan, 'r', lw=1, label='KDE')
# ax.plot(np.nan, np.nan, color=color_std, lw=1, label='$\sigma(\mathbf{k})$', ls=':')
ax.axvline(kMPE_EM, color='blue', ls='--',
           label = r'$\hat{\mathbf{k}}_{\mathrm{MPE, EM}}$')
ax_cont.axvline(kMPE_EM, color='blue', ls='--')
ax.axvline(kMPE_KDE, color='red', ls='--',
           label = r'$\hat{\mathbf{k}}_{\mathrm{MPE, KDE}}$')
ax_cont.axvline(kMPE_KDE, color='red', ls='--')

ax.axvline(kMPE_his, color=color_hist, ls='--',
           label=r'$\hat{\mathbf{k}}_{\mathrm{MPE, histogram}}$')
ax_cont.axvline(kMPE_his, color=color_hist, ls='--')

handles, labels = ax.get_legend_handles_labels()

#  Second column -------------
funtest = lambda X: branin_hoo(X, switch = False)

y_fun = funtest(comb).reshape(Npts, Npts)
x_star_2d_true = np.asarray([xx[y_fun[:, uu].argmin()] for uu in xrange(NptsU)])
val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(NptsU)])

ax_cont = plt.subplot(2, 2, 2)
ax_cont.set_title(nameBHs + r' and conditional minimisers')
ax_cont.contourf(X_, Y_, y_fun, resolution, cmap=colormap_contourf)
ax_cont.plot(x_star_2d_true, yy, 'r.', markersize=1)
# ax_cont.axvline(xx[(np.sum(bool_grid, 1)).argmax()], color = 'r', ls = '--')

ax_cont.set_xlabel(r'$\mathbf{k}$')
ax_cont.set_ylabel(r'$\mathbf{u}$')
ax = plt.subplot(2, 2, 4)
ax.set_title(r'Estimated density of the minimisers'
             # '\n'
             # r'and conditional moments'
             # "\n"
             # r'Differential entropy = ' +
             # str(round(entropy_from_array(x_star_2d_true), 3))
             )
ax = sns.distplot(x_star_2d_true, color = 'r',
                  kde_kws={"color": "r", "lw": 1},  # "label": "KDE"},
                  hist_kws ={"color": color_hist, 'alpha': .35})  # , "label": "Histogram"})
kde = ax.lines[0]
kMPE_KDE = kde.get_xdata()[kde.get_ydata().argmax()]

his = ax.patches
ba_height = np.asarray([bar.get_height() for bar in his])
ba_x = np.asarray([bar.get_x() for bar in his])
ba_x = ba_x + np.diff(ba_x)[0] / 2.0
kMPE_his = ba_x[ba_height.argmax()]

EM_xstar = fit_EM_density(x_star_2d_true, k=3)
kMPE_EM = xx[EM_xstar(xx).argmax()]

EM_xstar = fit_EM_density(x_star_2d_true, k=3)
ax.plot(xx, EM_xstar(xx), label = 'Mix of 3 Gaussians fitted by EM',
        lw=1, color='blue')
red_patch = mpatches.Patch(color=color_hist, alpha=0.35)

# ax.plot(np.nan, np.nan, 'm--', label =r'$\mu(\mathbf{k})$', lw=1)
# ax.plot(np.nan, np.nan, ls=':', color=color_std, label=r'$\sigma(\mathbf{k})$', lw=1)
# ax_mean = ax.twinx()
# ax_mean.set_ylabel(r'', color=color_mean)
# ax_mean.tick_params('y', colors=color_mean)

# ax_mean.plot(xx, np.mean(y_fun, 1), label=(r'$\mu(\mathbf{k})$'),
#              lw=1, ls='-.', color=color_mean)
# ax_mean.set_yticks([])

# ax_std = ax.twinx()
# ax_std.plot(xx, np.std(y_fun, 1), color=color_std, ls=':')
# ax_std.set_yticks([])
ax.set_ylim(0, 2)
ax.set_xlim(0, 5)
plt.xlim(comb[:, 0].min(), comb[:, 0].max())
ax.set_xlabel(r'$\mathbf{k}$')
ax.axvline(kMPE_EM, color='blue', ls='--',
           label = r'$\hat{\mathbf{k}}_{\mathrm{MPE, EM}}$')
ax_cont.axvline(kMPE_EM, color='blue', ls='--')
ax.axvline(kMPE_KDE, color='red', ls='--',
           label = r'$\hat{\mathbf{k}}_{\mathrm{MPE, KDE}}$')
ax_cont.axvline(kMPE_KDE, color='red', ls='--')

ax.axvline(kMPE_his, color=color_hist, ls='--',
           label=r'$\hat{\mathbf{k}}_{\mathrm{MPE, histogram}}$')
ax_cont.axvline(kMPE_his, color=color_hist, ls='--')
handles.append(red_patch)
labels.append('Histogram')
fig.legend(handles, labels, loc=8, ncol=3, bbox_to_anchor=(0.5, -0.025), frameon=False)
plt.tight_layout()

plt.subplots_adjust(bottom=0.17)
plt.savefig('/home/victor/Dropbox/PhD/Article/NPG1/Figures/branin_side_66.pdf')
plt.show()
plt.close()





# ------------------------------------------------------------------------
## - 2 figures side by side : Minimisers and relaxation of the constraint -
# ------------------------------------------------------------------------


resolution = 10
colormap_contourf = cm.viridis
colormap_region = cm.Wistia
color_std = 'limegreen'
color_mean = 'magenta'
color_hist = 'gray'
fig = plt.figure(figsize = (6, 6))
# First column ----------------------------------------------------------
funtest = lambda X: branin_hoo(X, switch = True)
y_fun = funtest(comb).reshape(Npts, Npts)

x_star_2d_true = np.asarray([xx[y_fun[:, uu].argmin()] for uu in xrange(NptsU)])
val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(NptsU)])
value, bool_grid = Jminus_min(alpha_BH, y_fun, val_star_2d_true)
value_15, bool_grid_15 = Jminus_min(1.5, y_fun, val_star_2d_true)

ax_cont = plt.subplot(2, 2, 1)  # Upper Left ------------------------------
ax_cont.set_title(nameBH + r'$(\mathbf{k},\mathbf{u})$')
ax_cont.contourf(X_, Y_, y_fun, resolution, cmap=colormap_contourf)
ax_cont.contour(X_, Y_, bool_grid, 3)
ax_cont.contour(X_, Y_, bool_grid_15, 3, cmap=colormap_region)
# ax_cont.plot(x_star_2d_true, yy, 'r.', markersize=1)  #  
ax_cont.axvline(xx[(np.sum(bool_grid, 1)).argmax()], color='r', ls = '--')
# ax_cont.axvline(xx[np.mean(y_fun, 1).argmin()], color=color_mean, ls = '--')
# ax_cont.axvline(xx[np.std(y_fun, 1).argmin()], color=color_std, ls = '--')
ax_cont.set_xlabel(r'$\mathbf{k}$')
ax_cont.set_ylabel(r'$\mathbf{u}$')

ax = plt.subplot(2, 2, 3)  # Lower Left --------------------------------

ax.set_title(r'Probability $\hat{\Gamma}_{\hat{\alpha}_1}(\mathbf{k})$'
             '\n'
             r'$\hat{\alpha}_1 = $' + str(round(alpha_BH, 4)))

# Histogram and KDE ----
ax = sns.distplot(x_star_2d_true, color=color_hist,
                  kde_kws={"color": color_hist, "lw": 2},  # "label": "KDE"},
                  kde = False, norm_hist=True
                  # hist_kws ={"color": 'r', 'alpha': .35},# "label": "Histogram"})
                  )
ax.axvline(xx[(np.sum(bool_grid, 1)).argmax()], color='r', ls='--',
           label=r'$\hat{\mathbf{k}}_1$')
ax.plot(xx, np.sum(bool_grid, 1) / float(NptsU), linewidth=2,
        color='black', label=r'$\hat{\Gamma}_{\alpha}(\mathbf{k})$')
ax.plot(xx, np.sum(bool_grid_15, 1) / float(NptsU), linewidth=1, color='dimgray',
        label=r'$\hat{\Gamma}_{1.5}(\mathbf{k})$')
ax.set_ylabel(r'$\hat{\Gamma}_{\alpha}$')
# Moment optimisers ----
# ax_mean = ax.twinx()
# ax_mean.plot(xx, np.mean(y_fun, 1), lw=1, color=color_mean, ls='-.')
# ax_mean.axvline(xx[np.mean(y_fun, 1).argmin()], color=color_mean, ls='--')
# ax_mean.set_ylabel(r'', color=color_mean)
# # ax_mean.tick_params('y', top=False, bottom=False, colors=color_mean)
# ax_mean.set_yticks([])

# ax_std = ax.twinx()
# ax_std.set_yticks([])
# ax_std.plot(xx, np.std(y_fun, 1), lw=1, color=color_std, ls=':')
# ax_std.axvline(xx[np.std(y_fun, 1).argmin()], color=color_std, ls='--')


ax.set_ylim(0, 1.2)
ax.set_xlabel(r'$\mathbf{k}$')
ax.set_xlim(comb[:, 0].min(), comb[:, 0].max())

#  Second column --------------------------------------------------------
funtest = lambda X: branin_hoo(X, switch = False)

y_fun = funtest(comb).reshape(Npts, Npts)
x_star_2d_true = np.asarray([xx[y_fun[:, uu].argmin()] for uu in xrange(NptsU)])
val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(NptsU)])

ax_cont = plt.subplot(2, 2, 2)  # Upper Right -------------------------------
ax_cont.set_title(nameBHs + r'$(\mathbf{k},\mathbf{u})$')
value, bool_grid = Jminus_min(alpha_BHs, y_fun, val_star_2d_true)
value_15, bool_grid_15 = Jminus_min(1.5, y_fun, val_star_2d_true)
ax_cont.contour(X_, Y_, bool_grid, 3)
ax_cont.contour(X_, Y_, bool_grid_15, 3, cmap=colormap_region)
ax_cont.contourf(X_, Y_, y_fun, resolution, cmap=colormap_contourf)
# ax_cont.plot(x_star_2d_true, yy, 'r.', markersize=1)
ax_cont.axvline(xx[(np.sum(bool_grid, 1)).argmax()], color = 'r', ls='--')
# ax_cont.axvline(xx[np.mean(y_fun, 1).argmin()], color=color_mean, ls='--')
# ax_cont.axvline(xx[np.std(y_fun, 1).argmin()], color=color_std, ls='--')
ax_cont.set_xlabel(r'$\mathbf{k}$')
ax_cont.set_ylabel(r'$\mathbf{u}$')

ax = plt.subplot(2, 2, 4)  # Lower Right -----------------------
ax.set_title(r'Probability $\hat{\Gamma}_{\hat{\alpha}_1}(\mathbf{k})$'
             '\n'
             r'$\hat{\alpha}_1 = $' + str(round(alpha_BHs, 4)))

# Histogram and KDE ----
ax = sns.distplot(x_star_2d_true, color=color_hist,
                  # kde_kws={"color": "r", "lw": 2},# "label": "KDE"},
                  hist_kws ={"color": color_hist, 'alpha': .35},
                  kde = False, norm_hist=True)  # , "label": "Histogram"})
ax.axvline(xx[(np.sum(bool_grid, 1)).argmax()], color='r', ls='--')
ax.plot(xx, np.sum(bool_grid, 1) / float(NptsU), linewidth=2,
        color='black', label = r'$\hat{\Gamma}_{\hat{\alpha}_1}(\mathbf{k})$')
ax.plot(xx, np.sum(bool_grid_15, 1) / float(NptsU), linewidth=1, color='dimgray',
        label=r'$\hat{\Gamma}_{1.5}(\mathbf{k})$')
ax.set_ylabel(r'$\hat{\Gamma}_{\alpha}$')
# Moment optimisers
# ax_mean = ax.twinx()
# ax_mean.plot(xx, np.mean(y_fun, 1), lw=1, color=color_mean, ls='-.')
# ax_mean.axvline(xx[np.mean(y_fun, 1).argmin()], color=color_mean, ls='--')
# ax_mean.set_ylabel(r'', color=color_mean)
# # ax_mean.tick_params('y', colors=color_mean)
# ax_mean.set_yticks([])

# ax_std = ax.twinx()
# ax_std.plot(xx, np.std(y_fun, 1), 'g:', lw=1)
# ax_std.axvline(xx[np.std(y_fun, 1).argmin()], color=color_std, ls='--')
# ax_std.set_yticks([])

ax.set_ylim(0, 1.2)
ax.set_xlim(0, 5)
ax.set_xlim(comb[:, 0].min(), comb[:, 0].max())
ax.set_xlabel(r'$\mathbf{k}$')

# Legend -------
# ax.plot(np.nan, np.nan, color=color_std, lw = 1, label = r'$\sigma(\mathbf{k})$',
#         ls=':')
# ax.plot(np.nan, np.nan, color_mean, label =r'$\mu(\mathbf{k})$', lw=1, ls='-.')
# ax.plot(np.nan, np.nan, color_mean, ls='--', label=r'$\mathbf{k}_{\mathrm{mean}}$')
ax.plot(np.nan, np.nan, 'r', ls='--', label=r'$\hat{\mathbf{k}}_1$')
# ax.plot(np.nan, np.nan, '.r', label=r'Conditional minimisers')
# ax.plot(np.nan, np.nan, color_std, ls='--', label=r'$\mathbf{k}_{\mathrm{var}}$')
ax.plot(np.nan, np.nan, color=colormap_contourf(np.inf),
        label=r'$\{(\mathbf{k},\mathbf{u}) \mid J(\mathbf{k},\mathbf{u}) = \hat{\alpha}_1 J^*(\mathbf{u}) \}$')
ax.plot(np.nan, np.nan, color=colormap_region(np.inf),
        label=r'$\{(\mathbf{k},\mathbf{u}) \mid J(\mathbf{k},\mathbf{u}) = 1.5 J^*(\mathbf{u}) \}$')
handles, labels = ax.get_legend_handles_labels()
red_patch = mpatches.Patch(color=color_hist, label='', alpha = 0.35)
handles.append(red_patch)
labels.append('Histogram of the minimisers')
fig.legend(handles, labels, loc=8, ncol=3, bbox_to_anchor=(0.5, 0.0025), frameon=False)

fig.tight_layout()
fig.subplots_adjust(bottom = 0.20)
plt.savefig('/home/victor/acadwriting/Article/NPG1/Figures/branin_side_66_relax_gamma_both.pdf')
plt.show()
plt.close()



## Illustration of $\alpha_p$ ------------

funtest = lambda X: branin_hoo(X, switch = False)
y_fun = funtest(comb).reshape(Npts, Npts)
fig = plt.figure(figsize = (5, 3))
x_star_2d_true = np.asarray([xx[y_fun[:, uu].argmin()] for uu in xrange(NptsU)])
val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(NptsU)])
value, bool_grid = Jminus_min(2.2196, y_fun, val_star_2d_true)
value_8, bool_grid_8 = Jminus_min(1.345, y_fun, val_star_2d_true)
value_5, bool_grid_5 = Jminus_min(1.064, y_fun, val_star_2d_true)

gamma_1 = np.sum(bool_grid, 1) / float(NptsU);
gamma_8 = np.sum(bool_grid_8, 1) / float(NptsU); max(gamma_8)
gamma_5 = np.sum(bool_grid_5, 1) / float(NptsU);max(gamma_5)

ax = plt.subplot()
ax.plot(xx, gamma_1, linewidth=1,
        color='black', label=r'$\Gamma_{\alpha}(\mathbf{k}),~\alpha = \alpha_{1}$')
ax.axhline(1, color='k', ls = ':')
ax.axvline(xx[gamma_1.argmax()], color='k', ls= '-')

ax.plot(xx, gamma_8, linewidth=1, color='silver',
        label=r'$\Gamma_{\alpha}(\mathbf{k}),~\alpha = \alpha_{0.8}$')
ax.axhline(.8, color='silver', ls = ':')
ax.axvline(xx[gamma_8.argmax()], color='silver', ls= '-')

ax.plot(xx, gamma_5, linewidth=1, color='gainsboro',
        label=r'$\Gamma_{\alpha}(\mathbf{k}),~\alpha = \alpha_{0.6}$')
ax.axhline(.6, color='gainsboro', ls = ':')
ax.axvline(xx[gamma_5.argmax()], color='gainsboro', ls= '-')


ax.annotate(r'$\mathbf{k}_1$', (xx[gamma_1.argmax()] + 0.1, 0.1))
ax.annotate(r'$\mathbf{k}_{0.8}$', (xx[gamma_8.argmax()] + 0.1, 0.1))
ax.annotate(r'$\mathbf{k}_{0.6}$', (xx[gamma_5.argmax()] + 0.1, 0.1))
ax.legend()
ax.set_xlabel(r'$\mathbf{k}$')
ax.set_ylabel(r'$\Gamma_{\alpha}$')
ax.set_title(r'Illustration of the probability $\Gamma_{\alpha_p}$ for different $p$' +'\n' + r'and associated $\mathbf{k}_p$')
plt.tight_layout()
# plt.show()
plt.savefig('/home/victor/acadwriting/Article/NPG1/Figures/illu_alpha_p.pdf')



## ----


# -----------------------------------------------------------------------
# -- 4 figures : Explanation of the procedure ---------------------------
# -----------------------------------------------------------------------

funtest = lambda X: branin_hoo(X, switch=True)
y_fun = funtest(comb).reshape(Npts, Npts)

x_star_2d_true = np.asarray([xx[y_fun[:, uu].argmin()] for uu in xrange(NptsU)])
val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(NptsU)])
resolution = 10
index = 280
ind = 599
ftsize = 12
fig = plt.figure(figsize = (6, 6))

axul = plt.subplot(2, 2, 1)
axul.set_title(r'Conditional minimisers')
axul.contourf(X_, Y_, y_fun, resolution)  # , cmap = cm.Greys)
axul.axhline(yy[index], color = 'r', ls='--')
axul.annotate(r'$\mathbf{u}$', xy=(0, yy[index]), xytext=(0.1, yy[index] + 0.1),
              fontsize = ftsize, color = 'white')
kstar = x_star_2d_true[index]
axul.axvline(kstar, color = 'r', ls = '--')
axul.annotate(r'$\mathbf{k}^*(\mathbf{u})$', xy=(kstar, 0),
              xytext=(kstar + 0.2, 0.1), color='white',
              fontsize = ftsize)
axul.annotate(r'$J^*(\mathbf{u})$', xy=(kstar, yy[index]),
              xytext=(kstar + 0.5, yy[index] + 0.5), color='white',
              arrowprops=dict(arrowstyle = '->', edgecolor='white'), fontsize = ftsize)
axul.plot(kstar, yy[index], 'r.', markersize = 1)
# axul.plot(xx, (y_fun[:, index] - y_fun[:, index].min()) / 1.4 + 3, color = 'white')
axul.set_xlabel(r'$\mathbf{k}$')
axul.set_ylabel(r'$\mathbf{u}$')

axll = plt.subplot(2, 2, 3)
axll.set_title(r'Relaxation of the constraint, $\alpha=1.5$')
axll.contourf(X_, Y_, y_fun, resolution)
value, bool_grid = Jminus_min(1.5, y_fun, val_star_2d_true)
# axll.axvline(kstar, color = 'r', ls = '--')
axll.contour(X_, Y_, bool_grid)
acceptable_values = np.where(bool_grid[:, ind], xx[ind], np.nan)
bad_values = np.where(bool_grid[:, ind], np.nan, xx[ind])
axll.annotate(r'$J(\mathbf{k},\mathbf{u}) < \alpha J^*(\mathbf{u})$',
              textcoords='data', xycoords='data', horizontalalignment='center',
              xy = (1.7, 3), xytext=(3.5, 4.5), color = 'cyan',
              arrowprops=dict(arrowstyle='->', edgecolor='cyan',
                              connectionstyle = 'angle, angleA=0, angleB=90',
                              mutation_scale=16),
              fontsize = ftsize)
axll.annotate(r'$J(\mathbf{k},\mathbf{u}) > \alpha J^*(\mathbf{u})$',
              textcoords='data', xycoords='data', horizontalalignment='center',
              xy = (3.5, 3), xytext=(3.5, 4), color = 'salmon',
              arrowprops=dict(arrowstyle='->', edgecolor='salmon',
                              mutation_scale=16),
              fontsize = ftsize)
axll.plot(xx, acceptable_values, color = 'cyan')
axll.plot(xx, bad_values, color = 'salmon')
axll.plot(x_star_2d_true, yy, 'r.', markersize = 1)
axll.axvline(xx[300], color='white', ls='--')
axll.annotate(r'$\mathbf{k}$', textcoords='data', xycoords='data', color='white',
              fontsize = ftsize,
              xy = (xx[300], 0), xytext=(xx[300] - 0.3, 0.1))
axll.set_xlabel(r'$\mathbf{k}$')
axll.set_ylabel(r'$\mathbf{u}$')
# --- Second column -----------------------------------------------------
axur = plt.subplot(2, 2, 2)
axur.set_title(r'Set of conditional minimisers')
axur.contourf(X_, Y_, y_fun, resolution)  # , cmap = cm.Greys)
axur.plot(x_star_2d_true, yy, 'r.', markersize = 1)
axur.set_xlabel(r'$\mathbf{k}$')
axur.set_ylabel(r'$\mathbf{u}$')

fig = plt.figure(figsize=(6, 6))
axlr = fig.add_subplot(1, 1, 1)
axlr.set_title(r'Region $R_{\alpha}(\mathbf{k})$')
axlr.contourf(X_, Y_, y_fun, resolution)
axlr.contour(X_, Y_, bool_grid)
# ind = 100
# values_prob = np.where(bool_grid[ind, :], xx[ind] * bool_grid[ind, :], np.nan)
# axlr.plot(values_prob, yy, 'm.', markersize = 1)
# mid = np.nanmean(np.where(bool_grid[ind, :], yy * bool_grid[ind, :], np.nan))
# rge = np.nanmax(np.where(bool_grid[ind, :], yy * bool_grid[ind, :], np.nan))\
#       - np.nanmin(np.where(bool_grid[ind, :], yy * bool_grid[ind, :], np.nan))
# axlr.annotate(r'$R_{\alpha}(\mathbf{k}_1)$', xy=(xx[ind] + 0.2, mid),
#               xytext=(4.5, 4.5), color='m', textcoords='data', xycoords='data',
#               arrowprops=dict(arrowstyle='-[, widthB=' + str(rge), edgecolor='m',
#                               connectionstyle = 'angle, angleA=90, angleB=0', mutation_scale=16))
u_ind = 300  # ((kstar - xx)**2).argmin()
axlr.axvline(xx[300], color='white', ls='--')
axlr.plot(x_star_2d_true, yy, 'r.', markersize = 1)
axlr.annotate(r'$\mathbf{k}$', textcoords='data', xycoords='data', color='white',
              fontsize = ftsize,
              xy = (xx[300], 0), xytext=(xx[300] - 0.3, 0.1))
values_prob = np.where(bool_grid[u_ind, :], xx[u_ind] * bool_grid[u_ind, :], np.nan)
axlr.plot(values_prob, yy, color='lime', markersize = 1)
mid = np.nanmean(np.where(bool_grid[u_ind, :], yy * bool_grid[u_ind, :], np.nan))
rge = np.nanmax(np.where(bool_grid[u_ind, :], yy * bool_grid[u_ind, :], np.nan))\
    - np.nanmin(np.where(bool_grid[u_ind, :], yy * bool_grid[u_ind, :], np.nan))
axlr.annotate(r'$R_{\alpha}(\mathbf{k})$', xy=(xx[u_ind] + 0.2, mid),
              xytext=(3.5, 3), color='lime', textcoords='data', xycoords='data', fontsize=ftsize,
              arrowprops=dict(arrowstyle='-[, widthB=' + str(rge + 7.0# + 0.70
              ), edgecolor='lime',
                              connectionstyle = 'angle, angleA=90, angleB=0',
                              mutation_scale=ftsize + 1))

# ind = 800
# values_prob = np.where(bool_grid[ind, :], xx[ind] * bool_grid[ind, :], np.nan)
# axlr.plot(values_prob, yy, 'm.', markersize = 1)
# mid = np.nanmean(np.where(bool_grid[ind, :], yy * bool_grid[ind, :], np.nan))
# rge = np.nanmax(np.where(bool_grid[ind, :], yy * bool_grid[ind, :], np.nan))\
#       - np.nanmin(np.where(bool_grid[ind, :], yy * bool_grid[ind, :], np.nan))
# axlr.annotate(r'$R_{\alpha}(\mathbf{k}_3)$', xy=(xx[ind] + 0.2, mid),
#               xytext=(4.5, 2), color='m', textcoords='data', xycoords='data',
#               arrowprops=dict(arrowstyle='-[, widthB=' + str(rge), edgecolor='m',
#                               connectionstyle = 'angle, angleA=90, angleB=0', mutation_scale=16))

axlr.set_xlabel(r'$\mathbf{k}$')
axlr.set_ylabel(r'$\mathbf{u}$')
plt.tight_layout()
plt.savefig('/home/victor/acadwriting/Article/NPG1/Figures/relaxation_tuto_4.pdf')
plt.show()
plt.close()


# ------------------------------------------------------------------------------
# COMPUTATION OF V@R and CV@R
# ------------------------------------------------------------------------------

# val_star_2d_true = J*(U)


def ValueAtRisk(y_fun, p, d_alpha):
    VaR = np.empty(len(y_fun[:, 0]))
    val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(len(y_fun[0, :]))])
    for i in xrange(len(y_fun[:, 0])):
        val = 0
        al = 1.0
        while val < p:
            al += d_alpha
            val = np.mean((y_fun[i, :] - al * val_star_2d_true) <= 0)
        VaR[i] = al
    return VaR


def CValueAtRisk(y_fun, p, VaR):
    if VaR is None:
        VaR = ValueAtRisk(y_fun, p, 0.1)
    CVaR = np.empty(len(y_fun[:, 0]))
    for i in xrange(len(y_fun[:, 0])):
        overshoot = (y_fun[i, :] / val_star_2d_true > VaR[i])
        CVaR[i] = np.mean(y_fun[i, overshoot] / val_star_2d_true[overshoot])
    return CVaR



d_alpha = 0.001
VaR090 = ValueAtRisk(y_fun, 0.9, d_alpha)
VaR095 = ValueAtRisk(y_fun, 0.95, d_alpha)
VaR099 = ValueAtRisk(y_fun, 0.99, d_alpha)
VaR100 = ValueAtRisk(y_fun, 1.0, d_alpha)
CVaR090 = CValueAtRisk(y_fun, 0.90, VaR090)
CVaR095 = CValueAtRisk(y_fun, 0.95, VaR095)
CVaR099 = CValueAtRisk(y_fun, 0.99, VaR099)
CVaR100 = CValueAtRisk(y_fun, 1.00, VaR100)

plt.plot(xx, VaR090, 'b')
plt.plot(xx, CVaR090, '--b')
plt.plot(xx, VaR095, 'm')
plt.plot(xx, CVaR095, '--m')
plt.plot(xx, VaR099, 'r')
plt.plot(xx, CVaR099, '--r')
plt.plot(xx, VaR100, 'g')
plt.plot(xx, CVaR100, '--g')
plt.show()


# ------------------------------------------------------------------------------
def compute_all_cdfs(y_fun, alpha=None, plot_curves=True):
    mmax = np.ceil(y_fun.max())
    mmin = np.floor(y_fun.min())
    range_fun = np.linspace(mmin, mmax, 500)
    NptsK, NptsU = y_fun.shape
    i_mean = np.mean(y_fun, 1).argmin()
    i_var = np.var(y_fun, 1).argmin()
    val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(len(y_fun[0, :]))])
    if alpha is None:
        alpha = alpha_check(y_fun, lower=1.0, upper=2.5, niter=20,
                            tol = 1e-9, p=1.0)
    kcheck_vec = plot_contourrobust(alpha, funtest=y_fun, xx=xx, yy=yy,
                                    save=False, ylim=None)
    plt.close()
    i_check1 = ((xx - kcheck_vec)**2).argmin()
    print kcheck_vec
    for i in xrange(NptsK):
        color = 'gray'
        alpha = 0.01
        label = None
        linewidth = 1
        ls = '-'
        zorder = 1
        if i == i_mean:
            color = 'm'
            alpha = 1
            label = r'$\mathbf{k}_{\mathrm{mean}}$'
            ls = '-.'
            linewidth = 2
            zorder = 10
        if i == i_var:
            color = 'limegreen'
            alpha = 1
            label = r'$\mathbf{k}_{\mathrm{var}}$'
            linewidth = 2
            ls = ':'
            zorder = 10
        if i == i_check1:
            color = 'r'
            alpha = 1
            label = r'$\mathbf{k}_1$'
            linewidth = 2
            ls = '-'
            zorder = 10
        if i == 1105:  # 1105 for the maximal ratio in Lorenz
            color = 'c'
            alpha = 1
            label = r'$\mathbf{k}$ of maximal ratio'
            linewidth = 2
            zorder = 10
        ecdf = ECDF(y_fun[i, :])
        plt.plot(range_fun, ecdf(range_fun), alpha=alpha,
                 color=color, label=label, lw=linewidth, ls=ls, zorder=zorder)
    ecdf_star = ECDF(val_star_2d_true)
    plt.plot(range_fun, ecdf_star(range_fun), alpha=1,
             color='black', label=r'CDF of $J^*(\mathbf{U})$', lw=2, ls=ls)
    plt.title(r'Estimated CDF of the profiles for different $\mathbf{k}$')
    plt.xlabel(r'$x$')
    plt.ylabel(r'$\mathbb{P}_U\left[J(\mathbf{k},\mathbf{U}) < x\right]$')
    plt.legend()

minimum=True

plt.figure(figsize = (8, 4))
plt.subplot(1, 2, 1)
compute_all_cdfs(y_fun, alpha_BH)
plt.subplot(1, 2, 2)
compute_all_cdfs(y_fun.T, alpha_BHs)
# compute_all_cdfs(y_fun, 1.31100463867)
plt.tight_layout()
# plt.savefig('/home/victor/cdf_lorenz_with_ratio.pdf')
plt.show()
plt.close()

# Definition: Z = max_k J(k,U)/J^*(U) RV


def compute_Zrv(y_fun):
    val_star_2d_true = np.asarray([y_fun[:, uu].min() for uu in xrange(len(y_fun[0, :]))])
    ratio = (y_fun / val_star_2d_true[:, None])
    return np.amax(ratio, 1)


Zrv = compute_Zrv(y_fun)
ecdf_Z = ECDF(Zrv)

plt.plot(ecdf_Z.x, ecdf_Z.y)
plt.show()














#
#
#

























# ------------------------------------------------------------------------------



#  Worst case
u_pire_true = [yy[y_fun[kk, :].argmax()] for kk in xrange(Npts)]
val_pire_true = [y_fun[kk, :].max() for kk in xrange(Npts)]


offset = -4
fig = plt.figure(figsize = (7, 10))
ax = fig.add_subplot(211, projection='3d')
surf = ax.plot_surface(X_, Y_, y_fun, cmap = cm.viridis)
surf2 = ax.contour(X_, Y_, y_fun, 15, offset = offset, cmap = cm.viridis)

ax.set_xlabel(r'$k$')
ax.set_ylabel(r'$u$')
ax.set_zlim(offset, 3)
ax.plot(x_star_2d_true, yy, offset, '.', color = 'salmon')
ax.plot(xx, u_pire_true, offset, '.b')
ax.plot(xx[MPE(y_fun, 1.535).argmax()] * np.ones_like(xx),
        yy, offset, color = 'm')
ax.plot(xx[np.asarray(val_pire_true).argmin()] * np.ones_like(xx),
        yy, offset, color = 'y')
ax.set_title('Surface of J(k,u), and different quantities derived')
## 
ax2 = fig.add_subplot(2, 1, 2)
ax2.plot(xx, val_pire_true, label = r'Worst case', color = 'blue')
ax2.axvline(xx[np.asarray(val_pire_true).argmin()], color = 'blue', ls = '--')
ax2.hist(x_star_2d_true, 15, density = True,
         label = r'Histogram of minimizers', color = 'lightsalmon')
ax2.plot(xx, np.mean(y_fun, 1), label = r'Mean', color = color_std)
ax2.axvline(xx[np.mean(y_fun, 1).argmin()], color = color_std, ls = '--')
ax3 = ax2.twinx()
ax3.plot(xx, np.var(y_fun, 1), label = r'Variance', color = 'm')
ax3.axvline(xx[np.var(y_fun, 1).argmin()], color = 'm', ls = '--')

fig.legend(loc=1, bbox_to_anchor=(1, 1), bbox_transform=ax2.transAxes)
ax2.set_xlabel('k')
ax3.tick_params('y', colors='m')
ax2.set_title('Marginal mean, worst case scenario, histogram of minimizers and variance')
fig.tight_layout()
plt.show()
# plt.savefig('../../PhD/Slides/Figures/surface_transp_vert.pdf',transparent = True)




# -----------------------------------------------------------------------------------
#  NO 3D
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble = r'\usepackage{amsmath} \usepackage{amssymb}')

u_pire_true = [yy[y_fun[kk, :].argmax()] for kk in xrange(Npts)]
val_pire_true = [y_fun[kk, :].max() for kk in xrange(Npts)]


fig = plt.figure(figsize = (7, 10))
ax = fig.add_subplot(2, 1, 1)
surf2 = ax.contourf(X_, Y_, y_fun, 15, cmap = cm.viridis)

ax.set_xlabel(r'$k$')
ax.set_ylabel(r'$u$')
line_mini, = ax.plot(x_star_2d_true, yy, '-r',
                     label = r'$\min_{\mathbf{k}}J(\mathbf{k},\mathbf{u})$')
line_wc, = ax.plot(xx, u_pire_true, '.w',
                   label = r'$\max_{\mathbf{u}} J(\mathbf{k},\mathbf{u})$')
# ax.plot(xx[MPE(y_fun, 1.535).argmax()] * np.ones_like(xx),
#         yy, color = 'm')
# ax.plot(xx[np.asarray(val_pire_true).argmin()] * np.ones_like(xx),
#         yy, color = 'black')
ax.set_title('Surface of J(k,u), and different quantities derived')
ax.legend(loc = 1)
line_mini.set_label('_nolegend_')
line_wc.set_label('_nolegend_')
##
ax2 = fig.add_subplot(2, 1, 2)
ax2.plot(xx, val_pire_true, label = r'Worst case', color = 'blue')
ax2.axvline(xx[np.asarray(val_pire_true).argmin()], color = 'blue', ls = '--')
ax2.hist(x_star_2d_true, 15, density = True,
         label = r'Histogram of minimizers', color = 'red', alpha=0.2)
kde = KernelDensity(kernel='gaussian', bandwidth = .10).fit(np.array(x_star_2d_true).reshape(-1, 1))
ax2.plot(xx, np.exp(kde.score_samples(xx.reshape(-1, 1))), label = 'KDE of the minimizers',
         color= 'red', linewidth = 2)


ax2.plot(xx, np.mean(y_fun, 1), label = r'Mean', color = 'green')
ax2.axvline(xx[np.mean(y_fun, 1).argmin()], color = 'green', ls = '--')
ax3 = ax2.twinx()
ax3.plot(xx, np.var(y_fun, 1), label = r'Variance', color = 'm', ls = '-')
ax3.axvline(xx[np.var(y_fun, 1).argmin()], color = 'm', ls = '-')
ax3.axvline(xx[np.asarray(val_pire_true).argmin()], color = 'blue', ls=':')
fig.legend(loc=1, bbox_to_anchor=(1, 1), bbox_transform=ax2.transAxes)
ax2.set_xlabel(r'$k$')
ax3.tick_params('y', colors='m')
ax2.set_title('Marginal mean, worst case scenario, histogram of minimizers and variance')
ax2.set_xlim([0, 5])
fig.tight_layout()
plt.show()

#  ---------------------------------------------------------------------------


alpha_vec = np.linspace(1.0, 1.53, 200)
minimum = True

max_val = np.empty(200)
max_arg = np.empty(200)
for i, al in enumerate(alpha_vec):
    max_val[i] = MPE(y_fun, al, minimum = minimum).max()
    max_arg[i] = MPE(y_fun, al, minimum = minimum).argmax()
fig = plt.figure()
ax = fig.add_subplot(2, 1, 1)
surf2 = ax.contourf(X_, Y_, y_fun, 15, cmap = cm.viridis)
ax.set_xlabel(r'$k$')
ax.set_ylabel(r'$u$')
ax.plot(x_star_2d_true, yy, color = 'red')
ax.plot(xx, u_pire_true, 'b')
# ax.plot(xx[MPE(y_fun, 1.535).argmax()]*np.ones_like(xx),yy, color = 'm')
ax.plot(xx[np.asarray(val_pire_true).argmin()] * np.ones_like(xx), yy, color = 'g', ls = '--')
normalize = False
plt.subplot(2, 1, 2)
plt.rc('text.latex', preamble = r'\usepackage{amsmath}')
plt.plot(xx, MPE(y_fun, 1.00, normalize, minimum), label = r'$\alpha=1$')
# plt.plot(xx,MPE(y_fun, 0.99, normalize), label = r'$\alpha=0.99$')
# plt.plot(xx,MPE(y_fun, 0.9, normalize), label = r'$\alpha=1.01$')
plt.plot(xx, MPE(y_fun, 1.10, normalize, minimum), label = r'$\alpha=1.10$')
# plt.plot(xx,MPE(y_fun, 1.35, normalize, minimum), label = r'$\alpha=1.35$')
plt.plot(xx, MPE(y_fun, alpha_check(y_fun), normalize, minimum), label = r'$\alpha=\check{\alpha}$')
plt.plot(xx, MPE(y_fun, 2.00, normalize, minimum), label = r'$\alpha=2.00$')
plt.plot(xx, MPE(y_fun, 3.00, normalize, minimum), label = r'$\alpha=3.00$')
plt.plot(xx, MPE(y_fun, 5.00, normalize, minimum), label = r'$\alpha=5.00$')
plt.xlim([0, 5])
# plt.axhline(0.5, ls = '--', color = 'black')
if minimum:
    plt.title(r'$R_{\alpha}(\mathbf{k}) = {P}_U[J(\mathbf{k},\mathbf{U}) \leq \alpha \min_{\tilde{\mathbf{k}}} \, J(\tilde{\mathbf{k}},\mathbf{U})]$')
else:
    plt.title(r'$R_{\alpha}(\mathbf{k}) = {P}_U[\alpha J(\mathbf{k},\mathbf{U}) \geq \max_{\tilde{\mathbf{k}}} \, J(\tilde{\mathbf{k}},\mathbf{U})]$')
plt.plot(xx[max_arg.astype(int)], max_val, '.')
plt.tight_layout()

plt.axvline(xx[np.asarray(val_pire_true).argmin()],
            color ='green', ls = '--', label = 'worstcase')
plt.axvline(xx[MPE(y_fun, alpha_check(y_fun), minimum = minimum).argmax()],
            color = 'red', ls = '--', label = '$\check{\mathbf{k}}$')
plt.axvline(xx[np.var(y_fun, 1).argmin()], color = 'blue', ls = '--', label = 'minV')
plt.axvline(xx[np.mean(y_fun, 1).argmin()], color = 'm', ls = '--', label = 'minE')
plt.legend()
plt.show()

# @np.vectorize
# def alpha_fun(alpha):
#     return (np.sum(MPE(y_fun, alpha) == 1.0) -1 )**2

# import scipy
# opti_alpha  = scipy.optimize.minimize_scalar(alpha_fun, bounds = (1.52,1.529), method = 'bounded')
# opti_alpha.x


## Estimation of probability --------------------------------

initial_DOE = lhs(n = 2, samples = 10, criterion='maximin', iterations = 5)
plt.plot(initial_DOE[:,0], initial_DOE[:,1], '.'); plt.show()


def branin_fun(X, scaled = False):
    """
    Branin function, X \in [-5,10] x [0,15] if scaled = False
    scaled = True -> Picheny scaled on unit cube
    """
    X = np.atleast_2d(X)
    a = 1.0
    b = 5.1/(4*np.pi**2)
    c = 5.0/np.pi
    r = 6.0
    s = 10.0
    t = 1/(8*np.pi)
    alpha = 1.0
    if not scaled:
        x1, x2 = X[:,0], X[:,1]
        return a *(x2 - b * x1**2 + c*x1 - r)**2 + s*(1-t)*np.cos(x1) + s
    else:
        x1,x2 = X[:, 0], X[:, 1]
        x1 = 15 * x1 - 5
        x2 = 15 * x2
        # alpha = 1/(51.95)
        # a = 1.0
        # b = 5.1/(4*np.pi**2)
        # c = 5.0/np.pi
        # r = 6.0
        # s = 10.0
        # t = 1/(8*np.pi)
        s = -44.81
        return alpha *(a * (x2 - b * x1**2 + c *x1 - r)**2 + s*(1-t)*np.cos(x1) + s)
        
gp = GaussianProcessRegressor(kernel = Matern(0.1*np.ones(2)))
Npts = 60
xx = np.linspace(0,1,Npts)
yy = xx.copy()
X_,Y_ = np.meshgrid(xx,yy, indexing = 'ij')
comb = np.array([X_,Y_]).T.reshape(-1,2)
output = branin_fun(comb, True).reshape(Npts,Npts)

out_DOE = branin_fun(initial_DOE, True)
gp.fit(initial_DOE, out_DOE)

gp_pred, gp_std = gp.predict(comb, return_std = True)


fig = plt.figure()
ax = fig.add_subplot(211, projection='3d')
surf = ax.plot_surface(X_, Y_, output, cmap = cm.viridis)
ax.set_xlabel(r'$x_1$')
ax.set_ylabel(r'$x_2$')
ax = fig.add_subplot(212, projection='3d')
surf = ax.plot_surface(X_, Y_, gp_pred.reshape(Npts,Npts), cmap = cm.viridis)
# ax.plot(x1min,yy,'.')
ax.set_xlabel(r'$x_1$')
ax.set_ylabel(r'$x_2$')
plt.show()

ssy = gp.sample_y(comb,n_samples = 2, random_state = 1234)

fig = plt.figure()
plt.title('Two samples path of $\bar{f}(x_1,x_2)$')
ax = fig.add_subplot(211, projection='3d')
surf = ax.plot_surface(X_, Y_, ssy[:,0].reshape(Npts,Npts), cmap = cm.viridis)
ax.set_xlabel(r'$x_1$')
ax.set_ylabel(r'$x_2$')
ax = fig.add_subplot(212, projection='3d')
surf = ax.plot_surface(X_, Y_, ssy[:,-1].reshape(Npts,Npts), cmap = cm.viridis)
ax.set_xlabel(r'$x_1$')
ax.set_ylabel(r'$x_2$')
plt.show()

# ssy = gp.sample_y(comb,n_samples = 10)


def prob_threshold(y_fun, u):
    return np.sum(y_fun < u)/float(y_fun.shape[0])

def prob_threshold_gp(gp, u, n_samples, Xgrid):
    samples = gp.sample_y(Xgrid, n_samples)
    prob_vec = [prob_threshold(samples[:,i],u) for i in xrange(n_samples)]
    return np.mean(prob_vec)

def slicer_gp_predict(gp, x2):
    return lambda x1: gp.predict(np.vstack([np.asarray(x1),np.ones_like(x1)*x2]).T)

def slicer_gp_sample_y(gp,x2):
    return lambda x1,n_samples: gp.sample_y(np.vstack([np.asarray(x1),np.ones_like(x1)*x2]).T, n_samples)

sslic = slicer_gp_sample_y(gp,yy[2])


def minimum_gp_sliced(gp, x2, gp_type = 'predict', n_samples = 10):
    if gp_type == 'predict':
        opt = scipy.optimize.minimize_scalar(slicer_gp_predict(gp,x2), bounds = (0,1), method = 'bounded')
    # elif gp_type == 'sample':
    #     fun = lambda x1: slicer_gp_sample_y(gp,x2)(x1,n_samples)
        
    # [opt = scipy.optimize.minimize(fun, x0 = 0,args = (n_samples), bounds = [(0,1)], tol = 1e-6) for i in xrange(n_samples)]

    return opt.x, opt.fun


def minimum_samples(gp,x2,n_samples):
    fun = lambda x1: slicer_gp_sample_y(gp,x2)(x1,n_samples).T
    
    fun_columns_append = np.empty([n_samples,1])
    xsearch = np.linspace(0,1,200)
    for x in xsearch:
        fun_columns_append = np.append(fun_columns_append, fun(x), axis= 1)
    fun_columns_append = fun_columns_append[:, 1:]
    return xsearch[fun_columns_append.argmin(axis=1)]


fig = plt.figure()
ax = plt.subplot2grid((2,2),(0,0), rowspan = 2)
for i in xrange(0,60,1):
    ax.plot(minimum_samples(gp,yy[i], 10))
x1min = [float(minimum_gp_sliced(gp,x2)[0]) for x2 in yy]
valmin = np.asarray([minimum_gp_sliced(gp,x2)[1] for x2 in yy])

ax = plt.subplot2grid((2,2),(0,1), projection='3d')
surf = ax.plot_surface(X_, Y_, gp_pred.reshape(Npts,Npts).T, cmap = cm.viridis)
ax.plot(x1min, yy, valmin[:,0],'.')
ax.set_xlabel(r'$x_1$')
ax.set_ylabel(r'$x_2$')
ax = plt.subplot2grid((2,2), (1,1))
ax.hist(x1min)
plt.show()


def J_min(fun, x2, n_restarts = 10):
    """
    Returns min_x1 J(x1,x2) = f(x2)
    """
    fun_scipy = lambda x1,x2: fun(np.vstack([np.asarray(x1),np.asarray(x2)]).T)
    opt1 = scipy.optimize.minimize(fun_scipy, x0 = 0.5, args = x2 , bounds = [(0,1)])  
    best_value = opt1.fun
    for i in xrange(n_restarts):
        opt = scipy.optimize.minimize(fun_scipy, x0 = float(np.random.uniform(size = 1)), args = x2 , bounds = [(0,1)])
        if opt.fun > best_value:
            best_value = opt.fun
            opt1 = opt
    return opt1

def diffJ_min(fun,X, alpha = 1.0): # fun prend 2 arguments
    X = np.atleast_2d(X)
    x1,x2 = X[:,0],X[:,1]
    minJ = J_min(fun,x2)
    return fun(X) - alpha*minJ.fun

diffJ_comb = np.asarray([float(diffJ_min(branin_fun, comb[i,:])) for i in xrange(comb.shape[0])])
diffJ_comb.min()

def acquisition_function(gp,X):
    mean_gp,sd_gp = gp.predict(X, return_std=True)
    return np.abs(mean_gp)/sd_gp

acquisition_function(gp, np.atleast_2d([1.0,1.0]))

fig = plt.figure()
ax = fig.add_subplot(311, projection='3d')
surf = ax.plot_surface(X_, Y_, gp_pred.reshape(Npts,Npts), cmap = cm.viridis)
ax.set_xlabel(r'$x_1$')
ax.set_ylabel(r'$x_2$')

ax = fig.add_subplot(312, projection='3d')
surf = ax.plot_surface(X_, Y_, diffJ_comb.reshape(Npts,Npts), cmap = cm.viridis)
ax.set_xlabel(r'$x_1$')
ax.set_ylabel(r'$x_2$')
ax = fig.add_subplot(313, projection='3d')
surf = ax.plot_surface(X_, Y_, acquisition_function(gp,comb).reshape(Npts,Npts), cmap = cm.viridis)
ax.set_xlabel(r'$x_1$')
ax.set_ylabel(r'$x_2$')
plt.show()

for i in np.linspace(0,1,20):
    minJ05 = [float(diffJ_min(gp.predict, [x,float(i)])) for x in xx]
    plt.plot(xx,minJ05)
plt.show()


def two_valleys(k,u, sigma = 1):
    return -u*np.exp(-(k-1)**2/sigma**2) -(1-u)*1.1*np.exp(-(k+1)**2/sigma**2) + np.exp(-k**2/sigma**2) 
    

kv = np.linspace(-2,2, 100)

plt.plot(kv,two_valleys(kv,0.5))
plt.plot(kv,two_valleys(kv,0.3))
plt.plot(kv,two_valleys(kv,0.4))
plt.plot(kv,two_valleys(kv,0.6))
plt.show()
