#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------------------------------------
# -                  POLYNOMIAL CHAOS -- with chaospy
# ---------------------------------------------------------------------------------

import HR_config.wrapper as swe
# from code_swe.animation_SWE import animate_SWE

import numpy as np
import matplotlib.pyplot as plt
import chaospy as cp
import scipy.optimize

# -- Paramètres de référence ------------------------------------------------------
# print 'Hauteur eau moyenne = ', swe.mean_h
# print 'Amplitude = ', swe.amplitude
# print 'Periode = ', swe.period
# print 'Phase = ', swe.phase
bcLref = lambda h, hu, t: swe.BCrand(h, hu, t, 'L',
                                     swe.mean_h, swe.amplitude,
                                     swe.period, swe.phase)

# [xr, href, uref, t] = swe.swe_KAP(swe.Kref, 5.0, 15.0)
# [xr, hrefApPm, urefApPm, t] = swe.swe_KAP(swe.Kref, 5.1, 14.8)
# [xr, hrefApPp, urefApPp, t] = swe.swe_KAP(swe.Kref, 5.2, 15.1)
# [xr, hrefAmPp, urefAmPp, t] = swe.swe_KAP(swe.Kref, 4.9, 15.2)
# [xr, hrefAmPm, urefAmPm, t] = swe.swe_KAP(swe.Kref, 4.8, 14.9)
# [xr, hrefAmPp50, urefAmPp50, t] = swe.swe_KAP(np.ones(200) * 3, 5.0, 15.0)
# animate_SWE(xr, [href, hrefAmPp50], swe.b, swe.D, ylim = [0, 30])

# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/href.npy'), href)
# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/hrefApPm.npy'), hrefApPm)
# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/hrefApPp.npy'), hrefApPp)
# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/hrefAmPp.npy'), hrefAmPp)
# np.save(('/home/victor/These/Bayesian_SWE/data_observations/'
#          '/hrefAmPm.npy'), hrefAmPm)
href = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
                '/href.npy'))
hrefApPm = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
                    '/hrefApPm.npy'))
hrefApPp = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
                    '/hrefApPp.npy'))
hrefAmPp = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
                    '/hrefAmPp.npy'))
hrefAmPm = np.load(('/home/victor/These/Bayesian_SWE/data_observations/'
                    '/hrefAmPm.npy'))


# -- Polynomial Chaps expansion -------------------------------------------------

hr_dic = {'href': href, 'ApPm': hrefApPm, 'AmPp': hrefAmPp, 'AmPm': hrefAmPm, 'ApPp': hrefApPp}

# swe.J_KAP_pw_obs(swe.Kref, A = swe.amplitude, P = swe.period,
#                       idx_to_observe = np.arange(swe.href.shape[0]), hreference = href)

# doit être égal à 0, [0.,...,0.]


def J_KAP_dummy(KAP):
    K = KAP[:, 0]
    A = KAP[:, 1]
    P = KAP[:, 2]
    return (K - 0.5)**2 + (A - 5.0)**2 + (P - 15.0)**2 + K * (P + K - A - 10)**2


def grad_KAP_dummy(KAP):
    K = KAP[:, 0]
    A = KAP[:, 1]
    P = KAP[:, 2]
    return 2 * (K - 0.5) + (P + K - A - 10)**2 + 2 * K * (P + K - A - 10)



idx_to_observe = np.arange(49, 200, 50, dtype = int)

bounds = np.array([[0, 1.5],       # for K
                   [4.7, 5.3],     # for A
                   [14.7, 15.3]])  # for P

K_to_eval = np.linspace(0, 1.5, 20)


distributionU = cp.J(cp.Uniform(4.7, 5.3),
                     cp.Uniform(14.7, 15.3))

absissas, weights = cp.generate_quadrature(8, distributionU, "C")
samples = distributionU.sample(0, "H")
polynomial_expansion = cp.orth_ttr(3, distributionU, normed= True)


def full_quadrature(K, quadrature, idx_K):
    dimquad, nquad = absissas.shape
    # print absissas.shape
    nK = len(K)
    # print nK
    to_eval = np.zeros([nquad * nK, dimquad + 1])
    for i in xrange(nquad):
        for j in xrange(nK):
            to_eval[i * nK + j, idx_K] = K[j]
            to_eval[i * nK + j, 1:] = quadrature[:, i]
    return to_eval


KAP_cp = full_quadrature(K_to_eval, absissas, [0])
response_cp = J_KAP_dummy(KAP_cp)
gradient_cp = grad_KAP_dummy(KAP_cp)
nquad = absissas.shape[1]
response = [(np.split(response_cp, nquad)[i]) for i in xrange(nquad)]
gradient = [(np.split(gradient_cp, nquad)[i]) for i in xrange(nquad)]


response_PC, coeff_resp = cp.fit_quadrature(polynomial_expansion, absissas, weights,
                                            response, retall = True, normalized= True)
gradient_PC, coeff_grad = cp.fit_quadrature(polynomial_expansion, absissas, weights,
                                            gradient, retall = True, normalized= True)
np.isclose(coeff_resp[0, :], cp.E(response_PC, distributionU))
np.isclose(np.sum(coeff_resp[1:, :]**2, 0), cp.Var(response_PC, distributionU))

np.isclose(coeff_grad[0, :], cp.E(gradient_PC, distributionU))
np.isclose(np.sum(coeff_grad[1:, :]**2, 0), cp.Var(gradient_PC, distributionU))

plt.subplot(1, 2, 1)
plt.plot(K_to_eval, cp.E(response_PC, distributionU))
plt.plot(K_to_eval, cp.E(response_PC, distributionU) + cp.Std(response_PC, distributionU), '--k')
plt.plot(K_to_eval, cp.E(response_PC, distributionU) - cp.Std(response_PC, distributionU), '--k')
plt.subplot(1, 2, 2)
plt.plot(K_to_eval, cp.Std(response_PC, distributionU))
plt.show()


def gradient_var(coeff_resp, coeff_grad):
    return np.sum(2 * coeff_resp[1:, :] * coeff_grad[1:, :], 0)


def gradient_sd(coeff_resp, coeff_grad):
    return gradient_var(coeff_resp, coeff_grad) / (2 * np.sqrt(np.sum(coeff_resp[1:, :]**2, 0)))


fun_PCE = lambda design: swe.J_KAP_array(design, idx_to_observe,
                                         hreference=href,
                                         parallel=True, ncores=4,
                                         adj_gradient=True)


def dummy_fun_PC(KAP):
    return J_KAP_dummy(KAP), grad_KAP_dummy(KAP)


def evaluate_mean_std_PCE(K, fun = fun_PCE,
                          quadratureU=absissas, weightsU=weights,
                          polynomial_expansion=polynomial_expansion,
                          distributionU=distributionU):
    design_to_evaluate = full_quadrature(K, quadratureU, [0])
    response_design, gradient_design = fun(design_to_evaluate)

    nquad = quadratureU.shape[1]
    response = [(np.split(response_design, nquad)[i]) for i in xrange(nquad)]
    gradient = [(np.split(gradient_design, nquad)[i]) for i in xrange(nquad)]
    response_PC, coeff_resp = cp.fit_quadrature(polynomial_expansion, quadratureU, weightsU,
                                                response, retall = True, normalized= True)
    gradient_PC, coeff_grad = cp.fit_quadrature(polynomial_expansion, quadratureU, weightsU,
                                                gradient, retall = True, normalized= True)
    mean_overU = cp.E(response_PC, distributionU)
    std_overU = cp.Std(response_PC, distributionU)
    grad_mean_overU = cp.E(gradient_PC, distributionU)
    grad_std_overU = gradient_sd(coeff_resp.reshape(len(response_PC.keys), -1),
                                 coeff_grad.reshape(len(response_PC.keys), -1))

    return mean_overU, std_overU, grad_mean_overU, grad_std_overU, coeff_resp, coeff_grad


a, b, c, d, cr, cg = evaluate_mean_std_PCE([0.1000], dummy_fun_PC, absissas,
                                           weights, polynomial_expansion, distributionU)
epsilon = 0.0000001
a2, b2, c2, d2, _, _ = evaluate_mean_std_PCE([0.1 + epsilon], dummy_fun_PC, absissas,
                                             weights, polynomial_expansion, distributionU)

print (a2 - a) / epsilon, c
print (b2 - b) / epsilon, d

# wmstd = weighted_mean_std([0.1], 0.0, fun_PCE, absissas, weights, polynomial_expansion, distributionU)



def weighted_mean_std(K, mu, fun,
                      quadratureU, weightsU, polynomial_expansion, distributionU,
                      convex=False):
    meanU, stdU, grad_meanU, grad_stdU, _, _ = evaluate_mean_std_PCE(K, fun,
                                                                     quadratureU,
                                                                     weightsU,
                                                                     polynomial_expansion,
                                                                     distributionU)
    if convex:
        return (1 - mu) * meanU + mu * stdU, (1 - mu) * grad_meanU + mu * grad_stdU
    else:
        return meanU + mu * stdU, (grad_meanU + mu * grad_stdU).reshape(-1)


# optim_mu0 = scipy.optimize.minimize(weighted_mean_std, x0 = [0.9784], args = (0.0, fun_PCE,
#                                                                               absissas,
#                                                                               weights,
#                                                                               polynomial_expansion,
#                                                                               distributionU, True),
#                                     jac=True,
#                                     options={'maxiter': 10, 'disp': True})
# optim_mu05 = scipy.optimize.minimize(weighted_mean_std, x0 = [0.5], args = (0.5, fun_PCE,
#                                                                             absissas,
#                                                                             weights,
#                                                                             polynomial_expansion,
#                                                                             distributionU),
#                                      jac=True,
#                                      options={'maxiter': 10, 'disp': True})
# print optim_mu05
optim_mu01 = scipy.optimize.minimize(weighted_mean_std, x0 = optim_mu01.x,
                                     args = (0.0, fun_PCE,
                                             absissas,
                                             weights,
                                             polynomial_expansion,
                                             distributionU, True),
                                     jac=True,
                                     options={'maxiter': 5, 'disp': True},
                                     bounds = [(0.0, 2.0)])
print optim_mu01

# CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH             

#  Cauchy                time 0.000E+00 seconds.
#  Subspace minimization time 0.000E+00 seconds.
#  Line search           time 0.000E+00 seconds.

#  Total User time 0.000E+00 seconds.

# >>>       fun: array([35030.11445658])
#  hess_inv: <1x1 LbfgsInvHessProduct with dtype=float64>
#       jac: array([[2.16603848e-05]])
#   message: 'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
#      nfev: 3
#       nit: 1
#    status: 0
#   success: True
#         x: array([0.85189651])



for name, hr in {'AmPm': hrefAmPm}.items():  # hr_dic.items():
    print name
    response_cp, gradient_cp = swe.J_KAP_array(KAP_cp,
                                               idx_to_observe = idx_to_observe,
                                               hreference = hr,
                                               parallel=True, ncores=4,
                                               adj_gradient=True)
    # response_cp = J_KAP_dummy(KAP_cp)
    nquad = absissas.shape[1]
    response = [(np.split(response_cp, nquad)[i]) for i in xrange(nquad)]
    gradient = [(np.split(gradient_cp, nquad)[i]) for i in xrange(nquad)]
    PC_approx = cp.fit_quadrature(polynomial_expansion, absissas, weights, response)
    expected = cp.E(PC_approx, distributionU)
    std = cp.Std(PC_approx, distributionU)

    plt.subplot(2, 2, 1)
    plt.plot(K_to_eval, expected, linewidth = 3)
    plt.plot(K_to_eval, expected + std, '--b')
    plt.plot(K_to_eval, expected - std, '--b')
    plt.subplot(2, 2, 2)
    plt.plot(K_to_eval, std)
    # For the gradient
    PC_approx = cp.fit_quadrature(polynomial_expansion, absissas, weights, gradient)
    expected = cp.E(PC_approx, distributionU)
    std = cp.Std(PC_approx, distributionU)
    plt.subplot(2, 2, 3)
    plt.plot(K_to_eval, expected, linewidth = 3)
    plt.plot(K_to_eval, expected + std, '--b')
    plt.plot(K_to_eval, expected - std, '--b')
    plt.subplot(2, 2, 4)
    plt.plot(K_to_eval, std)

    np.save('/home/victor/These/Bayesian_SWE/data_observations/PCEresponse_' + str(name),
            response)
    np.save('/home/victor/These/Bayesian_SWE/data_observations/PCEgradient_' + str(name),
            gradient)
    plt.savefig('/home/victor/These/PCE_' + str(name))
    plt.close()


for name, hr in hr_dic.items():
    print name
    # response_cp, gradient_cp = swe.J_KAP_array(KAP_cp,
    #                                            idx_to_observe = idx_to_observe,
    #                                            hreference = hr,
    #                                            parallel=True, ncores=4,
    #                                            adj_gradient=True)
    # # response_cp = J_KAP_dummy(KAP_cp)
    nquad = absissas.shape[1]
    # response = [(np.split(response_cp, nquad)[i]) for i in xrange(nquad)]
    # gradient = [(np.split(gradient_cp, nquad)[i]) for i in xrange(nquad)]

    response = np.load('/home/victor/These/Bayesian_SWE/data_observations/PCEresponse_' +
                       str(name) + '.npy')
    gradient = np.load('/home/victor/These/Bayesian_SWE/data_observations/PCEgradient_' +
                       str(name) + '.npy')
    PC_response = cp.fit_quadrature(polynomial_expansion, absissas, weights, response)
    PC_gradient = cp.fit_quadrature(polynomial_expansion, absissas, weights, gradient)
    expected = cp.E(PC_approx, distributionU)
    std = cp.Std(PC_approx, distributionU)

    plt.subplot(2, 2, 1)
    plt.plot(K_to_eval, expected, linewidth = 3)
    plt.plot(K_to_eval, expected + std, '--b')
    plt.plot(K_to_eval, expected - std, '--b')
    plt.subplot(2, 2, 2)
    plt.plot(K_to_eval, std)
    # For the gradient
    PC_approx = cp.fit_quadrature(polynomial_expansion, absissas, weights, gradient)
    expected = cp.E(PC_approx, distributionU)
    std = cp.Std(PC_approx, distributionU)
    plt.subplot(2, 2, 3)
    plt.axhline(0, color = 'black', ls = ':')
    plt.plot(K_to_eval, expected, linewidth = 3)
    plt.plot(K_to_eval, expected + std, '--b')
    plt.plot(K_to_eval, expected - std, '--b')
    plt.subplot(2, 2, 4)
    plt.plot(K_to_eval, std)

    plt.savefig('/home/victor/These/PCE_' + str(name))
    plt.close()

#  EOF ---------------------------------------------------------------------------
