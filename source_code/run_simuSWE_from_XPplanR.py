import numpy as np

# planXp = np.asarray(np.loadtxt("quadraturePCE_4_3.csv", skiprows = 1, delimiter = ",", usecols = (1,2,3,4)))
# planXp = np.asarray(np.loadtxt("quadraturePCE_4_2.csv", skiprows = 1, delimiter = ",", usecols = (1,2,3,4)))
planXp = np.asarray(np.loadtxt("quadraturePCE_4_3_Clenshaw.csv", skiprows = 1, delimiter = ",", usecols = (1,2,3,4)))
# planXp = np.asarray(np.loadtxt("quadraturePCE_4_3_Fejer.csv", skiprows = 1, delimiter = ",", usecols = (1,2,3,4)))

bounds = np.array([[19.95, 4.9, 49.9,-0.001],[20.05, 5.1, 50.1, 0.001]])

from py_sensitivity import *

def simulate(K,start=0, end=(planXp.shape[0])):
    response = np.zeros(end-start)
    responseGrad = np.zeros(end-start)
    for i,j in enumerate(xrange(start,end,1)):
        print j
        response[i],responseGrad[i] = J_sobol_grad(np.append(planXp[j,:],K))
    filenameresponse = "responsePCE_{}_{}.dat".format(start,end)
    np.savetxt(filenameresponse, response)
    filenameresponseGrad = "responseGradPCE_{}_{}.dat".format(start,end)
    np.savetxt(filenameresponseGrad, responseGrad)

    print "file " + filenameresponse + " and " + filenameresponseGrad + " saved"
    
# simulate(0,1)
# simulate(0,81)

def simulate2(K, filename):
    planXp = np.asarray(np.loadtxt(filename, skiprows = 1, delimiter = ",", usecols = (1,2,3,4)))
    start = 0
    end = planXp.shape[0]
    response = np.zeros(end-start)
    responseGrad = np.zeros(end-start)
    for i,j in enumerate(xrange(start,end,1)):
        print j
        response[i],responseGrad[i] = J_sobol_grad(np.append(planXp[j,:],K))
    filenameresponse = "responsePCE_{}_{}.dat".format(start,end)
    np.savetxt(filenameresponse, response)
    filenameresponseGrad = "responseGradPCE_{}_{}.dat".format(start,end)
    np.savetxt(filenameresponseGrad, responseGrad)
    print "file " + filenameresponse + " and " + filenameresponseGrad + " saved"
    return [response] + [responseGrad]

def simulate_input_01(K, planXp_raw):
    if isinstance(planXp_raw,basestring):
        planXp_raw = np.asarray(np.loadtxt(filename, skiprows = 1, delimiter = ",", usecols = (1,2,3,4)))
    else:
        planXp_raw = np.asarray(planXp_raw)
        
    
    start = 0
    end = planXp_raw.shape[0]
    response = np.zeros(end-start)
    responseGrad = np.zeros(end-start)
    bounds_lower = np.kron(np.ones(end), bounds[0,:]).reshape(end,4)
    bounds_upper = np.kron(np.ones(end), bounds[1,:]).reshape(end,4)
        
    planXp = (bounds_upper - bounds_lower)*planXp_raw + bounds_lower
    for i,j in enumerate(xrange(start,end,1)):
        print j
        response[i],responseGrad[i] = J_sobol_grad(np.append(planXp[j,:],K))
    filenameresponse = "responsePCE_{}_{}.dat".format(start,end)
    np.savetxt(filenameresponse, response)
    filenameresponseGrad = "responseGradPCE_{}_{}.dat".format(start,end)
    np.savetxt(filenameresponseGrad, responseGrad)
    print "file " + filenameresponse + " and " + filenameresponseGrad + " saved"
    return np.array([response,responseGrad]).tolist()
