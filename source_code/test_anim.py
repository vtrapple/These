import numpy as np
import pylab
from matplotlib import animation

fig = pylab.figure() # initialise la figure
def_lines = lambda Nlines: [pylab.plot([],[])[0] for _ in range(Nlines)]

def init_lines(lines):
    for line in lines:
        line.set_data([], [])
    return lines

def animate_curve(i,lines,h,b,xr):
    for j,line in enumerate(lines):
        line.set_data(xr,h[j][:,i] + b(xr))
    return lines

def animate_SWE(xr,h,b,D,ylim = [0,10]):
    Nlines = np.shape(h)[0]
    if b is None:
        b = lambda x: 0
    fig = pylab.figure()
    lines = def_lines(Nlines)
    pylab.xlim(D[0], D[1])
    pylab.ylim(ylim[0],ylim[1])
    
    animate = lambda i: animate_curve(i,lines,h,b,xr)
    init = lambda : init_lines(lines)
    ani = animation.FuncAnimation(fig, animate, init_func=init, frames=h[0].shape[1], blit=True, interval=1, repeat=False)
    pylab.fill_between(xr, b(xr), color = [0.5,0.5,0.5])
    pylab.xlabel('xr')
    pylab.ylabel('h')
    pylab.show()
    return ani
