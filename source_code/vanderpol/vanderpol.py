#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint



def forced_vanderpol(X, t, par):
    x = X[0]
    y = X[1]
    mu, A, w = par
    dydt = mu * (1 - x**2) * y - x + A * np.sin(w * t)
    dxdt = y
    return [dxdt, dydt]


X0 = [1, 0]
t = np.linspace(0, 50, 500)
par = [8.53, 1.2, 2 * np.pi / 10]

ref = odeint(lambda X, t: forced_vanderpol(X, t, par), X0, t)


def vanderpol_all(par, xref=ref):
    par = np.atleast_2d(par)
    J = np.empty(len(par))
    for i, param in enumerate(par):
        mu, A, w, x0, dx0 = param
        sol = odeint(lambda X, t: forced_vanderpol(X, t, [mu, A, w]), [x0, dx0], t)
        J[i] = np.sum((sol[:, 0] - xref[:, 0])**2)
    return J


def pred_window(X0, par, t_window):
    return odeint(lambda X, t: forced_vanderpol(X, t, par), X0, t_window)


if __name__ == '__main__':

    x = ref[:, 0]
    y = ref[:, 1]
    # plt.plot(t, x)
    # plt.show()

    plt.subplot(1, 2, 1)
    cost = []
    mu_vec = np.linspace(-0.02, 0.02, 50)
    for dmu in mu_vec:
        sol = odeint(lambda X, t: forced_vanderpol(X, t, [8.53 + dmu, 1.21, np.pi / 5]), X0, t)
        plt.plot(t, sol[:, 0], 'k', alpha=0.02)
        cost.append(np.sum((x - sol[:, 0])**2))
    plt.plot(t, x, 'r')
    plt.subplot(1, 2, 2)
    plt.plot(mu_vec, cost)
    plt.show()


# EOF ---------------------------------------------------------------------------
