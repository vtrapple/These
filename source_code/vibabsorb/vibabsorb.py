#!/usr/bin/env python
# -*- coding: utf-8 -*

import numpy as np
# import matplotlib.pyplot as plt




MU = 0.1
KSI1 = 0.1
OMEGA1 = 100


def Z_compute(beta, T, ksi2):
    term1 = 1 + (beta**2 / T**2) * (beta**2 - 1) \
        - beta**2 * (1 + MU) - 4 * KSI1 * ksi2 * beta**2 / T
    term2 = KSI1 * beta**3 / T**2 \
        + (ksi2 * beta**3 * (1 + MU) - ksi2 * beta) / T \
        - KSI1 * beta
    return np.sqrt(term1**2 + 4 * term2**2)


def max_displacement(*KTB):
    ksi2, T, beta = KTB
    Z = Z_compute(beta, T, ksi2)
    A1 = 1 - (beta**2 / T**2)
    A2 = 2 * (ksi2 * beta / T)**2
    return np.sqrt(A1**2 + A2**2) / Z


# EOF ----------------------------------------------------------------------------------------
